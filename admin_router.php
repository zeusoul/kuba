<?php
  if(adminLoginState()){
    if(count($url) > 1){
      $page = $url[1];
      $page = str_ireplace("-","_",$page);
      if(trim($page)=="") adminView("index");
      else if(file_exists("./app/admin-app/controller/".$page."_controller.php")){
        adminView($page);
      }
      else {
        adminView("404");
      }
    }
    else adminView("index");
  }
  else {
    adminView('giris');
  }

?>
