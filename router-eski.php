<?php

  if(isset($_GET["url"]) && $_GET["url"] != null && $_GET["url"] != ""){
    $url = explode("/",$_GET["url"]);
    $page = trim($url[0]);
    if($page == "yonetim"){
      require_once("admin_router.php");
    }
    else if($page == "api"){
      require_once("api_router.php");
    }
    else if(!sitePublicationStatus()){
      view("site_yapim_asamasinda");
    }
    else{
      $seoExplode = explode("-",$page);
      $seoCount = count($seoExplode);
      if($seoCount > 2 && $seoExplode[$seoCount-2] === "p" && (int)$seoExplode[$seoCount-1] > 0){
        $page = "urun";
        view("urun");
      }
      else{
        switch($page){
          case 'index':
          view('index');
          break;
          case 'servis':
          view('servis');
          break;
          case 'hakkimizda':
          view('hakkimizda');
          break;
          case 'sorular':
          view('sorular');
          break;
          case 'odeme':
          view('odeme');
          break;
          case 'kayit-giris':
          view('kayit_giris');
          break;
          case 'teslimat-adresi-sec':
          view('teslimat_adresi_sec');
          break;
          case 'fatura-adresi-sec':
          view('fatura_adresi_sec');
          break;
          case 'ürün':
          view('ürün');
          break;
          case 'iletisim':
          view('iletisim');
          break;
          case 'haber':
          view('haber');
          break;
          case 'haber-detay':
          view('haber_detay');
          break;
          case 'servis-detay':
          view('servis_detay');
          break;
          case 'sepet':
          view('sepet');
          break;
          case 'ürünler':
          view('ürünler');
          break;
          default:
          view("index");
          break;
          case 'sifremi-unuttum':
          view('sifremi_unuttum');
          break;  
          case 'kayit-basarili':
          view("kayit_basarili");
          break;
          case 'profil':
            if(count($url) > 1){
              if ($url[1]=="yorumlarim") view('yorumlarim');
              else if ($url[1]=="siparislerim") view('siparislerim');
              else if ($url[1]=="siparis-detay") view('siparis_detay');
              else if ($url[1]=="fatura") view('fatura');
              else if ($url[1]=="adreslerim") view('adreslerim');
              else if ($url[1]=="adres-duzenle") view('adres_duzenle');
              else if ($url[1]=="iade-taleplerim") view('iade_taleplerim');
              else if ($url[1]=="iade-talebi-olustur") view('iade_talebi_olustur');
              else if ($url[1]=="mesajlarim") view('mesajlarim');
              else if ($url[1]=="mesaj") view('mesaj');
              else view("profil");
            }
            else view("profil");
          break;
          case 'siparis-basarili':
          view('siparis_basarili');
          break;
          case 'siparis-basarisiz':
          view('siparis_basarisiz');
          break;
        }
      }
    }
  }
  else if(!sitePublicationStatus()){
    view("site_yapim_asamasinda");
  }
  else{
    view("index");
  }

?>
