<?php
  if(isset($_GET["url"]) && $_GET["url"] != null && $_GET["url"] != ""){
    $url = explode("/",$_GET["url"]);
    $page = trim($url[0]);
    if($page == "yonetim"){
      require_once("admin_router.php");
    }
    else if($page == "api"){
      require_once("api_router.php");
    }
    else if(!sitePublicationStatus()){
      view("site_yapim_asamasinda");
    }
    else{
      $seoExplode = explode("-",$page);
      $seoCount = count($seoExplode);
      if($seoCount > 2 && $seoExplode[$seoCount-2] === "p"){
        $page = "urun";
        view("urun");
      }
      else{
        switch($page){
          case 'uyelik':
            view('uyelik');
            break;
          case 'sifremi-unuttum':
            view('sifremi_unuttum');
            break;
          case 'kayit-basarili':
            view("kayit_basarili");
            break;
          case 'hesap-dogrula':
            view("hesap_dogrula");
            break;
          case 'kategori':
            view('kategori');
            break;
          case 'sayfa':
              view('sayfa');
              break;
          case 'hesap-dogrulama-durum':
              view('hesap_dogrulama_durum');
            break;
          case 'yorum-durum':
              view('yorum_durum');
            break;
          case 'profil':
              if(count($url) > 1){
                if ($url[1]=="yorumlarim") view('yorumlarim');
                else if ($url[1]=="siparislerim") view('siparislerim');
                else if ($url[1]=="siparis-detay") view('siparis_detay');
                else if ($url[1]=="fatura") view('fatura');
                else if ($url[1]=="adreslerim") view('adreslerim');
                else if ($url[1]=="adres-duzenle") view('adres_duzenle');
                else if ($url[1]=="iade-taleplerim") view('iade_taleplerim');
                else if ($url[1]=="iade-talebi-olustur") view('iade_talebi_olustur');
                else if ($url[1]=="mesajlarim") view('mesajlarim');
                else if ($url[1]=="mesaj") view('mesaj');
                else if ($url[1]=="favorilerim") view('favorilerim');
                else view("profil");
              }
              else view("profil");
            break;
          case 'sepet':
            view('sepet');
            break;
          case 'odeme':
            view('odeme');
            break;
          case 'payment-result':
            view('payment_result');
            break;
          case 'teslimat-adresi-sec':
            view('teslimat_adresi_sec');
            break;
          case 'fatura-adresi-sec':
            view('fatura_adresi_sec');
            break;
            case 'istek-listesi':
              view('istek_listesi');
              break;
          case 'siparis-tamamla':
            view('siparis_tamamla');
            break;
          case 'siparis-basarili':
            view('siparis_basarili');
            break;
          case 'siparis-basarisiz':
            view('siparis_basarisiz');
            break;
          case 'haber':
            view('haber');
            break;
          case 'iletisim':
            view('iletisim');
            break;
          case 'hakkimizda':
            view('hakkimizda');
            break;
          case 'haber-detay':
            view('haber_detay');
            break;
            case 'ürünler':
              view('ürünler');
              break;
          case 'sozlesme':
            view('sozlesme');
            break;
            case 'profil':
              view('profil');
              break;
          case 'urun-detay':
            view('urun_detay');
            break;
          case 'ebulten':
            view('ebulten');
              break;
          default:
            view("index");
            break;
        }
      }
    }
  }
  else if(!sitePublicationStatus()){
    view("site_yapim_asamasinda");
  }
  else{
    view("index");
  }

?>
