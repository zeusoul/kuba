<?php
  $url = explode("/",$_GET["url"]);
  $page = trim($url[1]);
  if($page == "send-product-trendyol"){
    apiView('product/send_product_trendyol');
    exit;
  }
  else if($page == "update-product-trendyol"){
    apiView('product/update_product_trendyol');
    exit;
  }
  else if($page == "send-product-n11"){
    apiView('product/send_product_n11');
    exit;
  }
  else if($page == "get-trendyol-orders"){
    apiView('order/get_trendyol_orders');
    exit;
  }
  else{
    response(array(
      "error" => "404NotFound",
      "errorMessage" => "Sayfa Bulunamadı"
    ));
    exit;
  }

?>
