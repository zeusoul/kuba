<?php
  if(!loginState()){
  //  header("location:".url("uyelik/odeme"));
    exit;
  }
  else if(!isset($_POST["teslimat_address_book_id"])){
  //  header("location:".url("teslimat-adresi-sec"));
    exit;
  }
  else if(!isset($_POST["fatura_address_book_id"])){
  //  header("location:".url("fatura-adresi-sec"));
    exit;
  }
  else{
    include(staticUrl("header.php"));
    include("./app/user-app/view/siparis_tamamla.php");
    include(staticUrl("footer.php"));

    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $teslimatAddressBookId = (int)$_POST["teslimat_address_book_id"];
    $faturaAddressBookId = (int)$_POST["fatura_address_book_id"];

    $addressObject = new Address();
    $addressObject->setUserId($userId);


    $addressObject->setAddressBookId($teslimatAddressBookId);
    $teslimatAddress = $addressObject->getAddressBook();

    #teslimat adresi kullanıcıya kayıtlı mı? kontrolü
    if(!is_array($teslimatAddress) || count($teslimatAddress) <= 0) {
      //header("location:".url("teslimat-adresi-sec?error"));
      exit;
    }
    else {
      $addressObject->setAddressBookId($faturaAddressBookId);
      $faturaAddress = $addressObject->getAddressBook();
      #fatura adresi kullanıcıya kayıtlı mı? kontrolü
      if(!is_array($faturaAddress) || count($faturaAddress) <= 0) {
        //header("location:".url("fatura-adresi-sec?error"));
        exit;
      }
      else if(isset($_POST["transfer-eft-payment"]) && isset($_POST["bank_info_id"])){
        #Havale/eft ile ödeme geldiyse
        $bankObject = new Bank();

        $bankId = (int)$_POST["bank_info_id"];
        $bankObject->setBankId($bankId);
        $bankDetails = $bankObject->getBankDetails();
        #banka var mı? yok mu? kontrolü
        if(count($bankDetails) <= 0) {
          #banka yoksa
          //header("location:".url("odeme"));
          exit;
        }
        else{
          #siparişi kaydedip tamamlandı safyasına gönder.

          $orderObject = new Order();
          $orderObject->setTeslimatAddressBookId($teslimatAddressBookId);
          $orderObject->setFaturaAddressBookId($faturaAddressBookId);
          $orderObject->setUserId($userId);
          $orderObject->setPaymentType(1);
          $orderObject->setBankId($bankId);

          $cartObject = new Cart();
          $userId = (int)$_SESSION[sessionPrefix()."user_id"];
          $userCart = $cartObject->getUserCart($userId);
          $cartInfo = $cartObject->calculateCart();
          $countTheCart = $cartObject->getCountTheCart();

          $orderControl = $orderObject->orderControl($cartInfo, $userCart, $countTheCart);

          if($orderControl == "Success") {
            $insertOrder = $orderObject->insertOrder($cartInfo, $userCart);
            if($insertOrder == "Success"){
              echo "sipariş alındı";
              header("location:".url("siparis-basarili?bank=$bankId"));
              exit;
            }
            else{
              echo ';
                <form method="POST" action="'.url("siparis-basarisiz").'" name="form1">
                  <input type="hidden" name="error" value="'.$insertOrder.'" >
                </form>
                <script language="javascript">
                  window.onload = function (){
                    document.form1.submit();
                  }
                </script>
              ';
              exit;
            }
          }
          else{
            echo ';
              <form method="POST" action="'.url("siparis-basarisiz").'" name="form1">
                <input type="hidden" name="error" value="'.$orderControl.'" >
              </form>
              <script language="javascript">
                window.onload = function (){
                  document.form1.submit();
                }
              </script>
            ';
            exit;
          }
        }
      }
      else header("location:".url("odeme"));
    }
  }

  exit;
?>
