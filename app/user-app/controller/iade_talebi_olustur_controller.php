<?php

  if(!loginState()){
    header("location:".url());
    exit;
  }
  else{
    $url = explode('/',$_GET["url"]);
    /*
      url[0] : profil
      url[1] : iade-talebi-olstur
      url[2] : product id
      url[3] : order no
    */
    if(count($url) != 4 || (int)$url[3] <= 0){
      header("location:".url());
      exit;
    }
    else{
      $orderNo = (int)$url[3];
      $barcode = $url[2];
      $userId = (int)$_SESSION[sessionPrefix()."user_id"];

      $orderObject = new Order();
      $orderObject->setUserId($userId);

      $orders = $orderObject->getOrders();

      $orderControl = false;
      foreach ($orders as $key => $order) {
        $oNo = (int)$order["order_no"];
        if($orderNo === $oNo){
          $orderControl = true;
          break;
        }
      }
      if(!$orderControl){
        header("location:".url("profil/siparislerim"));
        exit;
      }
      else{
        $orderObject->setOrderNo($orderNo);
        $orderDetails = $orderObject->getOrderDetails();

        $quantity = 0;
        $unitPrice = 0;
        $productControl = false;
        $productIndex=0;

        foreach ($orderDetails["product_variants"] as $key => $productVariant) {
          $orderBarcode = $productVariant["barcode"];
          if($barcode == $orderBarcode){
            $quantity = (int)$productVariant["quantity"];
            $unitPrice = (int)$productVariant["unit_price"];
            $productIndex = $key;
            $productControl = true;
            break;
          }
          else{
            echo "Barcode : $barcode";
            echo "- barcode : $orderBarcode";
          }
        }

        if(!$productControl){
          #İade talep edilen ürün, siparişteki ürünler arasında bulunamadı
          echo "İade talep edilen ürün, siparişteki ürünler arasında bulunamadı";
        //  header("location:".url("profil/siparis-detay/".$orderNo));
          exit;
        }
        else{
          $productDetails = $orderDetails["product_variants"][$productIndex];
          if(!is_array($productDetails) ||count($productDetails) <= 0){
            header("location:".url());
            exit;
          }
          else{
            $returnObject = new ProductReturn();
            $returnObject->setUserId($userId);
            $returns = $returnObject->getReturns();

            $returnControl = true;
            foreach ($returns as $key => $return) {
              $oNo = (int)$return["order_no"];
              $returnBarcode = $return["barcode"];
              if($orderNo === $oNo && $barcode == $returnBarcode){
                $returnControl = false;
                break;
              }
            }
            if(!$returnControl) $pageMessage = "Bu ürün için iade talebi bulunmaktadır.";
            else if((int)$orderDetails["status"] != 3){
              $pageMessage = "Talep oluşturabilmeniz için ürünün sizin tarafınıza teslim edilmesi gereklidir.";
              $pageMessage .= "<br/>";
              $pageMessage .= "Eğer ürün size teslim edildiyse lütfen daha sonra talep oluşturmayı deneyiniz.";
            }#iade
            else if(isset($_POST["return_btn"])){
              $returnedQuantity = (int)$_POST["returned_quantity"];
              $returnedPrice = (float)($returnedQuantity * $unitPrice);

              $returnObject->setOrderNo($orderNo);
              $returnObject->setBarcode($barcode);
              $returnObject->setReturnReason($_POST["reason"]);
              $returnObject->setReturnedQuantity($returnedQuantity);
              $returnObject->setReturnedPrice($returnedPrice);


              $insert = $returnObject->insertReturn();
              if($insert == "Success") {
                $page = "iade_islemi_basarili";
              }
              else $pageMessage = $insert;
            }


            $title = "İade Talebi Oluştur";
            $map = map("Ana Sayfa,Profil,İadelerim,İade Talebi Oluştur","index,profil,iadelerim,iade-talebi-olustur/$barcode");
          }
        }
      }
    }
  }
?>
