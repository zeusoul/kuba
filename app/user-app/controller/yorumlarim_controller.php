<?php
  if(!loginState()){
    header("location:".url("uyelik"));
  }
  else{
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $commentsObject = new ProductComments();

    $myComments = $commentsObject->getUserComments($userId);

    if(isset($_POST["updateComment"])){
      $commentsObject->setCommentId($_POST["commentId"]);
      $commentsObject->setProductComment($_POST["comment"]);
      $commentsObject->setProductRatePoint($_POST["ratingRadio"]);
      $update = $commentsObject->updateProductComment($userId);

      $pageMessage = $update ? "Yorum ve değerlendirmeniz güncellendi. Onayımız sonrası yayına alınacaktır." : "Yorum ve değerlendirmeniz güncellenemedi !";
    }
    else if(isset($_POST["deleteComment"])){
      $commentsObject->setCommentId($_POST["commentId"]);
      $delete = $commentsObject->deleteProductComment();

      $pageMessage = ($delete === "Success") ? "Yorumunuz Silindi." : $delete;
    }


    $page = "yorumlarim";
    $title = "Yorumlarım";
    $pageTitle = "Yorumlarım";
    $map = map("Anasayfa,Profil,Yorumlarım","index,profil,profil/yorumlarim");
  }
?>
