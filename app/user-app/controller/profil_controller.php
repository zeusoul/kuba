<?php

  if(!loginState()){
    header("location:".url("uyelik"));
    exit;
  }
  else {
    $member = new Member();
    $member->setUserId($_SESSION[sessionPrefix()."user_id"]);
    if(isset($_POST["update"])){
      $member->setName($_POST["name"]);
      $member->setSurname($_POST["surname"]);
      $member->setTel($_POST["tel"]);
      $member->setNewPass($_POST["new-pass"]);
      $member->setPass($_POST["pass"]);
      $update = $member->updateUserInformation();
      $pageMessage = ($update === "Success") ? "Bilgileriniz Güncellendi" : $update;

    }

    $customer = $member->getUserInformation();

    $title="Hesap Bilgileri";
    $pageTitle = "Hesap Bilgileri";
    $map = map("Anasayfa,Profil","index,profil");
  }

?>
