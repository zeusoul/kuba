<?php
  $page = "urun";
  $url = explode("/",$_GET["url"]);
  if(count($url) != 1) {
    header("location:".url());
    exit;
  }
  else {
    $product = trim($url[0]);
    $seoExplode = explode("-",$product);
    $seoCount = count($seoExplode);

    $barcode = $seoExplode[$seoCount-1];

    $productVariantObj = new ProductVariant();
    $productVariantObj->setBarcode($barcode);
    $productVariant = $productVariantObj->getProductVariantByBarcode();

    $productId = $productVariant["product_id"];
    $productVariantObj->setProductId($productId);
    $productVariants = $productVariantObj->getProductVariantsByProductId();

    $product = new Product();
    $product->setProductId($productId);

    $comments = new ProductComments();
    $comments->setProductId($productId);
    //Yorum kaydetme
    if(isset($_POST["insertComment"])){
      $userId = (int)$_SESSION[sessionPrefix()."user_id"];
      $comment = $_POST["comment"];
      $rating = $_POST["rating"];


      $comments->setProductComment($comment);
      $comments->setProductRatePoint($rating);
      $insert = $comments->insertProductComment($userId);
      $pageMessage = ($insert === "Success") ? true : $insert;

      $title = (is_bool($pageMessage)) ? "Yorum Eklendi" : $pageMessage;
      $page = "yorum_durum";
    }
    else {
      $productDetails = $product->getProductDetails();


      $tl_price = "";
      $tl_discount_price = "";
      if($productVariant["currency"] == "TL"){
        $productVariant["currency"] = "₺";
      }
      else if($productVariant["currency"] == "USD"){
        $productVariant["currency"] = "$";
        $tl_price = "(".round(((float)$productVariant["price"] * (float)$usd_selling),2)." ₺)";
        $tl_discount_price = "(".round(((float)$productVariant["discount_price"] * (float)$usd_selling),2)." ₺)";
      }
      else if($productVariant["currency"] == "EURO"){
        $productVariant["currency"] = "€";
        $tl_price = "(".round(((float)$productVariant["price"] * (float)$euro_selling),2)." ₺)";
        $tl_discount_price = "(".round(((float)$productVariant["discount_price"] * (float)$euro_selling),2)." ₺)";
      }
      else{
        // Kur bilgisi bulunamadı
        echo "string2";
        exit;
        header("location:".url());
        exit;
      }
      if(!is_array($productDetails) || count($productDetails) <= 0) {
        // Ürün bulunamadı
        echo "string3";
        exit;
        header("location:".url());
        exit;
      }
      else if($productDetails["release_status"] == "0"){
        // Ürün yayında değil
        echo "string4";
        exit;
        header("location:".url());
        exit;
      }
      else {
        $productFilters = $product->getProductFiltersAndValues();

        $productInfo = new ProductInformation();
        $productInfo->setProductId($productId);
        $productInformations = $productInfo->getProductInformations();


        $productImagesObject = new ProductImages();
        $productImagesObject->setProductId($productId);
        $productImages = $productImagesObject->getProductImages();

        $categoryId = (int)$productDetails["category_id"];
        $categoryObject = new Category();
        $categoryObject->setCategoryId($categoryId);
        $category = $categoryObject->getSelectedCategory();
        $categoryName = $category["name"];
        $categoryId = $category["category_id"];

        //Ürünün kategorisine ait ilgili ürünleri çekelim.
        $product->setCategoryId($categoryId);
        $relatedProducts = $product->getRelatedProducts($productId);

        #ceil methodu ile sayıyı yukarıya yuvarlıyoruz.
        $rating = ceil((float)$productDetails["rating"]);

        $productComments = $comments->getProductComments();
        #ürünün görüntülenme sayısını arttırıyoruz.
        $product->increaseProductViews();


        if(loginState()){
          $userId = (int)$_SESSION[sessionPrefix()."user_id"];
          $orderObject= new Order();
          $orderObject->setUserId($userId);
          $orders = $orderObject->getOrders();

          $reviewState = false;
          foreach ($orders as $key => $order) {
            $orderNo = $order["order_no"];
            $orderObject->setOrderNo($orderNo);
            $orderDetails = $orderObject->getOrderDetails();


            foreach ($orderDetails["product_variants"] as $key => $variant) {
              if($variant["barcode"] == $barcode) {
                $reviewState = true;
                break;
              }
            }
          }
        }
        $title = $productDetails["title"];
        $pageTitle = "ÜRÜN DETAYI";
        $map = map("Anasayfa,$categoryName,$title","index,kategori/$categoryName/$categoryId,".seoUrl($title)."-p-$productId");
      }
    }
  }

?>
