<?php
  if(loginState()){
    header("location:".url());
  }
  else{
    $title = "Hesap Doğrula";
    $url = explode('/',$_GET["url"]);
    if(count($url) != 2) header("location:".url("uyelik"));
    else{
      $md5UserId=$url[1];
      if(strlen($md5UserId) != 32) header("location:".url("uyelik"));
      else {
        $memberOb = new Member();
        $confirm = $memberOb->confirmUser($md5UserId);
        $pageMessage= $confirm;
        if($confirm === "Success") {
          $page = "hesap_dogrulama_durum";
          $title = "Hesabınız Doğrulandı";
        }
        else{
          $page = "hesap_dogrulama_durum";
          $title="Hesabınız Doğrulanamadı!";
        }
      }
    }
  }

?>
