<?php
  if(!loginState()){
    header("location:".url("uyelik"));
    exit;
  }
  else{
    $url = explode("/",$_GET["url"]);
    if(count($url) != 3){
      header("location:".url("profil/mesajlarim"));
      exit;
    }
    else{
      $messageId = (int)$url[2];
      $messageObject = new Message();
      $messageObject->setMessageId($messageId);
      $messageObject->setUserId($_SESSION[sessionPrefix()."user_id"]);

      #ajax kontrolü
      if(isset($_POST["message_list"])){
        $messages = $messageObject->getMessages();
        foreach ($messages as $key => $message) {
          if((int)$message["sender_id"] != -1) {
            echo '
            <div class="outgoing_msg">
              <div class="sent_msg">
                <span class="time_date"> <strong>Siz : </strong></span>
                <p> '.$message["content"].' </p>
                <span class="time_date"> '.$message["posting_date"].' </span>
              </div>
            </div>';
          }
          else {
            echo '
            <div class="incoming_msg">
              <div class="incoming_msg_img">
                <img src="'.publicUrl("img/user.png").'" alt="sunil">
              </div>
              <div class="received_msg">
                <span class="time_date">
                  <strong>'.$message["name_surname"].'</strong>
                </span>
                <div class="received_withd_msg">
                  <p>'.$message["content"].'</p>
                  <span class="time_date">'.$message["posting_date"].'</span>
                </div>
              </div>
            </div>';
          }
        }
        exit;
      }

      #ajax ile Mesaj gönderme kontrolü
      if(isset($_POST["send_message"])){
        $messageObject->setSubject($messageId." id'li mesaja cevap");
        $messageObject->setContent($_POST["message"]);
        $messageObject->setName($_SESSION[sessionPrefix()."name"]);
        $messageObject->setSurname($_SESSION[sessionPrefix()."surname"]);
        $messageObject->setParentMessageId($messageId);
        $messageObject->setEmail($_SESSION[sessionPrefix()."email"]);
        $messageObject->setTel($_SESSION[sessionPrefix()."tel"]);
        $messageObject->setSenderId($_SESSION[sessionPrefix()."user_id"]);
        $messageObject->setRecipientId(-1);

        $sendMessage = $messageObject->insertMessage();

        echo "$sendMessage";
        exit;
      }




      $messages= $messageObject->getMyMessages();

      if(count($messages) <= 0){
        header("location:".url("profil/mesajlarim"));
        exit;
      }
      else {
        $messageObject->updateMessageDisplay();
      }

      $pageTitle = "Mesajlaşmanız";
      $map = map("Ana Sayfa,Profil,Konuşma Geçmişi","index,profil,profil/mesaj/$mesajId");
      $title = "Mesajlaşmanız";
    }
  }

?>
