<?php
  if(loginState()){
    header("location:".url());
    exit;
  }
  else{
    $step = 1;
    $url = explode("/",$_GET["url"]);
    if(count($url) != 2){
      header("location:".url());
      exit;
    }
    else{
      if($url[1] == "step-1") {
        $step = 1;
        $page = "sifremi_unuttum_step_1";
        unset($_SESSION[sessionPrefix()."code"]);
        unset($_SESSION[sessionPrefix()."mail_to_be_sent"]);
        if(isset($_POST["send_mail"])){
          $email = $_POST["email"];

          $memberObject = new Member();
          $member = $memberObject->getUserInformation(md5($email));

          $mailObject = new Mail();

          $code = "";
          for ($i=0; $i < 4; $i++) {
            $code .= rand(0,9);
          }
          $subject = "Sifre Yenileme Kodunuz !";
          $content = "Merhaba ".$member["name"]." ".$member["surname"].",
            <br/>
            Şifrenizi yenileyebilmeniz için gereken kod : $code
            <br/>
            Bu isteği eğer siz yapmadıysanız lütfen bize haber verin.
            <br/>
            ".URL;
          $sendMail = $mailObject->sendMail($email,"İstanbul","Elektromatik",$subject,$content);
          if($sendMail){
            $_SESSION[sessionPrefix()."mail_to_be_sent"] = $email;
            $_SESSION[sessionPrefix()."code"] = $code;
            header("location:".url("sifremi-unuttum/step-2"));
            exit;
          }
        }
      }
      else if($url[1] == "step-2") {
        if(!isset($_SESSION[sessionPrefix()."code"]) || !isset($_SESSION[sessionPrefix()."mail_to_be_sent"])){
          header("location:".url("sifremi-unuttum/step-1"));
          exit;
        }
        else{
          $step = 2;
          $page = "sifremi_unuttum_step_2";
          if(isset($_POST["post_code"])){
            $code = (int)$_POST["code"];
            $originalCode = (int)$_SESSION[sessionPrefix()."code"];
            if($code === $originalCode){
              $email = $_SESSION[sessionPrefix()."mail_to_be_sent"];

              $memberObject = new Member();
              $member = $memberObject->getUserInformation(md5($email));

              $mailObject = new Mail();

              $memberObject->setUserId($member["user_id"]);
              $newPass = $memberObject->resetPassword();
              if(!is_string($newPass) || $newPass === "false" || strlen($newPass) != 8){

              }
              else{
                $subject = "Sifreniz";
                $content = "Merhaba ".$member["name"]." ".$member["surname"].",
                  <br/>
                  Lüffen şifrenizi kimseyle paylaşmayınız.
                  <br/>
                  Şifremi unuttum talebinde bulunduğunuz için şifrenizi sıfırladık.
                  <br/>
                  Yeni Şifreniz : ".$newPass."
                  <br/>
                  Bu isteği eğer siz yapmadıysanız lütfen bize haber verin.
                  <br/>
                  ".URL;
                $sendMail = $mailObject->sendMail($email,"İstanbul","Elektromatik",$subject,$content);
                if($sendMail){
                  $_SESSION[sessionPrefix()."mail_to_be_sent"] = $email;
                  $_SESSION[sessionPrefix()."code"] = $code;

                  unset($_SESSION[sessionPrefix()."code"]);
                  unset($_SESSION[sessionPrefix()."mail_to_be_sent"]);
                  $page = "sifremi_unuttum_step_3";
                }
              }
            }
          }
        }
      }
      else{
        header("location:".url());
        exit;
      }


      $title = "Şifremi Unuttum";
    }
  }

?>
