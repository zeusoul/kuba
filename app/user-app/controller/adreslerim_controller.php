<?php
  if(!loginState()){
    header("location:".url("uyelik"));
  }
  else{
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $addressObject = new Address();
    if(isset($_POST["deleteAddress"])){
      $addressObject->setAddressBookId($_POST["address_book_id"]);
      $addressObject->setUserId($userId);
      $delete = $addressObject->deleteAddressBook();
      $pageMessage = ($delete === "Success") ? "Adres Silindi" : $delete;
    }
    else if(isset($_POST["insertAddress"])) {

      include("./tr.php");
      $trArray = json_decode($tr);

      $plaka = (int)$_POST["city"];
      $city="";
      foreach ($trArray as $key => $value) {
        if((int)$value->plaka == $plaka){
          $city = $value->il;
          break;
        }
      }

      $addressObject->setTitle($_POST["title"]);
      $addressObject->setName($_POST["name_surname"]);
      $addressObject->setTel($_POST["tel"]);
      $addressObject->setPostCode((isset($_POST["post_code"])) ? $_POST["post_code"] : null);
      $addressObject->setCity($city);
      $addressObject->setCounty($_POST["county"]);
      $addressObject->setAddress($_POST["address"]);
      $addressObject->setTcNo($_POST["tc_no"]);
      $addressObject->setCompanyName($_POST["company_name"]);
      $addressObject->setTaxAdministration($_POST["tax_administration"]);
      $addressObject->setTaxNumber($_POST["tax_number"]);
      $addressObject->setType($_POST["intervaltype"]);

      $addressObject->setUserId($userId);
      $insert = $addressObject->insertAddressBook();


      $pageMessage= ($insert === "Success") ? "Adres Eklendi" : $insert;
    }

    $addressObject->setUserId($userId);
    $address = $addressObject->getAddressBooks();

    $title="Adreslerim";
    $pageTitle = "Adreslerim";
    $map = map("Anasayfa,Profil,Adreslerim","index,profil,profil/adreslerim");
  }

?>
