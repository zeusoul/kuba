<?php
  if(!loginState()){
    header("location:".url());
  }
  else {
    $member = new Member();
    $member->signOut();

    $cart = new Cart();
    $cart->emptyCart();
    header("location:".url());
  }
?>
