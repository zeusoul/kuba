<?php
  $url = explode('/',$_GET["url"]);
  if(!loginState()) {
    header("location:".url());
    exit;
  }
  else if(count($url) != 3) {
    header("location:".url("profil/siparislerim"));
    exit;
  }
  else{
    $orderObject = new Order();
    if (isset($_POST["cancel"])) {
      $orderNo = (int)$_POST["order_no"];
      $orderObject->setOrderNo($orderNo);
      $cancel = $orderObject->cancelOrder();
      if($cancel == "Success"){
        header("location:".url("profil/siparislerim?cancel"));
        exit;
      }
      else $pageMessage = $cancel;
    }

    $orderNo = (int)$url[2];
    $orderObject->setOrderNo($orderNo);
    $order = $orderObject->getOrderDetails();
    
    $title = "Sipariş Detay";
    $pageTitle = "Sipariş Detayı";
    $map = map("Anasayfa,Profil,Sipariş Detayı","index,profil,profil/siparis-detay/$orderNo");
  }

?>
