<?php

  if(!loginState()){
    header("location:".url("uyelik/odeme"));
    exit;
  }
  else if(!isset($_SESSION[sessionPrefix()."cart"]) || !is_array($_SESSION[sessionPrefix()."cart"]) || count($_SESSION[sessionPrefix()."cart"])<=0){
    header("location:".url());
    exit;
  }
  else if(!isset($_POST["satis_sozlesmesi"])){
    header("location:".url("sepet"));
    exit;
  }
  else {
    if(isset($_GET["error"])) $pageMessage = "Lütfen Kayıtlı Bir Adres Seçiniz.";
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $addressObject = new Address();
    $addressObject->setUserId($userId);

    if(isset($_POST["insertAddress"])) {
      include("./tr.php");
      $trArray = json_decode($tr);
      $plaka = (int)$_POST["city"];
      $city="";
      foreach ($trArray as $key => $value) {
        if((int)$value->plaka == $plaka){
          $city = $value->il;
          break;
        }
      }

      $addressObject->setTitle($_POST["title"]);
      $addressObject->setName($_POST["name_surname"]);
      $addressObject->setTel($_POST["tel"]);
      $addressObject->setPostCode((isset($_POST["post_code"])) ? $_POST["post_code"] : null);
      $addressObject->setCity($city);
      $addressObject->setCounty($_POST["county"]);
      $addressObject->setAddress($_POST["address"]);
      $addressObject->setTcNo($_POST["tc_no"]);
      $addressObject->setCompanyName($_POST["company_name"]);
      $addressObject->setTaxAdministration($_POST["tax_administration"]);
      $addressObject->setTaxNumber($_POST["tax_number"]);
      $addressObject->setType($_POST["intervaltype"]);
      $addressObject->setUserId($userId);

      $insert = $addressObject->insertAddressBook();
      $pageMessage= ($insert === "Success") ? "Adres Eklendi" : $insert;

    }

    $cart = new Cart();
    $cartInfo = $cart->calculateCart();

    $addressBooks = $addressObject->getAddressBooks();

    $pageTitle = "Teslimat Adresi Seç";
    $map = map("Ana Sayfa,Sepet,Teslimat Adresi Seç","index,sepet,teslimat-adresi-sec");
    $title = "Teslimat Adresi Seç";
  }


?>
