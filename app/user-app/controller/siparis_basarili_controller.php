<?php

  if(!loginState()){
    header("location:".url());
    exit;
  }
  else {
    $bankObject = new Bank();
    if(isset($_GET["bank"])){
      $bankId = (int)$_GET["bank"];
      $bankObject->setBankId($bankId);
      $bankDetails = $bankObject->getBankDetails();
      if(!is_array($bankDetails) || count($bankDetails) <= 0){
        header("location:".url());
        exit;
      }
    }
  }


?>
