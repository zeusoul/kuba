<?php

  if(loginState()){
    header("location:".url("index"));
    exit;
  }
  else {
    $pageToGoTo = "";
    $url = explode('/',$_GET["url"]);
    if(count($url) > 1) $pageToGoTo = $url[1];
    #Login bilgileri post edilmiş mi?
    if(isset($_POST["login"])){
      $member = new Member();
      $member->setEmail($_POST["email"]);
      $member->setPass($_POST["password"]);
      $logIn = $member->logIn();
      if($logIn === "Success"){
        #giriş başarılı
        $cartObject = new Cart();
        $userId = (int)$_SESSION[sessionPrefix()."user_id"];
        if(isset($_SESSION[sessionPrefix()."cart"]) && is_array($_SESSION[sessionPrefix()."cart"]) && count($_SESSION[sessionPrefix()."cart"])>0){
          $cart = $_SESSION[sessionPrefix()."cart"];
          foreach ($cart as $key => $productVariant) {
            $barcode = (int)$productVariant["barcode"];
            $quantity = (int)$productVariant["quantity"];
            $cartObject->setBarcode($barcode);
            $cartObject->setQuantity($quantity);
            $insertCart = $cartObject->insertCart($userId);
          }
        }
        $returnedValue = $cartObject->getUserCart($userId);
        header("location:".url($pageToGoTo));
      }
      else if(is_string($logIn)) $pageMessage = $logIn;
      else $pageMessage = "Kullanıcı Adı veya Şifre Hatalı";
    }#Sign Up bilgileri post edilmiş mi
    else if(isset($_POST["signUp"])){
      $user = new User();
      $user->setName($_POST["name"]);
      $user->setSurname($_POST["surname"]);
      $user->setEmail($_POST["email"]);
      $user->setTel($_POST["phone"]);
      $user->setPass($_POST["password"]);

      $signUp = $user->signUp();

      if($signUp === "Success"){
        $member = new Member();
        $member->setEmail($_POST["email"]);
        $memberInfo = $member->getUserInformation();

        $mail = new Mail();
        $subject = "Hesabınızı Doğrulayın !";
        $url = url("hesap-dogrula/".md5($memberInfo["user_id"]));
        $name = $memberInfo["name"]." ".$memberInfo["surname"];
        $content = "Merhaba $name, <br/>
        Kaydınızı tamamlamak için son bir adım kaldı. <br/>
        Kaydınızın tamamlanması için hesabınızın doğrulanması lazım.<br/>
        Aşağıdaki 'Hesabımı Doğrula' butonuna tıklayarak hesabınızı doğrulayabilirsiniz.<br/>
        <br/>
        <a href='$url' target='_blank'>
          <button style='out-line:0;
                         border:1px solid orange;
                         background-color:transparent;
                         color:black;
                         cursor:pointer;
                         padding:10px;
                         border-radius:5px;'>
            Hesabımı Doğrula
          </button>
        </a>
        ";
        $sendMail = $mail->sendMail($_POST["email"],"İstanbul","Elektromatik",$subject,$content);

        if(isset($settings["email_dogrulama"]) && (int)$settings["email_dogrulama"] == 1){
          header("location:".url("kayit-basarili/".md5($_POST["email"])));
        }
        else {
          $member = new Member();
          $member->setEmail($_POST["email"]);
          $member->setPass($_POST["pass"]);
          $logIn = $member->logIn();
          if($logIn === "Success") {
            header("location:".url($pageToGoTo));
            exit;
          }
          else $pageMessage = $logIn;
        }
      }
      else if(is_string($signUp) && trim($signUp) != "") $pageMessage = $signUp;
      else $pageMessage = "Kayıt Başarısız";
    }


    if(isset($_GET["ban"]) && count($_POST) == 0) $pageMessage = "Hesabınız Askıya Alındı";
    else if(isset($_GET["confirm"]) && count($_POST) == 0)$pageMessage = "Lütfen Hesabınızı Mailinizden Onaylayınız";


    $title = "Üyelik";
    $pageTitle = "Üyelik";
    $map = map("Ana Sayfa,Üyelik","index,uyelik");

  }

?>
