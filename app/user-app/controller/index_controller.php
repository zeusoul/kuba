<?php

$newsObj = new News();
$news = $newsObj->getNews();

$number_of_pages = (int)(ceil(count($news)/5));
  
  $page_number = 1;
  if(isset($_GET["page"])) $page_number = (int)$_GET["page"];
  if($page_number <= 0) $page_number = 1;
  else if($page_number > $number_of_pages) $page_number = $number_of_pages;
  
  $offset = ($page_number-1)*20;
  $limit = $offset+20;
  
  $mainImage = new MainImage();
  $firstImage = $mainImage->getFirstMainImage();
  $secondImage = $mainImage->getSecondMainImage();
  $thirdImage = $mainImage->getThirdMainImage();
  $fourthImage = $mainImage->getFourthMainImage();
  $fiveImage = $mainImage->getFiveMainImage();

  $productObject = new Product();
  $mostViewedProducts = $productObject->getTheMostViewedProducts();
  $lastProducts = $productObject->getLastProducts();
  $mostAddingToCart = $productObject->getTheMostAddedProductsToTheCart();
  $mostSellingProducts = $productObject->getTheMostSellingProducts();

  //printr($pages);

  $sliderObject = new Slider();
  $sliders = $sliderObject->getSliders();

  $pageTitle = "Anasayfa";
  $page = "index";
?>
