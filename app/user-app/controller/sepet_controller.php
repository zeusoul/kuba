<?php

  if(isset($_POST["delete"])){
    $barcode = $_POST["barcode"];
    $cart = new Cart();
    $cart->setBarcode($barcode);
    if(loginState()){
      $userId = (int)$_SESSION[sessionPrefix()."user_id"];
      $delete = $cart->deleteProductInCart($userId);

      if(!$delete) $pageMessage = "Ürün Sepetten Kaldırılamadı";
      else {
        $remove = $cart->removeInCart();
        $pageMessage = (is_bool($remove) && $remove) ? "Ürün Sepetten Kaldırıldı" : "Ürün Sepetten Kaldırılamadı";
        if(is_bool($remove) && $remove){
          $cart->increasingTheNumberOfExitsFromTheBasket();
        }
      }
    }
    else{
      $remove = $cart->removeInCart();
      $pageMessage = (is_bool($remove) && $remove) ? "Ürün Sepetten Kaldırıldı" : "Ürün Sepetten Kaldırılamadı";
      if(is_bool($remove) && $remove){
        $cart->increasingTheNumberOfExitsFromTheBasket();
      }
    }
  }#Ajax kontrolü
  else if(isset($_POST['barcode']) && isset($_POST['quantity'])){
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $barcode = $_POST["barcode"];
    $quantity = (int)$_POST["quantity"];

    $cart = new Cart();
    $cart->setBarcode($barcode);
    $cart->setQuantity($quantity);

    if(loginState()){
      $insertCart = $cart->insertCart($userId);
      if($insertCart === "Success"){
        $addToCart = $cart->addToCart();
        $count = $cart->getCountTheCart();
        if($addToCart == "Success"){
          $cart->increasingTheNumberOfAdditionsToTheBasket();
        }
        echo ($addToCart === "Success") ? $count : $addToCart;
      }
      else echo $insertCart;
    }
    else {
      $addToCart = $cart->addToCart();
      $count = $cart->getCountTheCart();
      if($addToCart == "Success"){
        $cart->increasingTheNumberOfAdditionsToTheBasket();
      }
      echo ($addToCart == "Success") ? $count : $addToCart;
    }
    exit;
  }
  $cartObject = new Cart();
  $cart = array();
  //printr($_SESSION[sessionPrefix()."cart"]);
  if(isset($_SESSION[sessionPrefix()."cart"]) && is_array($_SESSION[sessionPrefix()."cart"])){
    $totalPrice = 0;

    foreach ($_SESSION[sessionPrefix()."cart"] as $key => $productVariant) {
      $barcode = $productVariant["barcode"];
      $quantity = (int)$productVariant["quantity"];
      $productVariantObject = new ProductVariant();
      $productVariantObject->setBarcode($barcode);
      $productVariantDetails = $productVariantObject->getProductVariantByBarcode();

      $productId = $productVariantDetails["product_id"];
      $product = new Product();
      $product->setProductId($productId);
      $productDetails = $product->getProductDetails();

      $discountPrice = (float)$productVariantDetails["discount_price"];
      $currency = trim($productVariantDetails["currency"]);
      $discountPriceTL = $discountPrice;
      if($currency == "EURO") $discountPriceTL *= $euro_selling;
      else if($currency == "USD") $discountPriceTL *= $usd_selling;
      else if($currency != "TL"){
      //  header("location:".url());
      }

      $totalProductPrice = $discountPrice * $quantity;
      $totalProductPriceTL = $discountPriceTL * $quantity;
      $totalPrice += $totalProductPriceTL;
      $cart[$barcode] = array(
        "barcode"     =>  $barcode,
        "quantity"    =>  $quantity,
        "title"       =>  $productDetails["title"],
        "sub_title"   =>  $productDetails["sub_title"],
        "image"       =>  $productDetails["image"],
        "unit_price"  =>  $discountPrice,
        "currency"    =>  $currency,
        "total_price" =>  $totalProductPrice,
        "combine"     =>  $productVariantDetails["combine"]
      );
    }

    $kdvDahil = (float)round($totalPrice + ($totalPrice*18/100),2);
    $cartInfo = $cartObject->calculateCart();
  }

  $title = "Sepet";
  $pageTitle = "Sepet";
  $map = map("Ana Sayfa,Sepetim","index,sepet");



?>
