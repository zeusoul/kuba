<?php
  if(!loginState()) {
    header("location:".url("uyelik/odeme"));
    exit;
  }
  else if(!isset($_SESSION[sessionPrefix()."cart"]) || !is_array($_SESSION[sessionPrefix()."cart"]) || count($_SESSION[sessionPrefix()."cart"]) <= 0){
    header("location:".url());
    exit;
  }
  else if(!isset($_POST["teslimat_address_book_id"])){
    header("location:".url("teslimat-adresi-sec"));
    exit;
  }
  else if(!isset($_POST["fatura_address_book_id"])){
    header("location:".url("fatura-adresi-sec"));
    exit;
  }
  /*else if(!isset($_POST['satis_sozlesmesi'])){
    header("location:".url("sepet"));
    exit;
  }*/
  else{
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $teslimatAddressBookId = (int)$_POST["teslimat_address_book_id"];
    $faturaAddressBookId = (int)$_POST["fatura_address_book_id"];

    $orderObject = new Order();
    $orderObject->setTeslimatAddressBookId($teslimatAddressBookId);
    $orderObject->setFaturaAddressBookId($faturaAddressBookId);
    $orderObject->setUserId($userId);
    $orderObject->setPaymentType(0);
    $orderObject->setBankId(1);

    $cartObject = new Cart();
    $userCart = $cartObject->getUserCart($userId);
    $cartInfo = $cartObject->calculateCart();
    $countTheCart = $cartObject->getCountTheCart();

    if(!isset($_SESSION[sessionPrefix()."order_cart_info"])){
      $_SESSION[sessionPrefix()."order_cart_info"] = array();
    }
    if(!isset($_SESSION[sessionPrefix()."order_user_cart"])){
      $_SESSION[sessionPrefix()."order_user_cart"] = array();
    }
    $_SESSION[sessionPrefix()."order_cart_info"][] = $cartInfo;
    $_SESSION[sessionPrefix()."order_user_cart"][] = $userCart;
    $cartInfoIndex = count($_SESSION[sessionPrefix()."order_cart_info"])-1;
    $userCartIndex = count($_SESSION[sessionPrefix()."order_user_cart"])-1;
    $orderControl = $orderObject->orderControl($cartInfo, $userCart, $countTheCart);

    if($orderControl != "Success"){
      echo ';
        <form method="POST" action="'.url("siparis-basarisiz").'" name="form1">
          <input type="hidden" name="error" value="'.$orderControl.'" >
        </form>
        <script language="javascript">
          window.onload = function (){
            document.form1.submit();
          }
        </script>
      ';
      exit;
    }

    $addressObject = new Address();
    $addressObject->setUserId($userId);


    $addressObject->setAddressBookId($teslimatAddressBookId);
    $teslimatAddress = $addressObject->getAddressBook();

    #teslimat adresi kullanıcıya kayıtlı mı? kontrolü
    if(!is_array($teslimatAddress) || count($teslimatAddress) <= 0) {
      header("location:".url("teslimat-adresi-sec?error"));
      exit;
    }
    else {
      $addressObject->setAddressBookId($faturaAddressBookId);
      $faturaAddress = $addressObject->getAddressBook();
      #fatura adresi kullanıcıya kayıtlı mı? kontrolü
      if(!is_array($faturaAddress) || count($faturaAddress) <= 0) {
        header("location:".url("fatura-adresi-sec?error"));
        exit;
      }
      else{
        $bankObject = new Bank();
        $cartObject = new Cart();
        $cart = $_SESSION[sessionPrefix()."cart"];
        $banks  = $bankObject->getBanks();
        $cartInfo = $cartObject->calculateCart();
      }
      $cartObject = new Cart();

        $cart = array();
        if(isset($_SESSION[sessionPrefix()."cart"]) && is_array($_SESSION[sessionPrefix()."cart"])){
          $totalPrice = 0;
          foreach ($_SESSION[sessionPrefix()."cart"] as $key => $productVariant) {
            $barcode = $productVariant["barcode"];
            $quantity = (int)$productVariant["quantity"];
            $productVariantObject = new ProductVariant();
            $productVariantObject->setBarcode($barcode);
            $productVariant = $productVariantObject->getProductVariantByBarcode();

            $discountPrice = (float)$productVariant["discount_price"];
            $currency = trim($productVariant["currency"]);
            $discountPriceTL = $discountPrice;
            if($currency == "EURO") $discountPriceTL *= $euro_selling;
            else if($currency == "USD") $discountPriceTL *= $usd_selling;
            else if($currency != "TL"){
              echo "test7";
              exit;
              header("location:".url());
              exit;
            }

            $totalProductPrice = $discountPrice * $quantity;
            $totalProductPriceTL = $discountPriceTL * $quantity;
            $totalPrice += $totalProductPriceTL;
            $cart[$productId] = array(
              "product_id"  =>  $productId,
              "quantity"    =>  $quantity,
              "title"       =>  $productVariant["title"],
              "sub_title"   =>  $productVariant["sub_title"],
              "image"       =>  $productVariant["image"],
              "unit_price"  =>  $discountPrice,
              "currency"    =>  $currency,
              "total_price" =>  $totalProductPrice
            );
          }
          $kdvDahil = (float)round($totalPrice + ($totalPrice*18/100),2);
          $cartInfo = $cartObject->calculateCart();

          $title = "Ödeme";
          $pageTitle = "Ödeme";
          $map = map("Ana Sayfa,Sepet,Teslimat Adresi Seç,Fatura Adresi Seç,Ödeme","index,sepet,teslimat-adresi-sec,fatura-adresi-sec,odeme");
        }
    }
  }
  header("Refresh:180; url=".url("sepet"));
?>
