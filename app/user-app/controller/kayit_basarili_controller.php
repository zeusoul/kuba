<?php

  if(loginState()){
    header("location:".url());
  }
  else{
    $url = explode('/',$_GET["url"]);
    if(count($url) != 2) header("location:".url("uyelik"));
    else {
      $md5Email = $url[1];
      if(strlen($md5Email) != 32) header("location:".url("uyelik"));
      else{
        $member = new Member();
        $memberInfo = $member->getUserInformation($md5Email);
        if(is_array($memberInfo) && count($memberInfo) > 0){
          if($memberInfo["confirm"] == 1) header("location:".url("uyelik"));
          else {
            if(isset($_GET["sendLink"])){
              $mail = new Mail();
              $subject = "Hesabınızı Doğrulayın !";
              $url = url("hesap-dogrula/".md5($memberInfo["user_id"]));
              $name = $memberInfo["name"]." ".$memberInfo["surname"];
              $content = "Merhaba $name, <br/>
              Kaydınızı tamamlamak için son bir adım kaldı. <br/>
              Kaydınızın tamamlanması için hesabınızın doğrulanması lazım.<br/>
              Aşağıdaki 'Hesabımı Doğrula' butonuna tıklayarak hesabınızı doğrulayabilirsiniz.<br/>
              <br/>
              <a href='$url' target='_blank'>
                <button style='out-line:0;
                               border:1px solid orange;
                               background-color:transparent;
                               color:black;
                               cursor:pointer;
                               padding:10px;
                               border-radius:5px;'>
                  Hesabımı Doğrula
                </button>
              </a>
              ";
              $sendMail = $mail->sendMail($memberInfo["email"],"İstanbul","Elektromatik",$subject,$content);
              $pageMessage = (is_bool($sendMail) && $sendMail) ? "Doğrulama Linki Email Adresinize Gönderildi" : "Doğrulama Linki Email Adresinize Gönderilemedi!";
            }
          }
        }
        else header("location:".url("uyelik"));
      }
    }
  }

?>
