<?php

  $url = explode('/',$_GET["url"]);
  if(count($url) != 2){
    header("location:".url());
    exit;
  }
  else{
    $contract = $url[1];
    if($contract == "mesafeli-satis-sozlesmesi"){
      $page = "mesafeli_satis_sozlesmesi";
    }
    else if($contract == "gizlilik-politikasi"){
      $page = "gizlilik_politikasi";
      $title = "Gizlilik Politikası";
    }
    else if($contract == "teslimat-ve-iade-sartlari"){
      $page = "teslimat_ve_iade_sartlari";
      $title = "Teslimat ve iade koşulları";
    }
    else if($contract == "iptal-ve-iade-sartlari"){
      $page = "iptal_ve_iade_sartlari";
      $title = "İptal ve iade şartları";
    }
    else if($contract == "cerez-politikasi"){
      $page = "cerez_politikasi";
      $title = "Teslimat Koşulları";
    }
    else {
      header("location:".url());
      exit;
    }


  }

?>
