<?php

  if(!loginState()){
    header("location:".url("uyelik"));
    exit;
  }
  else{
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $returnObject = new ProductReturn();
    $returnObject->setUserId($userId);

    #talep iptali varmı?
    if(isset($_POST["cancel_the_return"])){
      $returnObject->setReturnId($_POST["return_id"]);
      $returnObject->setReasonForRejection("Kullanıcı tarafından iptal edildi");
      $cancel = $returnObject->cancelTheReturn();

      $pageMessage = ($cancel === "Success") ? "Talep İptal Edildi" : $cancel;
    }



    $returns = $returnObject->getReturns();
    $pageTitle = "İade Taleplerim";
    $map = map("Ana Sayfa,Profil,İade Taleplerim","index,profil,profil/iade-taleplerim");
    $title = "İade Taleplerim";

  }


?>
