<?php
  $url = explode("/",$_GET["url"]);

  $categoryId = (count($url) > 2) ? (int)$url[2] : 0;
  $getFilters = array();

  $get = $_GET;
  unset($get["url"]);
  unset($get["page"]);
  if(count($get) > 0){
    $request_uri_array = explode("?",$_SERVER["REQUEST_URI"]);
    $get_string = $request_uri_array[count($request_uri_array)-1];
    $get_string_array = explode("&",$get_string);
    unset($get_string_array[0]);
    $get_string = "";
    foreach ($get_string_array as $key => $value) {
      $get_string .= "$value&";
    }
  }

  if(isset($_GET["filter"])){
    $getFilters = $_GET;
    if(isset($_GET["url"]))unset($getFilters["url"]);
    if(isset($_GET["filter"])) unset($getFilters["filter"]);
    if(isset($_GET["page"])) unset($getFilters["page"]);
    if(isset($_GET["ranking"])) unset($getFilters["ranking"]);

    if(isset($getFilters["max"])     && trim($getFilters["max"]) == "") unset($getFilters["max"]);
    if(isset($getFilters["min"])     && trim($getFilters["min"]) == "") unset($getFilters["min"]);
    if(isset($getFilters["rating"])  && (int)$getFilters["rating"] <= 0) unset($getFilters["rating"]);
    if(isset($getFilters["filters"])){
      foreach ($getFilters["filters"] as $filterId => $valueId) {
        $filterId = (int)$filterId;
        $valueId = (int)$valueId;
        if($filterId <= 0 || $valueId <= 0){
          unset($getFilters["filters"][$filterId]);
        }
      }
      if(count($getFilters["filters"]) <= 0) unset($getFilters["filters"]);
    }
  }


  $productObject = new Product();
  $productObject->setCategoryId($categoryId);
  $products = (count($getFilters) > 0) ? $productObject->getFilteredProducts($getFilters) : $productObject->getProducts();
//  printr($products);


  if(isset($_GET["ranking"])){
    $ranking = (int)$_GET["ranking"];
    switch ($ranking) {
      case 1:
        $products = $productObject->record_sort($products, "product_price", true);
        break;
      case 2:
        $products = $productObject->record_sort($products, "product_price", false);
        break;
      case 3:
        $products = $productObject->record_sort($products, "rating", true);
        break;
      case 4:
        $products = $productObject->record_sort($products, "rating", false);
        break;
      default :
        $products = $productObject->record_sort($products, "model_name", false);
        break;
    }
  }
  else $products = $productObject->record_sort($products, "model_name", false);

  $number_of_pages = (int)(ceil(count($products) / 10));

  $page_number = 1;
  if(isset($_GET["page"])) $page_number = (int)$_GET["page"];
  if($page_number <= 0) $page_number = 1;
  else if($page_number > $number_of_pages) $page_number = $number_of_pages;


  $offset = ($page_number - 1) * 10;
  $limit = $offset + 10;

  $filter = new Filter();
  $filter->setCategoryId($categoryId);
  $filters = $filter->getCategoryFilters();

  $category = new Category();
  $category->setCategoryId($categoryId);
  $categoryDetails = $category->getSelectedCategory();


  $subCategories = $category->getSubCategories();

  $pageTitle = "Kategori";
  $title = (count($url) > 2) ? $categoryDetails["name"] : "Kategori";
  $page = "kategori";
  if(count($url) > 2){
    $map = map("Anasayfa,Kategori,".$categoryDetails["name"],"index,kategori,kategori/".seoUrl($url[1])."/".$url[2]);
  }
  else{
    $map = map("Anasayfa,Kategori","index,kategori");
  }

?>
