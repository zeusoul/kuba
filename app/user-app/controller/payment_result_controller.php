<?php

	$userId = (int)$_GET["user_id"];

	if(!loginState()){
		$member = new Member();
		$member->setUserId($userId);
		$login = $member->logIn();
	}

  $teslimatAddressBookId = (int)$_GET["teslimat_id"];
  $faturaAddressBookId = (int)$_GET["fatura_id"];

	$token = $_POST['token'];
	$siparis_no = (int)$_GET['siparis_no'];
	require_once('./iyzico/config.php');

	$request = new \Iyzipay\Request\RetrieveCheckoutFormRequest();
	$request->setLocale(\Iyzipay\Model\Locale::TR);
	$request->setConversationId($siparis_no);
	$request->setToken($token);
	$checkoutForm = \Iyzipay\Model\CheckoutForm::retrieve($request, Config::options());

	//print_r($checkoutForm->getStatus());
	$odeme_durum = $checkoutForm->getPaymentStatus();
	$islem_no = $checkoutForm->getpaymentId();

	if($odeme_durum == "FAILURE"){
		unset($_SESSION[sessionPrefix()."order_cart_info"][$cart_info_index]);
		unset($_SESSION[sessionPrefix()."order_user_cart"][$user_cart_index]);
		$errorMessage = trim($checkoutForm->errorMessage);
		if($errorMessage == "") $errorMessage = "ÖDEME İŞLEMİ BAŞARISIZ";
		echo "Ödeme Başarısız...";
		echo '
      <form method="POST" action="'.url("siparis-basarisiz").'" name="form1">
        <input type="hidden" name="error" value="'.$errorMessage.'">
      </form>
      <script language="javascript">
        window.onload = function (){
          document.form1.submit();
        }
      </script>
    ';
	}
	else if($odeme_durum == "SUCCESS"){
		$cart_info_index = (int)$_GET["cart_info_index"];
		$user_cart_index = (int)$_GET["user_cart_index"];


		$cartInfo = $_SESSION[sessionPrefix()."order_cart_info"][$cart_info_index];
		$userCart = $_SESSION[sessionPrefix()."order_user_cart"][$user_cart_index];
		echo "Ödeme Başarılı işlem numaranız :".$islem_no;
		#siparişi kaydedip tamamlandı safyasına gönder.
    $orderObject = new Order();
    $orderObject->setTeslimatAddressBookId($teslimatAddressBookId);
    $orderObject->setFaturaAddressBookId($faturaAddressBookId);
    $orderObject->setUserId($userId);
    $orderObject->setPaymentType(0);
    $orderObject->setBankId(0);

    $insertOrder = $orderObject->insertOrder($cartInfo, $userCart);
		echo "Response : ".$insertOrder;

		unset($_SESSION[sessionPrefix()."order_cart_info"][$cart_info_index]);
		unset($_SESSION[sessionPrefix()."order_user_cart"][$user_cart_index]);
    if($insertOrder == "Success") {
      echo "sipariş alındı";
      header("location:".url("siparis-basarili"));
      exit;
    }
	}



?>
