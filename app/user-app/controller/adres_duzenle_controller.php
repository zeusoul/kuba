<?php
  if(!loginState()){
    header("location:".url("uyelik"));
  }
  else{
    $url = explode("/",$_GET["url"]);
    if(count($url) != 3){
      header("location:".url("uyelik"));
    }
    else {
      include("./tr.php");
      $trArray = json_decode($tr,true);
      $md5AddressBookId = $url[2];
      if(strlen($md5AddressBookId) != 32){
        header("location:".url("profil/adreslerim"));
      }
      else {
        $userId=(int)$_SESSION[sessionPrefix()."user_id"];
        $address = new Address();
        $address->setUserId($userId);


        if(isset($_POST["updateAddress"])){
          $trArray = json_decode($tr);
          $plaka = (int)$_POST["city"];
          $city="";
          foreach ($trArray as $key => $value) {
            if((int)$value->plaka == $plaka){
              $city = $value->il;
              break;
            }
          }

          $address->setTitle($_POST["title"]);
          $address->setName($_POST["name_surname"]);
          $address->setTel($_POST["tel"]);
          $address->setPostCode((isset($_POST["post_code"])) ? $_POST["post_code"] : null);
          $address->setCity($city);
          $address->setCounty($_POST["county"]);
          $address->setAddress($_POST["address"]);
          $address->setTcNo($_POST["tc_no"]);
          $address->setCompanyName($_POST["company_name"]);
          $address->setTaxAdministration($_POST["tax_administration"]);
          $address->setTaxNumber($_POST["tax_number"]);
          $address->setType($_POST["intervaltype"]);
          $address->setUserId($userId);
          $update = $address->updateAddressBook($md5AddressBookId);
          $pageMessage = ($update === "Success") ? "Adres Güncellendi" : $update;

        }
        $addressBook = $address->getAddressBook($md5AddressBookId);
        if(!is_array($addressBook) || count($addressBook) <= 0){
          header("location:".url('adreslerim'));
          exit;
        }
        else {
          $page="adres_duzenle";
          $title = "Adres Düzenle";
          $pageTitle = "Adres Düzenle";
          $map = map("Anasayfa,Profil,Adres Düzenle","index,profil,profil/adres-duzenle/$md5AddressBookId");
        }
      }
    }
  }

?>
