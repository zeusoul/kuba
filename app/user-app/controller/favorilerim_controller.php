<?php
  if (!loginState()) {
    #Ajax kontrolü
    if(isset($_POST["ajax"])){
      echo "Lütfen önce giriş yapınız";
      exit;
    }
    header("localtion:".url());
  }
  else {
    $userId = (int)$_SESSION[sessionPrefix()."user_id"];
    $favorite = new Favorite();
    $favorite->setUserId($userId);

    #Ajax kontrolü
    if(isset($_POST["ajax"])){
      $favorite->setProductId($_POST["productId"]);
      echo $favorite->insertFavorite();
      exit;
    }

    $favorites = $favorite->getFavorites();

    $title = "Favorilerim";
    $pageTitle = "Favorilerim";
    $map = map("Anasayfa,Profil,Favorilerim","index,profil,profil/favorilerim");
  }
?>
