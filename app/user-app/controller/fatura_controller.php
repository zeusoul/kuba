<?php

  if(!loginState()){
    header("location:".url("uyelik"));
    exit;
  }
  else{
    $url = explode('/',$_GET["url"]);
    if(count($url) != 3){
      header("location:".url("profil/siparislerim"));
      exit;
    }
    else{
      $orderNo=  $url[2];
      $orderObj= new Order();

      $orderObj->setOrderNo($orderNo);
      $order=$orderObj->getOrderDetails();

      $user = $order["user_information"];

      if((int)$order["status"] < 1){
        header("location:".url("profil/siparislerim"));
        exit;
      }
      else if($user["user_id"] != $_SESSION[sessionPrefix()."user_id"]){
        header("location:".url("profil/siparislerim"));
        exit;
      }

      require_once  PATH . '/vendor/autoload.php';
      $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => [210,297]]);
      $HTML ='<style>
                  .kisisel{
                    padding:10px;
                    margin-bottom:10%;
                    border: 1px solid #000;
                    height:10% !important;

                  }
                  .kisisel p{
                    font-size: 13px;
                  }
                  table {
                    border-collapse: collapse;
                    font-size: 12px;
                    width:100%;
                  }
                  table th {
                    font-size:15px;
                    background-color: #cccccc ;
                  }
                  table, th, td {
                    border: 1px solid black;
                  }
                  .header{
                    margin-bottom:10%;
                  }
              </style>
              <div class="header">
                  <img  src="'. publicUrl("img/logo.jpg") .'" >
              </div>
              <div class="kisisel">
                <p> <strong>Sayın, '. $order['fatura_address']['name_surname'].' <strong></p>
                <p> Fatura Adresi : '. $order['fatura_address']['address'].$order['user_information']['county'].$order['user_information']['city'].'</p>
                <p> VD:'. $order['fatura_address']['tax_administration'] .' VN: '.$order['fatura_address']['tax_number'].'</p>
              </div>
              <table>
                <thead >
                <tr >
                  <th >Açıklama</th>
                  <th>Adet</th>
                  <th>Birim Fiyat</th>
                  <th>Toplam Fiyat</th>
                </tr>
                </thead>
                <tbody>

                ';

      $toplam=0;
      $kdv=0;
      foreach ($order['products'] as $key => $value) {
        $HTML.= "<tr><td>".$value['title']."</td>    <td>". $value['quantity'] ."</td>  <td> " . $value['unit_price']. "</td>  <td>". (float)$value['unit_price']*(float)$value['quantity'] ." TL</td></tr>";
        $toplam+=(float)$value['unit_price']*(float)$value['quantity'];
      }
      $HTML.="</tbody>
              </table>

                <table style='width:100%;margin-left:62%; margin-top:10px'>
                  <tr>
                    <td>
                      KDV
                    </td>
                    <td>
                      ". (float)$toplam*0.18." TL
                    </td>
                  </tr>
                  <tr>
                    <td>
                      TOPLAM:
                    </td>
                    <td>
                      ". (float)$toplam ." TL
                    </td>
                  </tr>
                </table>

              ";
      $mpdf->WriteHTML($HTML);
      $mpdf->outPut();
      exit;
    }
  }

?>
