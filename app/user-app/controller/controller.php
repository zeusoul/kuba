<?php
  #ajax kontrolü (cookie policy için)
  if(isset($_POST["cookie_policy"])){
    $_SESSION[sessionPrefix()."cookie_policy"] = "true";
    echo "true";
    exit;
  }
  #ajax kontrolü (+18 uyarısı için)
  if(isset($_POST["uyari_kabul"])){
    $_SESSION[sessionPrefix()."uyari_sayfasi"] = "kabul";
    echo "true";
    exit;
  }
  #ajax kontrolü (favori için)
  if(isset($_POST["favorite"])){
    if(!loginState()) echo "failed";
    else{
      $objFavorite = new Favorite();
      $objFavorite->setProductId($_POST["productId"]);
      $objFavorite->setUserId($_SESSION[sessionPrefix()."user_id"]);
      $res = $objFavorite->insertFavorite();
      echo $res;
    }
    exit;
  }
  #ajax kontrolü (ürün arama için)
  if(isset($_POST["search"]) && isset($_POST["value"])){
    $value = $_POST["value"];
    $productObject = new Product();
    $products = $productObject->getSearchProducts($value);
    if(!is_array($products) || count($products) == 0) echo "";
    else{
      foreach ($products as $key => $product) {
        $url = url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]);
        $image = $product["image"];
        echo "
        <div class='search-res-item mt-3'>
          <a href='".$url."' >
            <div class='container'>
              <div class='row'>
                <div class='col-md-2 p-0'>
                  <img src='".publicUrl("img/product-images/$image")."' width='50'/>
                </div>
                <div class='col-md-10 text-white p-2'>
                                                                                                              ".$product["title"]."
                </div>
              </div>
            </div>
          </a>
        </div>";
      }
    }
    exit;
  }#ajax kontrolü (Hesap doğrulama maili göndermek için)
  #ajax kontrolü (doğrulama maili gönderme için)
  if(isset($_POST["send_to_mail"])){
    if(!loginState()){
      echo "Lütfen önce giriş yapınız";
      exit;
    }
    else {
      $email = $_SESSION[sessionPrefix()."email"];
      $mail = new Mail();
      $subject = "Hesabınızı Doğrulayın !";
      $content = "Doğrulama Linki : ".url("hesap-dogrula/".md5($_SESSION[sessionPrefix()."user_id"]));
      $sendMail = $mail->sendMail($email,"İstanbul","Elektromatik",$subject,$content);

      if($sendMail){
        echo "E-Mail adresinize bir doğrulama linki gönderdik. O link üzerinden hesabınızı doğrulayabilirsiniz.";
        exit;
      }
      else{
        echo "Doğrulama linki E-Mail adresinize gönderilemedi!";
        exit;
      }
    }
  }

  #E-Bulten kayit
  if(isset($_POST["insertEbulten"])){
    if(isset($_POST["e_bulten_email"]) && trim($_POST["e_bulten_email"]) != ""){
      $objEbulten = new Ebulten();
      $objEbulten->setEmail($_POST["e_bulten_email"]);
      $res = $objEbulten->insertEbulten();
      $pageMessage = $res == "Success" ? "E-Bültene Kayıt Başarılı" : $res;
    }
  }


  #kur bilgileri

  if(!is_float($usd_selling) || !is_float($euro_selling) || $usd_selling <= 0 || $euro_selling <= 0){
    #Kur bilgileri çekilemez ise
    //exit;
  }
  #Oluşturulan sayfaları çek.
  $pageObject = new Pages();
  $pages = $pageObject->getAllPages();

  #Oluşturulan kategorileri çek.
  $categoryObject = new Category();
  $categories = $categoryObject->getCategories();

  #Genel ayarları çek.
  $settingsObject = new Settings();
  $settings = $settingsObject->getSettings();

  #Şirket bilgilerini çek
  $companyObject = new Company();
  $companyInformation = $companyObject->getCompanyInformation();

  #sepeti getir.
  $cartObject = new Cart();
  $cartCount = $cartObject->getCountTheCart();

  $memberInfo="";
  if(loginState()){
    $memberObject = new Member();
    if(isset($_POST["sign_out"])){
      $memberObject->signOut();
    }
    else{
      $memberObject->setUserId($_SESSION[sessionPrefix()."user_id"]);
      $memberInfo = $memberObject->getUserInformation();
      # Kullanıcı Ban Kontrolü
      if((int)$memberInfo["status"] == 0){
        $memberObject->signOut();
        header("location:".url("uyelik?ban"));
        exit;
      }# Onaylanmamış hesap kontrolü
      else if((int)$settings["email_dogrulama"] == 1 && (int)$memberInfo["confirm"] == 0){
        $memberObject->signOut();
        header("location:".url("uyelik?confirm"));
        exit;
      }

      $messageObject = new Message();
      $messageObject->setRecipientId($_SESSION[sessionPrefix()."user_id"]);
      $newMessageStatus = $messageObject->getUnvisitedMessages();
    }
  }

?>
