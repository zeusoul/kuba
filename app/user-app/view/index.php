
<main>
    <!-- slider-area start -->
    <!-- <section class="slider-area pos-relative"> 
    <div class="slider-active">
          <?php foreach ($sliders as $key => $slider): ?>
            <div class="single-slider slide-1-style slide-height-2 slide-height-4 d-flex align-items-center" data-background="<?php echo publicUrl("img/slider-images/".$slider["slider_image"]); ?>">
                <div class="shape-title shape-title-4 bounce-animate">
                    <h2><?php echo $slider["slider_year"]; ?></h2>
                </div>
                <div class="shape-icon shape-icon-4 bounce-animate">
                <img src="<?php echo publicUrl("img/slider-images/shape-icon.png"); ?>" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="slide-content slide-content-4 text-center pt-40">
                            
                                <div class="slide-btn">
                                    <?php if ($slider["slider_url"] != ""): ?>
                                    <a class="btn theme-btn" href="<?php echo $slider["slider_url"]; ?>" data-animation="fadeInLeft" data-delay=".7s">Alışverişe Başla</a>
                                    <?php endif; ?>
                                    <a class="btn white-btn" href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>" data-animation="fadeInRight" data-delay=".9s">category</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </section> -->
    <section class="slider-area pos-relative">
        <div class="slider-active">
          <?php foreach ($sliders as $key => $slider): ?>
              <div id="pm-slider-image" class="single-slider slide-1-style slide-height d-flex align-items-center" style="min-height:600px; background-repeat:no-repeat;" data-background="<?php echo publicUrl("img/slider-images/".$slider["slider_image"]); ?>">
                  <div class="shape-title bounce-animate">
                      <!-- <h6><?php echo $slider["slider_year"]; ?></h6> -->
                  </div>
                  <div class="container-fluid">
                      <div class="row">
                          <div class="col-xl-12" id="slider-image">
                              <div class="slide-content  slide-content-4 text-center pt-40">
                              <span style="color:#fe906e;"data-animation="fadeInRight" data-delay=".2s" ><?php echo $slider["slider_title"]; ?></span>
                                <h1 data-animation="fadeInUp" data-delay=".5s" ><?php echo $slider["slider_description"]; ?></h1>
                                  <div class="slide-btn">
                                    <?php if ($slider["slider_url"] != ""): ?>
                                        <a class="btn theme-btn" href="<?php echo $slider["slider_url"]; ?>" data-animation="fadeInLeft" data-delay=".7s">Alışverişe Başla</a>
                                    <?php endif; ?>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            <?php endforeach; ?>
        </div>
    </section>
    <!-- <?php foreach ($sliders as $key => $slider): ?>
    <div id="pm-slider-btn-mobile" class="col-xl-12 text-center mt-4" >
    <?php if ($slider["slider_url"] != ""): ?>
            <a class="btn theme-btn" href="<?php echo $slider["slider_url"]; ?>" data-animation="fadeInLeft" data-delay=".7s">Alışverişe Başla</a>
            <?php endif; ?>
        </div>
        <?php endforeach; ?> -->
    <!-- slider-area end -->
            <!-- product-area end -->
            <!-- banner area start -->
            <section class="banner-area pt-30 pl-15 pr-15">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="banner mb-30">
                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>>">    
                                <img src="<?php echo publicUrl("img/main-images/".$firstImage["image_name"]); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="banner mb-30">
                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">    
                                <img src="<?php echo publicUrl("img/main-images/".$secondImage["image_name"]); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="banner mb-30">
                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">   
                                 <img src="<?php echo publicUrl("img/main-images/".$thirdImage["image_name"]); ?>" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- banner area end -->
    <!-- ürünler -->
    <section class="product-area box-90 pt-70">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="area-title mb-50">
                        <h2 style="color:#fdbcb4;text-align:center;">Yeni Ürünler</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                        <div class="col-xl-12">
                            <div class="product-tab-content">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <div class="product-slider owl-carousel">     <?php foreach ($lastProducts as $key => $product): ?>
                                            <div class="pro-item">
                                                <div class="product-wrapper mb-50">
                                                    <div class="product-img mb-25">
                                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">
                                                    <img src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                    <img class="secondary-img" src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                </a>
                                                <div class="product-action text-center">
                                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>" title="Quick View">
                                                        <i class="flaticon-eye"></i>
                                                    </a>
                                                </div>
                                                    </div>
                                                    <div class="product-content">
                                                    <div class="pro-cat mb-10">
                                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["model_name"]; ?></a>
                                                </div>
                                                <h4>
                                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                                </h4>
                                                <div class="product-meta">
                                                    <div class="pro-price">
                                                        <span>₺<?php echo $product["variants"][0]["discount_price"]; ?></span>
                                                        <span class="old-price">₺<?php echo $product["variants"][0]["price"]; ?></span>
                                                    </div>
                                                </div>
                                                <div class="product-wishlist">
                                                    <a id="favoriteBtn-<?php echo $product["product_id"]; ?>" onclick="favorite(<?php echo $product["product_id"]; ?>)" class="<?php if($product["favorite"] == 1) echo "bg-danger"; ?>"><i style="margin-top:15px" class="far fa-heart" title="Wishlist"></i></a>
                                                </div>
                                                    </div>
                                                </div>
                                            </div>    <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </section>
    <!-- ürünler -->
    <!-- product-area start -->
    <!-- <section class="banner-area pt-30 pl-15 pr-15">
        <div class="container-fluid">
        <div class="row">
                <div class="col-xl-5 col-lg-12">
                    <div class="area-title mb-50">
                        <h2 style="color:#fdbcb4;">Kategoriler</h2>
                    </div>
                </div>
            </div>
            <div class="row">
            <?php $i=0; ?>
            <?php foreach ($categories as $key => $category): ?>
                <?php if ($category["category_image"] != ""): ?>
                <div class="col-lg-4 col-md-6">
                    <div class="banner mb-30">
                    <a href="<?php echo url("kategori/".seoUrl($category["name"])."/".$category["category_id"]); ?>"><img style="" src="<?php echo publicUrl("img/category-images/".$category["category_image"]); ?>" alt=""></a>
                    </div>
                </div>
                <?php endif; ?>
            <?php endforeach; ?>
            </div>
        </div>
    </section> -->
    <!-- banner area end -->
      <!-- en üst 2li banner -->
      <section class="top-seller-area box-90 pt-30">
            <div class="container-fluid">
                <div class="row"><br><br>
                    <div class="col-xl-12 col-lg-8 col-md-7">
                        <div class="area-title mb-50">
                            <h2 style="color:#fdbcb4;text-align:center;">Fırsat Zamanı</h2>
                            <p style="color:#000000;text-align:center;">Sizler tarafından en çok beğenilen kampanyalı ürünlerimizi sizler için derledik</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-5 col-lg-5">
                        <div class="top-seller mb-50">
                            <img style="max-width:75%;" src="<?php echo publicUrl("img/main-images/".$fourthImage["image_name"]); ?>" alt="">
                            <div class="seller-box text-center">
                                <div class="top-seller-content text-left">
                                    <h2 style="color:#fdbcb4;"><a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">Outlet Ürünler</a></h2>
                                    <div class="top-seller-btn" style="color:#fdbcb4;">
                                        <a href="<?php echo url("kategori"); ?>" class="btn theme-btn">Ürünlere Göz At </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-7">
                        <div class="top-seller text-right mb-50">
                            <img style="max-width:75%;" src="<?php echo publicUrl("img/main-images/".$fiveImage["image_name"]); ?>" alt="">
                            <div class="sellet-2-content">
                                <h2 style="color:#fdbcb4;"><a href="<?php echo url("kategori"); ?>">Çok Satanlar</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <!-- en üst 2'li  banner -->
    <!-- en yeni ürünler -->
    <section class="product-area box-90 pt-45 pb-40">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 col-lg-12">
                    <div class="area-title mb-50">
                        <h2 style="color:#fdbcb4;text-align:center;">En Çok Sepete Eklenen Ürünler</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="product-tab-content">
                        <div class="tab-content" id="myTabContent1">
                            <div class="tab-pane fade show active" role="tabpanel" aria-labelledby="home-tab">
                                <div class="product-slider owl-carousel">
                                <?php foreach ($mostAddingToCart as $key => $product): ?>
                                    <div class="pro-item">
                                        <div class="product-wrapper mb-50">
                                            <div class="product-img mb-25">
                                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">
                                                    <img src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                    <img class="secondary-img" src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                </a>
                                                <div class="product-action text-center">
                                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>" title="Quick View">
                                                        <i class="flaticon-eye"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <h4>
                                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                                </h4>
                                                <div class="product-meta">
                                                    <div class="pro-price">
                                                        <span>₺<?php echo $product["variants"][0]["discount_price"]; ?></span>
                                                        <span class="old-price">₺<?php echo $product["variants"][0]["price"]; ?></span>
                                                    </div>
                                                </div>
                                                <div class="product-wishlist">
                                                    <a id="favoriteBtn-<?php echo $product["product_id"]; ?>" onclick="favorite(<?php echo $product["product_id"]; ?>)" class="<?php if($product["favorite"] == 1) echo "bg-danger"; ?>"><i style="margin-top:15px" class="far fa-heart" title="Wishlist"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- en yeni ürünler



    <!-- latest-blog-area start -->
    <section class="latest-blog-area pt-95 box-90">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="area-title text-center mb-50">
                        <h2 style="color:#fdbcb4;">Haberler & Kampanyalar</h2>
                        <p>Hakkımızdaki son güncellemelere ve kampanyalara bu alandan ulaşabilirsiniz</p>
                    </div>
                </div>
            </div>
            <div class="row">
            <?php if(count($news) == 0){ ?>
                                Kayıtlı haber bulunamadı
                            <?php } else{ ?>
                            <?php foreach($news as $key => $new){ ?>
                            <?php
                                if($key < $offset) {continue;}
                                else if($key >= $limit) {break;}
                            ?>
                    <div class="col-xl-4 col-lg-6 col-md-6">
                        <div class="latest-news mb-40">
                            <div class="news__thumb mb-25">
                                <img src="<?php echo publicUrl('img/new-images/'.$new["new_image"]) ?>" alt="">
                            </div>
                            <div class="news__caption white-bg">
                                <div class="news-meta mb-15">
                                    <span><i class="far fa-calendar-check"></i> <?php echo $new["new_date"]; ?></span>
                                </div>
                                <h2 style="color:#fdbcb4;"><a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>"><?php echo $new["new_title"]; ?></a></h2>
                                <p><?php echo $new["new_desc"]; ?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                <?php }?>
            </div>
        </div>
    </section>
    <!-- latest-blog-area end -->


</main>
