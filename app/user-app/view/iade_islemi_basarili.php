    <div class="col-md-9 mt-3 mb-3 text-center">
      <img src="<?php echo publicUrl('img/success.png'); ?>" style="width:80px;height:80px;">
      <h5>İade Talebiniz Alındı !</h5>
      <p style="font-size:18px;color:#5C5C5C;">
        İade taleplerinizi görüntülemek için <a href="<?php echo url("profil/iade-taleplerim")?>">tıklayınız</a>.
      </p>
      <a href="<?php echo url("profil/siparislerim"); ?>" class="btn btn-success">Siparişlerim</a>
    <br><br>
    </div>
	</div>
</div>
