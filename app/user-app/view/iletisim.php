  <!-- contact-area start -->
  <section class="contact-area pt-120 pb-90" data-background="assets/img/bg/bg-map.png">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="contact text-center mb-30">
                            <i class="fas fa-envelope"></i>
                            <h3>E-Mail Adresimiz </h3>
                            <p><?php echo $companyInformation["company_email"]; ?></p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-4">
                        <div class="contact text-center mb-30">
                            <i class="fas fa-map-marker-alt"></i>
                            <h3>Adres</h3>
                            <p><?php echo $companyInformation["company_address"]; ?></p>
                        </div>
                    </div>
                    <div class="col-xl-4  col-lg-4 col-md-4 ">
                        <div class="contact text-center mb-30">
                            <i class="fas fa-phone"></i>
                            <h3>İletişim Numaralarımız</h3>
                            <p>Telefon: <?php echo $companyInformation["company_tel"]; ?></p>
                                <p>Telefon/Whatsapp: <?php echo $companyInformation["company_wp"]; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- contact-area end -->

        <!-- contact-form-area start -->
        <!-- <section class="contact-form-area grey-bg pt-100 pb-100">
            <div class="container">
                <div class="form-wrapper">
                    <div class="row align-items-center">
                        <div class="col-xl-8 col-lg-8">
                            <div class="section-title mb-55">
                                <p><span></span> Anything On  Mind</p>
                                <h1>Estimate  Idea</h1>
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-3 d-none d-xl-block ">
                            <div class="section-link mb-80 text-right">
                                <a class="btn theme-btn" href="#"><i class="fas fa-envelope"></i> make call</a>
                            </div>
                        </div>
                    </div>
                    <div class="contact-form">
                        <form  method="post">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-box user-icon mb-30">
                                        <input type="text" name="name" placeholder="Ad Soyad">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-box email-icon mb-30">
                                        <input type="text" name="email" placeholder=" Email">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-box phone-icon mb-30">
                                        <input type="text" name="phone" placeholder=" Telefon">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-box subject-icon mb-30">
                                        <input type="text" name="title" placeholder=" Konu">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-box message-icon mb-30">
                                        <textarea name="description" id="message" cols="30" rows="10" placeholder=" Mesajınız"></textarea>
                                    </div>
                                    <div class="contact-btn text-center">
                                        <button class="btn theme-btn" type="submit" name="sendMessage">Gönder</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section> -->
        <!-- contact-form-area end -->

        <section class="map-area">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3010.878914185617!2d28.9574727!3d41.0060234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab98695ca3d91%3A0x90bf96da4f6834fa!2sKatip%20Kas%C4%B1m%2C%20Asya%20Sk.%20No%3A30%2C%2034130%20Fatih%2F%C4%B0stanbul!5e0!3m2!1str!2str!4v1662507970141!5m2!1str!2str" width="1800" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
        </section>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvEEMx3XDpByNzYNn0n62Zsq_sVYPx1zY"></script>