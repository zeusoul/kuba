
<div class="container mb-5">
<!-- Horizontal Steppers -->
<div class="row">
  <div class="col-md-12 mt-3 mb-3">
   	 <h4 class="text-center font-weight-bold text-uppercase" >Teslimat Adresi</h4>
	  <div class="progress">
		  <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style="width: 25%"></div>
	</div>
  </div>
  <hr>
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="col-md-12">
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    </div>
  <?php } ?>
  <!--Address-->
  <div class="col-md-8">
    <div class="card shopping-cart">
      <div class="card-header   " >
        <i class="fa fa-map-marker" aria-hidden="true"></i>
          Teslimat Adresi Seçiniz
        <div class="clearfix"></div>
      </div>
      <div class="card-body">
        <div class="adress-list">
            <div class="col-md-12">
              <div>
                <a href=""class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse" >
                 <i class="fa fa-plus"></i>  Yeni Adres Ekle
                </a>
              </div>
              <div class="collapse mt-1 " id="collapse">
                <div class="card card-body ">
                  <form action="" method="post" class="form-row">
                    <input type="hidden" name="satis_sozlesmesi" value="<?php echo $_POST["satis_sozlesmesi"]; ?>">
                    <div class="form-group col-md-6 ">
                      <label for="title">Adres Adı *</label>
                      <input type="text" class="form-control" id="title" name="title" placeholder="Adres Adı : ev, iş.." value="<?php echo $_POST["title"]; ?>" required/>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="name_surname">Adınız Soyadınız *</label>
                      <input type="text" id="name_surname" class="form-control" name="name_surname" value="<?php echo $_POST["name_surname"]; ?>" placeholder="Adınız Soyadınız" required/>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="phone">Telefon Numarası *</label>
                      <input type="text" id="phone" minlength="10" maxlength="11" class="form-control" name="tel" value="<?php echo $_POST["tel"]; ?>" placeholder="Telefon Numarası" required/>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="post_code">Posta Kodu (Zorunlu Değil)</label>
                      <input type="text" id="post_code" class="form-control" name="post_code" value="<?php echo $_POST["post_code"]; ?>" placeholder="Posta Kodu">
                    </div>
                    <div class="form-group col-md-6">
                      <label for="Iller">Şehir *</label>
                      <select class="form-control" id="Iller" name="city" required/>
                       <option value="0">Lütfen Bir İl Seçiniz</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label for="Ilceler" name="county">İlçe *</label>
                      <select class="form-control" id="Ilceler" name="county" disabled="disabled" required/>
                       <option value="0">Lütfen Önce bir İl seçiniz</option>
                      </select>
                    </div>
                    <div class="form-group  col-md-12">
                      <label for="address">Adres Detayı</label>
                      <textarea name="address" class="form-control" id="address" cols="30" rows="3" minlength="5" required><?php echo $_POST["address"]; ?></textarea>
                    </div>
                    <div class="form-group  col-md-6">
                      <div id="bireysel" class="tab-pane <?php if(!isset($_POST["intervaltype"]) || $_POST["intervaltype"] == "1") echo "active"; ?>">
                      </div>
                        <div class="form-group  col-md-12">
                            <input id="optDaily" checked name="intervaltype" type="radio" value="1" data-target="#bireysel">
                            <label for="optDaily">Bireysel</label>
                        </div>
                        <!--Bireysel-->
                        <div class="form-group  col-md-12">
                          <label for="tc_no">TC NO</label>
                          <input type="text" name="tc_no" class="form-control" id="tc_no" maxlength="11"  value="<?php echo $_POST["tc_no"]; ?>"/>
                        </div>

                      </div>
                      <div class="form-group  col-md-6">
                        <div class="ml-2">
                            <input id="optWeekly" <?php if($_POST["intervaltype"] == "0") echo "checked"; ?> name="intervaltype" type="radio" value="0" data-target="#kurumsal">
                            <label for="optWeekly">Kurumsal</label>
                        </div>
                        <div id="kurumsal" class="tab-pane <?php if(isset($_POST["intervaltype"]) && $_POST["intervaltype"] == "0") echo "active"; ?>">
                          <!--kurumsal-->
                          <div class="form-group  col-md-12">
                            <label for="company_name">Şirket Adı</label>
                            <input type="text" name="company_name" class="form-control" id="company_name" value="<?php echo $_POST["company_name"]; ?>"/>
                          </div>
                          <div class="form-group  col-md-12">
                            <label for="tax_administration">Vergi Dairesi</label>
                            <input type="text" name="tax_administration" class="form-control" id="tax_administration" value="<?php echo $_POST["tax_administration"]; ?>"/>
                          </div>
                          <div class="form-group  col-md-12">
                            <label for="tax_number">Vergi No</label>
                            <input type="text" name="tax_number" maxlength="10" class="form-control" id="tax_number" value="<?php echo $_POST["tax_number"]; ?>"/>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-12 d-flex justify-content-end  ">
                      <input class="btn btn-secondary " type="submit" name="insertAddress" value="Ekle"/>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <form id="address_form" action="<?php echo url("fatura-adresi-sec"); ?>"   method="post">
              <input type="hidden" name="satis_sozlesmesi" value="<?php echo $_POST["satis_sozlesmesi"]; ?>">
              <div class="container">
                <div class="row">
                <?php $i=0; ?>
                <?php  foreach ($addressBooks as $addressBook ):?>
                  <?php $checked = ($i++ == 0) ? "checked" : ""; ?>
                  <div class="adress col-md-4 mt-2">
                    <div class="card" style="height:15rem;">
                      <label for="address_book_<?php echo $addressBook['address_book_id'];?>">
                        <div class="card-body">
                          <div class="custom-control custom-radio">
                            <h6 class="card-title">
                            <input type="radio" <?php echo $checked; ?> id="address_book_<?php echo $addressBook['address_book_id'];?>" name="teslimat_address_book_id" class="custom-control-input" value="<?php echo $addressBook["address_book_id"]; ?>" required>
                            <label class="custom-control-label" for="address_book_<?php echo $addressBook['address_book_id'];?>">
                              <span><?php echo $addressBook['title'] ?></span>
                            </label>
                            </h6>
                          </div>
                          <hr>
                          <h6 class="card-subtitle mb-2 text-muted"><?php echo $addressBook['name_surname']; ?></h6>
                          <p class="card-text"><?php echo $addressBook['tel']; ?></p>
                          <p class="card-text"><?php echo $addressBook['county']." / ".$addressBook["city"]; ?></p>
                          <h6 class="card-text " ><?php echo kisalt($addressBook['address'],50); ?></h6>
                        </div>
                      </label>
                    </div>
                  </div>
                <?php endforeach;?>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <!--Address-->
  <!--Sipariş özeti-->
  <div class="col-md-4">
    <div class="card shopping-cart">
      <div class="card-header " >
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
          Sipariş Özeti
        <div class="clearfix"></div>
      </div>
      <div class="card-body">
        <h6 class="card-title">Sipariş Özeti </h6>
        <hr>
        <table class="col-md-10">
          <tr>
            <td>Toplam Ürün</td>
            <td style="text-align:right"><?php echo $cartCount; ?> adet</td>
          </tr>
          <tr>
            <td>Toplam Fiyat</td>
            <td style="text-align:right"><?php echo $cartInfo["toplam_fiyat"]; ?> TL</td>
          </tr>
          <tr>
            <td>Kargo Ücreti</td>
            <td style="text-align:right">
              <?php if((float)$cartInfo["kargo_ucreti"] <= 0){ ?>
                <span class="badge badge-success">
                  Ücretsiz kargo
                </span>
              <?php } else { ?>
                <?php echo $cartInfo["kargo_ucreti"]; ?> ₺
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td>KDV Dahil</td>
            <td style="text-align:right"><?php echo $cartInfo["kdv_dahil_fiyat"]; ?> TL</td>
          </tr>
        </table>
        <hr>
        <table class="col-md-12" style="font-weight:bold;">
          <tr>
            <td>Ödenecek Tutar</td>
            <td><?php echo $cartInfo["odenecek_tutar"]; ?> TL</td>
          </tr>
          <tr>
            <td colspan="2">
              <input class="btn btn-success w-100" type="submit" form="address_form" name="select_address" value="Devam Et">
            </td>
          </tr>
        </table>
      </div>
    </div>
  </div>
  <!--Sipariş özeti-->
</div>
<!-- /.Horizontal Steppers -->
</div>
