<div class="container">
	<div class="row text-center">
    <div class="col-sm-3 col-sm-offset-3"></div>
    <div class="col-sm-6 col-sm-offset-3">
      <h3 class="mt-4" style="color:#F95858">HATA</h3>
      <img src="<?php echo publicUrl('img/warning.png'); ?>" style="width:80px;height:80px;">
      <h5>Sipariş Başarısız !</h5>
      <p style="font-size:18px;color:#5C5C5C;">
        <?php echo $_POST["error"]; ?>
      </p>
			<a href="<?php echo url("sepet"); ?>" class="btn btn-success">Siparişi Tamamla</a>
      <a href="<?php echo url(""); ?>" class="btn btn-dark">Ana Sayfa</a>
    <br><br>
    </div>
	</div>
</div>
