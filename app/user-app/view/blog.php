<main>
  <!-- shop-area start -->
  <section class="shop-area pt-100 pb-100">
      <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-4 p-0">
              <div class="sidebar-box p-0">
                <div class="shop-widget p-5">
                  <h3 class="shop-title">SON YAZILAR</h3>
                  <ul class="shop-link">
                    <?php foreach ($news as $new): ?>
                      <li>
                        <a href="<?php echo url("blog/".seoUrl($new["new_title"])."-b-".$new["new_id"]); ?>">
                          <?php echo $new['new_title'];?>
                        </a>
                      </li>
                    <?php endforeach;?>
                  </ul>
                </div>
              </div>
            </div>
              <div class="col-xl-8 col-lg-8">
                  <!-- tab content -->
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="row">
                            <?php foreach ($news as $key => $new): ?>
                              <?php //if($key == 10) break; ?>
                              <div class="col-lg-12 col-md-12">
                                  <div class="product-wrapper mb-50">
                                      <div class="product-img mb-25">
                                          <a href="<?php echo url(seoUrl("blog/".$new["new_title"])."-b-".$new["new_id"]); ?>">
                                              <img src="<?php echo publicUrl("img/new-images/".$new["new_image"]); ?>" alt="">
                                          </a>
                                      </div>
                                      <div class="product-content">
                                          <div class="pro-cat mb-10"><?php echo $new["new_title"]; ?></div>
                                          <h4><?php echo kisalt($new["new_content"],200); ?></h4>
                                      </div>
                                      <div class="product-content">
                                        <a class="btn theme-btn text-white"  href="<?php echo url("blog/".seoUrl($new["new_title"])."-b-".$new["new_id"]); ?>">DEVAMINI OKU</a>
                                      </div>
                                  </div>
                                </div>
                            <?php endforeach; ?>
                          </div>
                      </div>
                  </div>
                  <div class="basic-pagination basic-pagination-2 text-center mt-20">
                      <ul>
                          <li><a href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>"><i class="fas fa-angle-double-left"></i></a></li>

                          <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
                            <li class="<?php if($page_number == $i+1) echo "active"; ?>"><a href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a></li>
                          <?php } ?>


                          <li><a href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>"><i class="fas fa-angle-double-right"></i></a></li>
                      </ul>
                  </div>

              </div>

          </div>
      </div>
  </section>
  <!-- shop-area end -->
</main>
