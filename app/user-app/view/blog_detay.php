<main>
  <!-- shop-area start -->
  <section class="shop-area pt-100 pb-100">
      <div class="container">
          <div class="row">
            <div class="col-xl-4 col-lg-4 p-0">
              <div class="sidebar-box p-0">
                <div class="shop-widget p-5">
                  <h3 class="shop-title">SON YAZILAR</h3>
                  <ul class="shop-link">
                    <?php foreach ($news as $new): ?>
                      <li>
                        <a href="<?php echo url("blog/".seoUrl($new["new_title"])."-b-".$new["new_id"]); ?>">
                          <?php echo $new['new_title'];?>
                        </a>
                      </li>
                    <?php endforeach;?>
                  </ul>
                </div>
              </div>
            </div>
              <div class="col-xl-8 col-lg-8">
                  <!-- tab content -->
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="product-wrapper mb-50">
                                    <div class="product-img mb-25">
                                        <a href="<?php echo url(seoUrl("blog/".$newDetails["new_title"])."-b-".$newDetails["new_id"]); ?>">
                                            <img src="<?php echo publicUrl("img/new-images/".$newDetails["new_image"]); ?>" alt="">
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <div class="pro-cat mb-10"><?php echo $newDetails["new_title"]; ?></div>
                                        <h4><?php echo $newDetails["new_content"]; ?></h4>
                                    </div>
                                </div>
                              </div>
                          </div>
                      </div>
                  </div>

              </div>

          </div>
      </div>
  </section>
  <!-- shop-area end -->
</main>
