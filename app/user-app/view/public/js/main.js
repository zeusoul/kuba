$(document).ready(function(){
  $("#exit_cp").click(function(){
    var protocol = window.location.protocol;
    var pathname = window.location.pathname;
    var res = pathname.split("/");
    pathname = res[1];
    if(pathname.trim() != "") pathname+="/";
    var url = protocol+"//"+window.location.hostname+"/"+pathname;
    $.post(url,{cookie_policy : ""}, function(response){
      $("#cookie_policy").css("display","none");
    });
  });
});

function showLoading(){
  $("#loading-div").css("display","block");
}
function hideLoading(){
  $("#loading-div").css("display","none");
}

function showMessage(message){
  $("#message > strong").text(message);
  $("#message-div").css("display","block");
  setTimeout("hideMessage()",3000);
}
function hideMessage(){
  $("#message > strong").text("");
  $("#message-div").css("display","none");
}

$("#sign-in").click(function(){
  var email = $("#giris-email").val();
  var sifre = $("#giris-sifre").val();
  if(email.trim() != "" && sifre.trim() != ""){
    showLoading();
  }
});

$("#sign-up").click(function(){
  var email = $("#kayit-email").val();
  var ad = $("#kayit-ad").val();
  var soyad = $("#kayit-soyad").val();
  var tel = $("#kayit-tel").val();
  var sifre = $("#kayit-sifre").val();
  if(email.trim() != "" && ad.trim() != "" && soyad.trim() != "" && sifre.trim() != "" && sifre.length >= 8){
    showLoading();
  }
});
