$("#send_to_mail").click(function(){
  showLoading();
  var protocol = window.location.protocol;
  var hostname = window.location.hostname;
  var pathname = window.location.pathname;
  var res = pathname.split("/");
  pathname = res[1];
  if(hostname != "localhost") pathname = "";
  else if(pathname.trim() != "") pathname+="/";
  var url = protocol+"//"+hostname+"/"+pathname;
  $.post(url,{send_to_mail : ''},function(response){
    hideLoading();
    showMessage(""+response);
  });
});
