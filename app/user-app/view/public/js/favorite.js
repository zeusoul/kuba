
  function favorite(productId){
    var protocol = window.location.protocol;
    var hostname = window.location.hostname;
    var pathname = window.location.pathname;
    var res = pathname.split("/");
    pathname = res[1];
    if(hostname != "localhost") pathname = "";
    else if(pathname.trim() != "") pathname+="/";
    var url = protocol+"//"+hostname+"/"+pathname;

    $.post(url,{productId : productId, favorite : ""},function(response){
      if(response != "failed"){
        if($("#favoriteBtn-"+productId).attr("class") == ""){
          $("#favoriteBtn-"+productId).attr("class", "bg-danger");
        }
        else {
          $("#favoriteBtn-"+productId).attr("class", "");
        }
      }
      if(response == "failed") showMessage("Lütfen Giriş Yapınız");
      else showMessage(response);
    });
  }
