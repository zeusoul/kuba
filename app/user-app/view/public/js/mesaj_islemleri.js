$(document).ready(function(){
  document.onkeydown = mesajGonder;
  function mesajGonder(e){
    var tus = e.which;
    //Enter tusuna basildiginda
    if(tus==13){
      $("#message").attr("disabled","disabled");
      var $mesaj = $("#message").val();
      var url = window.location.href;
      $.post(url,{send_message : "", message : $mesaj},function(response){
        $("#message").removeAttr("disabled");
        $("#message").val("");
        if(response.trim() == "Success") {
          mesajListele();
        }
        else if(response == "uzun"){
          alert("boşluk bırakmanız lazım");
        }
        $("#message").focus();
      })
    }
  }
  var tempCallback="";
  function mesajListele(){
    var url = window.location.href;
    $.post(url,{message_list : ""},function(response){
      var mesajlar = $("#message_history").html();
      if(tempCallback != response){
        tempCallback = response;
        var element = document.getElementById("message_history");
        var scrollKontrol =0;
        if(element.scrollTop == element.scrollHeight - element.clientHeight) scrollKontrol++;
        $("#message_history").html(response);
        if(scrollKontrol > 0)element.scrollTop = element.scrollHeight - element.clientHeight;
      }
    });
    setTimeout(mesajListele, 10000);
  }
  $("#message_history").html("");
  mesajListele();

});
