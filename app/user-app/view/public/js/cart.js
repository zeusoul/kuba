function addToCart(barcode, page){
  showLoading();
  var quantity = 1;
  var protocol = window.location.protocol;
  var hostname = window.location.hostname;
  var pathname = window.location.pathname;
  var res = pathname.split("/");
  pathname = res[1];
  if(hostname != "localhost") pathname = "";
  else if(pathname.trim() != "") pathname+="/";
  var url = protocol+"//"+hostname+"/"+pathname+"sepet";

  $.post(url,{barcode : barcode, quantity : quantity},function(response){
    if(!parseInt(response)){
      showMessage(response);
      if(page == "sepet"){
        window.location.reload();
      }
    }
    else {
      $("#cart_count").text(response);
       showMessage('Ürün Sepete Eklendi');
       window.location.reload();

    }
    hideLoading();
  });

}
