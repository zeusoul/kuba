<div class="col-lg-3">
    <ul class="nav myaccount-tab-trigger" id="account-page-tab" role="tablist">
        <div class="footer-area box-90 pt-100 pb-60">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-3 col-lg-5 col-md-6 ">
                        <div class="footer-widget mb-40">
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "profil") echo "active"; ?>"
                                    href="<?php echo url("profil"); ?>" role="tab" aria-controls="account-orders"
                                    aria-selected="false">
                                    Hesabım
                                </a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "siparislerim") echo "active"; ?>"
                                    href="<?php echo url("profil/siparislerim"); ?>" role="tab"
                                    aria-controls="account-orders" aria-selected="false">Siparişlerim</a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "favorilerim") echo "active"; ?>"
                                    href="<?php echo url("profil/favorilerim"); ?>" role="tab"
                                    aria-controls="account-orders" aria-selected="false">Favorilerim</a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "adreslerim") echo "active"; ?>"
                                    href="<?php echo url("profil/adreslerim"); ?>" role="tab"
                                    aria-controls="account-address" aria-selected="false">Adreslerim</a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "yorumlarim") echo "active"; ?>"
                                    href="<?php echo url("profil/yorumlarim"); ?>" role="tab"
                                    aria-controls="account-details" aria-selected="false">Yorumlarım</a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "iade_taleplerim") echo "active"; ?>"
                                    href="<?php echo url("profil/iade-taleplerim"); ?>" role="tab"
                                    aria-controls="account-details" aria-selected="false">İade Taleplerim</a>
                            </li>
                            <li class="nav-item" style="border-style: groove;padding-right: 150px;padding-bottom: 15px;">
                                <a class="nav-link <?php if($page == "mesajlarim") echo "active"; ?>"
                                    href="<?php echo url("profil/mesajlarim"); ?>" role="tab"
                                    aria-controls="account-details" aria-selected="false">Mesajlarım</a>
                            </li>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </ul>
</div>