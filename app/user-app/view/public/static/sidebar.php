<div class="" style="width: 100%; height: 50px;style="margin-bottom:50px !important;"">

</div>
<div class="col-lg-3">
    <div class="widget mb-40">
        <div class="widget-title-box mb-30">
            <span class="animate-border"></span>
            <h3 class="widget-title">Hesap Ayarlarım</h3>
        </div>
        <ul class="cat">
            <li>
                <a class="nav-link <?php if($page == "profil") echo "active"; ?>"
                    href="<?php echo url("profil"); ?>" role="tab" aria-controls="account-orders"
                    aria-selected="false">
                    Hesabım
                </a>
            </li>
            <li>
            <a class="nav-link <?php if($page == "siparislerim") echo "active"; ?>"
                href="<?php echo url("profil/siparislerim"); ?>" role="tab"
                aria-controls="account-orders" aria-selected="false">Siparişlerim</a>
            </li>
            <li>
            <a class="nav-link <?php if($page == "urunlerim") echo "active"; ?>"
                href="<?php echo url("profil/urunlerim"); ?>" role="tab"
                aria-controls="account-orders" aria-selected="false">Ürünlerim</a>
            </li>
            <li>
            <a class="nav-link <?php if($page == "adreslerim") echo "active"; ?>"
                href="<?php echo url("profil/adreslerim"); ?>" role="tab"
                aria-controls="account-address" aria-selected="false">Adreslerim</a>
            </li>
            <li>
            <a class="nav-link <?php if($page == "yorumlarim") echo "active"; ?>"
                href="<?php echo url("profil/yorumlarim"); ?>" role="tab"
                aria-controls="account-details" aria-selected="false">Yorumlarım</a>
            </li>
        </ul>
    </div>
    <div class="widget mb-40 p-0 b-0">
        <div class="banner-widget">
            <a href="#"><img src="<?php echo publicUrl("img\HU6A0060.jpg"); ?>" alt=""></a>
        </div>
    </div>
</div>
