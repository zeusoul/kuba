  <style>
   
    .floats{
  position:fixed;
  width:60px;
  height:60px;
  bottom:116px;
  right:29px;
  background-color:#25d366;
  color:#FFF;
  padding-top:18px ;
  border-radius:50px;
  text-align:center;
  font-size:30px;
  box-shadow: 2px 2px 3px #999;
  z-index:100;
}
@media only screen and (max-width: 767px) {
  .space-the-top {
        margin-top:10px !important;
}
}
  </style>
  <hr>
  <!-- footer start -->
      <!-- footer start -->
      <footer>
            <div class="footer-area box-90 pt-60">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-xl-2 col-lg-2 col-md-3 d-lg-none d-xl-block">
                      <div class="footer-widget pl-50 mb-40">
                          <h3 style="color:#fdbcb4;">Kurumsal</h3>
                          <ul class="footer-link">
                              <!-- <li><a href="#">Facebook</a></li> -->
                              <li><a href="<?php echo url("hakkimizda"); ?>">Hakkımızda</a></li>
                              <li><a href="<?php echo url("iletisim"); ?>">Bize Ulaşın</a></li>
                              <li><a href="https://www.trendyol.com/magaza/moda-mahur-m-362992?sst=0">Trendyol</a></li>
                              <li><a href="https://api.whatsapp.com/send?phone=905442339096&text=Merhaba%20ModaMahur%20">Whatsapp</a></li>
                          </ul>
                      </div>
                  </div>
                  <div class="col-xl-2 col-lg-2 col-md-3 d-lg-none d-xl-block">
                      <div class="footer-widget pl-30 mb-40">
                      <h3 style="color:#fdbcb4;">Yardım</h3>
                    <ul class="footer-link">
                        <li><a href="<?php echo url("sozlesme/mesafeli-satis-sozlesmesi"); ?>">Mesafeli Satış Sözleşmesi</a></li>
                        <li><a href="<?php echo url("sozlesme/gizlilik-politikasi"); ?>">Gizlilik Politikası</a></li>
                        <li><a href="<?php echo url("sozlesme/iptal-ve-iade-sartlari"); ?>">İptal ve İade Şartları</a></li>
                        <li><a href="<?php echo url("sozlesme/teslimat-ve-iade-sartlari"); ?>">Teslimat ve İade Bilgileri</a></li>
                        <li><a href="<?php echo url("sozlesme/cerez-politikasi"); ?>">Çerez Politikası</a></li>
                    </ul>
                      </div>  
                  </div>
                  <div class="col-xl-3 col-lg-2 col-md-3">
                      <div class="footer-widget mb-40">
                      <h3 style="color:#fdbcb4;">Hesabım</h3>
                    <ul class="footer-link">
                        <li><a href="<?php echo url("uyelik"); ?>">Üye Ol</a></li>
                        <li><a href="<?php echo url("uyelik"); ?>">Giriş Yap</a></li>
                        <li><a href="<?php echo url("profil/siparislerim"); ?>">Siparişlerim</a></li>
                        <li><a href="<?php echo url("sepet"); ?>">Sepetim</a></li>
                        <li><a href="<?php echo url("hakkimizda"); ?>">Hakkımızda</a></li>
                        <li><a href="<?php echo url("iletisim"); ?>">İletişim</a></li>
                        <?php foreach ($pages as $key => $page): ?>
                          <?php if ($page["status"] == 1): ?>
                            <li><a href="<?php echo url("sayfa/".seoUrl($page["menu_title"])."/".$page["page_id"]); ?>"><?php echo $page["menu_title"]; ?></a></li>
                          <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                      </div>
                  </div>
                  <div class="col-xl-3 col-lg-5 col-md-6 ">
                      <div class="footer-banner">
                          <a href="<?php echo url(); ?>"><img style="max-width:45% !important;" src="<?php echo publicUrl("img/logo.jpg"); ?>" alt=""></a>
                          </div>
                      <div class="footer-widget mb-40">
                          <p>  <?php echo kisalt($companyInformation["company_about"],300); ?>
                          </p>
                          <div class="footer-time d-flex mt-30" style="    padding-right: 10px;">
                              <div class="time-text">
                              <span style="color:#fdbcb4;" >Sorunuz mu var? Bize <a href="<?php echo url("iletisim"); ?>" style="color:tomato;">ulaşabilirsiniz!</a> </span>
                            <h2><?php echo $companyInformation["company_tel"]; ?></h2>
                              </div>                               
                          </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
            <a href="https://api.whatsapp.com/send?phone=905442339096&text=Merhaba%20ModaMahur%20" class="floats" target="_blank">
<i class="fa fa-whatsapp my-float"></i>
</a>
            <div class="copyright-area box-105">
                <div class="container-fluid">
                    <div class="copyright-border pt-30 pb-30">
                        <div class="row">
                            <div class="col-xl-12 col-lg-6 col-md-6">
                                <div class="copyright text-center text-md-left">
                                <p style="color:#000000;">Copyright ©2022 <?php echo $companyInformation["company_name"] ;?> .Tüm Hakları Gizlidir. <br> Bu site bir <a target="_blank" href="https://www.insoftyazilim.com" style="color:#FF7B54 ">Insoft</a> E-Ticaret Paketi
                                    ürünüdür.</p>
                                </div>
                                <div class="copyright text-center text-md-right">
                                <img class="space-the-top" style="max-width:300px;margin-top:-50px" src="<?php echo publicUrl("img/footer_card.jpg"); ?>" alt="">
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        </footer>
        <!-- footer end -->

  
  <!-- Fullscreen search -->
  <div class="search-wrap">
  <div class="search-inner">
      <i class="fas fa-times search-close" id="search-close"></i>
      <div class="search-cell">
        <div class="search-field-holder">
            <input autocomplete="off" type="search" class="main-search-input" id="search_text" placeholder="ürün, marka veya kategori...">
            <div class="" id="search_results">

            </div>
        </div>
      </div>
  </div>
  </div> <!-- end fullscreen search -->
  <!-- sayfa mesajları için -->
  <!-- <div id="message-div" class="text-center">
    <div id="message" class="alert alert-info" role="alert">
      <strong>This is a dark alert—check it out!</strong>
    </div>
  </div>
  <?php if(!isset($_SESSION[sessionPrefix()."cookie_policy"]) || $_SESSION[sessionPrefix()."cookie_policy"] == "false"){ ?>
    <div class="alert alert-dark col-md-11" role="alert" id="cookie_policy">
        <div class="row">
          <div class="col-md-11">
            <small>
             
            </small>
          </div>
          <div class="col-md-1">
            <button type="button" class="btn" name="button" id="exit_cp">
              <i class="fa fa-times"></i>
            </button>
          </div>
      </div>
    </div> -->
  <?php } ?>

    <!-- JS here -->

    <script src="<?php echo publicUrl("js/popper.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/bootstrap.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/owl.carousel.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/isotope.pkgd.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/one-page-nav-min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/slick.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/jquery.meanmenu.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/ajax-form.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/wow.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/jquery.scrollUp.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/jquery.final-countdown.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/imagesloaded.pkgd.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/jquery.magnific-popup.min.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/plugins.js"); ?>"></script>
    <script src="<?php echo publicUrl("js/main2.js"); ?>"></script>
    
    <!-- Shopier JS -->
    <!--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/js/bootstrap.min.js"></script>
    <script src="https://s3.eu-central-1.amazonaws.com/shopier/framework.js"></script>
  -->


    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <script>
        $(document).ready(function(){
          showMessage("<?php echo $pageMessage; ?>");
        });
      </script>
    <?php } ?>
  </body>
</html>
<script>
  $(function() {
  $('.pop-up').hide();
  $('.pop-up').fadeIn(1000);
  
      $('.close-button').click(function (e) { 

      $('.pop-up').fadeOut(700);
      $('#overlay').removeClass('blur-in');
      $('#overlay').addClass('blur-out');
      e.stopPropagation();
        
    });
 });
</script>