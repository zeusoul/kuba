<style>
  .drop-dl ul {
    display: none;
  }

  .drop-dl ul li {
    list-style-type: none;
  }
</style>
<!doctype html>
<html class="no-js" lang="tr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php echo (isset($title) && trim($title) != "") ? "$title - ".$companyInformation["company_name"] : ((isset($companyInformation["company_name"])) ? $companyInformation["company_name"] : "Örnek Site Başlığı"); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="manifest" href="site.webmanifest">
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>">
        <!-- Place favicon.ico in the root directory -->

		<!-- CSS here -->
        <link rel="stylesheet" href="<?php echo publicUrl("css/bootstrap.min.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/owl.carousel.min.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/animate.min.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/magnific-popup.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/fontawesome-all.min.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/flaticon.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/meanmenu.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/meanmenu.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/slick.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/default.css"); ?> ">    
        <link rel="stylesheet" href="<?php echo publicUrl("css/style3.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/style2.css"); ?> ">
        <link rel="stylesheet" href="<?php echo publicUrl("css/responsive.css"); ?> ">
        
        <script src="<?php echo publicUrl("js/vendor/jquery-1.12.4.min.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/main.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/cart.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/search.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/mesaj_islemleri.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/tr.js"); ?>"></script>
        <script src="<?php echo publicUrl("js/favorite.js"); ?>"></script>

        </script>

    </head>
    <body>


        <!-- preloader -->
        <div id="preloader">
            <div class="preloader">
                <span></span>
                <span></span>
            </div>
        </div>
        <!-- preloader end  -->


        <!-- header start -->
<!-- header start -->
<header class="transparent-header transparent-header-2">
            <div class="header-area box-90">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-xl-1 col-lg-6 col-md-6 col-7 col-sm-3 d-flex align-items-center">
                            <div class="basic-bar cat-toggle">
                                <span class="bar1"></span>
                                <span class="bar2"></span>
                                <span class="bar3"></span>
                            </div>
                            <div class="logo">
                                <a href="<?php echo url(); ?>" >
                                  <?php if(isset($companyInformation["company_logo"]) && trim($companyInformation["company_logo"]) != ""){ ?>
                                    <img style="max-width:45% !important;" src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" >
                                  <?php } ?>
                                </a>
                            </div>
                            <div class="category-menu">
                                <h4 style="color:#fe906e;">Kategoriler</h4>
                                <ul class="submenu">
                                              <?php foreach ($categories as $category): ?>
                                                <li>
                                                    <a href="<?php echo url("kategori/".seoUrl($category["name"])."/".$category["category_id"]); ?>"><?php echo $category["name"]; ?></a>
                                                    <ul class="submenu  level-1">
                                                      <?php foreach ($categories["sub"] as $key => $subCategory): ?>
                                                        <li>
                                                            <a style="color:#fe906e;" href="<?php echo url("kategori/".$subCategory["name"]."/".$subCategory["id"]); ?>"><?php echo $subCategory["name"]; ?></a>
                                                        </li>
                                                      <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                              <?php endforeach; ?>
                                            </ul>

                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-6 col-md-8 col-8 d-none d-xl-block">
                            <div class="main-menu">
                                <nav id="mobile-menu">
                                    <ul>
                                        <li><a href="<?php echo url(); ?>">Anasayfa</a></li>
                                        <li>
                                            <a href="<?php echo url("hakkimizda"); ?>">Kurumsal</a>
                                            <ul class="submenu">
                                                <li>
                                                    <a href="<?php echo url("hakkimizda"); ?>">Hakkımızda</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo url("hakkimizda"); ?>">Misyonumuz ve Vizyonumuz</a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo url("iletisim"); ?>">İletişim</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="mega-menu">
                                            <a href="#">Tüm Kategoriler</a>
                                            <ul class="submenu">
                                              <?php foreach ($categories as $category): ?>
                                                <li>
                                                    <a href="<?php echo url("kategori/".seoUrl($category["name"])."/".$category["category_id"]); ?>"><?php echo $category["name"]; ?></a>
                                                    <ul class="submenu  level-1">
                                                      <?php foreach ($categories["sub"] as $key => $subCategory): ?>
                                                        <li>
                                                            <a href="<?php echo url("kategori/".$subCategory["name"]."/".$subCategory["id"]); ?>"><?php echo $subCategory["name"]; ?></a>
                                                        </li>
                                                      <?php endforeach; ?>
                                                    </ul>
                                                </li>
                                              <?php endforeach; ?>
                                            </ul>
                                        </li>
                                        <li><a href="<?php echo url("haber"); ?>">Kampanyalar</a></li>
                                        <li><a href="<?php echo url(); ?>">Çok Satanlar</a></li>
                                        <li><a href="<?php echo url(); ?>" style="color:red"> Outlet</a></li>
                                        <!-- <li>
                                          <a href="<?php echo url(""); ?>">  <?php foreach ($pages as $key => $page): ?>
                                            <?php if ($page["status"] == 1): ?>
                                              <li><a href="<?php echo url("sayfa/".seoUrl($page["menu_title"])."/".$page["page_id"]); ?>"><?php echo $page["menu_title"]; ?></a></li>
                                            <?php endif; ?>
                                            <?php endforeach; ?>
                                          </a>
                                        </li> -->
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-5 col-sm-9 pl-0">
                            <div class="header-right f-right">
                                <ul>
                                    <li class="search-btn">
                                      <a class="search-btn nav-search search-trigger" href="#"><i class="fas fa-search" style="padding-top: 15px;" ></i></a>
                                    </li>
                                    <!-- MEMBER -->
                                    <li class="login-btn">
                                      <?php if(!loginState()){ ?>
                                        <a href="<?php echo url("uyelik"); ?>"><i class="far fa-user" style="padding-top: 15px;" ></i></a></li>
                                      <?php } else{ ?>
                                        <li class="d-shop-cart"><a href="#"><i class="far fa-user" style="padding-top: 15px;" ></i></a>
                                            <ul class="minicart">
                                              <li>
                                                <div class="cart-content" style="margin-bottom:0 !important;">
                                                  <p style="margin:0 !important;"><?php echo $memberInfo["name"]." ".$memberInfo["surname"]; ?></p>
                                                </div>
                                              </li>
                                              <hr>
                                                <li>
                                                  <div class="cart-content">
                                                    <h3><a href="<?php echo url("profil/"); ?>">Hesap Bilgilerim</a></h3>
                                                    <h3><a href="<?php echo url("profil/siparislerim"); ?>">Siparişlerim</a></h3>
                                                    <h3><a href="<?php echo url("profil/adreslerim"); ?>">Adreslerim</a></h3>
                                                    <h3><a href="<?php echo url("profil/yorumlarim"); ?>">Yorumlarım</a></h3>
                                                    <h3><a href="<?php echo url("profil/iade-taleplerim"); ?>">İade Taleplerim</a></h3>
                                                    <h3><a href="<?php echo url("profil/mesajlarim"); ?>">Mesajlarım</a></h3>
                                                  </div>
                                                  <form class="text-center"  action="" method="post">
                                                    <button type="submit" name="sign_out" class="btn btn-outline-danger">Çıkış Yap</button>
                                                  </form>
                                                </li>
                                            </ul>
                                        </li>
                                      <?php } ?>
                                    </li>
                                    <!-- MEMBER -->
                                    <!-- CART -->
                                    <li class="d-shop-cart"  style="line-height: 3;">
                                      <a href="<?php echo url("sepet"); ?>">
                                        <i class="flaticon-shopping-cart" style="line-height: 3;"></i> <span class="cart-count"><?php echo $cartCount; ?></span>
                                      </a>
                                    </li>
                                    <!-- CART -->
                                </ul>
                            </div>
                        </div>
                        <div class="col-12 d-xl-none">
                            <div class="mobile-menu"></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header end -->

       
        <!-- header end -->
        <script>
    var selected = $('.drop-dl .current-lang img').attr('src');
    $('.lang-chose img').attr('src', selected);

    $('.lang-chose').click(function (event) {
      $('.drop-dl ul').slideToggle(500);
      event.preventDefault();
      return false;
    });

    $(document).bind('click', function (e) {
      var $clicked = $(e.target);
      if (!$clicked.parents().hasClass("drop-dl"))
        $(".drop-dl ul").hide();
    });
  </script>