
<!-- breadcrumb-area-start -->
<?php if (isset($bgImage)){ ?>
  <section class="breadcrumb-area" data-background="<?php echo publicUrl("img/$bgImage"); ?>">
<?php } else{ ?>
  <!-- <section class="breadcrumb-area" style="background: url(<?php echo publicUrl("img/background3.png"); ?>) no-repeat;background-position: top;"> -->
  <section class="breadcrumb-area" style="background: rgb(234 170 165 / 30%)">
<?php } ?>
    <div class="container">
      <div class="row">
        <div class="col-xl-12">
          <div class="breadcrumb-text text-center">
            <h1><?php echo $pageTitle; ?></h1>
            <ul class="breadcrumb-menu">
              <?php if(isset($map) && trim($map != "")){ ?>
                <?php echo $map; ?>
              <?php } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- breadcrumb-area-end -->

