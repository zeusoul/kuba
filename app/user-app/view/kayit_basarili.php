<div class="container">
	<div class="row text-center">
    <div class="col-sm-3 col-sm-offset-3"></div>
    <div class="col-sm-6 col-sm-offset-3">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <h3  class="mt-4" style="color:#0fad00">Kayıt Olduğunuz İçin Teşekkürler</h3>
    <img src="<?php echo publicUrl('img/success.png'); ?>" style="width:80px;height:80px;">
    <h5>Son Bir Adım Kaldı!</h5>
    <p style="font-size:18px;color:#5C5C5C;">
      Kaydınızı tamamlamak için son bir adım kaldı.<br>
      E-Mail adresinize bir doğrulama linki gönderdik.<br>
      Lütfen mailinizden doğrulama linkine tıklayarak hesabınızı doğrulayınız.
      <br> <hr>
      Link gelmedi mi?<a href="<?php echo url("kayit-basarili/$md5Email?sendLink"); ?>" class="btn btn-link"> Tekrar Gönder</a>
    </p>
    <a href="<?php echo url("uyelik"); ?>" class="btn theme-btn btn-success">Giriş Yap</a>
    <a href="<?php echo url(""); ?>" class="btn theme-btn-2 btn-dark">Ana Sayfa</a>
    <br><br>
    </div>
	</div>
</div>
