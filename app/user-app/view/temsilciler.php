
        <main>

        <!-- shop-area start -->
        <section class="shop-area pt-100 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="shop-banner mb-50">
                            <img src="img/bg/shop-banner.jpg" alt="">
                        </div>
                        <!-- tab filter -->
                        <div class="row mb-10">
                            <!-- toplam veri sayısı buraya gelecek -->
                            <!-- <div class="col-xl-5 col-lg-6 col-md-6">
                                <div class="product-showing mb-40">
                                    <p>Showing 1–22 of 32 results</p>
                                </div>
                            </div> -->
                            <div class="col-xl-5 col-lg-6 col-md-6">
                                <div class="product-showing mb-40">
                                    <p>Temsilcilerimiz Ve Bilgileri</p>
                                </div>
                            </div>
                            <div class="col-xl-7 col-lg-6 col-md-6">
                                <div class="shop-tab f-right">
                                    <ul class="nav text-center" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
                                                aria-selected="true"><i class="fas fa-list-ul"></i> </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
                                                aria-selected="false"><i class="fas fa-th-large"></i></a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- tab content -->
                        <div class="tab-content" id="myTabContent">
                            <!-- gorunum1 -->
                            <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="product-wrapper mb-50">
                                            <div class="product-img mb-25">
                                                <a href="product-details.html">
                                                    <img src="img/product/pro13.jpg" alt="">
                                                    <img class="secondary-img" src="img/product/pro14.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="product-content">
                                                <div class="pro-cat mb-10">
                                                    <a href="shop.html">decor, </a>
                                                    <a href="shop.html">furniture</a>
                                                </div>
                                                <h4>
                                                    <a href="product-details.html">Minimal Troma Furniture</a>
                                                </h4>
                                                <div class="product-meta">
                                                    <div class="pro-price">
                                                        <span>$119.00 USD</span>
                                                        <span class="old-price">$230.00 USD</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <!-- gorunum1 -->
                            <!-- gorunum2 -->
                            <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="row">
                                    <div class="col-xl-5">
                                        <div class="product-wrapper mb-30">
                                            <div class="product-img">
                                                <a href="product-details.html">
                                                    <img src="img/product/pro13.jpg" alt="">
                                                    <img class="secondary-img" src="img/product/pro14.jpg" alt="">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-7">
                                        <div class="product-content pro-list-content pr-0 mb-50">
                                            <h4>
                                                <a href="product-details.html">Minimal Troma Furniture</a>
                                            </h4>
                                            <div class="product-meta mb-10">
                                                <div class="pro-price">
                                                    <span>$119.00 USD</span>
                                                    <span class="old-price">$230.00 USD</span>
                                                </div>
                                            </div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- gorunum2 -->
                        </div>
                        <!-- sayfalandırma -->
                        <!-- <div class="basic-pagination basic-pagination-2 text-center mt-20">
                            <ul>
                                <li><a href="#"><i class="fas fa-angle-double-left"></i></a></li>
                                <li><a href="#">01</a></li>
                                <li class="active"><a href="#">02</a></li>
                                <li><a href="#">03</a></li>
                                <li><a href="#"><i class="fas fa-ellipsis-h"></i></a></li>
                                <li><a href="#"><i class="fas fa-angle-double-right"></i></a></li>
                            </ul>
                        </div> -->
                    </div>
                    <div class="col-xl-4 col-lg-4">
                        <div class="sidebar-box">
                            <div class="shop-widget">
                                <h3 class="shop-title">Markalar</h3>
                                <ul class="shop-link">
                                    <li><a href="shop.html"> Apex</a></li>
                                    <li><a href="shop.html"> Bata</a></li>
                                    <li><a href="shop.html"> Puma</a></li>
                                    <li><a href="shop.html"> Nike</a></li>
                                    <li><a href="shop.html"> Likoda</a></li>
                                    <li><a href="shop.html"> Piolaba</a></li>
                                </ul>
                            </div>

                            <div class="shop-widget">
                                <h3 class="shop-title">Kategoriler</h3>
                                <ul class="shop-link">
                                    <li><a href="shop.html"> Accessories</a></li>
                                    <li><a href="shop.html"> Bags</a></li>
                                    <li><a href="shop.html"> Clothing</a></li>
                                    <li><a href="shop.html"> Shoes</a></li>
                                    <li><a href="shop.html"> Exclusive</a></li>
                                    <li><a href="shop.html"> Uncategorized</a></li>
                                    <li><a href="shop.html"> Women</a></li>
                                </ul>
                            </div>

                            <div class="shop-widget">
                                <h3 class="shop-title">Taglar</h3>
                                <ul class="shop-tag">
                                    <li><a href="shop.html"> Minimal</a></li>
                                    <li><a href="shop.html"> T-Shirts</a></li>
                                    <li><a href="shop.html"> Pants</a></li>
                                    <li><a href="shop.html"> Jeants</a></li>
                                    <li><a href="shop.html"> Winter</a></li>
                                    <li><a href="shop.html"> Latest</a></li>
                                    <li><a href="shop.html"> New</a></li>
                                    <li><a href="shop.html"> Sale</a></li>
                                </ul>
                            </div>
                            <div class="shop-widget">
                                <h3 class="shop-title">Son Eklenen  Ürünler</h3>
                                <ul class="shop-sidebar-product">
                                    <li>
                                        <div class="side-pro-img">
                                            <a href="product-details.html"><img src="img/product/latest/shop-rsp1.jpg" alt=""></a>
                                        </div>
                                        <div class="side-pro-content">
                                            <div class="side-pro-rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <h5><a href="product-details.html">Raglan Baseball-Style</a></h5>
                                            <div class="side-pro-price">
                                                <span>$119.00 USD</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-pro-img">
                                            <a href="product-details.html"><img src="img/product/latest/shop-rsp3.jpg" alt=""></a>
                                        </div>
                                        <div class="side-pro-content">
                                            <div class="side-pro-rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <h5><a href="product-details.html">Raglan Baseball-Style</a></h5>
                                            <div class="side-pro-price">
                                                <span>$119.00 USD</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-pro-img">
                                            <a href="product-details.html"><img src="img/product/latest/shop-rsp2.jpg" alt=""></a>
                                        </div>
                                        <div class="side-pro-content">
                                            <div class="side-pro-rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <h5><a href="product-details.html">Raglan Baseball-Style</a></h5>
                                            <div class="side-pro-price">
                                                <span>$119.00 USD</span>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="side-pro-img">
                                            <a href="product-details.html"><img src="img/product/latest/shop-rsp4.jpg" alt=""></a>
                                        </div>
                                        <div class="side-pro-content">
                                            <div class="side-pro-rating">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                            </div>
                                            <h5><a href="product-details.html">Raglan Baseball-Style</a></h5>
                                            <div class="side-pro-price">
                                                <span>$119.00 USD</span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <div class="shop-widget">
                                <div class="shop-sidebar-banner">
                                    <a href="shop.html"><img src="img/banner/shop-banner.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- shop-area end -->


        </main>
    </body>
</html>
