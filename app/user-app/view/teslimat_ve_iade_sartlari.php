
<main>
  <!-- shop-area start -->
    <section class="blog-area pt-100 pb-60">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <article class="postbox post format-image mb-40">
                        <!-- <div class="postbox__thumb">
                            <a href="#">
                                <img src="img/blog/b10.jpg" alt="blog image">
                            </a>
                        </div> -->
                        <div class="postbox__text p-30">
                            <div class="post-meta mb-15">
                                <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                            </div>
                            <h3 class="blog-title blog-title-sm">
                                <a href="#">İADE ŞARTLARI VE KURALLARI </a>
                            </h3>
                            <div class="post-text">
                                <p>Satın almış olduğunuz ürünleri fatura tarihinden itibaren 30&nbsp;gün içinde iade edebilirsiniz. İade edeceğiniz ürünün orjinal koli ve ambalajı bozulmamış olmalıdır. Ambalajı olmayan veya bozulmuş olan ürünler kesinlikle iade alınmamaktadır. İade etmek istediğiniz ürün ile birlikte sevk irsaliyesini, garanti belgesini göndermelisiniz. İade etmek istediğiniz ürün ayıplı ise kargo ücreti tarafımızdan karşılanmaktadır. Diğer durumlarda ise kargo ücreti müşteriye aittir. Niteliği itibariyle iade edilemeyecek ürünler (Örn: Kullanım esnasında vücutla birebir temas gerektiren ürünler, küpe, kulaklık, iç giyim, bikini), tek kullanımlık ürünlerin iadesi mümkün değildir. DVD, VCD, CD, kaset, taşınabilir bilgisayar, cep telefonu, sarf malzemeleri (toner, kartuş vb.), kozmetik ürünleri gibi ürünlerin iade alınabilmesi için ambalajının ve/veya koruma bandının kesinlikle açılmamış olması gerekmektedir. Eğer ürün şirket adına fatura edilmiş ise ilgili şirket tarafından ürün için iade faturası düzenlenmelidir.</p>
                                <span><strong>Ürün İade Adresi:</strong></span><br>
                                <em><?php echo $companyInformation["company_name"]; ?> </em><br>
                                <em><?php echo $companyInformation["company_address"]; ?></em><br>
                            </div>
                        </div>
                    </article>
                    <article class="postbox post format-image mb-40" >
                        <div class="postbox__text p-30">
                            <div class="post-meta mb-15">
                                <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                            </div>
                            <h3 class="blog-title blog-title-sm">
                                <a href="#">TESLİMAT ŞARTLARI
                                    </a>
                            </h3>
                            <div class="post-text">
                                <p>Ürünleriniz stok durumuna göre siparişiniz onaylandığında tedarik edilmekte ve sonrasında kargoya verilmektedir. Siparişinizin onaylanabilmesi için satın almış olduğunuz ürünlerin ücretinin kredi kartı veya havale ile ödenmesi gerekmektedir. Onaylanan siparişler 2-7 iş günü içerisinde kargoya teslim edilecektir.</p>
                            </div><br><br><hr>
                            <h3 class="blog-title blog-title-sm">
                                <a href="#">TESLİMAT SIRASINDA İBRAZ EDİLMESİ GEREKEN BELGELER</a>
                            </h3>
                            <div class="post-text">
                                <p>Bilgi Teknolojileri ve İletișim Kurulu’nun 27/12/2016 tarih ve 2016/DK-YED/517 sayılı kararı ile onaylanan Posta Gönderilerine İlișkin Güvenlik Tedbirlerine Yönelik Usul ve Esaslara istinaden gönderi kabulünde uygulanacak yeni kural gereği ürünlerinizi kargo görevlisinden ya da mağaza personelinden alırken ehliyet, nufüs cüzdanı veya pasaportunuzu göstermeniz gerekmektedir.</p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </section>
  <!-- shop-area end -->
</main>              