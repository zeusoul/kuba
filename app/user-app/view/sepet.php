<main>
<!-- Cart Area Strat-->
<section class="cart-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-12">
                    <div class="table-content table-responsive">
                      <table class="table">
                        <?php if(count($cart) == 0) { ?>
                            <div class="col-12 col-sm-12 col-md-12 text-center">
                              <p>Sepetinizde ürün bulunmamaktadır</p>
                              <a class="btn btn-success" href="<?php echo url("kategori"); ?>">Kategori</a>
                              <a class="btn btn-dark" href="<?php echo url(); ?>">Ana Sayfa</a>
                            </div>
                        <?php } else { ?>
                          <thead>
                              <tr>
                                  <th class="product-thumbnail">Resim</th>
                                  <th class="cart-product-name">Ürün Başlığı</th>
                                  <th class="product-price">Ürün Varyantı</th>
                                  <th class="product-price">Fiyat</th>
                                  <th class="product-quantity">Adet</th>
                                  <th class="product-subtotal">Toplam Fiyat</th>
                                  <th class="product-remove">Sil</th>
                              </tr>
                          </thead>
                          <tbody>
                          <?php foreach ($cart as $key => $product): ?>
                            <?php
                              if($product["currency"] == "TL") $product["currency"] = "₺";
                              else if($product["currency"] == "USD") $product["currency"] = "$";
                              else if($product["currency"] == "EURO") $product["currency"] = "€";
                              else{
                              }
                            ?>
                              <tr>
                                  <td class="product-thumbnail">
                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>" target="_blank">
                                      <img width="100" src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="Uren's Cart Thumbnail">
                                    </a>
                                  </td>
                                  <td class="cart-product-name">
                                    <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>" target="_blank"><?php echo $product["title"]; ?></a>
                                  </td>
                                  <td class="product-price"><?php echo $product["combine"]; ?></td>
                                  <td class="product-price"><span class="amount"><?php echo $product["unit_price"]." ".$product["currency"]; ?></span></td>
                                  <td class="product-quantity">1</td>
                                  <td class="product-subtotal"><span class="amount"><?php echo $product["total_price"]." ".$product["currency"]; ?></span></td>
                                  <td class="product-remove">
                                    <form class="" action="" method="post">
                                      <input type="hidden" name="barcode" value="<?php echo $product["barcode"]; ?>">
                                      <button type="submit" name="delete" class="btn btn-outline-danger btn-xs">
                                        <i class="fa fa-trash" title="Remove"></i>
                                      </button>
                                    </form>
                                  </td>
                                </tr>
                            <?php endforeach; ?>
                          <?php } ?>
                        </tbody>
                      </table>
                    </div>
                    <?php if(count($cart) > 0) { ?>
                      <div class="row">
                          <div class="col-md-5 ml-auto">
                              <div class="cart-page-total">
                                  <h2>Sipariş Özeti</h2>
                                  <ul><li>Toplam Ürün <span><?php echo $cartCount; ?></span></li></ul>
                                  <ul><li>Toplam Fiyat<span><?php echo $cartInfo["toplam_fiyat"]; ?> ₺</span></li></ul>
                                  <ul><li>KDV Dahil Fiyat <span><?php echo $cartInfo["kdv_dahil_fiyat"]; ?> ₺</span></li></ul>
                                  <ul><li>Ödenecek Tutar <span><?php echo $cartInfo["odenecek_tutar"]; ?> TL</span></li></ul>
                                  <form class="" action="<?php echo url("odeme"); ?>" method="post">
                                    <div>
                                      <input type="checkbox" name="satis_sozlesmesi" id="" class="mr-1" required="">
                                      <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">Mesafeli Satış Sözleşmesi</a>
                                        'ni <b>okudum ve onaylıyorum</b>.
                                    </div>
                                    <input type="submit" href="" class="btn theme-btn-2 mt-2" value="Siparişi Tamamla"></input>
                                  </form>
                              </div>
                          </div>
                      </div>
                    <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- Cart Area End-->


</main>
