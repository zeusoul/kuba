

<main class="page-content">
        <!-- Begin Uren's Shop Left Sidebar Area -->
        <div class="account-page-area">
            <div class="container-fluid">
                <div class="row">
                  <?php include staticUrl("sidebar.php"); ?>
                    <div class="col-lg-9 col-md-7 order-1 order-lg-2 order-md-2">
                      <h2 class="h3 mb-3 text-black">Favorilerim  </h2>
                        <div class="shop-product-wrap grid gridview-3 img-hover-effect_area row">
                          <?php foreach ($favorites as $key => $favorite) { ?>
                            <?php $product = $favorite["product_details"]; ?>
                            <!-- ÜRÜN -->
                            <div class="col-lg-4">
                                <div class="product-slide_item">
                                    <div class="inner-slide">
                                        <div class="single-product">
                                            <div class="product-img">
                                                <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">
                                                    <img class="primary-img" src="<?php echo publicUrl('img/product-images/'.$product["image"]); ?>">
                                                    <?php if (count($product["product_images"]) > 0) { ?>

                                                      <img class="secondary-img" src="<?php echo publicUrl('img/product-images/'.$product["product_images"][0]["product_image"]); ?>">
                                                    <?php } ?>
                                                </a>
                                                <div class="add-actions">
                                                    <ul>
                                                        <li><a onclick="addToCart('<?php echo $product["variants"][0]["barcode"]; ?>','kategori')" class="uren-add_cart" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Sepete Ekle"><i
                                                            class="ion-bag"></i></a>
                                                        </li>
                                                        <li><a onclick="addToFavorite('<?php echo $product["product_id"]; ?>')" class="uren-wishlist" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Favorilere Ekle/Çıkar">
                                                          <i class="ion-android-favorite-outline" id="favorite-<?php echo $product["product_id"]; ?>" style="color:red;"></i></a>
                                                        </li>
                                                        <li class="quick-view-btn" data-toggle="modal" data-target="#exampleModalCenter"><a href="javascript:void(0)" data-toggle="tooltip" data-placement="top" title="Quick View"><i
                                                            class="ion-android-open"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-desc_info">
                                                  <div class="rating-box">
                                                    <ul>
                                                      <?php for ($i=0; $i <5 ; $i++) { ?>
                                                        <?php if ($i+1 <= ceil((float)$product["rating"])) { ?>
                                                        <li><i class="ion-android-star"></i></li>
                                                        <?php } else{ ?>
                                                          <li class="silver-color"><i class="ion-android-star"></i></li>
                                                        <?php } ?>
                                                      <?php } ?>
                                                    </ul>
                                                  </div>
                                                    <h6><a class="product-name" href="<?php echo url("urun-detay"); ?>">
                                                      <?php echo $product["title"]; ?>
                                                    </a></h6>
                                                    <div class="price-box">
                                                      <?php if((float)$product["variants"][0]["discount_price"] < (float)$product["variants"][0]["price"]){ ?>
                                                        <p class="price mb-1">
                                                          <span class="badge badge-danger">%<?php echo $product["variants"][0]["rate"]; ?></span>
                                                          <span class="new-price">
                                                            <?php echo $product["variants"][0]["currency"].$product["variants"][0]["discount_price"]; ?>
                                                          </span>
                                                          <p style="text-decoration: line-through;">
                                                            <?php echo $product["variants"][0]["currency"].$product["variants"][0]["price"]; ?>
                                                          </p>
                                                        </p>
                                                      <?php } else if((float)$product["variants"][0]["price"] > 0) {?>
                                                        <span class="new-price"><?php echo $product["variants"][0]["price"]." ".$product["variants"][0]["currency"]; ?></span>
                                                      <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- ÜRÜN -->
                          <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Uren's Shop Left Sidebar Area End Here -->
      </main>
