<main>
    <!-- shop-area start -->
        <section class="blog-area pt-100 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                  <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#"> Çerez Politikası </a>
                                </h3>
                                <div class="post-text">
                                <p>
                                  Çerez Politikamız,
                                  <a href="<?php echo url("sozlesme/gizlilik-politikasi"); ?>">Gizlilik Politikamızın</a>
                                  bir parçasını oluşturur.
                                </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                              <div class="post-meta mb-15">
                                <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                              </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">Çerez (Cookie) Nedir? </a>
                                </h3>
                                <div class="post-text">
                                  <p>
                                    Günümüzde neredeyse her web sitesi çerez kullanmaktadır. Size daha iyi, hızlı ve güvenli bir deneyim sağlamak için, çoğu internet sitesi gibi biz de çerezler kullanıyoruz. Çerez, bir web sitesini ziyaret ettiğinizde cihazınıza (örneğin; bilgisayar veya cep telefonu) depolanan küçük bir metin dosyasıdır. Çerezler, bir web sitesini ilk ziyaretiniz sırasında tarayıcınız aracılığıyla cihazınıza depolanabilirler. Aynı siteyi aynı cihazla tekrar ziyaret ettiğinizde tarayıcınız cihazınızda site adına kayıtlı bir çerez olup olmadığını kontrol eder. Eğer kayıt var ise, kaydın içindeki veriyi ziyaret etmekte olduğunuz web sitesine iletir. Bu sayede web sitesi, sizin siteyi daha önce ziyaret ettiğinizi anlar ve size iletilecek içeriği de ona göre tayin eder.
                                  </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                  <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                              </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">Çerezler Neden Kullanılır? </a>
                                </h3>
                                <div class="post-text">
                                <p>
                                  Bazı çerezler, daha önceki ziyaretlerinizde kullandığınız tercihlerin web sitesi tarafından hatırlanmasını sağlayarak, sonraki ziyaretlerinizin çok daha kullanıcı dostu ve kişiselleştirilmiş bir deneyim sunmasını sağlar.
                                  <br>
                                  Ayrıca, web sitesinde bulunan üçüncü taraflara ait linkler, bu üçüncü taraflara ait gizlilik politikalarına tabi olmakla birlikte, gizlilik uygulamalarına ait sorumluluk ornekalanadi.com’a ait olmamaktadır ve bu bağlamda ilgili link kapsamındaki site ziyaret edildiğinde siteye ait gizlilik politikasının okunması önerilmektedir.
                                </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                              <div class="post-meta mb-15">
                                  <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                              </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">Çerez Türleri </a>
                                </h3>
                                <div class="post-text">
                                  <p>
                                    <ul>
                                      Ana kullanım amacı kullanıcılara kolaylık sağlamak olan çerezler, temel olarak 4 ana grupta toplanmaktadır:
                                      <li>
                                        <b>Oturum Çerezleri:</b> Internet sayfaları arasında bilgi taşınması ve kullanıcı tarafından girilen bilgilerin sistemsel olarak hatırlanması gibi çeşitli özelliklerden faydalanmaya olanak sağlayan çerezlerdir ve internet sitesine ait fonksiyonların düzgün bir şekilde işleyebilmesi için gereklidir.
                                      </li>
                                      <li>
                                        <b>Performans Çerezleri:</b> Sayfaların ziyaret edilme frekansı, olası hata iletileri, kullanıcıların ilgili sayfada harcadıkları toplam zaman ile birlikte siteyi kullanım desenleri konularında bilgi toplayan çerezlerdir ve internet sitesinin performansını arttırma amacıyla kullanılmaktadır.
                                      </li>
                                      <li>
                                        <b>Fonksiyonel Çerezler:</b> Kullanıcıya kolaylık sağlanması amacıyla önceden seçili olan seçeneklerin hatırlatılmasını sağlayan çerezlerdir ve internet sitesi kapsamında kullanıcılara gelişmiş Internet özellikleri sağlanmasını hedeflemektedir.
                                      </li>
                                      <li>
                                        <b>Reklam Ve Üçüncü Taraf Çerezleri:</b> Üçüncü parti tedarikçilere ait çerezlerdir ve internet sitesindeki bazı fonksiyonların kullanımına ve reklam takibinin yapılmasına olanak sağlamaktadır.
                                      </li>
                                    </ul>
                                  </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">Çerezlerin Kullanım Amaçları </a>
                                </h3>
                                <div class="post-text">
                                  <p>
                                  <ul>
                                    <?php echo url(); ?> tarafından kullanılmakta olan çerezlere ait kullanım amaçları aşağıdaki gibidir:
                                    <li>
                                      <b>Güvenlik amaçlı kullanımlar: </b> ornekalanadi.com, sistemlerinin idaresi ve güvenliğinin sağlanması amacıyla, bu sitedeki fonksiyonlardan yararlanmayı sağlayan veyahut düzensiz davranışları tespit eden çerezler kullanabilmektedir.
                                    </li>
                                    <li>
                                      <b>İşlevselliğe yönelik kullanımlar: </b> ornekalanadi.com, sistemlerinin kullanımını kolaylaştırmak ve kullanıcı özelinde kullanım özellikleri sağlamak amacıyla, kullanıcıların bilgilerini ve geçmiş seçimlerini hatırlatan çerezler kullanabilmektedir.
                                    </li>
                                    <li>
                                      <b>Performansa yönelik kullanımlar: </b> ornekalanadi.com, sistemlerinin performansının artırılması ve ölçülmesi amacıyla, gönderilen iletilerle olan etkileşimi ve kullanıcı davranışlarını değerlendiren ve analiz eden çerezler kullanabilmektedir.
                                    </li>
                                    <li>
                                      <b>Reklam amaçlı kullanımlar: </b> ornekalanadi.com, kendine veya üçüncü taraflara ait sistemlerin üzerinden kullanıcıların ilgi alanları kapsamında reklam ve benzeri içeriklerin iletilmesi amacıyla, bu reklamların etkinliğini ölçen veya tıklanma durumunu analiz eden çerezler kullanabilmektedir.
                                    </li>
                                  </ul>
                                  </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">Çerezleri Kontrol Etme ve Silme </a>
                                </h3>
                                <div class="post-text">
                                  <p>
                                    Çerezlerin kullanımına ilişkin tercihlerinizi değiştirmek ya da çerezleri engellemek veya silmek için tarayıcınızın ayarlarını değiştirmeniz yeterlidir. Birçok tarayıcı çerezleri kontrol edebilmeniz için size çerezleri kabul etme veya reddetme, yalnızca belirli türdeki çerezleri kabul etme ya da bir web sitesi cihazınıza çerez depolamayı talep ettiğinde tarayıcı tarafından uyarılma seçeneği sunar. Aynı zamanda daha önce tarayıcınıza kaydedilmiş çerezlerin silinmesi de mümkündür. Çerezleri kontrol edilmesine veya silinmesine ilişkin işlemler kullandığınız tarayıcıya göre değişebilmektedir. Bazı popüler tarayıcıların çerezlere izin verme ya da çerezleri engelleme veya silme talimatlarına aşağıdaki linklerden ulaşılması mümkündür.
                                    <br>
                                    Çerez kullanım seçiminin değiştirilmesine ait yöntem, tarayıcı tipine bağlı olarak değişmekte olup, ilgili hizmet sağlayıcıdan dilendiği zaman öğrenilebilmektedir.
                                    <br>
                                    Bu politikanın en son güncellendiği tarih: 26/08/2020
                                  </p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
</main>