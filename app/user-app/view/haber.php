        <main>
        <!-- blog-area start -->
        <section class="blog-area pt-120 pb-80">
            <div class="container">
                <div class="row blog-masonry">
                    <!-- haber -->
                    <?php if(count($news) == 0){ ?>
                        Kayıtlı haber bulunamadı
                    <?php } else{ ?>
                    <?php foreach($news as $key => $new){ ?>
                    <?php
                        if($key < $offset) {continue;}
                        else if($key >= $limit) {break;}
                    ?>
                    <div class="col-lg-4 col-md-6 grid-item">
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__thumb">
                                <a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>">
                                    <img src="<?php echo publicUrl('img/new-images/'.$new["new_image"]) ?>" >
                                </a>
                            </div>
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </span><br>
                                    <span><a href="<?php echo url("") ?>"><i class="far fa-calendar"></i> <?php echo $new["new_date"]; ?></a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>"><?php echo $new["new_title"]; ?>
                                        .</a>
                                </h3>
                                <div class="post-text">
                                    <p> <?php echo kisalt($new["new_desc"],500); ?></p>
                                </div>
                                <div class="read-more">
                                    <a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>" class="read-more">Devamını Oku<i class="flaticon-right-arrow"></i></a>
                                </div>
                            </div>
                        </article>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <!-- haber -->
                </div>
                <!-- <div class="row">
                    <div class="col-12">
                        <div class="basic-pagination basic-pagination-2 text-center mb-40">
                            <ul>
                                <li><a href="<?php echo url("") ?>"><i class="fas fa-angle-double-left"></i></a></li>
                                <li><a href="<?php echo url("") ?>">01</a></li>
                                <li class="active"><a href="<?php echo url("") ?>">02</a></li>
                                <li><a href="<?php echo url("") ?>">03</a></li>
                                <li><a href="<?php echo url("") ?>"><i class="fas fa-ellipsis-h"></i></a></li>
                                <li><a href="<?php echo url("") ?>"><i class="fas fa-angle-double-right"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>
        </section>
        <!-- blog-area end -->


        </main>
    </body>
</html>
