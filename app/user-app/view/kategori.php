<main>
  <!-- shop-area start -->
  <section class="shop-area pt-100 pb-100">
      <div class="container">
          <div class="row">
              <div class="col-xl-8 col-lg-8">
                  <!-- tab filter -->
                  <div class="row mb-10">
                      <div class="col-xl-4 col-lg-4 col-md-6">
                          <div class="product-showing mb-40">
                              <p>
                                <?php
                                  if ($categoryId == 0) echo "Tüm Ürünler";
                                  else echo $categoryDetails["name"];
                                ?>
                              </p>
                          </div>
                      </div>
                  </div>
                  <!-- tab content -->
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="row">
                            <?php foreach ($products as $key => $product): ?>
                              <?php if($key == 10) break; ?>
                              <div class="col-lg-6 col-md-6">
                                  <div class="product-wrapper mb-50">
                                      <div class="product-img mb-25">
                                          <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>">
                                              <img src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                              <img class="secondary-img" src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                          </a>
                                          <div class="product-action text-center">
                                              <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>" title="İncele"><i class="flaticon-eye"></i></a>
                                          </div>
                                      </div>
                                      <div class="product-content">
                                          <div class="pro-cat mb-10">
                                            <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["model_name"]; ?></a>
                                          </div>
                                          <h4>
                                              <a href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                          </h4>
                                          <div class="product-meta">
                                              <div class="pro-price">
                                                  <span><?php echo $product["variants"][0]["currency"]." ".$product["variants"][0]["discount_price"]; ?></span>
                                                  <span class="old-price"><?php echo  $product["variants"][0]["currency"]." ".$product["variants"][0]["price"]; ?></span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                </div>
                            <?php endforeach; ?>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-xl-4 col-lg-4 p-0">
                  <div class="sidebar-box p-0">
                    <form class="basic-login p-5" action="" method="get">
                      <?php if (count($subCategories) > 0): ?>
                        <div class="shop-widget">
                          <h3 class="shop-title">Alt Kategoriler</h3>
                          <ul class="shop-link">
                            <?php foreach ($subCategories as $subCategory): ?>
                              <li>
                                <a href="<?php echo url("kategori/".seoUrl($subCategory["name"])."/".$subCategory["category_id"]); ?>">
                                  <?php echo $subCategory['name'];?>
                                </a>
                              </li>
                            <?php endforeach;?>
                          </ul>
                        </div>
                      <?php endif; ?>

                        <div class="shop-widget">
                            <h3 class="shop-title">Sıralama</h3>
                            <div class="price-filter">
                                <div class="price-slider-amount row">
                                  <select class="w-100" name="ranking" style="padding:20px !important;">
                                    <?php
                                      $selected = array("","","","","");
                                      if(isset($_GET["ranking"])) $selected[(int)$_GET["ranking"]] = "selected";
                                    ?>
                                    <option value="0" <?php echo $selected[0]; ?>>Akıllı Sıralama</option>
                                    <option value="1" <?php echo $selected[1]; ?>>Önce En Yüksek Fiyat</option>
                                    <option value="2" <?php echo $selected[2]; ?>>Önce En Düşük Fiyat</option>
                                    <option value="3" <?php echo $selected[3]; ?>>Önce En Yüksek Değerlendirme</option>
                                    <option value="4" <?php echo $selected[4]; ?>>Önce En Düşük Değerlendirme</option>
                                  </select>
                                </div>
                            </div>
                        </div>

                        <div class="shop-widget">
                            <h3 class="shop-title">Fiyat Aralığı</h3>
                            <div class="price-filter">
                                <div class="price-slider-amount row">
                                  <div class="col-6">
                                    <input type="text" name="min" value="<?php echo trim($_GET["min"]); ?>" class="" placeholder="En az">
                                  </div>
                                  <div class="col-6">
                                    <input type="text" name="max" value="<?php echo trim($_GET["max"]); ?>" class="form-control" placeholder="En çok">
                                  </div>
                                </div>
                            </div>
                        </div>

                        <?php if(isset($filters) && is_array($filters)){ ?>
                          <?php  foreach ($filters as $filter):?>
                            <div class="shop-widget">
                                <h3 class="shop-title"><?php echo $filter['name'];?></h3>
                                <div class="shop-link">
                                  <div class="custom-control custom-radio">
                                    <input checked type="radio" id="all-<?php echo $value['filter_value_id'];?>" name="filters[<?php echo $filter["filter_id"]; ?>]" class="custom-control-input" value="0">
                                    <label class="custom-control-label" for="all-<?php echo $value["filter_value_id"]; ?>">
                                      <span>Hepsi</span>
                                    </label>
                                  </div>
                                  <?php foreach ($filter["values"] as $value):
                                    $check = "";
                                    $filterId = (int)$filter["filter_id"];
                                    $valueId = $value["filter_value_id"];
                                    if($valueId == $getFilters["filters"][$filterId]) $check = "checked";
                                  ?>
                                    <div class="custom-control custom-radio">
                                      <input <?php echo $check; ?> type="radio" id="<?php echo $value['filter_value_id'];?>" name="filters[<?php echo $filter["filter_id"]; ?>]" class="custom-control-input" value="<?php echo $value['filter_value_id'];?>">
                                      <label class="custom-control-label" for="<?php echo $value["filter_value_id"]; ?>">
                                        <span> <?php echo $value['value'];?></span>
                                      </label>
                                    </div>
                                  <?php endforeach;?>
                                </div>
                            </div>
                          <?php endforeach; ?>
                        <?php } ?>

                        <div class="shop-widget">
                            <h3 class="shop-title">Ürün Puanı</h3>
                            <div class="shop-link">
                              <div class="custom-control custom-radio">
                                <input type="radio" id="customRadio0" name="rating" value="0" class="custom-control-input" checked>
                                <label class="custom-control-label" for="customRadio0">
                                  Hepsi
                                </label>
                              </div>
                              <?php for ($i=5; $i > 0 ; $i--) { ?>
                                <div class="custom-control custom-radio">
                                  <input type="radio" id="customRadio<?php echo $i; ?>" name="rating" value="<?php echo $i; ?>" class="custom-control-input" <?php if(isset($_GET["rating"]) && $_GET["rating"] == $i) echo "checked"; ?>>
                                  <label class="custom-control-label" for="customRadio<?php echo $i; ?>"></label>
                                    <div class="rating-box">
                                      <div class="side-pro-rating">
                                        <?php for ($j=0; $j < 5; $j++) { ?>
                                          <?php if($j<$i) { ?>
                                            <i class="fas fa-star"></i>
                                          <?php } ?>
                                        <?php } ?>
                                      </div>
                                  </div>
                                </div>
                              <?php } ?>
                            </div>
                        </div>

                        <div class="shop-widget">
                            <div class="shop-link">
                              <div class="custom-control custom-radio">
                                <button type="submit" class="btn theme-btn-2 w-100" name="filter">UYGULA</button>
                              </div>
                            </div>
                        </div>
                    </form>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- shop-area end -->
</main>
