    <div class="col-md-9 mt-3 mb-3">
      <h3 class=" text-center"><?php echo $messages["name_surname"]; ?>Mesajlaşma</h3>
      <div class="messaging">
        <div class="inbox_msg">
          <div class="mesgs" style="width:100%;">
            <div class="msg_history" id="message_history">
            <?php foreach ($messages as $key => $message) { ?>
              <?php if((int)$message["sender_id"] != -1) { ?>
                <div class="outgoing_msg">
                  <div class="sent_msg">
                    <span class="time_date"> <strong>Siz : </strong></span>
                    <p><?php echo $message["content"]; ?></p>
                    <span class="time_date"><?php echo $message["posting_date"]; ?></span>
                  </div>
                </div>
              <?php  ?>
            <?php } else { ?>
              <div class="incoming_msg">
                <div class="incoming_msg_img">
                  <img src="<?php echo publicUrl("img/user.png"); ?>" alt="sunil">
                </div>
                <div class="received_msg">
                  <span class="time_date">
                    <strong><?php echo $message["name_surname"]; ?> :</strong>
                  </span>
                  <div class="received_withd_msg">
                    <p><?php echo $message["content"]; ?></p>
                    <span class="time_date"><?php echo $message["posting_date"]; ?></span>
                  </div>
                </div>
              </div>
            <?php } ?>
          <?php } ?>
            </div>
            <div class="type_msg">
              <div class="input_msg_write">
                <input type="text" id="message" autocomplete="off" class="write_msg" name="message" placeholder="Mesajınızı yazınız" required/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>

<script type="text/javascript" src="<?php echo publicUrl("js/mesaj_islemleri.js"); ?>"></script>
