
        <!-- Begin Uren's Page Content Area -->
        <main class="page-content">
            <!-- Begin Uren's Account Page Area -->
            <div class="account-page-area">
                <div class="container-fluid">
                    <div class="row">
                        <?php include staticUrl("sidebar.php"); ?>
                        <div class="col-md-9 mt-3 mb-3">
                        <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
                          <div class="alert alert-info" role="alert">
                            <?php echo $pageMessage; ?>
                          </div>
                        <?php } ?>
                        <h2 class="h3 mb-3 text-black">Mesajlarım</h2>
                        <?php if(is_array($messages) && count($messages) >0){ ?>
                          <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                            <thead>
                              <tr>
                                <th>Mesaj Tarihi</th>
                                <th>Konu</th>
                                <th>Detay</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($messages as $message) { ?>
                                <?php $color = ($message["new_message"] === "true") ? "black" : "gray"; ?>
                                <form class="" action="" method="post">
                                  <tr>
                                    <td>
                                      <p style="color:<?php echo $color; ?>;"><?php echo $message["posting_date"]; ?></p>
                                    </td>
                                    <td>
                                      <p style="color:<?php echo $color; ?>;"><?php echo $message["subject"]; ?></p>
                                    </td>
                                    <td>
                                      <a href="<?php echo url("profil/mesaj/".$message["message_id"]); ?>">Detay</a>
                                    </td>
                                  </tr>
                                </form>
                              <?php } ?>
                            </tbody>
                          </table>
                        <?php } else {?>
                          <p>Mesajınız bulunmamaktadır.</p>
                        <?php } ?>
                  </form>
                    </div>
                </div>
            </div>
            <!-- Uren's Account Page Area End Here -->
        </main>
        <!-- Uren's Page Content Area End Here -->
