<div class="container">
	<div class="row text-center">
    <div class="col-sm-3 col-sm-offset-3"></div>
    <div class="col-sm-6 col-sm-offset-3">
      <h3 class="mt-4" style="color:#0fad00"> Teşekkürler </h3>
      <img src="<?php echo publicUrl('img/success.png'); ?>" style="width:80px;height:80px;">
      <h5>Siparişiniz Alındı</h5>
      <p style="font-size:18px;color:#5C5C5C;">
        Siparişlerin tümünü görüntülemek için <a href="<?php echo url("profil/siparislerim")?>">tıklayınız</a>.
      </p>
      <p>
      <?php if(isset($bankDetails) && count($bankDetails) > 0){ ?>
				<?php if($bankDetails["iban_no"] != "kapidaodeme"){ ?>
	        <h5>Banka Bilgileri</h5>
	        <ul style="list-style: none;padding-left: 0px;">
	          <li>Banka Adı : <?php echo $bankDetails["bank_name"]; ?></li>
	          <li>Hesap Adı : <?php echo $bankDetails["account_name"]; ?></li>
	          <li>Hesap No  : <?php echo $bankDetails["account_no"]; ?></li>
	          <li>Iban No   : <?php echo $bankDetails["iban_no"]; ?></li>
	        </ul>
				<?php } else { ?>
					<p>Ödeme seçeneği olarak kapıda ödeme seçildi.</p>
				<?php } ?>
      <?php } ?>
      </p>
      <a href="<?php echo url(); ?>" class="btn btn-success">Alışverişe Devam Et</a>
    <br><br>
    </div>
	</div>
</div>
