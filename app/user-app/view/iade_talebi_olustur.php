
<!-- Begin Uren's Page Content Area -->
<main class="page-content">
    <!-- Begin Uren's Account Page Area -->
    <div class="account-page-area">
        <div class="container-fluid">
          <div class="row">
            <?php require(staticUrl("sidebar.php")); ?>
            <div class="col-md-9 mt-3 mb-3">
              <h2 class="h3 mb-3 text-black">İade Talebi Oluştur  </h2>
              <hr>
              <?php if(isset($pageMessage) && trim($pageMessage) != null) {?>
                <div class="alert alert-info text-center" role="alert">
                  <strong><?php echo $pageMessage; ?></strong>
                </div>
                <hr>
              <?php } ?>
              <div class="col-md-3">
                <img src="<?php echo publicUrl("img/product-images/".$productDetails["image"]); ?>" alt="" style="max-width:150px;">
              </div>
              <div class="col-6">
                <p>
                  <a target="_blank" href="<?php echo url(seoUrl($productDetails["title"])."-p-".$barcode); ?>">
                    <?php echo $productDetails["title"]; ?>
                  </a>
                </p>
                <p>Barcode : <strong><?php echo $barcode; ?></strong> </p>
                <p>Adet : <strong><?php echo $quantity; ?></strong> </p>
                <p>Birim Fiyatı : <strong><?php echo $unitPrice; ?> TL (KDV Hariç)</strong> </p>
              </div>
              <div class="col-md-3"></div>
              <hr>
              <div class="col-md-12">
                <?php if((int)$orderDetails["status"] != 3 || !$returnControl){ ?>
                  <strong>Bu ürün için şuan talep oluşturamazsınız.</strong>
                  <a class="btn btn-dark" href="<?php echo url("profil/siparislerim"); ?>">Siparişlerim</a>
                <?php } else{ ?>
                  <form class="" action="" method="post">
                    <div class="col-md-12">
                      <label for="reason">Talebiniz için sebep seçiniz</label>
                      <select class="form-control" name="reason" id="reason" required>
                        <option value="Ürünü Beğenmedim">Ürünü Beğenmedim</option>
                        <option value="Ürün Arızalı">    Ürün Arızalı</option>
                        <option value="Faturadaki ürün ile bana gelen ürün farklı">Faturadaki ürün ile bana gelen ürün farklı</option>
                        <option value="Ürünün aparatı / aksesuarı eksik<">Ürünün aparatı / aksesuarı eksik</option>
                        <option value="Siparişimde eksik ürün var">Siparişimde eksik ürün var</option>
                        <option value="Ürünümün onarılmasını istiyorum">Ürünümün onarılmasını istiyorum</option>
                        <option value="Ürün hasarlı geldi, tutanak ve servis raporu var">Ürün hasarlı geldi, tutanak ve servis raporu var</option>
                        <option value="Ürün bozuldu, servis iadesi / değişim raporları var">Ürün bozuldu, servis iadesi / değişim raporları var</option>
                      </select>
                    </div>
                    <div class="col-md-12 mt-2">
                      <label for="returned_quantity">Bu ürünün kaç tanesini iade etmek istiyorsunuz</label>
                      <select class="form-control" id="returned_quantity" name="returned_quantity" required>
                        <?php for($i = 1; $i < $quantity; $i++){ ?>
                          <option value="<?php echo $i; ?>"><?php echo "$i Adet"; ?></option>
                        <?php } ?>
                        <option value="<?php echo $quantity; ?>">Hepsi</option>
                      </select>
                    </div>
                    <div class="col-md-12 mt-2">
                      <input class="form-control" type="submit" name="return_btn" value="Devam">
                    </div>
                  </form>
                <?php } ?>
              </div>
            </div>
        </div>
        </div>
    </div>
    <!-- Uren's Account Page Area End Here -->
</main>
<!-- Uren's Page Content Area End Here -->
