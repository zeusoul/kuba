<main>
  <!-- login Area Strat-->
  <section class="login-area pt-100 pb-100">
      <div class="container">
          <div class="row">
            <?php if (isset($pageMessage) && $pageMessage != ""): ?>
              <!-- Alert -->
              <div class="col-lg-12">
                <div class="alert alert-danger text-center" role="alert">
                  <?php echo $pageMessage; ?>
                </div>
              </div>
              <!-- Alert -->
            <?php endif; ?>
              <div class="col-lg-6">
                  <div class="basic-login">
                      <h3 class="text-center mb-60">Giriş Yap</h3>
                      <form action="" method="post">
                          <label for="email">Email Adresiniz <span>**</span></label>
                          <input value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>" id="email" name="email" type="text" placeholder="Email Adresinizi Giriniz..." required/>
                          <label for="pass">Şifreniz <span>**</span></label>
                          <input id="pass" name="password" type="password" placeholder="Şifrenizi Giriniz..." required/>
                          <div class="mt-10"></div>
                          <button type="submit" name="login" class="btn theme-btn-2 w-100">Giriş Yap</button>
                          <div class="or-divide"><span>o</span></div>
                      </form>
                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="basic-login">
                      <h3 class="text-center mb-60">Kayıt Ol</h3>
                      <form action="" method="post">
                          <label for="email">Email <span>**</span></label>
                          <input value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>" id="email" type="text" name="email" placeholder="Email Adresinizi Giriniz..." required/>
                          <label for="name">Ad <span>**</span></label>
                          <input value="<?php if(isset($_POST["name"])) echo $_POST["name"]; ?>" id="name" type="text" name="name" placeholder="Adınızı Giriniz..." required/>
                          <label for="surname">Soyad <span>**</span></label>
                          <input value="<?php if(isset($_POST["surname"])) echo $_POST["surname"]; ?>" id="surname" type="text" name="surname" placeholder="Soyadınızı Giriniz..." required/>
                          <label for="phone">Telefon <span>**</span></label>
                          <input value="<?php if(isset($_POST["phone"])) echo $_POST["phone"]; ?>" id="phone" type="text" name="phone" placeholder="Telefon Numaranızı Giriniz..." required/>
                          <label for="password">Şifre <span>**</span></label>
                          <input id="password" type="password" name="password" placeholder="Şifrenizi Giriniz..." required/>
                          <div class="mt-10"></div>
                          <button type="submit" name="signUp" class="btn theme-btn w-100">Kayıt Ol</button>
                          <div class="or-divide"><span>+</span></div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- login Area End-->
</main>
