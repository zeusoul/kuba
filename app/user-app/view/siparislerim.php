
<!-- Begin Uren's Page Content Area -->
<main class="page-content">
    <!-- Begin Uren's Account Page Area -->
    <div class="account-page-area">
        <div class="container-fluid">
            <div class="row">
                <?php require(staticUrl("sidebar.php")); ?>
                <div class="col-md-9 mt-3 mb-3">
                    <h2 class="h3 mb-3 text-black">Siparişlerim  </h2>
                    <?php if(isset($pageMessage) && trim($pageMessage) != null) {?>
                      <div class="alert alert-info text-center" role="alert">
                        <strong><?php echo $pageMessage; ?></strong>
                      </div>
                    <?php } ?>
                    <?php if(count($orders) <= 0){ ?>
                      Sipariş Bulunamadı
                    <?php } else{ ?>
                      <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                        <thead>
                          <tr>
                            <th>Sipariş Tarih</th>
                            <th>Sipariş No</th>
                            <th>Ürün Adeti</th>
                            <th>Sipariş Tutarı</th>
                            <th>Sipariş Durum</th>
                            <th>İşlemler</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($orders as $key => $order) :?>
                            <tr>
                              <td><?php echo $order["order_date"]; ?></td>
                              <td><?php echo $order["order_no"]; ?></td>
                              <td><?php echo $order["count_cart"]; ?></td>
                              <td><?php echo $order["order_amount"]; ?> ₺</td>
                              <td>
                                <span class="badge badge-<?php echo ($order['status']==-1) ? 'danger': (($order['status']== 3) ? "success" : "primary"); ?>" >
                                  <?php
                                    $status=(int)$order["status"];
                                    if ($status==-1) echo "İptal Edildi";
                                    else if ($status==0) echo "Onay Bekliyor";
                                    else if ($status==1) echo "Onaylandı";
                                    else if ($status ==2) echo "Kargoda";
                                    else if ($status == 3) echo "Teslim Edildi";
                                  ?>
                                </span>
                              </td>
                              <td>
                                <a href="<?php echo url("profil/siparis-detay/".$order["order_no"]) ?>" name="musteri-sec" class="btn btn-primary" >Detay</a>
                              </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      </table>
                    <?php } ?>
                  </div>
                </div>
              </div>

            </div>
        </div>
    </div>
    <!-- Uren's Account Page Area End Here -->
</main>
<!-- Uren's Page Content Area End Here -->
