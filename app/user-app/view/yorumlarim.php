
        <!-- Begin Uren's Page Content Area -->
        <main class="page-content">
            <!-- Begin Uren's Account Page Area -->
            <div class="account-page-area">
                <div class="container-fluid">
                    <div class="row">
                        <?php include staticUrl("sidebar.php"); ?>
                        <div class="col-md-9 mt-3 mb-3">
                          <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
                            <div class="alert alert-info" role="alert">
                              <?php echo $pageMessage; ?>
                            </div>
                          <?php } ?>
                          <h2 class="h3 mb-3 text-black">Yorumlarım</h2>
                          <?php if(is_array($myComments) && count($myComments) >0){ ?>
                            <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                              <thead>
                                <tr>
                                  <th>Ürün Başlık</th>
                                  <th>Yorum</th>
                                  <th>Değerlendirme</th>
                                  <th>İşlemler</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach($myComments as $key => $comment) { ?>
                                  <form class="" action="" method="post">
                                    <tr>
                                      <td width="20%">
                                        <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["product_id"]); ?>">
                                          <?php echo $comment["title"]; ?></td>
                                        </a>
                                      <td>
                                        <textarea class="form-control" type="text" name="comment" required><?php echo $comment["comment"]; ?></textarea>
                                      </td>

                                      <td width="25%">
                                        <?php for ($i=5; $i>0; $i--): ?>
                                          <?php $checked = ($i == (int)$comment["user_rating"]) ? "checked" : ""; ?>
                                          <div class="custom-control custom-radio">
                                            <input <?php echo $checked; ?> type="radio" id="customRadio<?php echo $i.''.$key; ?>"  name="ratingRadio" value="<?php echo $i; ?>" class="custom-control-input" required>
                                            <label class="custom-control-label" for="customRadio<?php echo $i.''.$key; ?>">
                                              <div class="sp-area sp-tab-style_left p-0" style="">
                                                <div class="sp-nav p-0" style="background-color: transparent !important;">
                                                  <div class="sp-content">
                                                    <div class="rating-box">
                                                      <ul>
                                                        <input type="radio" id="customRadio<?php echo $i; ?>"  name="ratingRadio" value="<?php echo $i; ?>" class="custom-control-input" required>
                                                        <?php  for ($j=0; $j < $i; $j++) {        ?>
                                                          <li><i class="ion-android-star"></i></li>
                                                        <?php  } ?>
                                                        <?php  for ($j=5-$i; $j < 0; $j++) {        ?>
                                                          <li class="silver-color"><i class="ion-android-star"></i></li>
                                                        <?php  } ?>
                                                      </ul>
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>
                                            </label>
                                          </div>
                                        <?php endfor; ?>
                                      </td>
                                      <td>
                                        <input type="hidden" name="commentId" value="<?php echo $comment["product_comment_id"]; ?>">
                                        <input type="submit" name="updateComment" class="btn btn-primary" value="Güncelle"/>
                                        <input type="submit" name="deleteComment" class="btn btn-danger" value="Sil"/>
                                      </td>
                                    </tr>
                                  </form>
                                <?php } ?>
                              </tbody>
                            </table>
                          <?php } else {?>
                            <p>Yorumunuz bulunmamaktadır.</p>
                          <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Uren's Account Page Area End Here -->
        </main>
        <!-- Uren's Page Content Area End Here -->
