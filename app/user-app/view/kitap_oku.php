<!DOCTYPE html>
<html lang="tr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Psikoloji Men - <?php echo $product["title"]; ?></title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href=" <?php echo publicUrl("images/favicon.ico"); ?>">
    <!-- Place favicon.ico in the root directory -->

<!-- CSS here -->
    <link rel="stylesheet" href="<?php echo publicUrl("css/bootstrap.min.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/owl.carousel.min.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/animate.min.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/magnific-popup.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/fontawesome-all.min.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/flaticon.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/meanmenu.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/meanmenu.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/slick.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/default.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/style.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/style2.css"); ?> ">
    <link rel="stylesheet" href="<?php echo publicUrl("css/responsive.css"); ?> ">

    <script src="<?php echo publicUrl("js/vendor/jquery-1.12.4.min.js"); ?>"></script>
    <style>

      body{
        background-color: #999 !important;
      }
      header{
        padding-top: 25px;
      }
      main{
        padding: 5px;
        padding-top:25px;
        padding-bottom: 200px;
      }
      footer{
        position: fixed;
        width: 100%;
        height: 150px;
        background-color: #999;
        bottom:0;
        left:0;
      }
      p{
        font-size:20px;
        color:#000;
      }
      #mobile-view > p{
        font-size:26pt;
        line-height: 150%;
      }
      #mobile-view > h1 {
        font-size:40pt;
      }
    </style>
  </head>
  <body>
    <div class="container">
      <div class="row">

        <div class="col-10" style="margin:auto;">
          <header id="header">
            <div class="row">
              <div class="col-4">

                <a href="<?php echo url(); ?>">
                  <img class="pm-logo" src="<?php echo publicUrl("img/logo.jpg"); ?>" alt="">
                </a>
              </div>
              <div class="col-8">
                <div class="basic-pagination basic-pagination-2 text-center float-right">
                      <ul>
                          <li><a href="<?php echo url("profil/urunlerim"); ?>" style="color:white;">X</a></li>
                      </ul>
                  </div>
              </div>
            </div>
          </header>

          <main id="desktop-view">
            <?php echo $pages[$pageNumber-1]["page_content"]; ?>
          </main>

        </div>
      </div>

      <main id="mobile-view" style="display:none;">
        <?php echo $pages[$pageNumber-1]["page_content"]; ?>
      </main>
    </div>

    <footer>
      <div class="container">
        <div class="row">
          <div class="col-10" style="margin:auto;">

            <div class="basic-pagination basic-pagination-2 text-center mt-20">
              <ul>
                <li class="float-left"><a href="<?php echo $pageNumber <= 1 ? url("kitap-oku?p=".$_GET["p"]."&page=1") : url("kitap-oku?p=".$_GET["p"]."&page=".($pageNumber-1)); ?>"><i class="fas fa-angle-double-left" style="color:white;"></i></a></li>
                <li style="padding-top:15px;"><h3><?php echo "$pageNumber/".count($pages); ?></h3> </li>
                <li class="float-right"><a href="<?php echo $pageNumber >= count($pages) ? url("kitap-oku?p=".$_GET["p"]."&page=".(count($pages))) : url("kitap-oku?p=".$_GET["p"]."&page=".($pageNumber+1)); ?>"><i class="fas fa-angle-double-right" style="color:white;"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  </body>

  <script>
    $(document).ready(function(){
      var width = $(window).width();
      if(width <= 1024){
        $("#desktop-view").css("display","none");
        $("#mobile-view").css("display","block");
        $("#header").css("height","250px");
      }
    });
  </script>
</html>
