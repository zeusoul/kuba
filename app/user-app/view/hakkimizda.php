

    <!-- page-title-area end -->
    <section class="about-area pt-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-7">
                    <div class="mission-title mb-30"> 
                        
                            <h2 style="color:#fdbcb4;">Hakkımızda</h2>
                        </div>
                            <div class="about-community-text mb-30">
                            <p>
                            <?php echo $companyInformation["company_about"];  ?>
                            </p>
                        </div>
                    
                </div>
                <div class="col-xl-5">
                    <div class="mission-img">
                        <img style="float: right;" src="<?php echo publicUrl("img/siyah-manken.jpg"); ?>" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="text-center">
                    <img src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="mission-area pt-30 pb-70 ">
        <div class="container">
            <div class="row">  
                <div class="col-xl-5">
                    <img style="float: left;" src="<?php echo publicUrl("img/mavi-manken.jpg"); ?>" alt="">
                </div>
                <div class="col-xl-7">
                    <div class="mission-title">
                        <h2 style="color:#fdbcb4;">Misyonumuz</h2>
                    </div>
                    <div class="mission-text mt-30">
                        <p>
                            <?php echo $companyInformation["company_mission"];  ?>
                        </p>
                    </div> <br><br><br><br>
                    <div class="mission-title mt-10">
                        <h2 style="color:#fdbcb4;">Vizyonumuz</h2>
                    </div>
                    <div class="mission-text mt-30">
                        <p>
                            <?php echo $companyInformation["company_vision"];  ?>
                        </p>
                    </div> 
                </div>
              
            </div>
        </div>
    </section>
    <!-- <section class="big-team-area">
        <div class="big-image">
            <img style="float: center;" src="<?php echo publicUrl("img/hakkimizda_banner2.jpeg"); ?>" alt="">
        </div> -->
        <!-- <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="testimonial-active owl-carousel theme-bg">
                        <div class="testimonial-item text-center">
                            <p>“Psikoloji Men'le tanışmadan önce kadınlarla alakalı bir sürü korkum vardı. İlgili ekipleri sayesinde hepsini yendim ve şuanda güzel bir ilişkim var. Kitapların çok etkili olduğunu söyleyebilirim, teşekkürler.”</p>
                            <span>- Harun Özen</span>
                        </div>
                        <div class="testimonial-item text-center">
                            <p>“En başta çok şüpheliydim fakat okuyunca anladım ki şüphelerimin hepsi boşaymış. Bu adamlara güvenin :).”</p>
                            <span>- Durmuş Pekmezci</span>
                        </div>
                        <div class="testimonial-item text-center">
                            <p>“Mutluluğu hep saçma sapan şeylerde aradım. Bu kadar basit bulacağımı düşünmezdim. Psikoloji Men ailesine katılmak benim için bir şereftir.”</p>
                            <span>- Semih Keçeci</span>
                        </div>
                        <div class="testimonial-item text-center">
                            <p>“Kitapların en sevdiğim yanı lafı uzatmadan hap bilgi vermeleri. Bilgili biri tarafından yazıldıkları belli.”</p>
                            <span>- Erol Uçar</span>
                        </div>
                        <div class="testimonial-item text-center">
                            <p>“Kadınlarla alakalı yanlışlarımı düzelttim diyebilirim, gayet memnunum. Daha çok beraber olacağız Ateş hocam.”</p>
                            <span>- Latif Karagöl</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
