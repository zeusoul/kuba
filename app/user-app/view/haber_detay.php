
        <div class="blog-area pt-100 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__thumb mb-35">
                                <img src="<?php echo publicUrl("img/new-images/".$newDetails["new_image"]); ?>" alt="blog image">
                            </div>
                            <div class="postbox__text bg-none">
                                <div class="post-meta mb-15">
                                    <span><i class="far fa-calendar-check"></i><?php echo $newDetails["new_date"]; ?> </span>
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?></a></span>
                                </div>
                                <h3 class="blog-title">
                                <?php echo $newDetails["new_title"]; ?>
                                </h3>
                                <div class="post-text mb-20">
                                    <blockquote>
                                        <p><?php echo $newDetails["new_desc"]; ?>.</p>
                                        <!-- <footer>- Rosalina Pong</footer> -->
                                    </blockquote>
                                    <p><?php echo $newDetails["new_content"]; ?></p>

                                    <!-- <div class="blog-inner-img mb-30 mt-30">
                                        <img src="img/blog/b2.jpg" alt="blog image">
                                    </div> -->

                                    <!-- <div class="inner-content">
                                        <h4>A cleansing hot shower or bath</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna
                                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                            aliquip ex ea commodo consequat.
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                            fugiat nulla pariatur. Excepteur
                                            sint
                                            occaecat cupidatat non proident, sunt in culpa qui officia.</p>
                                    </div>
                                    <div class="inner-content">
                                        <h4>Setting the mood with incense</h4>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor
                                            incididunt ut labore et dolore magna
                                            aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                            aliquip ex ea commodo consequat.
                                            Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                            fugiat nulla pariatur. Excepteur
                                            sint
                                            occaecat cupidatat non proident, sunt in culpa qui officia.</p>
                                    </div> -->
                                </div>
                                <div class="row mt-50">
                                    <!-- <div class="col-xl-4 col-lg-4 col-md-4 mb-15">
                                        <div class="blog-share-icon text-left text-md-right">
                                            <span>Share: </span>
                                            <a href="<?php echo url("hakkimizda"); ?>"><i class="fab fa-facebook-f"></i></a>
                                            <a href="<?php echo url("hakkimizda"); ?>"><i class="fab fa-twitter"></i></a>
                                            <a href="<?php echo url("hakkimizda"); ?>"><i class="fab fa-instagram"></i></a>
                                            <a href="<?php echo url("hakkimizda"); ?>"><i class="fab fa-google-plus-g"></i></a>
                                            <a href="<?php echo url("hakkimizda"); ?>"><i class="fab fa-vimeo-v"></i></a>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                            <div class="author mt-80 mb-40">
                                <div class="author-img text-center">
                                    <img src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt="">
                                </div>
                                <div class="author-text text-center">
                                    <h3> <?php echo $companyInformation["company_name"]; ?></h3>
                                    <div class="author-icon">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-behance-square"></i></a>
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fab fa-vimeo-v"></i></a>
                                    </div>
                                    <p> <?php echo $companyInformation["company_about"]; ?> </p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-lg-4">
                        <div class="widget mb-40">
                            <div class="widget-title-box mb-30">
                                <span class="animate-border"></span>
                                <h3 class="widget-title">Hizmet Verdiğimiz Pazaryerleri</h3>
                            </div>
                            <ul class="recent-posts">
                                <li>
                                    <div class="widget-posts-image">
                                        <a href="#"><img style=" width: fit-content;" src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt=""></a>
                                    </div>
                                    <div class="widget-posts-body">
                                        <h6 class="widget-posts-title" style="margin-top:15px"><a href="https://www.trendyol.com/magaza/moda-mahur-m-362992?sst=0">&nbsp;&nbsp;&nbsp;&nbsp;Trendyol</a></h6>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- <div class="widget mb-40">
                            <div class="widget-title-box mb-30">
                                <span class="animate-border"></span>
                            </div>
                            <div class="about-me text-center">
                                <h4><?php echo $companyInformation["company_name"]; ?></h4><br><br><br>
                                <p style="margin-top:10px"><?php echo $companyInformation["company_about"]; ?></p>
                                <div class="widget-social-icon">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-behance"></i></a>
                                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                                    <a href="#"><i class="fab fa-youtube"></i></a>
                                </div>
                            </div>
                        </div> -->
                        <div class="widget mb-40">
                            <div class="widget-title-box mb-30">
                                <span class="animate-border"></span>
                                <h3 class="widget-title">Diğer Haberler</h3>
                            </div>
                            <ul class="recent-posts">
                            <?php if(count($news) == 0){ ?>
								Kayıtlı haber bulunamadı
							<?php } else{ ?>
							<?php foreach($news as $key => $new){ ?>
							<?php
								if($key < $offset) {continue;}
								else if($key >= $limit) {break;}
							?>
                                <li>
                                    <div class="widget-posts-image">
                                        <a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>"><img src="<?php echo publicUrl('img/new-images/'.$new["new_image"]) ?>" alt=""></a>
                                    </div>
                                    <div class="widget-posts-body">
                                        <h6 class="widget-posts-title"><a href="<?php echo url("haber-detay/".seourl($new["new_title"])."-n-".$new["new_id"]); ?>">    <?php echo $newDetails["new_title"]; ?></a></h6>
                                        <div class="widget-posts-meta">    <?php echo $newDetails["new_date"]; ?> </div>
                                    </div>
                                </li>
                                <?php } ?>
				            <?php } ?>
                            </ul>
                        </div>
                        <div class="widget mb-40">
                            <div class="widget-title-box mb-30">
                                <span class="animate-border"></span>
                                <h3 class="widget-title">Kategoriler</h3>
                            </div>
                            <ul class="cat">
                                <?php foreach ($categories as $category): ?>
                                    <li>
                                        <a href="<?php echo url("kategori/".seoUrl($category["name"])."/".$category["category_id"]); ?>"><?php echo $category["name"]; ?></a>
                                        <ul class="cat">
                                            <?php foreach ($categories["sub"] as $key => $subCategory): ?>
                                            <li>
                                                <a href="<?php echo url("kategori/".$subCategory["name"]."/".$subCategory["id"]); ?>"><?php echo $subCategory["name"]; ?></a>
                                            </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </li>
                                    <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="widget mb-40">
                            <div class="widget-title-box mb-30">
                                <span class="animate-border"></span>
                                <h3 class="widget-title">Site Haritası</h3>
                            </div>
                            <div class="tag">
                                <a href="<?php echo url("iletisim"); ?>">İletişim</a>
                                <a href="<?php echo url("hakkimizda"); ?>">Hakkımızda</a>
                                <a href="<?php echo url("hakkimizda"); ?>">Vizyonumuz Ve Misyonumuz</a>
                                <a href="<?php echo url("sorular"); ?>">Sıkça Sorulan Sorular</a>
                                <a href="<?php echo url("uyelik"); ?>">Üye Girişi</a>
                                <a href="<?php echo url("uyelik"); ?>">Yeni Üyelik</a>
                                <a href="<?php echo url("hakkimizda"); ?>">Kampanyalar Ve Şartlar</a>
                                <a href="<?php echo url("mesafeli_satis_sozlesmesi"); ?>">Satış Sözleşmesi</a>
                                <a href="<?php echo url("iptal_ve_iade_sartlari"); ?>">İade & Değişim</a>
                                <a href="<?php echo url("gizlilik_politikasi"); ?>">Gizlilik ve Güvenlik</a>
                            </div>
                        </div>
                        <div class="widget mb-40 p-0 b-0">
                            <div class="banner-widget">
                                <a href="#"><img src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>