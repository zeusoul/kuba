
<!-- Begin Uren's Page Content Area -->
<main class="page-content">
    <!-- Begin Uren's Account Page Area -->
    <div class="account-page-area">
        <div class="container-fluid">
            <div class="row">
                <?php require(staticUrl("sidebar.php")); ?>
                <div class="col-md-9 mt-3 mb-3">
                  <?php if ((int)$order["status"] == -1) { ?>
                    <div class="alert alert-danger text-center" role="alert">
                      <strong>Bu Sipariş İptal Edildi!</strong>
                    </div>
                  <?php } ?>
                  <h2 class="h3 mb-3 text-black">Sipariş Detayı  </h2>
                  <hr>
                  <h5>Sipariş Bilgileri</h5>
                  <hr>
                  <?php if(isset($pageMessage) && trim($pageMessage) != null) {?>
                    <div class="alert alert-info" role="alert">
                      <?php echo $pageMessage; ?>
                    </div>
                  <?php } ?>
                  <form action="" method="post">
                    <div class="row">
                      <div class="form-group col-md-2">
                        <label ><strong>Sipariş Tarihi</strong> </label>
                        <p><?php echo $order["order_date"]; ?></p>
                      </div>
                      <div class="form-group col-md-2">
                        <label for=""><strong>Sipariş No</strong></label>
                        <p><?php echo $order["order_no"]; ?></p>
                      </div>
                      <div class="form-group col-md-3">
                          <label for="name"> <strong>Ödeme Yöntemi</strong> </label>
                          <p>
                            <?php
                              if ($order["payment_type"]==0) echo "Kredi kartı / Banka kartı";
                              else if ($order["payment_type"]==1) echo "Havale / EFT";
                            ?>
                          </p>
                      </div>
                      <div class="form-group col-md-2">
                        <label for="name"> <strong>Toplam Tutar</strong> </label>
                        <p><?php echo $order["order_amount"];  ?> TL</p>
                      </div>
                      <div class="form-group col-md-3">
                          <label for="name"> <strong>Sipariş Durumu</strong> </label>
                          <p>
                            <?php
                              $status = (int)$order["status"];
                              if($status == -1) echo "İptal Edildi";
                              else if($status == 0) echo "Onay Bekleniyor";
                              else if($status == 1) echo "Onaylandı";
                              else if($status == 2) echo "Kargoda <br>".$order["cargo_company"]." - ".$order["cargo_no"];
                              else if($status == 3) echo "Teslim Edildi";
                            ?>
                          </p>
                      </div>
                    </div>
                    <?php if($status >= 1) { ?>
                      <a target="_blank" class="btn btn-primary" href="<?php echo url("profil/fatura/".$order["order_no"]); ?>">
                        Faturayı Görüntüle
                      </a>
                    <?php } ?>
                    <hr>
                    <h4 class="mt-4">Sipariş Edilen Ürünler</h4>
                    <hr>
                    <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                      <thead>
                        <tr>
                          <th>Ürün Başlık</th>
                          <th>Ürün Varyantı</th>
                          <th>Ürün Barkod</th>
                          <th>Ürün Adeti</th>
                          <th>Ürün Birim Fiyat</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($order["product_variants"] as $key => $product) :?>
                          <tr>
                            <td><a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>" name="musteri-sec"  ><?php echo $product["title"]; ?></a></td>
                            <td><a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>" name="musteri-sec"  ><?php echo $product["combine"]; ?></a></td>
                            <td><?php echo $product["barcode"]; ?></td>
                            <td><?php echo $product["quantity"]; ?></td>
                            <td><?php echo $product["unit_price"]; ?> ₺</td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                </div>
                </div>

              </div>

            </div>
        </div>
    </div>
    <!-- Uren's Account Page Area End Here -->
</main>
<!-- Uren's Page Content Area End Here -->
