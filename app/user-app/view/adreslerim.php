
            <!-- Begin Uren's Page Content Area -->
            <main class="page-content">
                <!-- Begin Uren's Account Page Area -->
                <div class="account-page-area">
                    <div class="container-fluid">
                        <div class="row">
                        <?php include staticUrl("sidebar.php"); ?>

                    <div class="col-md-9">
                        <?php if(isset($pageMessage) && trim($pageMessage) != "") { ?>
                        <div class="alert alert-info" role="alert">
                            <?php echo $pageMessage; ?>
                        </div>
                        <?php } ?>
                        <h2>Adres Bilgileri</h2>
                        <hr>
                        <div>
                          <a href=""class="btn btn-secondary" type="button" data-toggle="collapse" data-target="#collapse" aria-expanded="false" aria-controls="collapse" >
                            <h5 class="small-title">ADRES EKLE</h5>
                          </a>                                                                                                                              
                        </div>
                        <div class="collapse mt-1 " style="padding-left: 0px;padding-right: 00px;" id="collapse">

                          <div class="card card-body myaccount-address">
                            <form action="" method="post" class="form-row">
                              <div class="form-group col-md-6 ">
                              <h6 class="small-title">Adres Adı *</h6>
                                  <input type="text" class="form-control" id="title" name="title" placeholder="Adres Adı : ev, iş.." value="<?php echo $_POST["title"]; ?>" required>
                              </div>
                              <div class="form-group col-md-6">
                              <h6 class="small-title">Adınız Soyadınız *</h6>
                                  <input type="text" id="name_surname" class="form-control" name="name_surname" value="<?php echo $_POST["name_surname"]; ?>" placeholder="Adınız Soyadınız" required>
                              </div>
                              <div class="form-group col-md-6">
                              <h6 class="small-title">Telefon Numarası *</h6>
                                  <input type="text" id="phone" minlength="10" maxlength="11" class="form-control" name="tel" value="<?php echo $_POST["tel"]; ?>" placeholder="Telefon Numarası" required>
                              </div>
                              <div class="form-group col-md-6">
                                  <h6 for="post_code">Posta Kodu (Zorunlu Değil)</h6>
                                  <input type="text" id="post_code" class="form-control" name="post_code" value="<?php echo $_POST["post_code"]; ?>" placeholder="Posta Kodu">
                              </div>
                              <div class="form-group col-md-6">
                                  <h6 for="Iller">Şehir *</h6>
                                  <select class="form-control" id="Iller" name="city" required>
                                  <option value="0">Lütfen Bir İl Seçiniz</option>
                                  </select>
                              </div>
                              <div class="form-group col-md-6">
                                  <h6 for="Ilceler" name="county">İlçe *</h6>
                                  <select class="form-control" id="Ilceler" name="county" disabled="disabled" required>
                                  <option value="0">Lütfen Önce bir İl seçiniz</option>
                                  </select>
                              </div>
                              <div class="form-group  col-md-12">
                                  <h6 for="address">Adres Detayı</h6>
                                  <textarea name="address" class="form-control" id="address" cols="30" rows="3" minlength="5" required><?php echo $_POST["address"]; ?></textarea>
                              </div>
                              <div class="form-group  col-md-6">
                                  <div id="bireysel" class="tab-pane <?php if(!isset($_POST["intervaltype"]) || $_POST["intervaltype"] == "1") echo "active"; ?>">
                                  </div>
                                  <div class="form-group  col-md-12">
                                      <input id="optDaily" checked name="intervaltype" type="radio" value="1" data-target="#bireysel">
                                      <h6 for="optDaily">Bireysel</h6>
                                  </div>
                                  <!--Bireysel-->
                                  <div class="form-group  col-md-12">
                                      <h6 for="tc_no">TC NO</h6>
                                      <input type="text" name="tc_no" class="form-control" id="tc_no" maxlength="11"  value="<?php echo $_POST["tc_no"]; ?>"/>
                                  </div>
                                  </div>
                                  <div class="form-group  col-md-6">
                                  <div class="ml-2">
                                      <input id="optWeekly" <?php if($_POST["intervaltype"] == "0") echo "checked"; ?> name="intervaltype" type="radio" value="0" data-target="#kurumsal">
                                      <h6 for="optWeekly">Kurumsal</h6>
                                  </div>
                                  <div id="kurumsal" class="tab-pane <?php if(isset($_POST["intervaltype"]) && $_POST["intervaltype"] == "0") echo "active"; ?>">
                                      <!--kurumsal-->
                                      <div class="form-group  col-md-12">
                                      <h6 for="company_name">Şirket Adı</h6>
                                      <input type="text" name="company_name" class="form-control" id="company_name" value="<?php echo $_POST["company_name"]; ?>"/>
                                      </div>
                                      <div class="form-group  col-md-12">
                                      <h6 for="tax_administration">Vergi Dairesi</h6>
                                      <input type="text" name="tax_administration" class="form-control" id="tax_administration" value="<?php echo $_POST["tax_administration"]; ?>"/>
                                      </div>
                                      <div class="form-group  col-md-12">
                                      <h6 for="tax_number">Vergi No</h6>
                                      <input type="text" name="tax_number" maxlength="10" class="form-control" id="tax_number" value="<?php echo $_POST["tax_number"]; ?>"/>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group col-md-12 d-flex justify-content-end  ">
                                  <input class="btn btn-secondary " type="submit" name="insertAddress" value="Ekle"/>
                              </div>
                            </form>
                        </div>
                        </div>
                        <div class="adress-list">
                          <div class="row">
                              <?php  foreach ($address as $value ):?>
                              <div class="adress col-md-4 mt-2">
                                  <div class="card" style="width: 17rem;">
                                  <div class="card-body">
                                      <h5 class="card-title"><?php echo $value['title'] ?></h5>
                                      <h6 class="card-subtitle mb-2 text-muted"><?php echo $value['name_surname']; ?></h6>
                                      <p class="card-text"><?php echo $value['tel']; ?></p>
                                      <p class="card-text"><?php echo $value['county']." / ".$value["city"]; ?></p>
                                      <h6 class="card-text " ><?php echo kisalt($value['address'],25); ?></h6>
                                      <form class="" action="" method="post">
                                      <input type="hidden" name="address_book_id" value="<?php echo trim($value["address_book_id"]); ?>">
                                      <input type="submit" class="btn btn-danger" name="deleteAddress" value="Sil"/>
                                      <a href="<?php echo url('profil/adres-duzenle/' . md5($value['address_book_id'])); ?>" class="btn btn-secondary">Düzenle</a>
                                      </form>
                                  </div>
                                  </div>
                              </div>
                              <?php endforeach;?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        </div>
        <!-- Uren's Account Page Area End Here -->
    </main>
    <!-- Uren's Page Content Area End Here -->
