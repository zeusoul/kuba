
      <div class="col-md-9">
        <?php if(isset($pageMessage) && trim($pageMessage) != "") { ?>
          <div class="alert alert-info" role="alert">
            <?php echo $pageMessage; ?>
          </div>
        <?php } ?>
        <h4>Adres Düzenle</h4>
          <div class="card card-body ">
            <form action="" method="post" class="form-row">
              <div class="form-group col-md-6 ">
                <label for="title">Adres Adı *</label>
                <input type="text" class="form-control" id="title" name="title" value="<?php echo $addressBook["title"]; ?>" placeholder="Adres Adı : ev, iş.." required/>
              </div>
              <div class="form-group col-md-6">
                <label for="name_surname">Adınız Soyadınız *</label>
                <input type="text" id="name_surname" class="form-control" name="name_surname" value="<?php echo $addressBook["name_surname"]; ?>" placeholder="Adınız Soyadınız" required/>
              </div>
              <div class="form-group col-md-6">
                <label for="phone">Telefon Numarası *</label>
                <input type="text" id="phone" minlength="10" maxlength="11" class="form-control" name="tel" value="<?php echo $addressBook["tel"]; ?>" placeholder="Telefon Numarası" required/>
              </div>
              <div class="form-group col-md-6">
                <label for="post_code">Posta Kodu (Zorunlu Değil)</label>
                <input type="text" id="post_code" class="form-control" name="post_code" value="<?php echo $addressBook["post_code"]; ?>" placeholder="Posta Kodu">
              </div>
              <div class="form-group col-md-6">
                <label for="Iller">Şehir *</label>
                <select class="form-control" id="Iller" name="city" required/>
                 <option value="0">Lütfen Bir İl Seçiniz</option>
                </select>
              </div>
              <div class="form-group col-md-6">
                <label for="Ilceler" name="county">İlçe *</label>
                <select class="form-control" id="Ilceler" name="county" disabled="disabled" required/>
                 <option value="0">Lütfen Önce bir İl seçiniz</option>
                </select>
              </div>
              <div class="form-group  col-md-12">
                <label for="address">Adres Detayı</label>
                <textarea name="address" class="form-control" id="address"  cols="30" rows="3" minlength="5" required><?php echo $addressBook["address"]; ?></textarea>
              </div>
              <div class="form-group  col-md-12">
                <div class="nav nav-tabs" role="tablist">
                  <div>
                    <input id="optDaily" checked name="intervaltype" type="radio" value="1" data-target="#scheduleDaily">
                    <label for="optDaily">Bireysel</label>
                  </div>
                  <div class="ml-2">
                    <input id="optWeekly" name="intervaltype" type="radio" value="0" data-target="#scheduleWeekly">
                    <label for="optWeekly">Kurumsal</label>
                  </div>
                </div>
                <div class="tab-content">
                  <div id="scheduleDaily" class="tab-pane active">
                    <!--Bireysel-->
                    <div class="form-group  col-md-12">
                      <label for="tc_no">TC NO</label>
                      <input type="text" name="tc_no" class="form-control" id="tc_no" maxlength="11"  value="<?php echo $addressBook["tc_no"]; ?>"/>
                    </div>
                  </div>
                  <div id="scheduleWeekly" class="tab-pane">
                    <!--kurumsal-->
                    <div class="form-group  col-md-12">
                      <label for="tax_administration">Vergi Dairesi</label>
                      <input type="text" name="tax_administration" class="form-control" id="tax_administration" value="<?php echo $addressBook["tax_administration"]; ?>"/>
                    </div>
                    <div class="form-group  col-md-12">
                      <label for="tax_number">Vergi No</label>
                      <input type="text" name="tax_number" maxlength="10" class="form-control" id="tax_number" value="<?php echo $addressBook["tax_number"]; ?>"/>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group col-md-12 d-flex justify-content-end  ">
                <input class="btn btn-secondary " type="submit" name="updateAddress" value="Güncelle"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php
    $plaka = 0;
    foreach ($trArray as $key => $city) {
      if($city["il"] == $addressBook["city"]){
        $plaka = $city["plaka"];
        break;
      }
    }
  ?>
  <script>
    $(document).ready(function(){
      var plaka = "<?php echo $plaka; ?>";
      $("#Iller").val(plaka);
    });
  </script>
