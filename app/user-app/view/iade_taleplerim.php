<!-- Begin Uren's Page Content Area -->
<main class="page-content">
    <!-- Begin Uren's Account Page Area -->
    <div class="account-page-area">
        <div class="container-fluid">
            <div class="row">
                <?php require(staticUrl("sidebar.php")); ?>
          <div class="col-md-9 mt-3 mb-3">
              <h2 class="h3 mb-3 text-black">İade Taleplerim  </h2>
              <?php if(isset($pageMessage) && trim($pageMessage) != null) {?>
                <div class="alert alert-info text-center" role="alert">
                  <strong><?php echo $pageMessage; ?></strong>
                </div>
              <?php } ?>
              <?php if(count($returns) <= 0){ ?>
                İade Talebi Bulunamadı
              <?php } else{ ?>
                <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                  <thead>
                    <tr>
                      <th>Talep Tarihi</th>
                      <th>Talep No</th>
                      <th>Sipariş No</th>
                      <th>Ürün</th>
                      <th>İade Edilen Adet</th>
                      <th>İade Edilecek Tutar</th>
                      <th>Durumu</th>
                      <th>İşlemler</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($returns as $key => $return) :?>
                      <tr>
                        <td><?php echo $return["return_date"]; ?></td>
                        <td><?php echo $return["return_id"]; ?></td>
                        <td><?php echo $return["order_no"]; ?></td>
                        <td>
                          <a target="_blank" href="<?php echo url(seoUrl($return["title"])."-p-".$return["product_id"]); ?>">
                            <?php echo $return["title"]; ?>
                          </a>
                        </td>
                        <td><?php echo $return["returned_quantity"]; ?></td>
                        <td><?php echo $return["returned_price"]; ?> ₺ (KDV Hariç)</td>
                        <td>
                          <span class="badge badge-<?php echo ($order['status']==-1) ? 'danger': (($order['status']== 1) ? "success" : "primary"); ?>" >
                            <?php
                              $status=(int)$return["return_status"];
                              if ($status==-1) echo "İptal Edildi";
                              else if ($status==0) echo "Onay Bekliyor";
                              else if ($status==1) echo "Onaylandı";
                            ?>
                          </span>
                          <?php
                            if($status == -1){
                              if(trim($return["reason_for_rejection"] != "")){ ?>
                                <small>
                                  <?php echo "(".$return["reason_for_rejection"].")"; ?>
                                </small>
                              <?php } ?>
                            <?php } ?>
                        </td>
                        <td>
                          <?php if ($status==0) { ?>
                            <form class="" action="" method="post">
                              <input type="hidden" name="return_id" value="<?php echo $return["return_id"]; ?>" required/>
                              <button class="btn btn-danger" type="input" name="cancel_the_return">Talebi İptal Et</button>
                            </form>
                          <?php } else if($status == -1) { ?>
                            <p>Talep İptal Edildi </p>
                          <?php } else if($status==1) { ?>
                            <button class="btn btn-primary" type="button" name="button">Faturayı Görüntüle</button>
                          <?php } ?>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
