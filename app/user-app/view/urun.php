<main>


<!-- shop-area start -->
<section class="shop-details-area pt-100 pb-100">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-4">
                <div class="product-details-img mb-10">
                    <div class="tab-content" id="myTabContentpro">
                        <div class="tab-pane fade show active" id="home" role="tabpanel">
                            <div class="product-large-img">
                                <img src="<?php echo publicUrl("img/product-images/".$productDetails["image"]); ?>" alt="">
                            </div>
                        </div>
                        <?php foreach ($productDetails["product_images"] as $key => $image): ?>
                          <div class="tab-pane fade" id="product-image-<?php echo $key; ?>" role="tabpanel">
                              <div class="product-large-img">
                                  <img src="<?php echo publicUrl("img/product-images/".$image["product_image"]); ?>" alt="">
                              </div>
                          </div>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div class="shop-thumb-tab mb-30">
                    <ul class="nav" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-selected="true">
                              <img src="<?php echo publicUrl("img/product-images/".$productDetails["image"]); ?>" alt="">
                            </a>
                        </li>
                        <?php foreach ($productImages as $key => $image): ?>
                          <li class="nav-item">
                              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#product-image-<?php echo $key; ?>" role="tab" aria-selected="false">
                                <img src="<?php echo publicUrl("img/product-images/".$image["product_image"]); ?>" alt="">
                              </a>
                          </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="col-xl-6 col-lg-8">
                <div class="product-details mb-30 pl-30">
                    <div class="details-cat mb-20">
                        <a href="#"><?php echo $productDetails["category_name"]; ?></a>
                    </div>
                    <h2 class="pro-details-title mb-15"><?php echo $productVariant["title"]; ?></h2>
                    <div class="product-desc variant-item">
                        <p><?php echo $productDetails["sub_title"]; ?></p>
                    </div>
                    <div class="product-variant">
                      <div class="details-price mb-20">
                        <span><?php echo $productVariant["discount_price"]; ?></span>
                        <span class="old-price"><?php echo $productVariant["price"]; ?></span>
                      </div>
                        <div class="product-info-list variant-item">
                            <ul>
                                <li><span>Marka</span> <?php echo $productDetails["brand_name"]; ?></li>
                                <li><span>Ürün Kodu</span> <?php echo $productDetails["product_code"]; ?></li>
                                <li><span>Stok Durumu</span> <span class="in-stock"><?php echo $productVariant["stock"] > 0 ? "Mevcut" : "Stokta Yok"; ?></span></li>
                            </ul>
                        </div>
                        <div class="product-action-details variant-item">
                            <div class="product-details-action">
                              <button class="btn theme-btn" onclick="addToCart('<?php echo $productVariant["barcode"]; ?>','sepet')">sepete ekle</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-50">
            <div class="col-xl-8 col-lg-8">
                <div class="product-review">
                    <ul class="nav review-tab" id="myTabproduct" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab6" data-toggle="tab" href="#home6" role="tab" aria-controls="home"
                                aria-selected="true">Ürün Açıklaması </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab6" data-toggle="tab" href="#profile6" role="tab" aria-controls="profile"
                                aria-selected="false">Değerlendirmeler (<?php echo count($productComments); ?>)</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent2">
                        <div class="tab-pane fade show active" id="home6" role="tabpanel" aria-labelledby="home-tab6">
                            <div class="desc-text">
                                <p><?php echo $productDetails["content"]; ?></p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile6" role="tabpanel" aria-labelledby="profile-tab6">
                            <div class="desc-text review-text">
                                <div class="product-commnets">
                                  <?php foreach ($productComments as $key => $comment): ?>
                                    <div class="product-commnets-list mb-25 pb-15">
                                        <div class="pro-comments-img">
                                            <img src="img/product/comments/01.png" alt="">
                                        </div>
                                        <div class="pro-commnets-text">
                                            <h4><?php echo $comment["name"]; ?> -
                                                <span><!-- Tarih --></span>
                                            </h4>
                                            <div class="pro-rating">
                                              <?php for ($i=0; i< (int)$productComment["rating"]; $i++): ?>
                                                <i class="far fa-star"></i>
                                              <?php endfor; ?>
                                            </div>
                                            <p><?php echo $comment["comment"]; ?></p>
                                        </div>
                                      </div>
                                  <?php endforeach; ?>
                                </div>
                                <?php if(!loginState()){ ?>
                                  <p>Değerlendirme yapabilmeniz için önce giriş yapmanız lazım.</p>
                                  <a href="<?php echo url("uyelik"); ?>">Giriş Yap</a>
                                <?php } else{ ?>
                                  <?php if (!$reviewState){ ?>
                                    <p><!-- Sipariş yoksa yorum da yok  --></p>
                                  <?php } else { ?>
                                    <div class="review-box mt-50">
                                      <h4>Yorum Ekle</h4>
                                      <div class="your-rating mb-40">
                                        <span>Puan :</span>
                                        <div class="rating-list">
                                          <a href="#">
                                            <i class="far fa-star"></i>
                                          </a>
                                          <a href="#">
                                            <i class="far fa-star"></i>
                                          </a>
                                          <a href="#">
                                            <i class="far fa-star"></i>
                                          </a>
                                          <a href="#">
                                            <i class="far fa-star"></i>
                                          </a>
                                          <a href="#">
                                            <i class="far fa-star"></i>
                                          </a>
                                        </div>
                                      </div>
                                      <form class="review-form" action="#">
                                        <div class="row">
                                          <div class="col-xl-12">
                                            <label for="message">Yorumunuz : </label>
                                            <textarea name="message" id="message" cols="30" rows="10"></textarea>
                                          </div>
                                          <div class="col-xl-6">
                                            <label for="r-name">İsim</label>
                                            <input type="text" id="r-name">
                                          </div>
                                          <div class="col-xl-6">
                                            <label for="r-email">Email</label>
                                            <input type="email" id="r-email">
                                          </div>
                                          <div class="col-xl-12">
                                            <button class="btn theme-btn">KAYDET</button>
                                          </div>
                                        </div>
                                      </form>
                                    </div>
                                  <?php } ?>
                              <?php  } ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="pro-details-banner">
                    <a href="shop.html"><img src="img/banner/pro-details.jpg" alt=""></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- shop-area end -->

    <!-- product-area start -->
    <!--
    <section class="product-area pb-100">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="area-title text-center mb-50">
                        <h2>Releted Products</h2>
                        <p>Browse the huge variety of our products</p>
                    </div>
                </div>
            </div>
            <div class="product-slider-2 owl-carousel">
                <div class="pro-item">
                    <div class="product-wrapper">
                        <div class="product-img mb-25">
                            <a href="product-details.html">
                                <img src="img/product/pro4.jpg" alt="">
                                <img class="secondary-img" src="img/product/pro5.jpg" alt="">
                            </a>
                            <div class="product-action text-center">
                                <a href="#" title="Shoppingb Cart">
                                    <i class="flaticon-shopping-cart"></i>
                                </a>
                                <a href="#" title="Quick View">
                                    <i class="flaticon-eye"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <i class="flaticon-compare"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="pro-cat mb-10">
                                <a href="shop.html">decor, </a>
                                <a href="shop.html">furniture</a>
                            </div>
                            <h4>
                                <a href="product-details.html">Raglan Baseball Style shirt</a>
                            </h4>
                            <div class="product-meta">
                                <div class="pro-price">
                                    <span>$119.00 USD</span>
                                    <span class="old-price">$230.00 USD</span>
                                </div>
                            </div>
                            <div class="product-wishlist">
                                <a href="#"><i class="far fa-heart" title="Wishlist"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pro-item">
                    <div class="product-wrapper">
                        <div class="product-img mb-25">
                            <a href="product-details.html">
                                <img src="img/product/pro5.jpg" alt="">
                                <img class="secondary-img" src="img/product/pro6.jpg" alt="">
                            </a>
                            <div class="product-action text-center">
                                <a href="#" title="Shoppingb Cart">
                                    <i class="flaticon-shopping-cart"></i>
                                </a>
                                <a href="#" title="Quick View">
                                    <i class="flaticon-eye"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <i class="flaticon-compare"></i>
                                </a>
                            </div>
                            <div class="sale-tag">
                                <span class="new">new</span>
                                <span class="sale">sale</span>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="pro-cat mb-10">
                                <a href="shop.html">decor, </a>
                                <a href="shop.html">furniture</a>
                            </div>
                            <h4>
                                <a href="product-details.html">Raglan Baseball Style shirt</a>
                            </h4>
                            <div class="product-meta">
                                <div class="pro-price">
                                    <span>$119.00 USD</span>
                                    <span class="old-price">$230.00 USD</span>
                                </div>
                            </div>
                            <div class="product-wishlist">
                                <a href="#"><i class="far fa-heart" title="Wishlist"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pro-item">
                    <div class="product-wrapper">
                        <div class="product-img mb-25">
                            <a href="product-details.html">
                                <img src="img/product/pro7.jpg" alt="">
                                <img class="secondary-img" src="img/product/pro8.jpg" alt="">
                            </a>
                            <div class="product-action text-center">
                                <a href="#" title="Shoppingb Cart">
                                    <i class="flaticon-shopping-cart"></i>
                                </a>
                                <a href="#" title="Quick View">
                                    <i class="flaticon-eye"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <i class="flaticon-compare"></i>
                                </a>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="pro-cat mb-10">
                                <a href="shop.html">decor, </a>
                                <a href="shop.html">furniture</a>
                            </div>
                            <h4>
                                <a href="product-details.html">Raglan Baseball Style shirt</a>
                            </h4>
                            <div class="product-meta">
                                <div class="pro-price">
                                    <span>$119.00 USD</span>
                                    <span class="old-price">$230.00 USD</span>
                                </div>
                            </div>
                            <div class="product-wishlist">
                                <a href="#"><i class="far fa-heart" title="Wishlist"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pro-item">
                    <div class="product-wrapper">
                        <div class="product-img mb-25">
                            <a href="product-details.html">
                                <img src="img/product/pro9.jpg" alt="">
                                <img class="secondary-img" src="img/product/pro10.jpg" alt="">
                            </a>
                            <div class="product-action text-center">
                                <a href="#" title="Shoppingb Cart">
                                    <i class="flaticon-shopping-cart"></i>
                                </a>
                                <a href="#" title="Quick View">
                                    <i class="flaticon-eye"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <i class="flaticon-compare"></i>
                                </a>
                            </div>
                            <div class="sale-tag">
                                <span class="new">new</span>
                                <span class="sale">sale</span>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="pro-cat mb-10">
                                <a href="shop.html">decor, </a>
                                <a href="shop.html">furniture</a>
                            </div>
                            <h4>
                                <a href="product-details.html">Raglan Baseball Style shirt</a>
                            </h4>
                            <div class="product-meta">
                                <div class="pro-price">
                                    <span>$119.00 USD</span>
                                    <span class="old-price">$230.00 USD</span>
                                </div>
                            </div>
                            <div class="product-wishlist">
                                <a href="#"><i class="far fa-heart" title="Wishlist"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pro-item">
                    <div class="product-wrapper">
                        <div class="product-img mb-25">
                            <a href="product-details.html">
                                <img src="img/product/pro1.jpg" alt="">
                                <img class="secondary-img" src="img/product/pro11.jpg" alt="">
                            </a>
                            <div class="product-action text-center">
                                <a href="#" title="Shoppingb Cart">
                                    <i class="flaticon-shopping-cart"></i>
                                </a>
                                <a href="#" title="Quick View">
                                    <i class="flaticon-eye"></i>
                                </a>
                                <a href="#" data-toggle="tooltip" data-placement="right" title="Compare">
                                    <i class="flaticon-compare"></i>
                                </a>
                            </div>
                            <div class="sale-tag">
                                <span class="new">new</span>
                                <span class="sale">sale</span>
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="pro-cat mb-10">
                                <a href="shop.html">decor, </a>
                                <a href="shop.html">furniture</a>
                            </div>
                            <h4>
                                <a href="product-details.html">Raglan Baseball Style shirt</a>
                            </h4>
                            <div class="product-meta">
                                <div class="pro-price">
                                    <span>$119.00 USD</span>
                                    <span class="old-price">$230.00 USD</span>
                                </div>
                            </div>
                            <div class="product-wishlist">
                                <a href="#"><i class="far fa-heart" title="Wishlist"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  -->
    <!-- product-area end -->


</main>
