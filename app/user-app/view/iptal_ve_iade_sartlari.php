    <main>
    <!-- shop-area start -->
        <section class="blog-area pt-100 pb-60">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">İPTAL VE İADE&nbsp;ŞARTLARI </a>
                                </h3>
                                <div class="post-text">
                                    <p>Şirketimiz, tüketici haklarını korumakta ve satış sonrası müşteri memnuniyetini en ön planda tutmaktadır. Satın aldığınız ürünlerle ilgili yaşayabileceğiniz memnuniyetsizlik, üretim ve servis kaynaklı her türlü sorun, titizlikle değerlendirilmekte ve en kısa sürede çözüme kavuşturulmaktadır.<br>
                                        Ürün iadesi konusunda sizlere daha iyi ve hızlı bir şekilde hizmet verebilmemiz için gerekli olan şartları aşağıda bulabilirsiniz; Bunlardan herhangi birinin eksik olması durumunda ürün iadesi kabul edilmemektedir.<br></p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">İADE İÇİN İSTEDİKLERİMİZ</a>
                                </h3>
                                <div class="post-text">
                                <p>1. İADE EDİLECEK ÜRÜN&nbsp;<br>
                                    2. İADE EDİLECEK ÜRÜNÜN SEVK İRSALİYESİ (Tüm nüshaları ile birlikte)<br>
                                    &nbsp;<br>
                                    <span><strong>1. İADE EDİLECEK ÜRÜN</strong></span><br>
                                    <br>
                                    a- Kullanılmış veya hasar görmüş ürünlerin iadesi kabul edilmemektedir.<br>
                                    b- İade edilecek ürünün, 30 gün içerisinde teslim alındığı şekilde, standart aksesuarları ile birlikte eksiksiz ve hasarsız olarak teslim edilmesi gerekmektedir.<br>
                                    &nbsp;<br>
                                    <span><strong>2. İADE EDİLECEK ÜRÜNÜN SEVK İRSALİYESİ</strong></span><br>
                                    <br>
                                    a- İade işlemi için ürünün sevk irsaliyesinin gönderilmesi gerekmektedir.<br>
                                    .</p>  
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">DİĞER ŞARTLAR ve İADE KARGO ÜCRETLERİ HAKKINDA</a>
                                </h3>
                                <div class="post-text">
                                <p>a- İade etmek istediğiniz ürün / ürünler ayıplı ise anlaşmalı kargo firmalarımızla gönderildiği taktirde, kargo ücreti firmamız tarafından karşılanmaktadır.<br>
                                    b- Kalite Güvence departmanımıza gelen ürünlerin, iade şartlarına uygun ulaştırılması durumunda ürün tutarlarının iadesi, ürünün tarafımıza ulaştığı gün işleme alınacaktır. İadenin hesabınıza yansıma süresi, bankanızın inisiyatifindedir. Kredi kartına yapılan iadeler 7 gün içerisinde hesaba yansımaktadır.<br>
                                    c- İade edeceğiniz ürünleri Yurtiçi Kargo ile ücretsiz gönderebilirsiniz.<br>
                                </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">İADE EDİLECEK ÜRÜNÜN GÖNDERİLMESİ</a>
                                </h3>
                                <div class="post-text">
                                <p>İade edilecek ürün sadece anlaşmalı olduğumuz kargo firmaları tarafından teslim alınabilir. Bunun dışında herhangi bir kargo şirketiyle elimize ulaşan ürünlerin iadesi ve ulaşım bedeli kabul edilmeyecektir.<br>
                                    Genel iade şartları aşağıdaki gibidir;<br>
                                    - İadeler mutlak surette orijinal kutu veya ambalajı ile birlikte yapılmalıdır.<br>
                                    - Orijinal kutusu/ambalajı bozulmuş (örnek: orijinal kutu üzerine kargo etiketi yapıştırılmış ve kargo koli bandı ile bantlanmış ürünler kabul edilmez) tekrar satılabilirlik özelliğini kaybetmiş, başka bir müşteri tarafından satın alınamayacak durumda olan ürünlerin iadesi kabul edilmemektedir.<br>
                                    - İade etmek istediğiniz ürün ile birlikte orijinal sevk irsaliyesi(sizdeki bütün kopyaları) ve iade sebebini içeren bir dilekçe göndermeniz gerekmektedir.<br>
                                    - İade etmek istediğiniz ürün / ürünler ayıplı ise kargo ücreti firmamız tarafından karşılanmaktadır. Bu durumda da anlaşmalı kargolar ile gönderim yapmanız gerekir. Diğer durumlarda ise kargo ücreti size aittir.<br>
                                    <br>
                                </p>
                                </div>
                            </div>
                        </article>
                        <article class="postbox post format-image mb-40">
                            <div class="postbox__text p-30">
                                <div class="post-meta mb-15">
                                    <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                </div>
                                <h3 class="blog-title blog-title-sm">
                                    <a href="#">SİPARİŞ İPTALİ</a>
                                </h3>
                                <div class="post-text">
                                <p>
                                Değerli müşterilerimiz yanlış verdiğiniz veya vazgeçtiğiniz siparişlerinizi iptal ettirebilirsiniz. Bazen yoğunluktan mailler geç kontrol edilmektedir. Ürünün kargoya verilmiş olma ihtimali ortaya çıkmaktadır. İptal işlemlerinde ürün kargoya verilmemişse ücretin tamamı işlemi gerçekleştirdiğiniz yöntemle iade edilir. Ürün kargoya verilmişse kargo ücreti veya ücretleri kesilerek iadesi yapılır.&nbsp;<br>
                                <br>
                                <span><strong>Ürün İade Adresi:</strong></span><br>
                                <em><?php echo $companyInformation["company_name"]; ?> </em><br>

                                <em><?php echo $companyInformation["company_address"]; ?></em><br>

                                &nbsp;&nbsp;<br>
                                </p>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    <!-- shop-area end -->
    </main>
