
<!-- Begin Uren's Page Content Area -->
<main class="page-content">
    <!-- Begin Uren's Account Page Area -->
    <div class="account-page-area">
        <div class="container">
            <div class="row">
                <?php require(staticUrl("sidebar.php")); ?>
                <div class="col-md-9 mt-3 mb-3">
                    <h2 class="h3 mb-3 text-black">E-Kitaplarım</h2>
                    <?php if(isset($pageMessage) && trim($pageMessage) != null) {?>
                      <div class="alert alert-info text-center" role="alert">
                        <strong><?php echo $pageMessage; ?></strong>
                      </div>
                    <?php } ?>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="row">
                              <?php foreach ($orders as $key => $order): ?>
                                <?php foreach ($order["order_details"]["product_variants"] as $key => $product): ?>
                                  <?php if ($product["category_id"] == 219): ?>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="product-wrapper mb-50">
                                            <div class="product-img mb-25">
                                                <a href="<?php echo url("kitap-oku?p=".$product["product_id"]); ?>">
                                                    <img src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                    <img class="secondary-img" src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
                                                </a>
                                                <div class="product-action text-center">
                                                    <a href="<?php echo url("kitap-oku?p=".$product["product_id"]); ?>" title="İncele"><i class="flaticon-eye"></i></a>
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="pro-cat mb-10">
                                                  <a href="<?php echo url("kitap-oku?p=".$product["product_id"]); ?>">E-KİTAP</a>
                                                </div>
                                                <h4>
                                                    <a href="<?php echo url("kitap-oku?p=".$product["product_id"]); ?>"><?php echo $product["title"]; ?></a>
                                                </h4>
                                            </div>
                                            <div class="product-meta">
                                                <div class="pro-price">
                                                    <a href="<?php echo url("kitap-oku?p=".$product["product_id"]); ?>" class="btn theme-btn-2 w-100">OKU</a>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endforeach; ?>
                            </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
        </div>
    </div>
    <!-- Uren's Account Page Area End Here -->
</main>
<!-- Uren's Page Content Area End Here -->
