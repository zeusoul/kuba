<main>
  <!-- shop-area start -->
  <section class="shop-area pt-100 pb-100">
      <div class="container">
          <div class="row">
              <div class="col-xl-12 col-lg-12">
                  <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                          <div class="row">
                            <?php foreach ($images as $key => $image): ?>
                              <?php if($key == 10) break; ?>
                              <div class="col-lg-4 col-md-2">
                                  <div class="product-wrapper mb-50">
                                      <div class="product-img mb-25">
                                          <a href="#">
                                              <img src="<?php echo publicUrl("img/result-images/".$image["image"]); ?>" alt="">
                                          </a>
                                      </div>
                                  </div>
                                </div>
                            <?php endforeach; ?>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- shop-area end -->
</main>
