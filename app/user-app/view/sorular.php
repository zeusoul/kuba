<main>
  <!-- shop-area start -->
  <section class="blog-area pt-100 pb-60">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <article class="postbox post format-image mb-40">
                                <!-- <div class="postbox__thumb">
                                    <a href="#">
                                        <img src="img/blog/b10.jpg" alt="blog image">
                                    </a>
                                </div> -->
                                <div class="postbox__text p-30">
                                    <div class="post-meta mb-15">
                                        <span><a href="<?php echo url("hakkimizda"); ?>"><i class="far fa-user"></i> <?php echo $companyInformation["company_name"]; ?> </a></span>
                                    </div>
                                    <h3 class="blog-title blog-title-sm">
                                        <a href="#">Paketleme nasıl yapılıyor?
                                            </a>
                                    </h3>
                                    <div class="post-text">
                                        <p>Siz değerli müşterilerimizin gönül rahatlığıyla siparişlerini alabilmeleri için ürünler öncelikle kendinden yapışkanlı hediye poşetlerine 
                                            ardından kendi markamızın poşetine ve son olarak da kapalı kargo poşetlerine konulup gönderilmektedir. 
                                            Ayrıca faturalarımızda da sadece ürün kodu yazdığı için kargonuzun içeriğini de kimse bilememektedir.</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
            </section>
  <!-- shop-area end -->
</main>
