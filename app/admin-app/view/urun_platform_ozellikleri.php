<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <h2>Platform Özellikleri Ekle</h2>
  <hr class="mb-4">

  <div class="card">
    <div class="card-body card-block">
      <form class="" action="" method="get">
        <div class="row">
          <div class="col-md-2">
            Platform Seçiniz :
          </div>
          <div class="col-md-4">
            <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
            <select  class="form-control" name="platform_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($platforms as $platform) { ?>
                <option <?php if(isset($_GET["platform_id"]) && $_GET["platform_id"] == $platform["platform_id"]) echo "selected"; ?> value="<?php echo $platform["platform_id"]; ?>"><?php echo $platform["platform_name"]; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2">
            <button  class="btn btn-primary" type="submit">Seç</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php if(isset($_GET["platform_id"])){ ?>
    <?php if(isset($fatalError) && trim($fatalError) != ""){ ?>
      <div class="alert alert-danger" role="alert">
        <?php echo $fatalError; ?>
      </div>
    <?php } else { ?>
      <div class="card">
        <div class="card-header">
          <strong>Kategori</strong> Eşleştir
        </div>
        <div class="card-body card-block">
          <form id="insertPlatformAttributes" action="" method="post">
            <div class="row">
              <?php foreach ($attributes["categoryAttributes"] as $key => $attribute): ?>
                <div class="col-md-6 mb-2">
                  <div class="row">
                    <div class="col-md-12">
                      <input type="hidden" name="attributes[]" value="<?php echo $attribute["attribute"]["id"]."-".$attribute["attribute"]["name"]; ?>">
                      <label for=""><?php echo $attribute["attribute"]["name"]; ?></label>
                      <?php if($attribute['required']):?>
                          <small><strong>(Zorunlu)</strong></small>
                      <?php endif;?>
                    </div>
                    <div class="col-md-12">
                      <select class="form-control" name="attribute_values[]" <?php if($attribute['required']) echo "required"; ?>>
                        <option value="">Seçiniz</option>
                        <?php foreach ($attribute["attributeValues"] as $key => $value): ?>
                          <?php
                            $selected = "";
                            foreach ($platformProductAttributes as $key => $platformProductAttribute) {
                              if($platformProductAttribute["platform_attribute_code"] == $attribute["attribute"]["id"]){
                                if($platformProductAttribute["platform_attribute_value_code"] == $value["id"]){
                                  $selected = "selected";
                                  break;
                                }
                              }
                            }
                          ?>
                          <option <?php echo $selected; ?> value="<?php echo $value["id"]."-".$value["name"]; ?>"><?php echo $value["name"]; ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
              <?php endforeach; ?>
            </div>
          </form>
        </div>
        <div class="card-footer">
          <button form="insertPlatformAttributes" type="submit" class="btn btn-success" name="insertPlatformAttributes">Kaydet</button>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
</div>
