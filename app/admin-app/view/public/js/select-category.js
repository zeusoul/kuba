function selectChange(index) {
    var protocol = window.location.protocol;
    var pathname = window.location.pathname;
    var hostname = window.location.hostname;
    var url = (hostname == "localhost") ? protocol+"//"+hostname+"/"+pathname : protocol+"//"+hostname+""+pathname;
    var id = $("#category-select-"+index).val();
    var category_selects = $(".category-selects");
    for (var i = index; i < category_selects.length; i++) {
      $(".category-selects")[i].remove();
    }
    $.ajax({
      type: "POST",
      url: url,
      data: { id: id },
      success: function(response) {
        var subCategory = JSON.parse(response);
        var options = ``;
        $.each(subCategory, function(indexInArray, valueOfElement) {
          options = options + `<option value="` + valueOfElement['category_id'] + `"> ` + valueOfElement['name'] + `</option>`;
        });
        if(options != ``){
          $("#selects").append(`
            <div class="form-group col-md-3 category-selects">
              <select onchange="selectChange(`+(index+1)+`)" id="category-select-`+(index+1)+`" multiple name="category-`+(index+1)+`" class="form-control" style=" height: 180px;" >
              </select>
            </div>`
          );
          $('#category-select-'+(index+1)).show();
          $('#category-select-'+(index+1)).html(options);
        }
      }
    });
  }
