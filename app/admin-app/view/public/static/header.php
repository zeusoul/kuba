
<!DOCTYPE html>
<html lang="tr">
  <head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="istanbul elektromatik yönetim paneli">

    <?php if(isset($companyInformation["company_header_logo"]) && trim($companyInformation["company_header_logo"]) != ""){ ?>
      <link rel="shortcut icon" href="<?php echo publicUrl("img/".$companyInformation["company_header_logo"]); ?>">
    <?php } else { ?>
      <link rel="shortcut icon" href="<?php echo publicUrl("img/ex_header_logo.png"); ?>">
    <?php } ?>

    <!-- Title Page-->
    <title><?php echo (!isset($title) || trim($title)==null) ? "Yönetim" : $title; ?></title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/font-awesome-4.7/css/font-awesome.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/font-awesome-5/css/fontawesome-all.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/mdi-font/css/material-design-iconic-font.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo publicAdminUrl("vendor/bootstrap-4.1/bootstrap.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo publicAdminUrl("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/wow/animate.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/css-hamburgers/hamburgers.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/slick/slick.css"); ?>" rel="stylesheet" media="all">


    <link href="<?php echo publicAdminUrl("vendor/select2/select2.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/perfect-scrollbar/perfect-scrollbar.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/vector-map/jqvmap.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo publicAdminUrl("css/theme.css"); ?>" rel="stylesheet" media="all">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.23/af-2.3.5/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/cr-1.5.3/fc-3.3.2/fh-3.1.7/kt-2.5.3/r-2.2.7/rg-1.1.2/rr-1.2.7/sc-2.0.3/sb-1.0.1/sp-1.2.2/sl-1.3.1/datatables.min.css"/>




    <!-- Jquery JS-->
    <script src="<?php echo publicAdminUrl("vendor/jquery-3.2.1.min.js")   ?>"></script>
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
    <script src="https://cdn.datatables.net/v/bs4/jq-3.3.1/jszip-2.5.0/dt-1.10.23/af-2.3.5/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/cr-1.5.3/fc-3.3.2/fh-3.1.7/kt-2.5.3/r-2.2.7/rg-1.1.2/rr-1.2.7/sc-2.0.3/sb-1.0.1/sp-1.2.2/sl-1.3.1/datatables.min.js"></script>


    <?php
      $url = explode('/',$_GET["url"]);
      if(count($url) > 2 && $url[1] == "mesaj"){
    ?>
    <link href="<?php echo publicAdminUrl("css/message.css"); ?>" rel="stylesheet" media="all">
    <?php } ?>
  </head>



  <body class="s">
    <div class="page-wrapper">
      <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar2">
          <div class="logo">
            <a href="<?php echo adminUrl(""); ?>">
              <?php if(isset($companyInformation["company_logo"]) && trim($companyInformation["company_logo"]) != ""){ ?>
                  <img src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt="">
              <?php } else { ?>
                <img src="<?php echo publicUrl('img/ex_logo.png'); ?>" alt="">
              <?php } ?>
            </a>
          </div>
          <div class="menu-sidebar2__content js-scrollbar1">

            <nav class="navbar-sidebar2">
              <ul class="list-unstyled navbar__list">

                <li>
                  <a target="_blank" href="<?php echo url("") ?>">
                    <i class="fas fa-share"></i> Siteye git
                  </a>
                </li>
                <li>
                  <a href="<?php echo adminUrl("") ?>">
                    <i class="fas fa-home"></i> Anasayfa
                  </a>
                </li>
                <li>
                  <a href="<?php echo adminUrl("referans-ekle"); ?>">
                    <i class="fas fa-shopping-cart"></i>Referans Ekle
                  </a>
                </li>
                <li>
                  <a href="<?php echo adminUrl("referans-kategori-islemleri"); ?>">
                    <i class="fas fa-shopping-cart"></i>Referans Kategori İslemleri
                  </a>
                </li>
                <li>
                  <a href="<?php echo adminUrl("anlik-sepetler"); ?>">
                    <i class="fas fa-shopping-cart"></i>Anlık Sepetler
                  </a>
                </li>

                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-cogs"></i> Ayarlar
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl('genel-ayarlar'); ?>"><i class="fas fa-cogs"></i> Genel Ayarlar</a>
                    <a href="<?php echo adminUrl("banka-bilgileri"); ?>"><i class="fas fa-bank"></i>Banka Bilgileri</a>
                    <a href="<?php echo adminUrl("sirket-bilgileri"); ?>"><i class="fas fa-info"></i>Şirket Bilgileri</a>
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-th-large" aria-hidden="true"></i> Ürün İşlemleri
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl("urun-islemleri")?>"><i class="fas fa-shopping-cart"></i> Ürün Listesi</a>
                    <a href="<?php echo adminUrl("urun-ekle")?>"><i class="fa fa-plus-circle" aria-hidden="true"></i> Yeni Ürün Ekle</a>
                    <a href="<?php echo adminUrl("urun-yorumlari"); ?>">
                      <i class="fas fa-comment"></i>Ürün Yorumları
                      <?php if ($numberOfComments > 0) { ?>
                        <label style="color:orange"><?php echo "($numberOfComments)"; ?></label>
                      <?php } ?>
                    </a>
                  </div>
                </li>
                <!-- Haberler -->
                <li>
                  <a href="<?php echo adminUrl("haberler"); ?>">
                    <i class="fas fa-newspaper"></i>Haberler
                  </a>
                </li>
                <!-- Haberler -->
                <!-- Kategori işlemleri -->
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-table"></i> Kategori Ayarları
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl("kategori-islemleri"); ?>"><i class="fas fa-table"></i> Kategori İşlemleri</a>
                    <a href="<?php echo adminUrl("filtre-ekle"); ?>"><i class="fas fa-filter"></i> Filtre İşlemleri</a>
                  </div>
                </li>
                <!-- Kategori işlemleri -->
                <!-- Kategori işlemleri -->
                <li class="nav-item dropdown">
                  <a href="<?php echo adminUrl("varyant-islemleri"); ?>">
                    <i class="fa fa-align-right" aria-hidden="true"></i> Varyant İşlemleri
                  </a>
                </li>
                <!-- Kategori işlemleri -->
                <!-- Marka İşlemleri -->
                <li class="nav-item dropdown">
                  <a class="nav-link" href="<?php echo adminUrl("markalar"); ?>">
                    <i class="fas fa-copyright"></i> Marka İşlemleri
                  </a>
                </li>
                <!-- Marka İşlemleri -->
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-user-circle" aria-hidden="true"></i> Müşteri İşlemleri
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl("musteri-listesi"); ?>"><i class="fa fa-users" aria-hidden="true"></i> Müşteriler</a>
                    <a href="<?php echo adminUrl("siparisler"); ?>">
                      <i class="fas fa-truck"></i>Sipariş Yönetimi
                      <?php if ($numberOfOrders > 0) { ?>
                        <label style="color:orange"><?php echo "($numberOfOrders)"; ?></label>
                      <?php } ?>
                    </a>
                    <a href="<?php echo adminUrl('iadeler'); ?>"><i class="fas fa-exchange-alt"></i>İade Talepleri</a>
                    <a href="<?php echo adminUrl("destek-mesaj"); ?>">
                      <i class="fas fa-envelope-open"></i>
                      Destek Mesajı
                      <?php if((int)$newMessageStatus === 1){ ?>
                        <i style="color:orange; font-size:8pt;" class="fas fa-circle"></i>
                      <?php } ?>
                    </a>
                  </div>
                </li>

                <!-- Site Görünümü -->
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-eye" aria-hidden="true"></i> Site Görünümü
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl("sayfa-islemleri"); ?>"><i class="fas fa-file-text"></i> Sayfa işlemleri</a>
                    <a href="<?php echo adminUrl("slider-islemleri"); ?>"><i class="fas fa-images"></i> Slider işlemleri</a>
                    <a href="<?php echo adminUrl("anasayfa-resim"); ?>"><i class="fas fa-photo"></i>Anasayfa resimleri</a>
                  </div>
                </li>
                <!-- Site Görünümü -->
                <!-- Platform İşlemleri -->
                <!-- <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-share-alt-square"></i> Platform İşlemleri
                  </a>
                  <div class="dropdown-menu  bg-dark" aria-labelledby="navbarDropdown">
                    <a href="<?php echo adminUrl("platform-bilgileri"); ?>"><i class="fas fa-info-circle"></i> Platform Bilgileri</a>
                    <a href="<?php echo adminUrl("platform-bilgisi-ekle"); ?>"><i class="fas fa-plus-circle"></i> Platform Bilgisi Ekle</a>
                  </div>
                </li>
              </ul> -->
            </nav>
          </div>
        </div>
      </aside>
      <!-- END MENU SIDEBAR-->
          <!-- PAGE CONTAINER-->
          <div class="page-container2">
              <!-- HEADER DESKTOP-->
              <header class="header-desktop2">
                  <div class="section__content section__content--p30">
                      <div class="container-fluid">
                          <div class="header-wrap2">
                              <div class="logo d-block d-lg-none">
                                  <a href="#">
                                      <img src="images/icon/logo-white.png" alt="CoolAdmin" />
                                  </a>
                              </div>
                              <div class="header-button2">
                                    <div class="header-button-item ">
                                      <a href="<?php echo adminUrl("cikis");?>" class="fa fa-user"></a>
                                    </div>
                                    <div class="header-button-item ">
                                      <a href="<?php echo adminUrl("cikis");?>"  class="fa fa-power-off"></a>
                                    </div>
                                    <div class="header-button-item mr-0 js-sidebar-btn">
                                    <i class="zmdi zmdi-menu"></i>
                                </div>
                                <div class="setting-menu js-right-sidebar d-none d-lg-block">
                                    <div class="account-dropdown__body">
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-account"></i><?php echo $_SESSION[sessionPrefix()."admin_name"]?></a>
                                        </div>
                                        <div class="account-dropdown__item">
                                            <a href="#">
                                                <i class="zmdi zmdi-settings"></i>Setting</a>
                                        </div>
                                    </div>
                                </div>

                              </div>
                          </div>
                      </div>
                  </div>
              </header>
              <aside class="menu-sidebar2 js-right-sidebar d-block d-lg-none">
                  <div class="logo">
                      <a href="#">
                          <img src="images/icon/logo-white.png" alt="Cool Admin" />
                      </a>
                  </div>
                  <div class="menu-sidebar2__content js-scrollbar2">
                      <div class="account2">
                          <div class="image img-cir img-120">
                              <img src="images/icon/avatar-big-01.jpg" alt="John Doe" />
                          </div>
                          <h4 class="name">john doe</h4>
                          <a href="#">Sign out</a>
                      </div>
                      <nav class="navbar-sidebar2">
                          <ul class="list-unstyled navbar__list">
                              <li class="active has-sub">
                                  <a class="js-arrow" href="#">
                                      <i class="fas fa-tachometer-alt"></i>Dashboard
                                      <span class="arrow">
                                          <i class="fas fa-angle-down"></i>
                                      </span>
                                  </a>
                                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                                      <li>
                                          <a href="index.html">
                                              <i class="fas fa-tachometer-alt"></i>Dashboard 1</a>
                                      </li>
                                      <li>
                                          <a href="index2.html">
                                              <i class="fas fa-tachometer-alt"></i>Dashboard 2</a>
                                      </li>
                                      <li>
                                          <a href="index3.html">
                                              <i class="fas fa-tachometer-alt"></i>Dashboard 3</a>
                                      </li>
                                      <li>
                                          <a href="index4.html">
                                              <i class="fas fa-tachometer-alt"></i>Dashboard 4</a>
                                      </li>
                                  </ul>
                              </li>
                              <li>
                                  <a href="inbox.html">
                                      <i class="fas fa-chart-bar"></i>Inbox</a>
                                  <span class="inbox-num">3</span>
                              </li>
                              <li>
                                  <a href="#">
                                      <i class="fas fa-shopping-basket"></i>eCommerce</a>
                              </li>
                              <li class="has-sub">
                                  <a class="js-arrow" href="#">
                                      <i class="fas fa-trophy"></i>Features
                                      <span class="arrow">
                                          <i class="fas fa-angle-down"></i>
                                      </span>
                                  </a>
                                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                                      <li>
                                          <a href="table.html">
                                              <i class="fas fa-table"></i>Tables</a>
                                      </li>
                                      <li>
                                          <a href="form.html">
                                              <i class="far fa-check-square"></i>Forms</a>
                                      </li>
                                      <li>
                                          <a href="calendar.html">
                                              <i class="fas fa-calendar-alt"></i>Calendar</a>
                                      </li>
                                      <li>
                                          <a href="map.html">
                                              <i class="fas fa-map-marker-alt"></i>Maps</a>
                                      </li>
                                  </ul>
                              </li>
                              <li class="has-sub">
                                  <a class="js-arrow" href="#">
                                      <i class="fas fa-copy"></i>Pages
                                      <span class="arrow">
                                          <i class="fas fa-angle-down"></i>
                                      </span>
                                  </a>
                                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                                      <li>
                                          <a href="login.html">
                                              <i class="fas fa-sign-in-alt"></i>Login</a>
                                      </li>
                                      <li>
                                          <a href="register.html">
                                              <i class="fas fa-user"></i>Register</a>
                                      </li>
                                      <li>
                                          <a href="forget-pass.html">
                                              <i class="fas fa-unlock-alt"></i>Forget Password</a>
                                      </li>
                                  </ul>
                              </li>
                              <li class="has-sub">
                                  <a class="js-arrow" href="#">
                                      <i class="fas fa-desktop"></i>UI Elements
                                      <span class="arrow">
                                          <i class="fas fa-angle-down"></i>
                                      </span>
                                  </a>
                                  <ul class="list-unstyled navbar__sub-list js-sub-list">
                                      <li>
                                          <a href="button.html">
                                              <i class="fab fa-flickr"></i>Button</a>
                                      </li>
                                      <li>
                                          <a href="badge.html">
                                              <i class="fas fa-comment-alt"></i>Badges</a>
                                      </li>
                                      <li>
                                          <a href="tab.html">
                                              <i class="far fa-window-maximize"></i>Tabs</a>
                                      </li>
                                      <li>
                                          <a href="card.html">
                                              <i class="far fa-id-card"></i>Cards</a>
                                      </li>
                                      <li>
                                          <a href="alert.html">
                                              <i class="far fa-bell"></i>Alerts</a>
                                      </li>
                                      <li>
                                          <a href="progress-bar.html">
                                              <i class="fas fa-tasks"></i>Progress Bars</a>
                                      </li>
                                      <li>
                                          <a href="modal.html">
                                              <i class="far fa-window-restore"></i>Modals</a>
                                      </li>
                                      <li>
                                          <a href="switch.html">
                                              <i class="fas fa-toggle-on"></i>Switchs</a>
                                      </li>
                                      <li>
                                          <a href="grid.html">
                                              <i class="fas fa-th-large"></i>Grids</a>
                                      </li>
                                      <li>
                                          <a href="fontawesome.html">
                                              <i class="fab fa-font-awesome"></i>FontAwesome</a>
                                      </li>
                                      <li>
                                          <a href="typo.html">
                                              <i class="fas fa-font"></i>Typography</a>
                                      </li>
                                  </ul>
                              </li>
                          </ul>
                      </nav>
                  </div>
              </aside>
              <!-- END HEADER DESKTOP-->


              <!-- MAIN CONTENT-->
              <div class="main-content">
                  <div class="section__content">
                      <div class="container-fluid">
                          <div class="row">
                              <div class="col-lg-12">
                                <div class="recent-report2">
