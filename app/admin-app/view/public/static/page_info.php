
<!-- breadcrumb-area-start -->
<section class="breadcrumb-area" data-background="<?php echo publicUrl("img/bg/page-title.png"); ?>">
  <div class="container">
    <div class="row">
      <div class="col-xl-12">
        <div class="breadcrumb-text">
          <h1><?php echo $pageTitle; ?></h1>
          <br>
          <ul class="breadcrumb-menu">
            <?php if(isset($map) && trim($map != "")){ ?>
              <?php echo $map; ?>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- breadcrumb-area-end -->
<hr class="mb-4">
