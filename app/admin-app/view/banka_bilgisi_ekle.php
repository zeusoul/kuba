<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Banka</strong> Bilgileri Ekle
    </div>
    <div class="card-body card-block">
      <form id="insertForm" action="" method="post" class="form-horizontal" enctype="multipart/form-data">

        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="bank" class=" form-control-label">Banka Logo </label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="bank" name="bank_image">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Banka Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["bank_name"] ?>" name="bank_name" placeholder="Banka adı giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->

        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Banka Şube Kodu *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["branch_code"] ?>" name="branch_code" placeholder="Banka Şube Kodu Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Banka Hesap No *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["account_no"] ?>" name="account_no" placeholder="Banka Hesap No giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->

        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Banka Hesap Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["account_name"] ?>" name="account_name" placeholder="Banka Hesap Adı giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->

        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Banka IBAN No *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["iban"] ?>" name="iban" placeholder="Banka IBAN No giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->


      </form>
    </div>
    <div class="card-footer">
      <button form="insertForm" type="submit" name="insertBank" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
</div>
