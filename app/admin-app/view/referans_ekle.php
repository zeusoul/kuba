<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <h2>Referans Ekle</h2>
    <hr class="mb-4">
  <a href="<?php echo adminUrl("referans-kategori-sec/referan-ekle"); ?>" class="btn">Referans Kategorisini Değiştir</a>
  <div class="card">
    <div class="card-header">
      <strong>Referans</strong> Bilgileri
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="selectedCategory" class=" form-control-label">Seçilen İl Adı</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $selectedReferenceCategory["reference_category_name"]; ?>
            <input type="hidden" id="selectedReferenceCategory" name="referenceCategoryId" value="<?php echo $selectedReferenceCategoryId; ?>" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz (Zorunlu)</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="reference_image" name="reference_image">
          </div>
        </div>
        <!-- input -->
           <!-- input -->
           <div class="row form-group">
          <div class="col col-md-3">
            <label for="referenceMail" class=" form-control-label">Temsilci'nin Adı Soyadı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="referenceName" value="<?php echo $_POST["referenceName"]; ?>" name="referenceName" placeholder="Temsilci'nin Adı Ve Soyadı" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="referencePhone" class=" form-control-label">Temsilci Telefon Numarası *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="referencePhone" value="<?php echo $_POST["referencePhone"]; ?>" name="referencePhone" placeholder="Temsilci Telefon Numarası Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
           <!-- input -->
           <div class="row form-group">
          <div class="col col-md-3">
            <label for="referenceMail" class=" form-control-label">Temsilci Mail Adresi *</label>
          </div>
          <div class="col-12 col-md-9"> 
            <input type="text" id="referenceMail" value="<?php echo $_POST["referenceMail"]; ?>" name="referenceMail" placeholder="Temsilci Mail Adresi Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="referenceRole" class=" form-control-label">Temsilci Rolü (Görevi) *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="referenceRole" value="<?php echo $_POST["referenceRole"]; ?>" name="referenceRole" placeholder="Temsilci Rolü (Görevi)" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="referenceAddress" class=" form-control-label">Temsilci Adresi *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="referenceAddress" value="<?php echo $_POST["referenceAddress"]; ?>" name="referenceAddress" placeholder="Temsilci Adresi Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="referenceAddress" class=" form-control-label">Temsilci Adresi *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="referenceAddress" value="<?php echo $_POST["referenceAddress"]; ?>" name="referenceAddress" placeholder="Temsilci Adresi Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- select -->
        <div class="card-footer">
          <button type="submit" name="insertReference" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Ekle
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
