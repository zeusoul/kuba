<?php if(isset($pageMessage) && trim($pageMessage) != ""):?>
  <div class="alert alert-info" role="alert">
    <?php echo $pageMessage; ?>
  </div>
<?php endif;?>
<form class="" action="" method="post">
  <div class="row form-group">
  <div class="col col-md-6">
    <p>Filtre Adı</p>
    <input type="text" class="form-control" name="filterName" value="" required>
  </div>
    <div class="col col-md-2">
      <input class="btn btn-success mt-4" type="submit" name="insertFilter" value="EKLE" />
    </div>
  </div>
</form>


<div class="vue-lists">
  <?php if(count($filters) > 0) { ?>
    <ul>
      <?php foreach ($filters as $key => $filter): ?>
        <li>
          <form class="" action="" method="post">
            <div class="row form-group">
              <div class="col col-md-12">
                  <div class="input-group">
                      <div class="col">
                        <input class="form-control" type="text" name="filterName" value="<?php echo  $filter["name"]; ?> ">
                        <input type="hidden" name="filterId" value="<?php echo $filter["filter_id"]; ?>">
                      </div>
                      <div class="col">
                        <input class="btn btn-primary" type="submit" name="updateFilter" value="Güncelle">
                        <a href="<?php echo adminUrl("filtre-deger-ekle/".$filter["filter_id"])?>" class="btn btn-dark" type="submit" name="" value="Filtre değerleri">Filtre Değerleri</a>
                        <input class="btn btn-danger" type="submit" name="deleteFilter" value="Sil">
                      </div>
                  </div>
              </div>
            </div>
          </form>
        </li>
      <?php endforeach; ?>
    </ul>
<?php } else { ?>
  <p>Kayıtlı Filtre Bulunamadı.</p>
<?php } ?>
</div>
