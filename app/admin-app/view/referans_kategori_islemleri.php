<h2>Referans Kategori İşlemleri</h2>
<hr class="mb-4">
<?php if(isset($pageMessage) && trim($pageMessage)):?>
<div class="alert alert-info" role="alert">
  <?php echo $pageMessage; ?>
</div>
<?php endif;?>
<form class="" action="" method="post">
  <h4 class="mb-4">Referans Kategori Ekle</h4>
        <div class="row form-group">
            <div class="col col-md-6">
                <p>Referans Kategori İsmi</p>
                <input type="text" class="form-control" name="referenceCategoryName" value="" required>
            </div>

            <div class="col col-md-2">
            <input class="btn btn-success mt-4" type="submit" name="referenceInsertCategory" value="EKLE" />
            </div>
        </div>
</form>

    <hr class="mt-5 mb-5">
<div class="vue-lists mt-4">
<h4>Oluşturulan Referans Kategorileri</h4>
<br>
  <?php
    printMenu($menu);
    function printMenu($items){ ?>
      <ul>
      <?php foreach ($items as $item) {
        $referenceCategoryId = $item["reference_category_id"];
        $referenceCategoryName = $item["reference_category_name"];
      ?>
        <li>
          <form action='' method='post'>
            <div class='row form-group'>
              <div class='col col-md-6'>
                <input type='text' class='form-control' name='referenceCategoryName' value='<?php echo $item["reference_category_name"]; ?>' required />
                <input type='hidden' name='referenceCategoryId' value='<?php echo $item["reference_category_id"]; ?>' />
              </div>
              <div class='col col-md-6'>
                <input class='btn btn-success' type='submit' name='updateReferenceCategory' value='Düzenle'/>
                <input class='btn btn-danger' type='submit' name='deleteReferenceCategory' value='Sil'/>
              </div>
            </div>
            </form>
        </li>
        <?php } ?>
      </ul>
      <?php } ?>
</div>
