<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Slider </strong>Bilgileri
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz</label>
          </div>
          <div class="col col-md-9">
            <img width="150" src="<?php echo publicUrl("img/slider-images/".$sliderDetails["slider_image"]); ?>" alt="">
            <input type="file" id="image" name="image">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="year" class=" form-control-label">Yıl</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="year" value="<?php echo $sliderDetails["slider_year"]; ?>" name="year" placeholder="Yıl Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Başlık</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $sliderDetails["slider_title"]; ?>" name="title" placeholder="Başlık Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="description" class=" form-control-label">Açıklama</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="description" value="<?php echo $sliderDetails["slider_description"]; ?>" name="description" placeholder="Açıklama Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="url" class=" form-control-label">Yönlendirilecek Sayfa</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="url" value="<?php echo $sliderDetails["slider_url"]; ?>" name="url" placeholder="www.orneksayfa.com" class="form-control" >
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="p4ice" class=" form-control-label">Gösterilecek Fiyat Bilgisi</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="price" value="<?php echo $sliderDetails["slider_price"]; ?>" name="price" placeholder="$29" class="form-control" >
          </div>
        </div>
        <!-- input -->
        <div class="card-footer">
          <button type="submit" name="updateSlider" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Güncelle
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
