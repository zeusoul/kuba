<div class="row">
  <div class="col-md-12">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <!-- DATA TABLE -->
    <h2>Değerlendirmeler</h2>
    <hr class="mb-4">

    <div class="table-responsive table-responsive-data2">
      <hr>
      <p class="text-center">
        <?php if(count($eva) > 0){ ?>
          Toplam <?php echo count($eva); ?> Değerlendirmeler Arasından Gösterilen Satır : <?php echo $offset."-".$limit; ?>
        <?php } ?>
      </p>
      <hr>

      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>">Previous</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">Next</a>
        </li>
      </ul>

      <table class="table table-data2">
        <thead>
          <tr>
            <th>ID</th>
            <th>Yazar</th>
            <th>Hizmet Gördüğü <br> Departman</th>
            <th>İçerik</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($eva) || !is_array($eva) || count($eva) <= 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Kayıtlı Değerlendirme Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php foreach ($eva as $key => $evas) { ?>
              <?php
                if($key < $offset) {continue;}
                else if($key >= $limit) {break;}
               ?>
              <tr class="tr-shadow">
                <td><?php echo $evas["evaluate_id"]; ?></td>
                <td class="desc">
                  <?php echo $evas["eva_name"]; ?>
                </td>
                <td>
                  <?php echo kisalt($evas["eva_service"],20); ?>
                </td>
                <td>
                  <?php echo kisalt($evas["content"],20); ?>
                </td>
                <td>
                  <div class="table-data-feature">
                    <form class="" action="" method="post">
                      <input type="hidden" name="eva_id" value="<?php echo $evas["evaluate_id"]; ?>">
                      <button type="submit" name="delete_eva" class="item btn "><i class="zmdi zmdi-delete"></i></button>
                    </form>
                  </div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>
      <hr>
      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>">Previous</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">Next</a>
        </li>
      </ul>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
