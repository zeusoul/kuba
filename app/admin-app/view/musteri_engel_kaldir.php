

<div class="card">
    <div class="card-header">
        Hesap <strong>Engel Kaldır</strong> 
    </div>
    <div class="card-body card-block">
        <form action="" method="post" class="form-horizontal">
            <div class="row form-group">
                <div class="col col-md-3">
                    <label  class=" form-control-label">Engellenen Hesap</label>
                </div>
                <div class="col-12 col-md-9">
                    <label  class=" form-control-label"><strong><?php echo $userDetails["name"]." ". $userDetails["surname"] ?></strong></label>
                    <input type="hidden" name="userId" value="<?php echo $userDetails["user_id"] ?>">
                </div>
                </div>
                <div class="row form-group">
                    <div class="col ">
                        <label  class=" form-control-label"><strong>Hesabın Engelini Kaldırmak İstediğinizden Emin Misiniz?</strong></label>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" name="engelle" class="btn btn-primary btn-sm">
                    <i class="fa fa-dot-circle-o"></i> Engel Kaldır
                </button>
                <button   name="vazgec" class="btn btn-danger btn-sm">
                    <i class="fa fa-ban"></i> Vazgeç
                </button>
            </div>
    </form>
</div>

