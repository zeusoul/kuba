
<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
        <form class="" action="" method="post">
          <div class="row">
            <div class="col-md-4">
              <input type="text" class="form-control" name="brand_name" value="" placeholder="Marka giriniz" required>
            </div>
            <div class="col-md-2">
              <button type="submit" name="insertBrand" class="au-btn au-btn-icon au-btn--green au-btn--small">
                <i class="zmdi zmdi-plus"></i> Ekle
              </button>
            </div>
          </div>
        </form>
      </div>
      <hr>
      <?php if(isset($pageMessage)){ ?>
        <div class="alert alert-info" role="alert">
          <?php echo $pageMessage; ?>
        </div>
      <?php } ?>
      <?php if(!isset($brands) || !is_array($brands) || count($brands) == 0){ ?>
        <tr class="tr-shadow">
          <td colspan="5" class="desc">Kayıt Bulunamadı</td>
        </tr>
      <?php } else { ?>
        <table class="table table-data2">
          <thead>
            <tr>
              <th>#</th>
              <th>Marka</th>
              <th>İşlemler</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 1; ?>
            <?php foreach ($brands as $key => $brand) { ?>
              <tr class="tr-shadow">
                <td><?php echo $i++; ?></td>
                <td>
                  <?php echo $brand["brand_name"]; ?>
                </td>
                <td>
                  <form class="" action="" method="post">
                    <input type="hidden" name="brand_id" value="<?php echo $brand["brand_id"]; ?>">
                    <button type="submit" name="deleteBrand" class="btn btn-danger">Sil</button>
                    <a class="btn btn-secondary" href="<?php echo adminUrl("marka-eslestir?id=".$brand["brand_id"]); ?>">Marka Eşleştir</a>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
