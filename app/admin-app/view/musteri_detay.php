
<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>

  <div class="card">
    <div class="card-header">
      <strong>Müşteri</strong> Bilgileri
    </div>
    <div class="card-body card-block">
      <?php if(count($userDetails) <= 0) { ?>
        <h3>OOPS!</h3>
        <p>Müşteri Bilgileri Bulunamadı!</p>
      <?php } else{ ?>
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Müşteri Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $userDetails["name"]; ?>
          </div>
        </div>
        <!-- input -->

        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Müşteri Soyadı *</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $userDetails["surname"]; ?>
          </div>
        </div>
        <!-- input -->
         <!-- input -->
         <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Müşteri Mail *</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $userDetails["email"]; ?>
            <?php if((int)$userDetails["confirm"] == 0) {?>
              <p style="color:red">(Doğrulanmadı)</p>
            <?php } else {?>
              <p style="color:green">(Doğrulandı)</p>
            <?php } ?>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
         <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Müşteri Telefon *</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $userDetails["tel"]; ?>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Müşteri Engel Durumu *</label>
          </div>
          <div class="col-12 col-md-9">
            <?php if((int)$userDetails["status"] == 1) { ?>
                  <span class="status--process">
                    Erişime Açık
                  </span>
                  <a  href="<?php echo adminUrl("musteri-engelle/".$userDetails["user_id"]."/musteri-detay") ?>" class="btn btn-danger btn-sm  "> Engelle  </a>
            <?php } else { ?>
                  <span class="status--process" style="color:red;">
                    Hesap Engellenmiş
                  </span>
                  <a  href="<?php echo adminUrl("musteri-engel-kaldir/".$userDetails["user_id"]."/musteri-detay") ?>" class="btn btn-primary btn-sm  "> Engel Kaldır  </a>
            <?php } ?>
          </div>
        </div>
        <!-- input -->
          <hr>
        <!-- sepet -->
        <h5 class="mb-3">Anlık Sepet</h5>
        <!-- sepet -->
        <?php if(!isset($cart["cart"]) || !is_array($cart["cart"]) || count($cart["cart"]) <= 0){ ?>
          <tr class="tr-shadow">
            <td colspan="5" class="desc">Sepet Bulunamadı</td>
          </tr>
        <?php } else { ?>
          <tr class="tr-shadow">
            <td>
              <div class="row form-group">
                <div class="col col-md-3">Toplam Ürün Sayısı</div>
                <div class="col col-md-1"> : </div>
                <div class="col col-md-8"><strong> <?php echo $cart["cart_count"]." Adet Ürün"; ?></strong></div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">Toplam Fiyat</div>
                <div class="col col-md-1"> : </div>
                <div class="col col-md-8"><strong> <?php echo $cart["total_price"]." TL (KDV + Kargo Hariç)"; ?></strong></div>
              </div>
              <div class="row form-group">
                <div class="col col-md-3">Son Güncelleme</div>
                <div class="col col-md-1"> : </div>
                <div class="col col-md-8"><strong> <?php echo $cart["date_of_update"]; ?></strong></div>
              </div>
            </td>
          </tr>
         <tr>
          <td colspan="4" >
            <div>
              <div class="card card-body">
                <table class="table table-data2">
                  <thead>
                    <tr>
                      <th>Başlık</th>
                      <th>Ürün ID</th>
                      <th>Fiyat</th>
                      <th>Adet</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($cart["cart"] as $key => $product) {
                      $productDetails = $product["product_details"];
                    ?>
                    <tr class="tr-shadow">
                        <td class="desc">
                          <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["product_id"]); ?>"><?php echo $productDetails["title"]; ?></a>
                        </td>
                        <td>
                          <?php echo $product["product_id"]; ?>
                        </td>
                        <td>
                          <?php echo $productDetails["discount_price"]." ".$productDetails["currency"]; ?>
                        </td>
                        <td> <?php echo $product["quantity"]; ?> Adet</td>
                    </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </td>
         </tr>
          <?php } ?>
        <?php } ?>
        <!-- ADRESLER -->
        <hr>
        <h5 class="mb-3">Adresler</h5>
          <?php if(!is_array($adresBilgileri) || count($adresBilgileri) == 0){ ?>
            <p>Kayıtlı Adres Bilgisi Bulunamadı.</p>
          <?php } else { ?>
            <?php  foreach ($adresBilgileri as $value ):?>
              <div class="adress col-md-12 mt-2">
                <div class="card" >
                  <div class="card-body">
                    <h5 class="card-title"><?php echo $value['title'] ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted"><?php echo $value['name_surname']; ?></h6>
                    <p class="card-text"><?php echo $value['tel']; ?></p>
                    <p class="card-text"><?php echo $value['county']." / ".$value["city"]; ?></p>
                    <h6 class="card-text " ><?php echo $value['address'] ?></h6>
                  </div>
                </div>
              </div>
            <?php endforeach;?>
          <?php } ?>
          <hr>
        <h5 class="mb-3">Siparişler</h5>
        <?php if(!isset($orders) || !is_array($orders) || count($orders) <= 0){ ?>
          <tr class="tr-shadow">
            <td colspan="5" class="desc">Sipariş Bulunamadı</td>
          </tr>
          <?php } else { ?>
        <!-- Siparişler -->
          <table class="table table-data2">
            <thead>
              <tr>
                <th>Sipariş Tarihi</th>
                <th>Ürün Adeti</th>
                <th>Sipariş Tutarı</th>
                <th>Sipariş Durumu</th>
                <th>Kargo Şirketi</th>
                <th>Kargo Takip No</th>
                <th>Detay</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $key => $order) { ?>
              <tr class="tr-shadow">
                <td><?php echo $order["order_date"]; ?></td>
                <td><?php echo $order["count_cart"]; ?> ₺</td>
                <td><?php echo $order["order_amount"]; ?> ₺</td>
                <td>
                  <?php if($order["status"] == -1){ ?>
                    <span class="badge badge-danger">
                       İptal Edildi
                    </span>
                  <?php } else if($order["status"] == 0){ ?>
                    <span class="badge badge-primary">
                       Onay Bekliyor
                    </span>
                  <?php } else if($order["status"] == 1){ ?>
                    <span class="badge badge-success">
                      Onaylandı
                    </span>
                  <?php } else if($order["status"] == 2){ ?>
                    <span class="badge badge-success">
                      Kargoya Verildi
                    </span>
                  <?php } else if($order["status"] == 3){ ?>
                    <span class="badge badge-success">
                      Teslim Edildi
                    </span>
                  <?php } ?>
                </td>
                <td><?php echo $order["cargo_company"]; ?></td>
                <td><?php echo $order["cargo_no"]; ?></td>
                <!-- işlemler -->
                <td>
                  <div class="table-data-feature">
                    <a target="" href="<?php echo adminUrl("siparis-detay/".$order["order_no"]) ?>" class="item  "> <i class="zmdi zmdi-eye"></i>  </a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          </table>
        <?php } ?>
        <hr>
    </div>
  </div>
</div>
