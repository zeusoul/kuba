<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right"></div>
        <table class="table table-data2">
          <thead>
            <tr>
              <th>Müşteri Adı</th>
              <th>Müşteri Telefon</th>
              <th>Sepet Tutarı (KDV Hariç)</th>
              <th>Ürün Sayısı</th>
              <th>Son Güncelleme Tarihi</th>
              <th>Ürünler</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!isset($carts) || !is_array($carts) || count($carts) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Sepet Bulunamadı</td>
              </tr>
            <?php } else { $i=0; ?>
              <?php foreach ($carts as $key => $cart) { ?>
                <tr class="tr-shadow">
                  <td class="desc">
                    <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$cart["user_id"]); ?>"> <?php echo $cart["name"]." ".$cart["surname"]; ?></a>
                  </td>
                  <td>
                    <a href="tel:<?php echo $cart["tel"]; ?>"><?php echo $cart["tel"]; ?></a>
                  </td>
                  <td>
                     <?php echo $cart["total_price"]." TL"; ?>
                  </td>
                  <td>
                    <?php echo $cart["cart_count"]." Adet Ürün"; ?>
                  </td>
                  <td>
                    <?php echo $cart["date_of_update"]; ?>
                  </td>
                  <!-- işlemler -->
                  <td>
                    <div class="table-data-feature">
                      <a  href="javascript:;"  class="item  " data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="false">
                        <i class="zmdi zmdi-shopping-cart"></i>
                      </a>
                    </div>
                  </td>
                </tr>
               <tr>
                <td colspan="4" >
                <div class="collapse" id="collapse<?php echo $i++; ?>">
                  <div class="card card-body">
                    <table class="table table-data2">
                      <thead>
                        <tr>
                          <th>Başlık</th>
                          <th>Ürün Barkod</th>
                          <th>Fiyat</th>
                          <th>Adet</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php if(!isset($cart["cart"]) || !is_array($cart["cart"]) || count($cart["cart"]) <= 0){ ?>
                          <tr class="tr-shadow">
                          <td colspan="5" class="desc">Ürün Bulunamadı</td>
                          </tr>
                      <?php } else { ?>
                          <?php foreach ($cart["cart"] as $key => $product) {
                            $productDetails = $product["product_details"];
                          ?>
                            <tr class="tr-shadow">
                              <td class="desc">
                                <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>"><?php echo $productDetails["title"]; ?></a>
                              </td>
                              <td>
                                <?php echo $product["barcode"]; ?>
                              </td>
                              <td>
                                <?php echo $productDetails["discount_price"]." ".$productDetails["currency"]; ?>
                              </td>
                              <td> <?php echo $product["quantity"]; ?> Adet</td>
                            </tr>
                          <?php } ?>
                      <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </td>
           </tr>
         <?php } ?>
      <?php } ?>
    </tbody>
    </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
