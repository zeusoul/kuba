<div class="row">
  <div class="col-md-12">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <br/>
    <!--Email Doğrulama -->
    <form class="" action="" method="post">
      <h4>Site Görünümü Ayarı</h4>
      <hr>
      <div class="row form-group">
        <div class="col col-md-2">
          Yayın Durumu :
        </div>
        <div class="col col-md-2">
          <?php
            if((int)$settings["site_yayin_durumu"] == 0) echo "<p style='color:red;'>Yapım aşamasında</p>";
            else echo "<p style='color:green;'>Yayında</p>";
          ?>
        </div>
        <div class="col col-md-6">
          <select class="form-control" name="site_yayin_durumu" required>
            <option value="2" selected>Seçiniz..</option>
            <option value="1">Yayında</option>
            <option value="0">Yapım Aşamasında</option>
          </select>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="update_yayin_durumu" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Email Doğrulama -->

    <!--Email Doğrulama -->
    <form class="" action="" method="post">
      <h4>E-Mail Doğrulama</h4>
      <hr>
      <div class="row form-group">
        <div class="col col-md-2">
          Durum :
        </div>
        <div class="col col-md-2">
          <?php
            if((int)$settings["email_dogrulama"] == 0) echo "<p style='color:red;'>Kapalı</p>";
            else echo "<p style='color:green;'>Açık</p>";
          ?>
        </div>
        <div class="col col-md-6">
          <select class="form-control" name="email_dogrulama" required>
            <option value="2" selected>Seçiniz..</option>
            <option value="1">Açık</option>
            <option value="0">Kapalı</option>
          </select>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateEmailDogrulama" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Email Doğrulama -->
    <!--Kargo Bilgileri -->
    <h4>Kargo Bilgileri</h4>
    <hr>
    <!--Kargo Ücreti-->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-6">
          Kargo Ücreti :
        </div>
        <div class="col col-md-2">
          <?php
            if((int)$settings["kargo_ucreti"] == 0) echo "<p style='color:green;'>Ücretsiz</p>";
            else echo "<p style='color:blue;'>".$settings["kargo_ucreti"]." TL</p>";
          ?>
        </div>
        <div class="col col-md-2">
          <input class="form-control" type="number" name="kargo_ucreti" value="<?php echo (int)$settings["kargo_ucreti"]; ?>">
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateKargoUcreti" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Kargo Ücreti-->
    <!--Kargo Ücreti Sınırı-->
    <form class="" action="" method="post">
      <hr>
      <div class="row form-group">
        <div class="col col-md-6">
          Ücretsiz Kargo İçin Minimum Fiyat Sınırı <BR>
          (Ücretsiz kargo istemiyorsanız 0 giriniz)
        </div>
        <div class="col col-md-2">
          <?php
            echo "<p style='color:blue;'>".(int)$settings["ucretsiz_kargo_siniri"]." TL</p>";
          ?>
        </div>
        <div class="col col-md-2">
          <input class="form-control" type="number" name="ucretsiz_kargo_siniri" value="<?php echo (int)$settings["ucretsiz_kargo_siniri"]; ?>">
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateUcretsizKargoSiniri" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Kargo Ücreti Sınırı-->
    <!--Kargo Bilgileri -->
  </div>
</div>
