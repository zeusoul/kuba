<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-body card-block">
      <p>Market Adı : <?php echo $platformInfo["market_name"]; ?></p>
      <p>Platform : <?php echo $platformInfo["platform_name"]; ?></p>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>API</strong> Bilgisi Ekle
    </div>
    <div class="card-body card-block">
      <form id="updateApi" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <?php if($platformInfo["platform_id"] == "1"){ ?>
          <!-- TRENDYOL -->
            <!-- input -->
            <div class="row form-group">
              <div class="col col-md-2">
                <label for="username" class=" form-control-label">API Key *</label>
              </div>
              <div class="col-12 col-md-9">
                <input type="text" id="username" value="<?php echo $platformInfo["key1"] ?>" name="username" placeholder="username" class="form-control" required>
              </div>
            </div>
            <!-- input -->
            <!-- input -->
            <div class="row form-group">
              <div class="col col-md-2">
                <label for="password" class=" form-control-label">API Secret *</label>
              </div>
              <div class="col-12 col-md-9">
                <input type="text" id="password" value="<?php echo $platformInfo["key2"] ?>" name="password" placeholder="password" class="form-control" required>
              </div>
            </div>
            <!-- input -->
            <!-- input -->
            <div class="row form-group">
              <div class="col col-md-2">
                <label for="supplier_id" class=" form-control-label">Supplier ID *</label>
              </div>
              <div class="col-12 col-md-9">
                <input type="text" id="supplier_id" value="<?php echo $platformInfo["key3"] ?>" name="supplier_id" placeholder="supplier_id" class="form-control" required>
              </div>
            </div>
            <!-- input -->
          <!-- TRENDYOL -->
        <?php } else if($platformInfo["platform_id"] == "2"){ ?>
          <!-- Hepsiburada -->
          <!-- input -->
          <div class="row form-group">
            <div class="col col-md-2">
              <label for="username" class=" form-control-label">Kullanıcı Adı *</label>
            </div>
            <div class="col-12 col-md-9">
              <input type="text" id="username" value="<?php echo $platformInfo["key1"] ?>" name="username" placeholder="username" class="form-control" required>
            </div>
          </div>
          <!-- input -->
          <!-- input -->
          <div class="row form-group">
            <div class="col col-md-2">
              <label for="password" class=" form-control-label">Şifre *</label>
            </div>
            <div class="col-12 col-md-9">
              <input type="text" id="password" value="<?php echo $platformInfo["key2"] ?>" name="password" placeholder="password" class="form-control" required>
            </div>
          </div>
          <!-- input -->
          <!-- input -->
          <div class="row form-group">
            <div class="col col-md-2">
              <label for="supplier_id" class=" form-control-label">Tedarikçi ID *</label>
            </div>
            <div class="col-12 col-md-9">
              <input type="text" id="supplier_id" value="<?php echo $platformInfo["key3"] ?>" name="supplier_id" placeholder="supplier_id" class="form-control" required>
            </div>
          </div>
          <!-- input -->
          <!-- Hepsiburada -->
        <?php } ?>
      </form>
    </div>
    <div class="card-footer">
      <button form="updateApi" type="submit" name="updateApi" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
</div>
