<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Kategori</strong> Düzenle
    </div>
    <div class="card-body card-block">
      <form action="" id="updateCategoryForm" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col-md-2">
            <label for="" class=" form-control-label">Kategori Resmi</label>
          </div>
          <div class="col-md-2">
            <img width="200" src="<?php echo publicUrl("img/category-images/".$category["category_image"]); ?>" alt="">
          </div>
          <div class="col col-md-3">
            <input type="file" class="form-control" name="category_image" value="">
          </div>
          <div class="col-md-2">
            <label for="" class=" form-control-label">Kategori Adı</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="category_name" value="<?php echo $category["name"]; ?>" required>
          </div>
        </div>
        <!-- input -->
      </form>
    </div>
    <div class="card-footer">
      <button type="submit" form="updateCategoryForm" name="updateCategory" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Güncelle
      </button>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>Alt</strong> Kategoriler
    </div>
    <div class="card-body card-block">
      <?php if(count($category["sub"]) == 0){ ?>
        <p>Alt kategori bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>Kategori Adı</th>
            <th>Kategori Resmi</th>
            <th>Düzenle</th>
            <th>Sil</th>
          </thead>
          <?php foreach ($category["sub"] as $key => $sub): ?>
            <tr>
              <td><?php echo $sub["name"]; ?></td>
              <td>
                <?php if($sub["category_image"] != ""){ ?>
                  <img width="100" src="<?php echo publicUrl("img/category-images/".$sub["category_image"]); ?>" alt="category_image">
                <?php } else{ ?>
                  <p>Resim yok</p>
                <?php } ?>
              </td>
              <td>
                <a class="btn btn-success" href="<?php echo adminUrl("kategori-duzenle?id=".$sub["category_id"]); ?>">Düzenle</a>
              </td>
              <td>
                <form class="" action="" method="post">
                  <button type="submit" class="btn btn-danger" name="delete_category">Sil</button>
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      <?php } ?>
    </div>
  </div>
</div>
