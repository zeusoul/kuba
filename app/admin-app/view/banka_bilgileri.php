<div class="row">
  <div class="col-md-12">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <!-- DATA TABLE -->
    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
        <a href="<?php echo adminUrl("banka-bilgisi-ekle"); ?>" class="au-btn au-btn-icon au-btn--green au-btn--small">
          <i class="zmdi zmdi-plus"></i>Yeni Banka Bilgisi Ekle
        </a>
      </div>
      <table class="table table-data2">
        <thead>
          <tr>
            <th>Banka</th>
            <th>Hesap Adı</th>
            <th>Şube Kodu</th>
            <th>Hesap No</th>
            <th>Iban No</th>
            <th>İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($bankInformations) || !is_array($bankInformations) || count($bankInformations) <= 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Banka Bilgileri Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php foreach ($bankInformations as $key => $bankInformation) { ?>
              <form class="" action="" method="post">
                <tr class="tr-shadow">
                  <td class="desc">
                    <?php echo $bankInformation["bank_name"]; ?>
                  </td>
                  <td>
                    <?php echo $bankInformation["account_name"]; ?>
                  </td>
                  <td>
                    <?php echo $bankInformation["branch_code"]; ?>
                  </td>
                  <td>
                    <?php echo $bankInformation["account_no"]; ?>
                  </td>
                  <td><?php echo $bankInformation["iban_no"]; ?></td>
                  <td>
                    <div class="table-data-feature">
                      <input type="hidden" name="bank_info_id" value="<?php echo $bankInformation["bank_info_id"]; ?>" required>
                      <button type="submit" class="item btn" name="deleteBank"><i class="zmdi zmdi-delete"></i></button>
                    </div>
                  </td>
                </tr>
              </form>
            <?php } ?>
          <?php } ?>


        </tbody>
      </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
