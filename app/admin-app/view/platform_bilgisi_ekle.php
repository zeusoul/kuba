<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Platform Bilgisi</strong> Ekle
    </div>
    <div class="card-body card-block">
      <form id="insertForm" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="market_namef" class=" form-control-label">Market Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="market_name" value="<?php echo $_POST["market_name"]; ?>" name="market_name" placeholder="Market Adı" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- select -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="platform_id" class=" form-control-label">Platform *</label>
          </div>
          <div class="col-12 col-md-9">
            <select class="form-control" name="platform_id" required>
              <?php foreach($platforms as $platform){ ?>
                <option value="" selected>Seçiniz</option>
                <option value="<?php echo $platform["platform_id"]; ?>"><?php echo $platform["platform_name"]; ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <!-- select -->
      </form>
    </div>
    <div class="card-footer">
      <button form="insertForm" type="submit" name="insertPlatformInfo" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
</div>
