<div class="row">
  <div class="col-md-12">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <br/>
    <!--Şirket Adı -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Şirket Adı</h4>
        </div>
        <div class="col col-md-8">
          <input class="form-control" type="text" name="company_name" value="<?php echo $companyInformation["company_name"]; ?>" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyName" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Adı -->

    <!--Şirket Telefon Numarası -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Telefon Numarası</h4>
        </div>
        <div class="col col-md-8">
          <input class="form-control" type="text" name="company_tel" value="<?php echo $companyInformation["company_tel"]; ?>" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyTel" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Telefon Numarası  -->

    <!--Şirket Wp Numarası -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Whatsapp Numarası</h4>
        </div>
        <div class="col col-md-8">
          <input class="form-control" type="text" name="company_wp" value="<?php echo $companyInformation["company_wp"]; ?>" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyWp" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Wp Numarası  -->

    <!--Şirket Email Adresi -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Email Adresi</h4>
        </div>
        <div class="col col-md-8">
          <input class="form-control" type="text" name="company_email" value="<?php echo $companyInformation["company_email"]; ?>" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyEmail" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Email Adresi  -->

    <!--Şirket Logo -->
    <form class="" action="" method="post" enctype="multipart/form-data">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Şirket <br>Logosu</h4>
        </div>
        <div class="col col-md-2">
          <?php if(isset($companyInformation["company_logo"]) && trim($companyInformation["company_logo"]) != ""){ ?>
            <img src="<?php echo publicUrl("img/".$companyInformation["company_logo"]); ?>" alt="" width="100px">
          <?php } else { ?>
            <img src="<?php echo publicUrl("img/ex_logo.png"); ?>" alt="" width="100px">
          <?php } ?>
        </div>
        <div class="col col-md-6">
          <input class="form-control" type="file" name="company_logo" value="" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyLogo" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Logo  -->

    <!--Şirket Başlık Logo -->
    <form class="" action="" method="post" enctype="multipart/form-data">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Site Başlığı İçin Logo</h4>
        </div>
        <div class="col col-md-2">
          <?php if(isset($companyInformation["company_header_logo"]) && trim($companyInformation["company_header_logo"]) != ""){ ?>
            <img src="<?php echo publicUrl("img/".$companyInformation["company_header_logo"]); ?>" alt="" width="100px">
          <?php } else { ?>
            <img src="<?php echo publicUrl("img/ex_header_logo.png"); ?>" alt="" width="100px">
          <?php } ?>
        </div>
        <div class="col col-md-6">
          <input class="form-control" type="file" name="company_header_logo" value="" required>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyHeaderLogo" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Başlık Logo  -->

    <!--Şirket Adresi -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Adres</h4>
        </div>
        <div class="col col-md-8">
          <textarea class="form-control" name="company_address" rows="4" cols="80" required><?php echo $companyInformation["company_address"]; ?></textarea>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyAddress" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Adresi  -->

    <!--Şirket Hakkında -->
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-2">
          <h4>Şirket Hakkında</h4>
        </div>
        <div class="col col-md-8">
          <textarea class="form-control" name="company_about" rows="4" cols="80" required><?php echo $companyInformation["company_about"]; ?></textarea>
        </div>
        <div class="col col-md-2">
          <input type="submit" name="updateCompanyAbout" class="btn btn-secondary" value="Güncelle">
        </div>
      </div>
    </form>
    <!--Şirket Adresi  -->

  </div>
</div>
