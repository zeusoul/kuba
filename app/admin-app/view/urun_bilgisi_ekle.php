
<?php if(isset($_GET["success"])) {?>
  <div class="alert alert-info" role="alert">
    Ürün Başarıyla Eklendi
  </div>
<?php } else if(isset($_GET["updateSuccess"])) {?>
  <div class="alert alert-info" role="alert">
    Ürün Başarıyla Güncellendi
  </div>
<?php } ?>

<p> <b>Seçilen Ürün :</b>
  <a target="_blank" href="<?php echo url(seoUrl($productDetails["title"])."-p-".$productDetails["product_id"]); ?>">
    <?php echo $productDetails["title"]; ?>
  </a>
</p>
<hr>
<?php if(isset($pageMessage) && trim($pageMessage) != ""):?>
  <div class="alert alert-info" role="alert">
    <?php echo $pageMessage; ?>
  </div>
<?php endif;?>
<br>
<h4>Ürün Bilgisi Ekle (ÖR: Garanti süresi)</h4>
<form class="" action="" method="post">
    <div class="row form-group mt-2" >
        <div class="col col-md-4">
            <p>Ürün Bilgisi</p>
            <input type="text" class="form-control" name="info" value="" required>
        </div>
        <div class="col col-md-4">
            <p class="text-left">Değeri</p>
            <input type="text" class="form-control" name="value" value="" required>
        </div>
        <div class="col col-md-4">
            <input class="btn btn-success mt-4" type="submit" name="insertProductInfo" value="EKLE" />
            <?php if(!isset($_GET["edit"])){ ?>
              <a class="btn btn-primary mt-4"  href="<?php echo adminUrl("urun-varyant-sec?productId=$productId") ?>"  >İLERİ</a>
            <?php } ?>
        </div>
    </div>
</form>

<div class="vue-lists mt-4">
<h4 class="mt-2">Ürün Bilgileri</h4>
  <?php if(!is_array($informations) || count($informations) <= 0){ ?>
    <p>Ürüne ait özellik bulunamadı</p>
  <?php } else{ ?>
    <ul class="mt-2">
      <li>
        <div class="row">
          <div class="col-4">
            <strong>Ürün Bilgisi</strong>
          </div>
          <div class="col-4">
            <strong>Değeri</strong>
          </div>
        </div>
      </li>

      <?php foreach ($informations as $key => $info) { ?>
        <li>
            <form action='' method='post'>
              <input type="hidden" name="info_id" value="<?php echo $info["product_information_id"]; ?>">
                <div class='row form-group'>
                    <div class='col col-md-4'>
                        <input type='text' class='form-control' name='info' value='<?php echo $info["info"]; ?>' required />
                    </div>
                    <div class='col col-md-4'>
                        <input type='text' class='form-control' name='value' value='<?php echo $info["value"]; ?>' required />
                    </div>
                    <div class='col col-md-4'>
                        <input class='btn btn-success' type='submit' name='updateProductInfo' value='Düzenle'/>
                        <input class='btn btn-danger' type='submit' name='deleteProductInfo' value='Sil'/>
                    </div>
                </div>
          </form>
        </li>
      <?php } ?>
    </ul>
  <?php } ?>
</div>
