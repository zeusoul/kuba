<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>

  <div class="card">
    <div class="card-header">
      <strong>İade</strong> Bilgileri
    </div>
    <div class="card-body card-block">
        <h2 class="h3 mb-3 text-black">Müşteri Bilgileri</h2>
        <hr>
          <div class="row">
            <div class="form-group col-md-3">
              <label for="name"> <strong>Ad Soyad</strong> </label>
              <p><?php echo $userDetails["name"]." ".$userDetails["surname"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Telefon</strong> </label>
              <p><?php echo $userDetails["tel"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Email</strong> </label>
              <p><?php echo $userDetails["email"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Detay</strong> </label>
              <br>
              <a class="btn btn-primary" href="<?php echo adminUrl("musteri-detay/".$userDetails["user_id"]); ?>">Detay</a>
            </div>
          </div>
        <hr>
        <h2 class="h3 mb-3 text-black">İade Talebi Detayı  </h2>
        <hr>
        <div class="row">
             <div class="form-group col-md-3">
                <label for="name"> <strong>İade Talebi Tarihi</strong> </label>
                <p><?php echo $returnDetails["return_date"]; ?> TL </p>
              </div>
              <div class="form-group col-md-3">
                <label for=""> <strong>Sipariş No</strong> </label>
                <p>
                  <a target="_blank" href="<?php echo adminUrl("siparis-detay/".$returnDetails["order_no"]); ?>">
                    <?php echo $returnDetails["order_no"]; ?>
                  </a>
                </p>
              </div>
              <div class="form-group col-md-3">
                <label for=""> <strong>Talep No</strong> </label>
                <p><?php echo $returnDetails["return_id"]; ?></p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>İade Edilecek Ürün</strong> </label>
                  <a target="_blank" href="<?php echo url(seoUrl($returnDetails["title"])."-p-".$returnDetails["barcode"]); ?>">
                    <?php echo $returnDetails["title"]; ?>
                  </a>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>İade Edilecek Adet</strong> </label>
                  <p><?php echo $returnDetails["returned_quantity"];  ?> Adet</p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>İade Edilecek Tutar</strong> </label>
                  <p><?php echo $returnDetails["returned_price"];  ?> ₺ +KDV</p>
                  <p>KDV Dahil : <?php echo ((float)$returnDetails["returned_price"] * 118 / 100); ?> ₺</p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>İade Sebebi</strong> </label>
                  <p><?php echo $returnDetails["return_reason"];  ?></p>
              </div>
              <div class="col-12 text-center mt-2">
                <a class="btn btn-sm btn-primary" href="?fatura">Fatura Yazdır</a>
                </div>
              <?php if(isset($orderDetails["bank_details"])){ ?>
                <div class="form-group col-md-12">
                  <hr>
                  <label for="name"> <strong>Banka Bilgileri</strong> </label>
                  <p>
                    <?php
                       echo $orderDetails["bank_details"]["bank_name"];
                       echo "<br>";
                       echo $orderDetails["bank_details"]["account_name"];
                       echo "<br>";
                       echo $orderDetails["bank_details"]["iban_no"];
                    ?>
                  </p>
                </div>
              <?php } ?>

            </div>
            <hr>
            <h5 class="mb-3">Adresler</h5>
            <hr>


            <div class="row">
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Teslimat Adresi</h5>
                            <h5 class="card-title"><?php echo $orderDetails["teslimat_address"]['title'] ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $orderDetails["teslimat_address"]['name_surname']; ?></h6>
                            <p class="card-text"><?php echo $orderDetails["teslimat_address"]['tel']; ?></p>
                            <p class="card-text"><?php echo $orderDetails["teslimat_address"]['county']." / ".$orderDetails["teslimat_address"]["city"]; ?></p>
                            <h6 class="card-text " ><?php echo $orderDetails["teslimat_address"]['address'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["teslimat_address"]['company_name'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["teslimat_address"]['tax_administration'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["teslimat_address"]['tax_number'] ?></h6>
                        </div>
                    </div>
                </div>
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Fatura Adresi</h5>
                            <h5 class="card-title"><?php echo $orderDetails["fatura_address"]['title'] ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $orderDetails["fatura_address"]['name_surname']; ?></h6>
                            <p class="card-text"><?php echo $orderDetails["fatura_address"]['tel']; ?></p>
                            <p class="card-text"><?php echo $orderDetails["fatura_address"]['county']." / ".$orderDetails["teslimat_address"]["city"]; ?></p>
                            <h6 class="card-text " ><?php echo $orderDetails["fatura_address"]['address'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["fatura_address"]['company_name'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["fatura_address"]['tax_administration'] ?></h6>
                            <h6 class="card-text " ><?php echo $orderDetails["fatura_address"]['tax_number'] ?></h6>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h4 class="mt-4">Durum Güncellemesi</h4>
            <hr>
            <div class="mt-3 col-md-12">
              <?php if($returnDetails["return_status"] == -1){ ?>
                <div class="alert alert-danger text-center" role="alert">
                  <strong>Bu talep iptal edildi</strong>
                  <small>(<?php echo $returnDetails["reason_for_rejection"]; ?>)</small>
                </div>
              <?php } else if($returnDetails["return_status"] == 1) {?>
                <div class="alert alert-success text-center" role="alert">
                  <strong>Bu talep onaylandı</strong>
                </div>
              <?php } else if($returnDetails["return_status"] == 0) {?>



                <div class="row">
                  <form class="" action="" method="post">
                    <div class="col-md-2"><button class="btn btn-success" type="submit" name="confirmTheReturn">Talebi Onayla</button></div>
                  </form>
                  <form class="" action="" method="post">
                    <input type="hidden" name="user_id" value="<?php echo $returnDetails["user_id"]; ?>" required/>
                    <div class="col-md-12 row">
                      <div class="col-md-4">
                        <button class="btn btn-danger" type="submit" name="cancelTheReturn">Talebi İptal Et</button>
                      </div>
                      <div class="col-md-8">
                        <input class="form-control" type="text" name="reason" placeholder="İptal Edilme Sebebi" required/>
                      </div>
                    </div>
                  </form>
                </div>

              <?php } ?>
            </div>

    </div>
    </div>
  </div>
</div>
