<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <a href="<?php echo adminUrl("kategori-sec/urun-ekle"); ?>" class="btn">Kategori Değiştir</a>
  <div class="card">
    <div class="card-header">
      <strong>Ürün</strong> Bilgileri
    </div>
    <div class="card-body card-block">
      <form id="insertProductForm" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="selectedCategory" class=" form-control-label">Seçilen Kategori</label>
          </div>
          <div class="col-12 col-md-9">
            <?php echo $selectedCategoryName; ?>
            <input type="hidden" id="selectedCategory" name="categoryId" value="<?php echo $selectedCategoryId; ?>" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz (Önerilen : 405x440)</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Ürün Başlığı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["title"]; ?>" name="title" placeholder="Ürün Başlığı Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="subTitle" class=" form-control-label">Ürün Alt Başlığı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="subTitle" value="<?php echo $_POST["subTitle"]; ?>" name="subTitle" placeholder="Ürün Alt Başlığı Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- select -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="brand" class=" form-control-label">Marka * </label>
          </div>
          <div class="col-12 col-md-9">
            <select class="form-control" name="brand_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($brands as $key => $brand): ?>
                <option value="<?php echo $brand["brand_id"]; ?>"><?php echo $brand["brand_name"]; ?></option>
              <?php endforeach; ?>
            </select>
          </div>
        </div>
        <!-- select -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="model" class=" form-control-label">Model *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="model" value="<?php echo $_POST["model"]; ?>" name="model" placeholder="Model Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="productCode" class=" form-control-label">Ürün Kodu *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="productCode" value="<?php echo $_POST["productCode"]; ?>" name="productCode" placeholder="Ürün Kodunu Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->

        <?php foreach ($filters as $filter ): ?>
        <!-- select-foreach -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="<?php echo $filter["filter_id"]; ?>" class=" form-control-label"><?php echo $filter['name'] ?></label>
          </div>
          <div class="col col-md-5">
            <select class="form-control" id="<?php echo $filter["filter_id"]; ?>" name="<?php echo $filter['filter_id'] ?>" required >
              <?php foreach ($filter['values'] as $value): ?>
                <option value="<?php echo $value['filter_value_id'] ?>" ><?php echo $value['value'] ?></option>
              <?php endforeach;?>
            </select>
          </div>
          <div class="col col-md-4">
            <input type="text"  value="" name="txt-<?php echo $filter['filter_id'] ?>" placeholder="Yeni değer" value="0" class="form-control" >
          </div>
        </div>
        <!-- select-foreach -->
        <?php endforeach;?>


        <!-- editor -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="content" class=" form-control-label">Ürün Açıklama</label>
          </div>
          <div class="col-12 col-md-9">
            <textarea value="<?php echo $_POST["content"]; ?>" name="content" required></textarea>
            <script>
              CKEDITOR.replace( 'content' );
            </script>
          </div>
        </div>
        <!-- editor -->
        <!-- select -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="status" class=" form-control-label">Yayın Durumu *</label>
          </div>
          <div class="col-12 col-md-9">
            <select class="form-control" id="status" name="status" required>
              <option value="1" selected>Yayınla</option>
              <option value="0">Gizle</option>
            </select>
          </div>
        </div>
        <!-- select -->
      </form>
    </div>
    <div class="card-footer">
      <button form="insertProductForm" type="submit" name="insertProduct" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> İleri
      </button>
    </div>
  </div>
</div>
