<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Yeni Varyant</strong> Ekle
    </div>
    <div class="card-body card-block">
      <form action="" id="insertVariantForm" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col-md-1">
            <label for="" class=" form-control-label">Varyant Kodu</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="variant_code" value="">
          </div>
          <div class="col-md-1">
            <label for="" class=" form-control-label">Varyant</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="variant" value="" required>
          </div>
        </div>
        <!-- input -->
      </form>
    </div>
    <div class="card-footer">
      <button type="submit" form="insertVariantForm" name="insertVariant" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>Kayıtlı</strong> Varyantlar
    </div>
    <div class="card-body card-block">
      <?php if(count($variants) == 0){ ?>
        <p>Kayıtlı varyant bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>Varyant Kodu</th>
            <th>Varyant</th>
            <th>İşlemler</th>
          </thead>
          <?php foreach ($variants as $key => $variant): ?>
            <tr>
              <td><?php echo $variant["variant_code"]; ?></td>
              <td><?php echo $variant["variant_name"]; ?></td>
              <td>
                <form class="" action="" method="post">
                  <input type="hidden" name="variant_id" value="<?php echo $variant["variant_id"]; ?>">
                  <button type="submit" class="btn btn-danger" name="delete_variant">Sil</button>
                  <a class="btn btn-success" href="<?php echo adminUrl("varyant-degerleri?variantId=".$variant["variant_id"]); ?>">Değer Gir</a>
                  <a class="btn btn-dark" href="<?php echo adminUrl("varyant-eslestir?variantId=".$variant["variant_id"]); ?>">Eşleştir</a>
                </form>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      <?php } ?>
    </div>
  </div>
</div>
