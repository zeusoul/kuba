<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->

    <form class="" action="" method="get">
      <div class="row form-group">
            <div class="col col-md-4">
              <input id="cc-number" name="value" type="text" value="<?php echo $_GET["value"]; ?>" class="form-control" placeholder="ara">
            </div>
            <div class="col col-md-6">
              <select class="form-control" name="type" required>
                <?php
                  if($_GET["type"] == "title") $titleSelected = "selected";
                  else if($_GET["type"] == "model_name") $modelNameSelected = "selected";
                  else if($_GET["type"] == "brand_name") $brandNameSelected = "selected";
                  else if($_GET["type"] == "product_code") $productCodeSelected = "selected";
                ?>
                <option value="title" <?php echo $titleSelected; ?>>Ürün Başlığına Göre</option>
                <option value="model_name" <?php echo $modelNameSelected; ?>>Modele Göre</option>
                <option value="brand_name" <?php echo $brandNameSelected; ?>>Markaya Göre</option>
                <option value="product_code" <?php echo $productCodeSelected; ?>>Ürün Koduna Göre</option>
              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col col-md-2">
              <button type="submit" name="search" class="btn btn-filter btn-secondary">
                <i class="fa fa-search"></i>
              </button>
            </div>

      </div>
    </form>

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
        <a href="<?php echo adminUrl("urun-ekle"); ?>" class="au-btn au-btn-icon au-btn--green au-btn--small">
          <i class="zmdi zmdi-plus"></i>Yeni Ürün Ekle
        </a>
      </div>
      <hr>
      <p class="text-center">
        <?php if(count($products) > 0){ ?>
          Toplam <?php echo $result["total_products_count"]; ?> Ürün Arasından Gösterilen Satır : <?php echo $offset."-".$limit; ?>
        <?php } ?>
      </p>
      <hr>

      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>"><</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">></a>
        </li>
      </ul>

      <table class="table table-data2">
        <thead>
          <tr>
            <th>Ürün ID</th>
            <th>Başlık</th>
            <th>Ürün Kodu</th>
            <th>Durum</th>
            <th>İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($products) || !is_array($products) || count($products) <= 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Ürün Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php foreach ($products as $key => $product) { ?>
              <?php
                if($key < $offset) {continue;}
                else if($key >= $limit) {break;}
               ?>
              <tr class="tr-shadow">
                <td><?php echo $product["product_id"]; ?></td>
                <td class="desc">
                  <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                </td>
                <td>
                  <?php echo $product["product_code"]; ?>
                </td>
                <td>
                  <?php if((int)$product["release_status"] == 1) { ?>
                    <span class="status--process">
                      Yayında
                    </span>
                  <?php } else { ?>
                    <span class="status--process" style="color:red;">
                      Gizli
                    </span>
                  <?php } ?>
                </td>
                <td>
                  <div class="dropdown">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink<?php echo $key; ?>" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      İşlemler
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink<?php echo $key; ?>">
                      <a class="dropdown-item" href="<?php echo adminUrl("urun-duzenle/".$product["product_id"]); ?>">Ürünü Düzenle</a>
                      <a class="dropdown-item" href="<?php echo adminUrl("urun-bilgisi-ekle/".$product["product_id"]."?edit"); ?>">Ürün Bilgisi Ekle / Düzenle</a>
                      <a class="dropdown-item" href="<?php echo adminUrl("urun-varyant-ekle?productId=".$product["product_id"]); ?>">Ürün Varyantlarını Düzenle</a>
                      <a class="dropdown-item" href="<?php echo adminUrl("urun-resimleri/".$product["product_id"]); ?>">Ürün Resimleri Ekle / Düzenle</a>
                      <a class="dropdown-item" href="">Platform Özelliklerini Ekle (Entegrasyon için zorunlu)</a>
                      <a class="dropdown-item" href="<?php echo adminUrl("urun-sil/".$product["product_id"]); ?>">Ürünü Sil</a>
                    </div>
                  </div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>
      <hr>
      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>">Previous</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">Next</a>
        </li>
      </ul>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
