
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2>Kategori Seç</h2>
      <hr class="mb-4">
      <!-- içerik -->

      <?php if(!is_array($categories) || count($categories)==0){ ?>
        <p style="color:red;">Kayıtlı kategori bulunamadı! Lütfen önce kategori ekleyiniz</p>
        <a href="<?php echo adminUrl("kategori-islemleri"); ?>">
          Kategori eklemek için tıklayınız
        </a>

      <?php } else {?>
        <h6>Kategori seç</h6>
        <form action="" method="post">
           <div class="row" id="selects">
              <div class="form-group col-md-3 category-selects" >
                <select onchange="selectChange(1)" id="category-select-1" multiple name="category-1" class="form-control" style=" height: 180px;" >
                  <?php foreach ($categories as $row) { ?>
                    <option value="<?php echo $row['reference_category_id']; ?>"><?php echo  $row['reference_category_name']; ?></option>
                  <?php }?>
                </select>
              </div>
           </div>
           <div class="row">
             <div class="form-group col-md-6 d-flex justify-content-end  text-center">
               <input class="btn btn-secondary " type="submit" name="select-category" value="Devam">
             </div>
           </div>
        </form>
      <?php } ?>
    </div>
  </div>
</div>
