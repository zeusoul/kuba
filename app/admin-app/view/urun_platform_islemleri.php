<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-body card-block">
      <form class="" action="" method="get">
        <div class="row">
          <div class="col-md-2">
            Platform Seçiniz :
          </div>
          <div class="col-md-4">
            <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
            <select  class="form-control" name="platform_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($platforms as $platform) { ?>
                <option <?php if(isset($_GET["platform_id"]) && $_GET["platform_id"] == $platform["platform_id"]) echo "selected"; ?> value="<?php echo $platform["platform_id"]; ?>"><?php echo $platform["platform_name"]; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2">
            <button  class="btn btn-primary" type="submit">Seç</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php if(isset($_GET["platform_id"]) && (int)$_GET["platform_id"] > 0){ ?>
    <?php if(isset($fatalError) && trim($fatalError) != ""){ ?>
      <div class="alert alert-danger" role="alert">
        <?php echo $fatalError; ?>
      </div>
    <?php } else { ?>
      <div class="card">
        <div class="card-header">
          <strong>Platform</strong> İşlemleri
        </div>
        <div class="card-body card-block">
          <div class="">
            Seçilen Ürün : <a target="_blank" href="<?php echo url(seoUrl($productDetail["title"])."-p-".$productDetail["variants"][0]["barcode"]); ?>"><?php echo $productDetail["title"]; ?></a>
          </div>
          <hr>
          <form id="insertPlatformAttributes" action="" method="post">
            <div class="row">
              <?php if($_GET["platform_id"] == 1){ ?>
                <form class="" action="index.html" method="post">
                  <div class="col-md-1">
                    <label for="">Şifreniz </label>
                  </div>
                  <div class="col-md-2">
                    <input type="password" class="form-control" name="password" value="" required>
                  </div>
                  <div class="col-md-2">
                    <button type="submit" class="btn btn-success" name="sendTrendyol">Trendyol Platformuna Gönder</button>
                  </div>
                  <div class="col-md-2 ml-2">
                    <button type="submit" class="btn btn-success" name="updateTrendyol">Ürün Bilgilerini Güncelle</button>
                  </div>
                </form>
              <?php } ?>
            </div>
          </form>
        </div>
        <div class="card-footer">
          <button form="insertPlatformAttributes" type="submit" class="btn btn-success" name="insertPlatformAttributes">Kaydet</button>
        </div>
      </div>
    <?php } ?>
  <?php } ?>
</div>
