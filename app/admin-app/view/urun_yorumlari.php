<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <form class="" action="" method="get">
      <div class="row form-group">
            <div class="col col-md-4">
              <input id="cc-number" name="value" type="text" value="<?php echo $_GET["value"]; ?>" class="form-control" placeholder="ara">
            </div>
            <div class="col col-md-6">
              <select class="form-control" name="type" required>
                <option value="users.name" selected>Müşteri Adına Göre</option>
                <option value="products.product_code">Ürün Koduna Göre</option>
              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col col-md-2">
              <button type="submit" name="search" class="btn btn-filter btn-secondary">
                <i class="fa fa-search"></i>
              </button>
            </div>

      </div>
    </form>

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">

      </div>
        <table class="table table-data2">
          <thead>
            <tr>
              <th>Müşteri Adı</th>
              <th>Yorum İçeriği</th> <!-- burası kisalt() fonksiyonu ile kısaltılacak -->
              <th>Değerlendirme</th>
              <th>Durum</th>
              <th>Ürün Kodu</th>
              <th>
                İşlemler<br/>
                [Onay][Sil]
              </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!isset($productComments) || !is_array($productComments) || count($productComments) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Yorum Bulunamadı</td>
              </tr>
            <?php } else { ?>
              <?php foreach ($productComments as $key => $productComment) { ?>
                <form class="" action="" method="post">
                <tr class="tr-shadow">
                  <td class="desc">
                    <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$productComment["user_id"]); ?>">
                      <?php echo $productComment["name"]; ?>
                    </a>
                  </td>
                  <td>
                    <button type="button" class="yorum-goster btn  btn-danger" data-toggle="popover" title="<?php echo $productComment["name"]; ?>" data-content="<?php echo $productComment["comment"]?>">
                        <?php echo kisalt($productComment['comment'],10) ?>
                    </button>
                  </td>
                  <td><?php echo $productComment["rating"]; ?></td>
                  <td>
                    <?php if((int)$productComment["comment_status"] == 1) { ?>
                      <span class="status--process">
                        Yayında
                      </span>
                    <?php } else { ?>
                      <span class="status--process" style="color:red;">
                        Onay Bekliyor
                      </span>
                    <?php } ?>
                  </td>
                  <td>
                    <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["product_id"]); ?>">
                      <?php echo $productComment["product_code"]; ?>
                    </a>
                  </td>
                  <!-- işlemler -->
                  <td>
                    <div class="table-data-feature">
                      <input type="hidden" name="userId" value="<?php echo $productComment["user_id"];?>">
                      <input type="hidden" name="commentId" value="<?php echo $productComment["product_comment_id"]; ?>">
                      <?php if((int)$productComment["comment_status"] == 0) { ?>
                        <button type="submit" name="confirmComment" class="item">
                         <i class="zmdi zmdi-check"></i>
                          <!-- yorum onay -->
                        </button>
                      <?php } ?>
                      <button type="submit" name="deleteComment" class="item">
                        <i class="zmdi zmdi-delete"></i>
                        <!-- yorum iptal -->
                      </button>
                    </div>
                  </td>
                </tr>
              </form>
              <?php } ?>
            <?php } ?>


          </tbody>
        </table>

    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
