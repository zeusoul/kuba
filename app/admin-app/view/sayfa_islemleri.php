<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
      <a href="<?php echo adminUrl("sayfa-ekle")?>" class="btn btn-primary mb-3"> Yeni Sayfa Ekle</a>
    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
      </div>
        <table class="table table-data2">
          <thead>
            <tr>
              <th>Sayfa Adı</th>
              <th>Yayın Durumu</th>
              <th>
                İşlemler
              </th>
            </tr>
          </thead>
          <tbody>
            <?php if(!isset($pages) || !is_array($pages) || count($pages) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Sayfalar Bulunamadı</td>
              </tr>
            <?php } else { ?>
              <?php foreach ($pages as $key => $page) { ?>
                <tr class="tr-shadow">
                  <td class="desc">
                    <a target="_blank" href="<?php echo url("sayfa/".$page["menu_title"]."/".$page["page_id"]) ?>"><?php echo $page["menu_title"]; ?></a>
                  </td>
                  <td>
                    <?php if((int)$page["status"] == 1) { ?>
                      <span class="status--process">
                        Sayfa Yayında
                      </span>
                    <?php } else { ?>
                      <span class="status--process" style="color:red;">
                        Yayında Değil
                      </span>
                    <?php } ?>
                  </td>
                  <!-- işlemler -->
                  <td>
                      <a  href="<?php echo adminUrl("sayfa-duzenle/".$page["page_id"]) ?>" class="btn btn-primary  "> Düzenle </a>
                      <a  href="<?php echo adminUrl("sayfa-duzenle/".$page["page_id"]."?deletePage") ?>" class="btn btn-danger  "> Sil </a>
                  </td>
                </tr>
      <?php } ?>
    <?php } ?>
    </tbody>
    </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
