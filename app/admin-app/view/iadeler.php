<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>

    <form class="" action="" method="get">
      <div class="row form-group">
            <div class="col col-md-4">
              <input id="cc-number" name="value" type="text" value="<?php echo $_GET["value"]; ?>" class="form-control" placeholder="ara">
            </div>
            <div class="col col-md-6">
              <select class="form-control" name="type" required>
                <?php if(isset($_GET["type"])){
                  if($_GET["type"] == "name") $nameSelect = "selected";
                  else if($_GET["type"] == "surname") $surnameSelect = "selected";
                }?>
                <option value="return_id" selected>Talep Numarasına Göre</option>
                <option value="name" <?php echo $nameSelect; ?>>Müşteri Adına Göre</option>
                <option value="surname" <?php echo $surnameSelect; ?>>Müşteri Soyadına Göre</option>

              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col col-md-2">
              <button type="submit" name="search" class="btn btn-filter btn-secondary">
                <i class="fa fa-search"></i>
              </button>
            </div>

      </div>
    </form>

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">

      </div>

        <table class="table table-data2">
          <thead>
            <tr>
              <th>Talep No</th>
              <th>Sipariş No</th>
              <th>Müşteri Adı Soyadı</th>
              <th>Talep Tarihi</th>
              <th>İade Edilecek Tutar</th>
              <th>Durumu</th>
              <th>Müşteri Tel. No</th>
              <th>Detay</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!isset($returns) || !is_array($returns) || count($returns) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Sipariş Bulunamadı</td>
              </tr>
            <?php } else { ?>
              <?php foreach ($returns as $key => $return) { ?>

                <tr class="tr-shadow" style="border:0px; border-style:solid; border-bottom-width:1px; border-top-width:1px;">
                  <td>
                    <strong><?php echo $return["return_id"] ?></strong>
                  </td>
                  <td>
                    <a target="_blank" href="<?php echo adminUrl("siparis-detay/".$return["order_no"]); ?>">
                      <strong><?php echo $return["order_no"]; ?></strong>
                    </a>
                  </td>
                  <td>
                    <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$return["user_id"]); ?>">
                      <strong><?php echo $return["name"]." ".$return["surname"]; ?></strong>
                    </a>
                  </td>
                  <td>
                    <strong><?php echo $return["return_date"]; ?></strong>
                  </td>
                  <td>
                    <strong><?php echo $return["returned_price"]; ?> ₺ +KDV</strong>
                  </td>
                  <td>
                    <?php if($return["return_status"] == -1) { ?>
                        <span class="badge badge-danger" >
                           İptal Edildi
                        </span>
                    <?php } else if($return["return_status"] == 0) { ?>
                        <span class="badge badge-primary" >
                           Onay Bekliyor
                        </span>
                    <?php } else if($return["return_status"] == 1) { ?>
                      <span class="badge badge-success" >
                        Onaylandı
                      </span>
                    <?php } ?>
                  </td>
                  <td>
                    <strong><?php echo $return["tel"]; ?></strong>
                  </td>
                  <!-- işlemler -->
                  <td>
                    <div class="table-data-feature">
                      <a href="<?php echo adminUrl("iade-detay/".$return["return_id"]); ?>" class="item  "> <i class="zmdi zmdi-eye"></i>  </a>
                    </div>
                  </td>
                </tr>
      <?php } ?>
    <?php } ?>
    </tbody>
    </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
