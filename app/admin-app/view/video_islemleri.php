<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
    </div>
    <div class="card-body card-block">

      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="video" class=" form-control-label"><h3>Video Yükle</h3></label>
          </div>
          <div class="col col-md-4">
            <label for="video" class=" form-control-label">Video Youtube Linki</label>
          </div>
          <div class="col col-md-8">
            <input class="form-control" type="text" name="video_url" placeholder="ör:www.youtube.com?v=hw5223j4b" required>
          </div>
          <div class="col col-md-4 mt-1">
            <label for="video" class=" form-control-label">Video Başlığı</label>
          </div>
          <div class="col col-md-8 mt-1">
            <input class="form-control" type="text" name="video_title" placeholder="">
          </div>
          <div class="col col-md-4 mt-1">
            <label for="video" class=" form-control-label">Video Açıklaması</label>
          </div>
          <div class="col col-md-8 mt-1">
            <input class="form-control" type="text" name="video_desc" placeholder="">
          </div>
          <div class="col col-md-12">
            <button type="submit" name="insertVideo" class="btn btn-primary btn-sm float-right mt-1">
              <i class="fa fa-dot-circle-o"></i> Yükle
            </button>
          </div>
        </div>
        <!-- input -->
      </form>
      <hr>
      <form action="" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Yüklü Videolar</h3></label>
          </div>
          <?php if(count($videos) == 0) { ?>
            <div class="col col-md-12">
              <p>Yüklü Video Bulunamadı.</p>
            </div>
          <?php } else { ?>
            <?php foreach($videos as $video){ ?>
              <div class="col col-md-2">
                <iframe width="200" height="150" src="<?php echo $video["video_url"]; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <input type="hidden" name="video_id" value="<?php echo $video["video_id"]; ?>">
                <button style="width:75px; background: red; margin-left: 60px;" type="submit" name="deleteVideo" class="btn btn-primary btn-sm">
                  <i class="fas fa-trash-alt"></i> Sil
                </button>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
        <!-- input -->
      </form>


    </div>
  </div>
</div>
