<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
  <!-- -->
    <div class="card-header">
      <strong>Sol Tarafa</strong>  Eklenecek Resim
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="firstImage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
  <!-- -->
  <div class="card">
    <div class="card-header">
      <strong>Ortaya</strong>  Eklenecek Resim
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image2" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="secondImage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  <!--  -->
  <div class="card">
    <div class="card-header">
      <strong>Sağ Tarafa</strong>  Eklenecek Resim
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image3" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="thirdImage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>Alt1 Tarafına</strong>  Eklenecek Resim
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image4" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="fourthImage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>Alt2 Tarafına</strong>  Eklenecek Resim
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image5" required>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" name="fiveImage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
