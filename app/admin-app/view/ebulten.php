<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <form class="" action="" method="post">
      <div class="row form-group">
        <div class="col col-md-12">
          <textarea name="message" rows="8" cols="80" class="form-control"></textarea>
          <script>
            CKEDITOR.replace( 'message' );
          </script>
        </div>
        <div class="col col-md-2 mt-2">
          <input type="submit" class="btn btn-primary" name="sendMessage" value="Gönder">
        </div>
      </div>
    </form>
    <div class="col col-md-12">
      <p>E-Bültene kayıtlı kişi sayısı : <?php echo $numRows; ?></p>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
