<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <h2>Referanslar</h2>
    <hr class="mb-4">
  <div class="card">
    <div class="card-header">
      <strong>Referanslar</strong>
    </div>
    <div class="card-body card-block">
          <div class="col-12 col-md-9">
            <a href="<?php echo adminUrl("referans-ekle"); ?>">
              <button type="submit" class="btn btn-warning btn-sm">
                <i class="fa fa-dot-circle-o"></i> Referans Ekle
              </button>
            </a>
          </div>
      <!-- <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Referans Ekle</h3></label>
          </div>
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image">
            <button type="submit" name="insertReference" class="btn btn-primary btn-sm">
              <i class="fa fa-dot-circle-o"></i> Ekle
            </button>
          </div>
        </div>
      </form> -->
      <hr>
      <form action="" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Kayıtlı Referanslar</h3></label>
          </div>
          <?php if(count($references) == 0) { ?>
            <div class="col col-md-12">
              <p>Kayıtlı Referans Bulunamadı.</p>
            </div>
          <?php } else { ?>
            <?php foreach($references as $reference){ ?>
              <div class="col col-md-2">
                <img width="200" src="<?php echo publicUrl("img/reference-images/".$reference["reference_image"]); ?>" alt="">
                <input type="hidden" name="reference_id" value="<?php echo $reference["reference_id"]; ?>">
                <button style="margin-top:-75px; width:75px; background: red; margin-left: 60px;" type="submit" name="deleteReference" class="btn btn-primary btn-sm">
                  <i class="fas fa-trash-alt"></i> Sil
                </button>
                  <!-- <form action="" method="post" class="form-horizontal">
                    <div class="col col-md-2">
                      <label for="reference" class=" form-control-label">Numarası</label>
                      <input class="form-control" type="text" name="reference_order" placeholder="<?php echo $reference["reference_order"]; ?>">
                      <br>
                      <button type="submit" name="referenceOrder" class="btn btn-warning btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Güncelle
                      </button>
                    </div>
                  </form> -->
              </div>
            <?php } ?>
          <?php } ?>
        </div>
        <!-- input -->
      </form>


    </div>
  </div>
</div>
