<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Ürün</strong> Varyantları
    </div>
    <div class="card-body card-block">
      <form action="" id="insertVariantForm" method="post" class="form-horizontal">

        <?php if(count($variants) > 0){ ?>
          <?php foreach ($variants as $variant ): ?>
          <!-- select-foreach -->
          <div class="row form-group">
            <div class="col-md-1">
              <label for="" class=" form-control-label"><?php echo $variant['variant_name'] ?></label>
            </div>
            <div class="col col-md-3">
              <select class="form-control" name="variantsAndValues[<?php echo $variant["variant_id"]; ?>]" required>
                <?php foreach ($variant["values"] as $key => $value): ?>
                  <option value="<?php echo $value["variant_value_id"]; ?>"><?php echo $value["variant_value_name"]; ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <!-- select-foreach -->
          <?php endforeach;?>
          <hr>
        <?php } ?>
        <!-- input -->
        <div class="row form-group">
          <div class="col-md-1">
            <label for="" class=" form-control-label">Barkod</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="barcode" value="" required>
          </div>
          <div class="col-md-1">
            <label for="" class=" form-control-label">Stok Kodu</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="stock_code" value="" required>
          </div>
          <div class="col-md-1">
            <label for="" class=" form-control-label">Stok</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="stock" value="" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col-md-1">
            <label for="" class=" form-control-label">Fiyat</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="price" value="" required>
          </div>
          <div class="col-md-1">
            <label for="" class=" form-control-label">İndirimli Fiyat</label>
          </div>
          <div class="col col-md-2">
            <input type="text" class="form-control" name="discount_price" value="" required>
          </div>
          <div class="col-md-1">
            <label for="" class=" form-control-label">Para Birimi</label>
          </div>
          <div class="col col-md-2">
            <select class="form-control" name="currency" required>
              <option value="TL" selected>TL</option>
              <option value="EUR">EURO</option>
              <option value="USD">DOLAR</option>
            </select>
          </div>
        </div>
        <!-- input -->
      </form>
    </div>
    <div class="card-footer">
      <button form="insertVariantForm" type="submit" name="insertVariant" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
      <a href="<?php echo adminUrl("urun-islemleri"); ?>" class="btn btn-success btn-sm"> Bitir</a>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <strong>Ürüne Ait </strong> Varyantlar
    </div>
    <div class="card-body card-block">
      <?php if(count($productVariants) == 0){ ?>
        <p>Ürüne ait kayıtlı varyant bulunamadı</p>
      <?php } else{ ?>
          <table class="table">
            <thead>
              <th>Barkod</th>
              <th>Kombin</th>
              <th>Stok Kodu</th>
              <th>Stok</th>
              <th>Fiyat</th>
              <th>İnd. Fiyat</th>
              <th>Para Birimi</th>
              <th>#</th>
              <th>#</th>
            </thead>
            <?php foreach ($productVariants as $key => $productVariant): ?>
              <tr>
                <form class="" action="" method="post">
                  <td><input class="form-control" type="text" name="barcode" value="<?php echo $productVariant["barcode"]; ?>" required></td>
                  <td><?php echo $productVariant["combine"]; ?></td>
                  <td><input class="form-control" type="text" name="stock_code" value="<?php echo $productVariant["stock_code"]; ?>" required></td>
                  <td><input class="form-control" type="text" name="stock" value="<?php echo $productVariant["stock"]; ?>" required></td>
                  <td><input class="form-control" type="text" name="price" value="<?php echo $productVariant["price"]; ?>" required></td>
                  <td><input class="form-control" type="text" name="discount_price" value="<?php echo $productVariant["discount_price"]; ?>" required></td>
                  <td>
                    <select class="form-control" name="currency" required>
                      <option value="TL" <?php if($productVariant["currency"] == "TL") echo "selected"; ?>>TL</option>
                      <option value="EUR" <?php if($productVariant["currency"] == "EUR") echo "selected"; ?>>EURO</option>
                      <option value="USD" <?php if($productVariant["currency"] == "USD") echo "selected"; ?>>DOLAR</option>
                    </select>
                  <td>
                    <input type="hidden" name="product_variant_id" value="<?php echo $productVariant["product_variant_id"]; ?>">
                    <button type="submit" class="btn btn-danger" name="delete_product_variant">Sil</button>
                  </td>
                  <td><button type="submit" class="btn btn-primary" name="update_product_variant">Güncelle</button></td>
                </form>
              </tr>
            <?php endforeach; ?>
          </table>

      <?php } ?>
    </div>
  </div>

</div>
