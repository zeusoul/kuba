<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Ürün</strong> Varyantları
    </div>
    <div class="card-body card-block">
      <form id="form1" action="<?php echo adminUrl("urun-varyant-ekle"); ?>" method="get" class="form-horizontal">
        <input type="hidden" name="productId" value="<?php echo $productId; ?>">
        <?php if(!is_array($variants) || count($variants) == 0){ ?>
          <p>Kayıtlı varyant bulunamadı. İleri butonuna tıklayarak varyantsız ürün ekleyebilirsiniz.</p>
        <?php } else{ ?>
          <!-- input -->
          <?php foreach ($variants as $variant ): ?>
            <!-- select-foreach -->
            <div class="row form-group">
              <div class="form-check ml-2">
                <input class="form-check-input" name="variants[]" type="checkbox" value="<?php echo $variant["variant_id"]; ?>" id="<?php echo $variant["variant_id"]; ?>">
                <label class="form-check-label" for="<?php echo $variant["variant_id"]; ?>">
                  <?php echo $variant["variant_name"]; ?>
                </label>
              </div>
            </div>
            <!-- select-foreach -->
          <?php endforeach;?>
        <?php } ?>

      </form>
    </div>
    <div class="card-footer">
      <button type="submit" form="form1" name="selectVariants" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> İleri
      </button>
    </div>
  </div>
</div>
