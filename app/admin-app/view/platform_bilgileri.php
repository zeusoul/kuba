
<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
        <a href="<?php echo adminUrl("platform-bilgisi-ekle"); ?>" class="au-btn au-btn-icon au-btn--green au-btn--small">
          <i class="zmdi zmdi-plus"></i>Yeni Ekle
        </a>
      </div>
      <hr>

      <table class="table table-data2">
        <thead>
          <tr>
            <th>#</th>
            <th>Market Adı</th>
            <th>Platform Adı</th>
            <th>İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($platformInformations) || !is_array($platformInformations) || count($platformInformations) == 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Kayıt Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php $i = 1; ?>
            <?php foreach ($platformInformations as $key => $platformInformation) { ?>
              <tr class="tr-shadow">
                <td><?php echo $i++; ?></td>
                <td>
                  <?php echo $platformInformation["market_name"]; ?>
                </td>
                <td>
                  <?php echo $platformInformation["platform_name"]; ?>
                </td>
                <td>
                  <div class="dropdown">
                    <a class="btn btn-secondary" href="<?php echo adminUrl("api-bilgisi?id=".$platformInformation["id"]); ?>">
                      API Bilgisi Ekle/Düzenle
                    </a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
