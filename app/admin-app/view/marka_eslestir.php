<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="mb-4">
    Seçilen Marka : <strong> <?php echo $brand["brand_name"]; ?> </strong>
  </div>

  <div class="card">
    <div class="card-header">
      <strong>Eşleştirilmiş </strong> Platformlar
    </div>
    <div class="card-body card-block">
      <?php if(count($platformBrands) == 0){ ?>
        <p>Kayıt bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>#</th>
            <th>Platform</th>
            <th>Kategori Kodu</th>
            <th>Sil</th>
          </thead>
          <tbody>
            <?php $i=1; ?>
            <?php foreach ($platformBrands as $key => $platformBrand) { ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $platformBrand["platform_name"]; ?></td>
                <td><?php echo $platformBrand["platform_brand_code"]; ?></td>
                <td>
                  <form class="" action="" method="post">
                    <input type="hidden" name="platform_brand_id" value="<?php echo $platformBrand["platform_brand_id"]; ?>">
                    <button type="submit" class="btn btn-danger" name="deleteMatch">Sil</button>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
    </div>
  </div>


  <div class="card">
    <div class="card-body card-block">
      <form class="" action="" method="get">
        <div class="row">
          <div class="col-md-2">
            Platform Seçiniz :
          </div>
          <div class="col-md-4">
            <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
            <select  class="form-control" name="platform_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($platforms as $platform) { ?>
                <option <?php if(isset($_GET["platform_id"]) && $_GET["platform_id"] == $platform["platform_id"]) echo "selected"; ?>
                    value="<?php echo $platform["platform_id"]; ?>">
                    <?php echo $platform["platform_name"]; ?>
                </option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2">
            <button  class="btn btn-primary" type="submit">Seç</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <?php if(isset($_GET["platform_id"])){ ?>
    <div class="card">
      <div class="card-header">
        <strong>Marka</strong> Ara
      </div>
      <div class="card-body card-block">
        <form class="" action="" method="get">
        <div class="row">
            <div class="col-md-2">
              <label for="">Marka Ara</label>
            </div>
            <div class="col-md-4">
              <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
              <input type="hidden" name="platform_id" value="<?php echo $_GET["platform_id"]; ?>">
              <input class="form-control" type="text" name="brand_name" value="<?php echo isset($_GET["brand_name"]) ? $_GET["brand_name"] : ""; ?>" required>
            </div>
            <div class="col-md-2">
              <button class="btn btn-success" type="submit">Ara</button>
            </div>
        </div>
      </form>
      </div>
    </div>
  <?php } ?>
  <?php if(isset($_GET["platform_id"]) && isset($_GET["brand_name"])){ ?>
    <div class="card">
      <div class="card-header">
        <strong>Marka</strong> Eşleştir
      </div>
      <div class="card-body card-block">
        <table id="categories-table" class="table table-striped table-bordered " >
          <thead>
            <th>Marka Kodu</th>
            <th>Marka Adı</th>
            <th>İşlemler</th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($brands as $brand) {  ?>
              <tr>
                <td><?php echo $brand["id"]; ?></td>
                <td><?php echo $brand["name"]; ?></td>
                <td>
                  <form class="" action="" method="post">
                    <input type="hidden" name="platform_id" value="<?php echo $_GET['platform_id']; ?>">
                    <input type="hidden" name="platform_brand_code" value="<?php echo $brand["id"]; ?>">
                    <input type="hidden" name="brand_id" value="<?php echo $_GET['id']; ?>">
                    <button type="submit" name="matchBrand" class="btn btn-sm btn-success"> <i class="fas fa-check-circle"></i></button>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  <?php } ?>
</div>
