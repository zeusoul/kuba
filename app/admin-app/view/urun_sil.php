<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <strong>Ürünü Silmek İstediğinzden Emin Misiniz?</strong>
      <form class="" action="" method="post">
        <input class="btn btn-danger" type="submit" name="confirm" value="Ürünü Sil"/>
        <input class="btn btn-success" type="submit" name="cancel" value="Vazgeç">
      </form>
    </div>
    <div class="card-body card-block">
      <div class="row form-group">
        <div class="col col-md-6">
          <strong>Ürünün Özellikleri : </strong>
        </div>
      </div>
      <!-- input -->
      <div class="row form-group">
        <div class="col col-md-3">
          <label for="selectedCategory" class=" form-control-label">Ürün Başlığı</label>
        </div>
        <div class="col-12 col-md-9">
          <?php echo $product["title"]; ?>
        </div>
      </div>
      <!-- input -->
      <!-- input -->
      <div class="row form-group">
        <div class="col col-md-3">
          <label for="image" class=" form-control-label">Ürün Kodu</label>
        </div>
        <div class="col-12 col-md-9">
          <?php echo $product["product_code"]; ?>
        </div>
      </div>
      <!-- input -->
      <!-- input -->
      <div class="row form-group">
        <div class="col col-md-3">
          <label for="title" class=" form-control-label">Ürün Resmi</label>
        </div>
        <div class="col-12 col-md-9">
          <img src="<?php echo publicUrl("img/product-images/".$product["image"]); ?>" alt="">
        </div>
      </div>
      <!-- input -->
    </div>
  </div>
</div>
