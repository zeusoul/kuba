<div class="row">
  <div class="col-md-12">
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <!-- DATA TABLE -->

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">
        <a href="<?php echo adminUrl("haber-ekle"); ?>" class="au-btn au-btn-icon au-btn--green au-btn--small">
          <i class="zmdi zmdi-plus"></i>Yeni Haber Ekle
        </a>
      </div>
      <hr>
      <p class="text-center">
        <?php if(count($news) > 0){ ?>
          Toplam <?php echo count($news); ?> Haber Arasından Gösterilen Satır : <?php echo $offset."-".$limit; ?>
        <?php } ?>
      </p>
      <hr>

      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>">Previous</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">Next</a>
        </li>
      </ul>

      <table class="table table-data2">
        <thead>
          <tr>
            <th>ID</th>
            <th>Resim</th>
            <th>Başlık</th>
            <th>Açıklama</th>
            <th>İçerik</th>
            <th>Tarih</th>
            <th>İşlemler</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($news) || !is_array($news) || count($news) <= 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Kayıtlı Haber Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php foreach ($news as $key => $new) { ?>
              <?php
                if($key < $offset) {continue;}
                else if($key >= $limit) {break;}
               ?>
              <tr class="tr-shadow">
                <td><?php echo $new["new_id"]; ?></td>
                <td>
                  <img src="<?php echo publicUrl("img/new-images/".$new["new_image"]); ?>" width="100px" alt="">
                </td>
                <td class="desc">
                  <?php echo $new["new_title"]; ?>
                </td>
                <td>
                  <?php echo kisalt($new["new_desc"],20); ?>
                </td>
                <td>
                  <?php echo kisalt($new["new_content"],20); ?>
                </td>
                <td>
                  <?php echo $new["new_date"]; ?>
                </td>
                <td>
                  <div class="table-data-feature">
                    <a class="item btn " href="<?php echo adminUrl("haber-duzenle?new_id=".$new["new_id"]);?>"><i class="zmdi zmdi-edit"></i></a>
                    <form class="" action="" method="post">
                      <input type="hidden" name="new_id" value="<?php echo $new["new_id"]; ?>">
                      <button type="submit" name="delete_new" class="item btn "><i class="zmdi zmdi-delete"></i></button>
                    </form>
                  </div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>
      <hr>
      <ul class="pagination">
        <li class="page-item <?php if($page_number <= 1) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number <= 1) ? "javascript:;" : "?page=".($page_number-1); if("&$get_string" != "") echo $get_string; ?>">Previous</a>
        </li>
        <?php for ($i=0; $i < $number_of_pages; $i++) { ?>
          <li class="page-item <?php if($page_number == $i+1) echo "active"; ?>">
            <a class="page-link" href="<?php echo "?page=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
          </li>
        <?php } ?>
        <li class="page-item <?php if($page_number >= $number_of_pages) echo "disabled"; ?>">
          <a class="page-link" href="<?php echo ($page_number >= $number_of_pages) ? "javascript:;" : "?page=".($page_number+1); if($get_string != "") echo "&$get_string"; ?>">Next</a>
        </li>
      </ul>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
