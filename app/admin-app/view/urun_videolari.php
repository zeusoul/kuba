<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <h2>Ürün Videoları</h2>
    <hr class="mb-4">
  <div class="card">
    <div class="card-header">
      <strong>Seçilen Ürün : </strong>
      <a target="_blank" href="<?php echo url("urun/".seoUrl($productDetails["title"])."-p-".$productDetails["product_id"]); ?>"><?php echo $productDetails["title"]; ?></a>
    </div>
    <div class="card-body card-block">

      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="video" class=" form-control-label"><h3>Video Yükle</h3></label>
          </div>
          <div class="col col-md-2">
            <label for="video" class=" form-control-label">Video Youtube Linki</label>
          </div>
          <div class="col col-md-6">
            <input class="form-control" type="text" name="video_url" placeholder="ör:www.youtube.com?v=hw5223j4b" required>
          </div>
          <div class="col col-md-3">
            <button type="submit" name="insertProductVideo" class="btn btn-primary btn-sm">
              <i class="fa fa-dot-circle-o"></i> Yükle
            </button>
          </div>
        </div>
        <!-- input -->
      </form>
      <hr>
      <form action="" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Yüklü Videolar</h3></label>
          </div>
          <?php if(count($productVideos) == 0) { ?>
            <div class="col col-md-12">
              <p>Yüklü Video Bulunamadı.</p>
            </div>
          <?php } else { ?>
            <?php foreach($productVideos as $productVideo){ ?>
              <div class="col col-md-2">
                <iframe width="200" height="150" src="<?php echo $productVideo["product_video_url"]; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <input type="hidden" name="product_video_id" value="<?php echo $productVideo["product_video_id"]; ?>">
                <button style="width:75px; background: red; margin-left: 60px;" type="submit" name="deleteProductVideo" class="btn btn-primary btn-sm">
                  <i class="fas fa-trash-alt"></i> Sil
                </button>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
        <!-- input -->
      </form>


    </div>
  </div>
</div>
