<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Yeni Varyant Değeri</strong> Ekle
    </div>
    <div class="card-body card-block">
      <form action="" id="insertVariantValueForm" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col-md-2">
            <label for="" class=" form-control-label">Varyant Değeri</label>
          </div>
          <div class="col col-md-4">
            <input type="text" class="form-control" name="variant_value" value="" required>
          </div>
        </div>
        <!-- input -->
      </form>
    </div>
    <div class="card-footer">
      <button type="submit" form="insertVariantValueForm" name="insertVariantValue" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
  <div class="card">
    <div class="card-header">
      <strong>Kayıtlı</strong> Varyantlar
    </div>
    <div class="card-body card-block">
      <?php if(count($variantValues) == 0){ ?>
        <p>Kayıtlı varyant bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>ID</th>
            <th>Varyant Değeri</th>
            <th>Sil</th>
            <th>Eşleştir</th>
          </thead>
          <?php foreach ($variantValues as $key => $variantValue): ?>
            <tr>
              <td><?php echo $variantValue["variant_value_id"]; ?></td>
              <td><?php echo $variantValue["variant_value_name"]; ?></td>
              <td>
                <form class="" action="" method="post">
                  <input type="hidden" name="variant_value_id" value="<?php echo $variantValue["variant_value_id"]; ?>">
                  <button type="submit" class="btn btn-danger" name="delete_variant_value">Sil</button>
                </form>
              </td>
              <td> <a class="btn btn-dark" href="<?php echo adminUrl("varyant-degeri-eslestir?variantValueId=".$variantValue["variant_value_id"]); ?>">Eşleştir</a> </td>
            </tr>
          <?php endforeach; ?>
        </table>
      <?php } ?>
    </div>
  </div>
</div>
