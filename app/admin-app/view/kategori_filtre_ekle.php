<?php if(isset($pageMessage) && trim($pageMessage)):?>
  <div class="alert alert-info" role="alert">
    <?php echo $pageMessage; ?>
  </div>
<?php endif;?>
<p>
  <b>Seçilen Kategori : </b> <?php echo $categoryInfo["name"]; ?> &nbsp;
  <a class="" href="<?php echo adminUrl("kategori-sec/kategori-filtre-ekle"); ?>">Kategoriyi Değiştir</a>
</p>
<br>
<form class="" action="" method="post">
  <?php if(count($filters) > 0){ ?>
    <div class="row form-group">
      <div class="col col-md-5">
        <p>Eklenecek Filtre</p>
        <select class="form-control" name="filterId" required>
          <?php foreach ($filters as $key => $filter) { ?>
            <option value="<?php echo $filter["filter_id"]; ?>"><?php echo $filter["name"]; ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="col col-md-1">
        <input class="btn btn-success mt-4" type="submit" name="insertCategoryFilter" value="EKLE" />
      </div>
      <div class="col col-md-6">
        <input class="mt-4" id="sub_categories" type="checkbox" name="sub_categories" value="1">
        <label class="mt-4" for="sub_categories">Alt Kategorilerede Uygula</label>
      </div>
    </div>
  <?php } else { ?>
    <div class="alert alert-info" role="alert">
      Kategoriye filtre ekleyebilmeniz için önce filtre eklemelisiniz. <br>
      <a href="<?php echo adminUrl("filtre-ekle"); ?>">Filtre eklemek için tıklayınız</a>
    </div>
  <?php } ?>
</form>

<h4><?php echo $category["name"]; ?> Kategorisine Ait Filtreler</h4>
<div class="vue-lists mt-4">
  <?php if(!is_array($categoryFilters) || count($categoryFilters) <= 0) { ?>
    <p>Filtre Bulunamadı</p>
  <?php } else { ?>
  <ul>
      <?php foreach ($categoryFilters as $key => $categoryFilter):?>
      <!-- burassı döngü -->
      <li>
          <!-- özellik -->
          <form action='' method='post'>
            <div class='row form-group'>
              <div class='col col-md-6'>
                <input type='text' class='form-control' disabled name='filterName' value='<?php echo $categoryFilter['name'];?>' required />
                <input type='hidden' name='filterId' value='<?php echo $categoryFilter['filter_id'];?>' />
              </div>
              <div class='col col-md-1'>
                <input class='btn btn-danger' type='submit' name='deleteCategoryFilter' value='Kaldır'/>
              </div>
              <div class='col col-md-5'>
                <input class=" "   id="<?php echo $categoryFilter["filter_id"]; ?>" type="checkbox" name="sub_categories" value="1">
                <label for="<?php echo $categoryFilter["filter_id"]; ?>">Alt kategorilerden de kaldır</label>
              </div>
            </div>
          </form>
      </li>
      <!-- burası döngü -->
      <?php endforeach;?>
    <?php } ?>
  </ul>
</div>
