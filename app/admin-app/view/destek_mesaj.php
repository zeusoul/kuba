<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>

    <form class="" action="" method="get">
      <div class="row form-group">
            <div class="col col-md-4">
              <input id="cc-number" name="value" type="text" value="<?php echo $_GET["value"]; ?>" class="form-control" placeholder="ara">
            </div>
            <div class="col col-md-6">
              <select class="form-control" name="type" required>
                <option value="users.name" selected>Müşteri Adına Göre</option>
                <option value="products.product_code">Ürün Koduna Göre</option>
              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col col-md-2">
              <button type="submit" name="search" class="btn btn-filter btn-secondary">
                <i class="fa fa-search"></i>
              </button>
            </div>
      </div>
    </form>

    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right">

      </div>
        <table class="table table-data2">
          <thead>
            <tr>
              <th></th>
              <th>Mesaj Tarihi</th>
              <th>Müşteri Adı Soyadı</th>
              <th>KONU</th> <!-- burası kisalt() fonksiyonu ile kısaltılacak -->
              <th>İşlemler</th>
            </tr>
          </thead>
          <tbody>
            <?php if(!isset($messages) || !is_array($messages) || count($messages) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Destek Mesajı Bulunamadı</td>
              </tr>
            <?php } else { ?>
            <?php foreach ($messages as $key => $message) { ?>
              <?php $color = ($message["new_message"] === "true") ? "black" : "gray"; ?>
              <tr class="tr-shadow">
                <td>
                  <?php if($message["new_message"] === "true"){ ?>
                    <h3 style="color:<?php echo $color; ?>;">*</h3>
                  <?php } ?>
                </td>
                <td><p style="color:<?php echo $color; ?>;"><?php echo $message["posting_date"]; ?></p></td>
                <td>
                  <p style="color:<?php echo $color; ?>;">
                    <?php if((int)$message["sender_id"] > 0) { ?>
                      <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$message["sender_id"]); ?>">
                    <?php } ?>
                    <?php echo $message["name_surname"]; ?>
                    <?php if($message["sender_id"] <= 0) echo "(Misafir Kullanıcı)"; ?>
                    <?php if((int)$message["sender_id"] > 0) { ?>
                      </a>
                    <?php } ?>
                  </p>
                </td>
                <td>
                  <p style="color:<?php echo $color; ?>;" title="<?php echo $message['subject']; ?>"><?php echo $message['subject']; ?></p>
                </td>
                <td>
                  <div class="table-data-feature">
                    <a class="item" href="<?php echo adminUrl("mesaj/".$message["message_id"]."/".$message["sender_id"]); ?>"><i class="zmdi zmdi-eye"></i></a>
                  </div>
                </td>
              </tr>
            <?php } ?>
          <?php } ?>
        </tbody>
      </table>

    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
