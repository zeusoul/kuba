<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Slider</strong> Bilgileri
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz * (Önerilen : 1920x700)</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="year" class=" form-control-label">Yıl</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="year" value="<?php echo $_POST["year"]; ?>" name="year" placeholder="Yıl Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Başlık</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["title"]; ?>" name="title" placeholder="Başlık Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="description" class=" form-control-label">Açıklama</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="description" value="<?php echo $_POST["description"]; ?>" name="description" placeholder="Açıklama Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="router" class=" form-control-label">Yönlendirilecek Sayfa</label>
          </div>
          <div class="col-12 col-md-9">
            <input class="form-control" type="text" name="url" value="<?php echo $_POST["url"]; ?>" placeholder="www.orneklink.com">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="price" class=" form-control-label">Gösterilecek Fiyat Bilgisi</label>
          </div>
          <div class="col-12 col-md-9">
            <input class="form-control" type="text" name="price" value="<?php echo $_POST["price"]; ?>" placeholder="$29">
          </div>
        </div>
        <!-- input -->
        <div class="card-footer">
          <button type="submit" name="insertSlider" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
