<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Yeni Sayfa</strong> Ekle
    </div>
    <div class="card-body card-block">
      <form id="insertForm" action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Sayfa Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["menu-title"]; ?>" name="menu-title" placeholder="Menüde Gösterilecek Ad" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- editor -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="content" class=" form-control-label">Sayfa İçerik</label>
          </div>
          <div class="col-12 col-md-9">
            <textarea value="<?php echo $_POST["content"]; ?>" name="content" required></textarea>
            <script>
              CKEDITOR.replace( 'content' );
            </script>
          </div>
        </div>
        <!-- editor -->
        <!-- select -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="price" class=" form-control-label">Yayın Durumu *</label>
          </div>
          <div class="col-12 col-md-9">
            <select class="form-control" name="status" required>
              <option value="1" selected>Yayınla</option>
              <option value="0">Yayında DEĞİL</option>
            </select>
          </div>
        </div>
        <!-- select -->
      </form>
    </div>
    <div class="card-footer">
      <button form="insertForm" type="submit" name="insertPage" class="btn btn-primary btn-sm">
        <i class="fa fa-dot-circle-o"></i> Kaydet
      </button>
    </div>
  </div>
</div>
