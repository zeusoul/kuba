<?php if(isset($pageMessage) && trim($pageMessage)):?>
<div class="alert alert-info" role="alert">
  <?php echo $pageMessage; ?>
</div>
<?php endif;?>
<form class="" action="" method="post" enctype="multipart/form-data">
  <div class="row form-group">
    <div class="col col-md-3">
      <p>Kategori Resmi (Önerilen : 770x300)</p>
      <input type="file" class="form-control" name="categoryImage" value="" >
    </div>
    <div class="col col-md-3">
      <p>Kategori İsmi *</p>
      <input type="text" class="form-control" name="categoryName" value="" required>
    </div>
    <div class="col col-md-4">
      <p>Üst Kategori *</p>
      <select class="form-control" name="parentId">
        <option value="0" selected required>YOK</option>
        <?php foreach ($categories as $key => $category) { ?>
          <option value="<?php echo $category["category_id"]; ?>">
            <?php if (isset($category["parent"])): ?>
              <?php echo $category["parent"]["name"]; ?>
              >
            <?php endif; ?>
           <strong><?php echo $category["name"]; ?></strong>
          </option>
        <?php } ?>
      </select>
    </div>
    <div class="col col-md-2">
      <input class="btn btn-success mt-4" type="submit" name="insertCategory" value="EKLE" />
    </div>
  </div>
</form>

<hr class="mt-5 mb-5">
<div class="vue-lists mt-4">
  <h4>Oluşturulan Kategoriler</h4>
  <br>
  <?php
    printMenu($menu);
    function printMenu($items){ ?>
      <ul>
      <?php foreach ($items as $item) {
        $categoryId = $item["category_id"];
        $name = $item["name"];
      ?>
        <li>
          <form class="" action="" method="post">
            <div class='row form-group'>
              <div class='col col-md-6'>
                <input type='text' class='form-control' name='categoryName' value='<?php echo $item["name"]; ?>' required />
                <input type='hidden' name='categoryId' value='<?php echo $categoryId ?>' />
              </div>
              <div class='col col-md-6'>
                <a class='btn btn-success' href="<?php echo adminUrl("kategori-duzenle?id=$categoryId"); ?>">Düzenle</a>
                <input class='btn btn-danger' type='submit' name='deleteCategory' value='Sil'/>
                <a href="<?php echo adminUrl("kategori-filtre-ekle/$categoryId"); ?>" class='btn btn-primary' type='submit' >Kategori Özellikleri</a>
                <a href="<?php echo adminUrl("kategori-eslestir?id=$categoryId"); ?>" class='btn btn-dark' type='submit' >Kategori Eşleştir</a>
              </div>
            </div>
          </form>
        </li>
        <?php
            if (count($item['subCategories'])>0) {
              printMenu($item['subCategories']);
            }
          }
        ?>
    </ul>
  <?php } ?>
</div>
