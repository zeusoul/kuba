<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>
    <form class="" action="" method="get">
      <div class="row form-group">
            <div class="col col-md-4">
              <input name="value" type="text" value="<?php echo $_GET["value"]; ?>" class="form-control" placeholder="ara">
            </div>
            <div class="col col-md-6">
              <select class="form-control" name="type" required>
                <?php
                  if($_GET["type"]=="name") $nameSelected="selected";
                  else if($_GET["type"]=="surname") $surnameSelected="selected";
                  else if($_GET["type"]=="email") $emailSelected="selected";
                ?>
                <option value="name" <?php echo $nameSelected; ?>>Müşteri Adına Göre</option>
                <option value="surname" <?php echo $surnameSeelcted; ?>>Müşteri Soyadına Göre</option>
                <option value="email" <?php echo $emailSelected; ?>>Email Adresine Göre</option>
              </select>
              <div class="dropDownSelect2"></div>
            </div>
            <div class="col col-md-2">
              <button type="submit" name="search" class="btn btn-filter btn-secondary">
                <i class="fa fa-search"></i>
              </button>
            </div>
      </div>
    </form>
    <div class="table-responsive table-responsive-data2">
      <div class="table-data__tool-right"></div>
      <table class="table table-data2">
        <thead>
          <tr>
            <th>Müşteri Adı</th>
            <th>Kayıt Tarihi</th>
            <th>Engel Durumu</th>
            <th class="text-center">
              İşlemler<br/>
              [Engel][Detay]
            </th>
          </tr>
        </thead>
        <tbody>
          <?php if(!isset($users) || !is_array($users) || count($users) <= 0){ ?>
            <tr class="tr-shadow">
              <td colspan="5" class="desc">Kayıtlı Müşteri Bulunamadı</td>
            </tr>
          <?php } else { ?>
            <?php foreach ($users as $key => $user) { ?>
              <tr class="tr-shadow">
                <td class="desc">
                  <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$user["user_id"]) ?>"><?php echo $user["name"]." ".$user["surname"]; ?></a>
                </td>
                <td>
                  <?php echo $user["register_date"]; ?>
                </td>
                <td>
                  <?php if((int)$user["status"] == 1) { ?>
                    <span class="status--process">
                      Erişime Açık
                    </span>
                  <?php } else { ?>
                    <span class="status--process" style="color:red;">
                      Hesap Engellenmiş
                    </span>
                  <?php } ?>
                </td>
                <!-- işlemler -->
                <td class="text-center">
                  <div class="table-data-feature text-center">
                  <?php if((int)$user["status"] == 1) { ?>
                    <a  href="<?php echo adminUrl("musteri-engelle/".$user["user_id"]) ?>" class="item  "> <i class="zmdi zmdi-block-alt"></i>  </a>
                  <?php } else { ?>
                    <a  href="<?php echo adminUrl("musteri-engel-kaldir/".$user["user_id"]) ?>" class="item  "> <i class="zmdi zmdi-check"></i>  </a>
                  <?php } ?>
                    <a target="_blank" href="<?php echo adminUrl("musteri-detay/".$user["user_id"]) ?>" class="item  "> <i class="zmdi zmdi-account-circle"></i>  </a>
                  </div>
                </td>
              </tr>
          <?php } ?>
        <?php } ?>
        </tbody>
      </table>
    </div>
    <!-- END DATA TABLE -->
  </div>
</div>
