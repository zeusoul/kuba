<div class="row">
  <div class="col-md-12">
    <!-- DATA TABLE -->
    <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
      <div class="alert alert-info" role="alert">
        <?php echo $pageMessage; ?>
      </div>
    <?php } ?>

    <ul class="nav nav-tabs" id="tab" role="tablist">
      <li class="nav-item" role="presentation">
        <a class="nav-link <?php if(!isset($_GET["trendyolPage"])) echo "active"; ?>" id="modamahur-tab" data-toggle="tab" href="#modamahur" role="tab" aria-controls="modamahur" aria-selected="true">Modamahur</a>
      </li>
      <li class="nav-item" role="presentation">
        <a class="nav-link <?php if(isset($_GET["trendyolPage"])) echo "active"; ?>" id="trendyol-tab" data-toggle="tab" href="#trendyol" role="tab" aria-controls="trendyol" aria-selected="false">Trendyol</a>
      </li>
    </ul>
    <div class="tab-content pt-4" id="tabContent">
      <div class="tab-pane fade <?php if(!isset($_GET["trendyolPage"])) echo "show active"; ?>" id="modamahur" role="tabpanel" aria-labelledby="modamahur-tab">
        <!-- Modamahur sipariş listesi -->
        <!-- Bilgilendirme -->
        <div class="col-md-12 text-center">
          Toplam <?php echo $orders["total_orders"]; ?> kayıt arasından gösterilen aralık : <?php echo $offset." - ".$limit; ?>
        </div>
        <!-- Bilgilendirme -->
        <!-- Sayfalandırma -->
        <hr>
        <div class="col-md-12 text-center">
          <?php for ($i = 0; $i < $numberOfPages; $i++): ?>
            <form class="label pulse pulse-info" action="" method="get">
              <?php foreach ($_GET as $key => $value) { ?>
                <?php if($key != "page" && $key != "trendyolPage"){ ?>
                  <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
                <?php } ?>
              <?php } ?>
              <input type="hidden" name="page" value="<?php echo $i + 1; ?>">
              <button type="submit" class="btn p-0 m-0 ">
                <span class="label pulse pulse-info <?php if($pageNumber == $i+1) echo "bg-info text-white"; ?>">
                  <span class="position-relative"><?php echo $i+1; ?></span>
                  <span class="pulse-ring"></span>
                </span>
              </button>
            </form>
          <?php endfor; ?>
        </div>
        <hr>
        <!-- Sayfalandırma -->
        <table class="table">
          <thead>
            <tr>
              <th>Sipariş No</th>
              <th>Müşteri Adı</th>
              <th>Müşteri Soyadı</th>
              <th>Müş. Tel. No</th>
              <th>Sipariş Tarihi</th>
              <th>Sipariş Tutarı</th>
              <th>Sipariş Durumu</th>
              <th>İşlemler</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <form class="" action="" method="get">
                <td> <input class="form-control form-control-sm" type="text" name="order_no" value="<?php echo $_GET["order_no"]; ?>"> </td>
                <td> <input class="form-control form-control-sm" type="text" name="name" value="<?php echo $_GET["name"]; ?>"> </td>
                <td> <input class="form-control form-control-sm" type="text" name="surname" value="<?php echo $_GET["surname"]; ?>"> </td>
                <td> <input class="form-control form-control-sm" type="text" name="phone" value="<?php echo $_GET["phone"]; ?>"> </td>
                <td> <input class="form-control form-control-sm" type="text" name="order_date" value="<?php echo $_GET["order_date"]; ?>"> </td>
                <td> <input class="form-control form-control-sm" type="text" name="order_price" value="<?php echo $_GET["order_price"]; ?>"> </td>
                <td>
                  <select class="form-control form-control-sm" name="status">
                    <option value="" selected>Seçiniz..</option>
                    <option <?php if($_GET["status"] === -1) echo "selected"; ?> value="-1">İptal Edilen Siparişler</option>
                    <option <?php if($_GET["status"] === 0) echo "selected"; ?> value="0">Onay Bekleyen Siparişler</option>
                    <option <?php if($_GET["status"] === 1) echo "selected"; ?> value="1">Onaylanan Siparişler</option>
                    <option <?php if($_GET["status"] === 2) echo "selected"; ?> value="2">Kargoya Verilen Siparişler</option>
                    <option <?php if($_GET["status"] === 3) echo "selected"; ?> value="3">Teslim Edilen Siparişler</option>
                  </select>
                </td>
                <td> <button class="btn btn-secondary btn-sm float-right" type="submit" name="search"><i class="fa fa-search"></i></button> </td>
              </form>
            </tr>
            <?php if(!isset($orders) || !is_array($orders) || count($orders) <= 0){ ?>
              <tr class="tr-shadow">
                <td colspan="5" class="desc">Sipariş Bulunamadı</td>
              </tr>
            <?php } else { ?>
              <?php foreach ($orders["orders"] as $key => $order) { ?>
                <tr class="tr-shadow">
                  <td>
                    <a href="<?php echo adminUrl("siparis-detay/".$order["order_no"]); ?>">
                      <strong><?php echo $order["order_no"]; ?></strong>
                    </a>
                  </td>
                  <td>
                    <a href="<?php echo adminUrl("musteri-detay/".$order["user_id"]); ?>">
                      <strong><?php echo $order["name"]; ?></strong>
                    </a>
                  </td>
                  <td>
                    <a href="<?php echo adminUrl("musteri-detay/".$order["user_id"]); ?>">
                      <strong><?php echo $order["surname"]; ?></strong>
                    </a>
                  </td>
                  <td><?php echo $order["tel"]; ?></td>
                  <td><?php echo $order["order_date"]; ?></td>
                  <td>  <?php echo $order["order_amount"]; ?> ₺</td>

                  <td>
                    <?php if($order["status"] == -1) { ?>
                        <span class="badge badge-danger" >
                           İptal Edildi
                        </span>
                    <?php } else if($order["status"] == 0) { ?>
                        <span class="badge badge-primary" >
                           Onay Bekliyor
                        </span>
                    <?php } else if($order["status"] == 1) { ?>
                      <span class="badge badge-primary" >
                        Onaylandı
                      </span>
                    <?php } else if($order["status"] == 2) { ?>
                        <span class="badge badge-primary" >
                             Kargoya Verildi
                        </span>
                    <?php } else if($order["status"] == 3) { ?>
                        <span class="badge badge-success" >
                            Teslim Edildi
                        </span>
                    <?php } ?>
                  </td>
                  <!-- işlemler -->
                  <td>
                    <div class="table-data-feature">
                      <a href="<?php echo adminUrl("siparis-detay/".$order["order_no"]) ?>" class="item"> <i class="zmdi zmdi-eye"></i>  </a>
                    </div>
                  </td>
                </tr>
              <?php } ?>
            <?php } ?>
          </tbody>
        </table>
        <!-- Sayfalandırma -->
        <hr>
        <div class="col-md-12 text-center">
          <?php for ($i = 0; $i < $numberOfPages; $i++): ?>
            <form class="label pulse pulse-info" action="" method="get">
              <?php foreach ($_GET as $key => $value) { ?>
                <?php if($key != "page" && $key != "trendyolPage"){ ?>
                  <input type="hidden" name="<?php echo $key; ?>" value="<?php echo $value; ?>">
                <?php } ?>
              <?php } ?>
              <input type="hidden" name="page" value="<?php echo $i + 1; ?>">
              <button type="submit" class="btn p-0 m-0 ">
                <span class="label pulse pulse-info <?php if($pageNumber == $i+1) echo "bg-info text-white"; ?>">
                  <span class="position-relative"><?php echo $i+1; ?></span>
                  <span class="pulse-ring"></span>
                </span>
              </button>
            </form>
          <?php endfor; ?>
        </div>
        <hr>
        <!-- Sayfalandırma -->
        <!-- Modamahur sipariş listesi -->
      </div>
      <div class="tab-pane fade <?php if(isset($_GET["trendyolPage"])) echo "show active"; ?>" id="trendyol" role="tabpanel" aria-labelledby="trendyol-tab">
        <!-- Trendyol sipariş listesi -->
        <!-- Bilgilendirme -->
        <div class="col-md-12 text-center">
          Toplam <?php echo $trendyolOrders["totalElements"]; ?> kayıt arasından gösterilen aralık : <?php echo $trendyolOffset." - ".$trendyolLimit; ?>
        </div>
        <!-- Bilgilendirme -->
        <!-- Sayfalandırma -->
        <hr>
        <div class="col-md-12 text-center">
          <ul class="pagination">
            <li class="page-item <?php if($trendyolPageNumber <= 1) echo "disabled"; ?>">
              <a class="page-link" href="<?php echo ($trendyolPageNumber <= 1) ? "javascript:;" : "?trendyolPage=".($trendyolPageNumber-1); if("&$get_string" != "") echo $get_string; ?>">
                <i class="fas fa-angle-left"></i>
              </a>
            </li>
          <?php for ($i = 0; $i < $trendyolNumberOfPages; $i++): ?>
            <li class="page-item <?php if($trendyolPageNumber == $i+1) echo "active"; ?>">
              <a class="page-link" href="<?php echo "?trendyolPage=".($i+1); if($get_string != "") echo "&$get_string"; ?>"><?php echo $i+1; ?></a>
            </li>
          <?php endfor; ?>
          <li class="page-item <?php if($trendyolPageNumber >= $trendyolNumberOfPages) echo "disabled"; ?>">
            <a class="page-link" href="<?php echo ($trendyolPageNumber >= $trendyolNumberOfPages) ? "javascript:;" : "?trendyolPage=".($trendyolPageNumber+1); if($get_string != "") echo "&$get_string"; ?>">
              <i class="fas fa-angle-right"></i>
            </a>
          </li>
        </ul>
        </div>
        <hr>
        <!-- Sayfalandırma -->
        <table class="table">
          <thead>
            <tr>
              <th>Sipariş No</th>
              <th>Müşteri Adı</th>
              <th>Telefon Numarası</th>
              <th>Sipariş Tarihi</th>
              <th>Toplam Tutar</th>
              <th>Sipariş Durumu</th>
              <th>İşlemler</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($trendyolOrders["content"] as $key => $order): ?>
              <tr>
                <td><?php echo $order["orderNumber"]; ?></td>
                <td><?php echo $order["customerFirstName"]." ".$order["customerLastName"]; ?></td>
                <td><?php echo $order["invoiceAddress"]["phone"]; ?></td>
                <td><?php echo date("d.m.Y H:i:s", substr($order["orderDate"], 0, -3)); ?></td>
                <td><?php echo $order["totalPrice"]; ?></td>
                <td>
                  <?php
                    if($order["shipmentPackageStatus"] == "Created") echo "Oluşturuldu";
                    else if($order["shipmentPackageStatus"] == "Picking") echo "Hazırlanıyor";
                    else if($order["shipmentPackageStatus"] == "Invoiced") echo "Faturalandırıldı";
                    else if($order["shipmentPackageStatus"] == "Shipped") echo "Kargoya Verildi";
                    else if($order["shipmentPackageStatus"] == "Picking") echo "Toplanıyor";
                    else if($order["shipmentPackageStatus"] == "Cancelled") echo "İptal Edildi";
                    else if($order["shipmentPackageStatus"] == "Delivered") echo "Teslim Edildi";
                    else if($order["shipmentPackageStatus"] == "UnDelivered") echo "Teslim Edilemedi";
                    else if($order["shipmentPackageStatus"] == "Returned") echo "İade Edildi";
                    else if($order["shipmentPackageStatus"] == "Repack") echo "Yeniden Paketlendi";
                    else if($order["shipmentPackageStatus"] == "UnPacked") echo "Paketlenemedi";
                    else if($order["shipmentPackageStatus"] == "UnSupplied") echo "Tedarik Edilemedi";
                  ?>
                </td>
                <th>
                  <div class="table-data-feature">
                    <a href="<?php echo adminUrl("trendyol-siparis-detay?orderNo=".$order["orderNumber"]) ?>" class="item"> <i class="zmdi zmdi-eye"></i>  </a>
                  </div>
                </th>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
        <!-- Trendyol sipariş listesi -->
      </div>
    </div>
  </div>
</div>
