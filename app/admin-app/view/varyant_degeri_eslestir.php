<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <p>Seçilen Değer : <strong><?php echo $variantValue["variant_value_name"]; ?></strong> </p>
  <hr class="mb-4">
  <div class="card">
    <div class="card-header">
      <strong>Eşleştirilmiş </strong> Platformlar
    </div>
    <div class="card-body card-block">
      <?php if(count($platformVariantValues) == 0){ ?>
        <p>Kayıt bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>#</th>
            <th>Platform</th>
            <th>Varyant Değeri Kodu</th>
            <th>Sil</th>
          </thead>
          <tbody>
            <?php $i=1; ?>
            <?php foreach ($platformVariantValues as $key => $platformVariantValue) { ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $platformVariantValue["platform_name"]; ?></td>
                <td><?php echo $platformVariantValue["platform_variant_value_code"]; ?></td>
                <td>
                  <form class="" action="" method="post">
                    <button type="submit" class="btn btn-danger" name="deleteMatch">Sil</button>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
    </div>
  </div>


  <div class="card">
    <div class="card-body card-block">
      <form class="" action="" method="get">
        <div class="row">
          <div class="col-md-2">
            Platform Seçiniz :
          </div>
          <div class="col-md-4">
            <input type="hidden" name="variantValueId" value="<?php echo $_GET["variantValueId"]; ?>">
            <select  class="form-control" name="platform_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($platforms as $platform) { ?>
                <option <?php if(isset($_GET["platform_id"]) && $_GET["platform_id"] == $platform["platform_id"]) echo "selected"; ?> value="<?php echo $platform["platform_id"]; ?>"><?php echo $platform["platform_name"]; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2">
            <button  class="btn btn-primary" type="submit">Seç</button>
          </div>
        </div>
      </form>
    </div>
  </div>

  <?php if(isset($_GET["platform_id"])){ ?>
    <div class="card">
      <div class="card-header">
        <strong>Varyant Değeri</strong> Eşleştir
      </div>
      <?php if(!is_array($variantValues) || count($variantValues) == 0){ ?>
        <div class="card-body card-block">
          <div class="alert alert-info text-center" role="alert">
            TRENDYOL PLATFORMUNDA BU VARYANTA DİLERSENİZ ÖZEL DEĞER EKLEYEBİLİRSİNİZ
          </div>
          <form class="row" action="" method="post">
            <div class="col-2">
              <label for="">Özel Değer Gir :</label>
            </div>
            <div class="col-8">
              <input class="form-control" type="text" name="platform_variant_value_code" value="">
            </div>
            <div class="col-2">
              <input type="hidden" name="platform_id" value="<?php echo $_GET['platform_id']; ?>">
              <input type="hidden" name="variant_value_id" value="<?php echo $_GET['variantValueId']; ?>">
              <button type="submit" class="btn btn-success" name="matchVariantValue">Eşleştir</button>
            </div>
          </form>
        </div>
        <hr>
      <?php } ?>
      <div class="card-body card-block">
        <table id="variant-values-table" class="table table-striped table-bordered dataTable no-footer" role="grid" aria-describedby="variant-values-table_info">
          <thead>
            <th>Değer Kodu</th>
            <th>Değer Adı</th>
            <th>İşlemler</th>
          </thead>
          <tbody>
          <?php $i=0; foreach ($variantValues as $variantValue) {  ?>
              <tr>
                <td><?php echo $variantValue["id"]; ?></td>
                <td><?php echo $variantValue["name"]; ?></td>
                <td>
                  <form class="" action="" method="post">
                    <input type="hidden" name="platform_id" value="<?php echo $_GET['platform_id']; ?>">
                    <input type="hidden" name="platform_variant_value_code" value="<?php echo $variantValue["id"]; ?>">
                    <input type="hidden" name="variant_value_id" value="<?php echo $_GET['variantValueId']; ?>">
                    <button type="submit" name="matchVariantValue" class="btn btn-sm btn-success"> <i class="fas fa-check-circle"></i></button>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
      <div class="card-footer">
      </div>
    </div>
  <?php } ?>
</div>
<script type="text/javascript">
  $(document).ready(function() {
     var table = $('#variant-values-table').DataTable();
  });
</script>
