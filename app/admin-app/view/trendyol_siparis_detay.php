<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>

  <div class="card">
    <div class="card-header">
      <strong>Sipariş</strong> Bilgileri
    </div>
    <div class="card-body card-block">
        <h2 class="h3 mb-3 text-black">Müşteri Bilgileri</h2>
        <hr>
          <div class="row">
            <div class="form-group col-md-3">
              <label for="name"> <strong>Ad Soyad</strong> </label>
              <p><?php echo $order["customerFirstName"]." ".$order["customerLastName"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Telefon</strong> </label>
              <p><?php echo $order["invoiceAddress"]["phone"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Email</strong> </label>
              <p><?php echo $order["customerEmail"]; ?></p>
            </div>
          </div>
        <hr>
        <h2 class="h3 mb-3 text-black">Sipariş Detay  </h2>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="name"> <strong>Sipariş Tarihi</strong> </label>
                <p><?php echo date("d.m.Y H:i:s", substr($order["orderDate"], 0, -3)); ?></p>
              </div>
              <div class="form-group col-md-3">
                <label for=""> <strong>Sipariş No</strong> </label>
                <p><?php echo $order["orderNumber"]; ?></p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>Ödeme Yöntemi</strong> </label>
                  <p><?php echo "Kredi Kartı / Banka Kartı"; ?></p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>Toplam Tutar</strong> </label>
                  <p><?php echo $order["totalPrice"];?> TL</p>
              </div>
              <div class="col-12 text-center mt-2">
                <a class="btn btn-sm btn-primary" href="?fatura">Fatura Yazdır</a>
              </div>
            </div>
            <hr>
            <h5 class="mb-3">Adresler</h5>
            <hr>
            <div class="row">
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Teslimat Adresi</h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $order["shipmentAddress"]['firstName']." ".$order["shipmentAddress"]['lastName']; ?></h6>
                            <p class="card-text"><?php echo $order["shipmentAddress"]['phone']; ?></p>
                            <p class="card-text"><?php echo $order["shipmentAddress"]['district']." / ".$order["shipmentAddress"]["city"]; ?></p>
                            <p class="card-text">Posta Kodu : <?php echo $order["shipmentAddress"]['postalCode']; ?></p>
                            <h6 class="card-text " ><?php echo $order["shipmentAddress"]['fullAddress'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["shipmentAddress"]['company'] ?></h6>
                            <h6 class="card-text " ><?php echo $order['taxNumber'] ?></h6>
                        </div>
                    </div>
                </div>
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Fatura Adresi</h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $order["invoiceAddress"]['firstName']." ".$order["invoiceAddress"]['lastName']; ?></h6>
                            <p class="card-text"><?php echo $order["invoiceAddress"]['phone']; ?></p>
                            <p class="card-text"><?php echo $order["invoiceAddress"]['district']." / ".$order["invoiceAddress"]["city"]; ?></p>
                            <p class="card-text">Posta Kodu : <?php echo $order["invoiceAddress"]['postalCode']; ?></p>
                            <h6 class="card-text " ><?php echo $order["invoiceAddress"]['fullAddress'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["invoiceAddress"]['company'] ?></h6>
                            <h6 class="card-text " ><?php echo $order['taxNumber'] ?></h6>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h4 class="mt-4">Sipariş Edilen Ürünler</h4>
            <hr>
            <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                <thead>
                <tr>
                  <th>Barkod</th>
                  <th>Ürün Başlık</th>
                  <th>Ürün Rengi</th>
                  <th>Ürün Bedeni(Size)</th>
                  <th>Ürün Adeti</th>
                  <th>Ürün Birim Fiyat</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order["lines"] as $key => $product) :?>
                    <tr>
                      <td><?php echo $product["barcode"]; ?></td>
                      <td><?php echo $product["productName"]; ?></td>
                      <td><?php echo $product["productColor"]; ?></td>
                      <td><?php echo $product["productSize"]; ?></td>
                      <td><?php echo $product["quantity"]; ?></td>
                      <td><?php echo $product["amount"]; ?> ₺</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <h4 class="mt-4">Durum Güncellemesi</h4>
            <hr>
            <div class="mt-3">
              <?php if($order["status"] == -1){ ?>
                <div class="alert alert-danger text-center" role="alert">
                  <strong>Bu sipariş iptal edildi</strong>
                </div>
              <?php } else {?>
                <form class="" action="" method="post">
                    <input type="hidden" name="order_no" value="<?php echo $order["order_no"]; ?>">
                    <div class="form-group">
                        <label for=""><strong>Durum</strong></label>
                        <select  class="form-control col-6" name="status" id="">
                          <option value="-1" <?php if($order["status"]==-1) echo "selected"; ?> >İptal Edildi</option>
                          <option value="0" <?php  if($order["status"]==0) echo  "selected"; ?> >Onay Bekliyor</option>
                          <option value="1" <?php  if($order["status"]==1) echo  "selected"; ?> >Onaylandı</option>
                          <option value="2" <?php  if($order["status"]==2) echo  "selected"; ?> >Kargoya Verildi</option>
                          <option value="3" <?php  if($order["status"]==3) echo  "selected"; ?> >Teslim Edildi</option>
                        </select>
                    </div>
                    <label for=""><strong>Kargo Bilgileri</strong></label>
                    <div class="form-row">
                        <div class="form-group  col-6">
                            <label for="">Kargo Bilgileri:</label>

                            <input type="text" class=" form-control" name="cargo_company" placeholder="Kargo Şirketi Adı" value="<?php echo $order["cargo_company"];?>">
                        </div>
                        <div class="form-group col-6 ">
                            <label for="">Kargo Takip No:</label>

                            <input type="text" class="  form-control" name="cargo_no" placeholder="Kargo Takip No" value="<?php echo $order["cargo_no"];?>">
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" name="update" value="Durumu Güncelle" >
                </form>
              <?php } ?>
            </div>

    </div>
    </div>
  </div>
</div>
