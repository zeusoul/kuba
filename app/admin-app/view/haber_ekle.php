<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Haber</strong> Bilgileri
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Başlık *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $_POST["title"]; ?>" name="title" placeholder="Başlık Giriniz" class="form-control" required>
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="description" class=" form-control-label">Açıklama</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="description" value="<?php echo $_POST["description"]; ?>" name="description" placeholder="Açıklama Giriniz" class="form-control">
          </div>
        </div>
        <!-- input -->
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="router" class=" form-control-label">Haber İçeriği</label>
          </div>
          <div class="col-12 col-md-9">
            <textarea value="<?php echo $_POST["content"]; ?>" name="content" required></textarea>
            <script>
              CKEDITOR.replace( 'content' );
            </script>
          </div>
        </div>
        <!-- input -->
        <div class="card-footer">
          <button type="submit" name="insertNew" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
