<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong> Sayfa</strong> Düzenle
    </div>
    <div class="card-body card-block">
      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="title" class=" form-control-label">Sayfa Adı *</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="text" id="title" value="<?php echo $pageDetails["menu_title"]; ?>" name="menu-title" placeholder="Menüde Gösterilecek Ad" class="form-control" required>
          </div>
        </div>
        <!-- input -->

        <!-- editor -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="content" class=" form-control-label">Sayfa İçerik</label>
          </div>
          <div class="col-12 col-md-9">
            <textarea name="content" required> <?php echo $pageDetails["content"]; ?></textarea>
            <script>
              CKEDITOR.replace( 'content' );
            </script>
          </div>
        </div>
        <!-- editor -->
        <!-- select -->
        <div class="row form-group">
          <div class="col col-md-3">
            <label for="price" class=" form-control-label">Yayın Durumu *</label>
          </div>
          <div class="col-12 col-md-9">
            <select class="form-control" name="status" required>
              <option value="1" <?php if((int)$pageDetails["status"]==1) echo "selected" ?>>Yayınla</option>
              <option value="0" <?php if((int)$pageDetails["status"]==0) echo "selected" ?>>Yayında DEĞİL</option>
            </select>
          </div>
        </div>
        <!-- select -->
        <div class="card-footer">
          <button type="submit" name="updatePage" class="btn btn-primary btn-sm">
            <i class="fa fa-dot-circle-o"></i> Kaydet
          </button>
          <button type="submit" name="deletePage" class="btn btn-danger btn-sm">
            <i class="fa fa-dot-circle-o"></i> Sayfayı Sil
          </button>
        </div>
      </form>
    </div>
  </div>
</div>
