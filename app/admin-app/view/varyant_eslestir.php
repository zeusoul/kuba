<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="mb-4">
    Seçilen Varyant : <strong> <?php echo $variant["variant_name"]; ?> </strong>
  </div>

  <div class="card">
    <div class="card-header">
      <strong>Eşleştirilmiş </strong> Platformlar
    </div>
    <div class="card-body card-block">
      <?php if(count($platformVariants) == 0){ ?>
        <p>Kayıt bulunamadı</p>
      <?php } else{ ?>
        <table class="table">
          <thead>
            <th>#</th>
            <th>Platform</th>
            <th>Kategori Kodu</th>
            <th>Sil</th>
          </thead>
          <tbody>
            <?php $i=1; ?>
            <?php foreach ($platformVariants as $key => $platformVariant) { ?>
              <tr>
                <td><?php echo $i++; ?></td>
                <td><?php echo $platformVariant["platform_name"]; ?></td>
                <td><?php echo $platformVariant["platform_variant_code"]; ?></td>
                <td>
                  <form class="" action="" method="post">
                    <input type="hidden" name="platform_variant_id" value="<?php echo $platformVariant["platform_variant_id"]; ?>">
                    <button type="submit" class="btn btn-danger" name="deleteMatch">Sil</button>
                  </form>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
    </div>
  </div>


  <div class="card">
    <div class="card-body card-block">
      <form class="" action="" method="get">
        <div class="row">
          <div class="col-md-2">
            Platform Seçiniz :
          </div>
          <div class="col-md-4">
            <input type="hidden" name="variantId" value="<?php echo $_GET["variantId"]; ?>">
            <select  class="form-control" name="platform_id" required>
              <option value="">Seçiniz</option>
              <?php foreach ($platforms as $platform) { ?>
                <option <?php if(isset($_GET["platform_id"]) && $_GET["platform_id"] == $platform["platform_id"]) echo "selected"; ?>
                    value="<?php echo $platform["platform_id"]; ?>">
                    <?php echo $platform["platform_name"]; ?>
                </option>
              <?php } ?>
            </select>
          </div>
          <div class="col-md-2">
            <button  class="btn btn-primary" type="submit">Seç</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <?php if(isset($_GET["platform_id"])){ ?>
    <div class="card">
      <div class="card-header">
        <strong>Kategori</strong> Seçiniz
      </div>
      <div class="card-body card-block">
        <?php if(is_array($categories) && count($categories) > 0){ ?>
          <form class="" action="" method="get">
            <div class="row">
                <div class="col-md-2">
                  <label for="">Kategori</label>
                </div>
                <div class="col-md-4">
                  <input type="hidden" name="variantId" value="<?php echo $_GET["variantId"]; ?>">
                  <input type="hidden" name="platform_id" value="<?php echo $_GET["platform_id"]; ?>">
                  <select class="form-control" name="category_code" required>
                    <option value="">Seçiniz</option>
                    <?php foreach ($categories as $key => $category) { ?>
                      <option <?php if(isset($_GET["category_code"]) && $_GET["category_code"] == $category["platform_category_code"]) echo "selected"; ?> value="<?php echo $category["platform_category_code"]; ?>"><?php echo $category["name"]; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-2">
                  <button class="btn btn-success" type="submit">Seç</button>
                </div>
            </div>
          </form>
        <?php } else { ?>
          <p>Seçmiş olduğunuz platforma ait eşleştirilmiş kategori bulunamadı.</p>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
  <?php if(isset($_GET["platform_id"]) && isset($_GET["category_code"])){ ?>
    <div class="card">
      <div class="card-header">
        <strong>Varyant</strong> Eşleştir
      </div>
      <div class="card-body card-block">
        <?php if(!is_array($variants) || count($variants) == 0){ ?>
          <p>Bu kategoriye ait varyant bulunamadı.</p>
        <?php } else{ ?>
          <table id="categories-table" class="table table-striped table-bordered " >
            <thead>
              <th>Varyant Kodu</th>
              <th>Varyant Adı</th>
              <th>İşlemler</th>
            </thead>
            <tbody>
            <?php $i=0; foreach ($variants as $variant) {  ?>
                <tr>
                  <td><?php echo $variant["id"]; ?></td>
                  <td><?php echo $variant["name"]; ?></td>
                  <td>
                    <form class="" action="" method="post">
                      <input type="hidden" name="platform_id" value="<?php echo $_GET['platform_id']; ?>">
                      <input type="hidden" name="platform_variant_code" value="<?php echo $variant["id"]; ?>">
                      <input type="hidden" name="platform_category_code" value="<?php echo $_GET["category_code"]; ?>">
                      <input type="hidden" name="variant_id" value="<?php echo $_GET['variantId']; ?>">
                      <button type="submit" name="matchVariant" class="btn btn-sm btn-success"> <i class="fas fa-check-circle"></i></button>
                    </form>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        <?php } ?>
      </div>
    </div>
  <?php } ?>
</div>
