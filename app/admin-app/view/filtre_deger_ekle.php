<?php if(isset($pageMessage) && trim($pageMessage) != ""):?>
  <div class="alert alert-info" role="alert">
    <?php echo $pageMessage; ?>
  </div>
<?php endif;?>

<h4 >Seçilen Filtre : <?php echo $Selectedfilter["name"] ?></h4>
<a href="<?php echo adminUrl("filtre-ekle")?>">Tüm Filtreleri Gör</a>

<form class="" action="" method="post">
  <div class="row form-group">

      <div class="col col-md-5">
        <p>Değer</p>
        <input type="text" class="form-control" name="valueName" required>
      </div>
      <div class="col col-md-2">
        <input class="btn btn-success mt-4" type="submit" name="insertFilterValue" value="EKLE" />
      </div>
  </div>
</form>


<div class="vue-lists">
  <h6>Değerler</h6>
  <ul>

    <?php foreach ($values as $key => $filter): ?>
      <form action="" method="POST">
        <li class="mt-2">
        <div class="input-group">
          <div class="col">
            <input type="text"  class="form-control" name="valueName" id="" value="<?php echo $filter["value"]; ?>">
          </div>
          <div class="col">
            <input class="btn btn-primary" type="submit" name="updateFilterValue" value="Güncelle">
            <input class="btn btn-danger" type="submit" name="deleteFilterValue" value="Sil">
            <input type="hidden" name="filterValueId" value="<?php echo $filter["filter_value_id"]; ?>">
            <input type="hidden" name="filterId" value="<?php echo $filter["filter_id"]; ?>">
          </div>
        </div>
        </li>
      </form>
    <?php endforeach; ?>

  </ul>

</div>
