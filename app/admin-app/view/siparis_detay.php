<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>

  <div class="card">
    <div class="card-header">
      <strong>Sipariş</strong> Bilgileri
    </div>
    <div class="card-body card-block">
        <h2 class="h3 mb-3 text-black">Müşteri Bilgileri</h2>
        <hr>
          <div class="row">
            <div class="form-group col-md-3">
              <label for="name"> <strong>Ad Soyad</strong> </label>
              <p><?php echo $order["user_information"]["name"]." ".$order["user_information"]["surname"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Telefon</strong> </label>
              <p><?php echo $order["user_information"]["tel"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Email</strong> </label>
              <p><?php echo $order["user_information"]["email"]; ?></p>
            </div>
            <div class="form-group col-md-3">
              <label for="name"> <strong>Detay</strong> </label>
              <br>
              <a class="btn btn-primary" href="<?php echo adminUrl("musteri-detay/".$order["user_information"]["user_id"]); ?>">Detay</a>
            </div>
          </div>
        <hr>
        <h2 class="h3 mb-3 text-black">Sipariş Detay  </h2>
        <hr>
        <div class="row">
            <div class="form-group col-md-3">
                <label for="name"> <strong>Sipariş Tarihi</strong> </label>
                <p><?php echo $order["order_date"]; ?></p>
              </div>
              <div class="form-group col-md-3">
                <label for=""> <strong>Sipariş No</strong> </label>
                <p><?php echo $order["order_no"]; ?></p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>Ödeme Yöntemi</strong> </label>
                  <p>
                    <?php
                      if ($order["payment_type"]==0) echo "Kredi kartı / Banka kartı";
                      else if ($order["payment_type"]==1) echo "Havale / EFT";
                    ?>
                  </p>
              </div>
              <div class="form-group col-md-3">
                  <label for="name"> <strong>Toplam Tutar</strong> </label>
                  <p>
                      <?php
                      echo $order["order_amount"];
                      ?> TL
                  </p>
              </div>
              <div class="col-12 text-center mt-2">
                <a class="btn btn-sm btn-primary" href="?fatura">Fatura Yazdır</a>
                </div>
              <?php if(isset($order["bank_details"])){ ?>
                <div class="form-group col-md-12">
                  <hr>
                  <label for="name"> <strong>Banka Bilgileri</strong> </label>
                  <p>
                    <?php
                       echo $order["bank_details"]["bank_name"];
                       echo "<br>";
                       echo $order["bank_details"]["account_name"];
                       echo "<br>";
                       echo $order["bank_details"]["iban_no"];
                    ?>
                  </p>
                </div>
              <?php } ?>

            </div>
            <hr>
            <h5 class="mb-3">Adresler</h5>
            <hr>


            <div class="row">
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Teslimat Adresi</h5>
                            <h5 class="card-title"><?php echo $order["teslimat_address"]['title'] ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $order["teslimat_address"]['name_surname']; ?></h6>
                            <p class="card-text"><?php echo $order["teslimat_address"]['tel']; ?></p>
                            <p class="card-text"><?php echo $order["teslimat_address"]['county']." / ".$order["teslimat_address"]["city"]; ?></p>
                            <h6 class="card-text " ><?php echo $order["teslimat_address"]['address'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["teslimat_address"]['company_name'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["teslimat_address"]['tax_administration'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["teslimat_address"]['tax_number'] ?></h6>
                        </div>
                    </div>
                </div>
                <div class="adress col-md-6 mt-2">
                    <div class="card" >
                        <div class="card-body">
                            <h5 class="mb-3">Fatura Adresi</h5>
                            <h5 class="card-title"><?php echo $order["fatura_address"]['title'] ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo $order["fatura_address"]['name_surname']; ?></h6>
                            <p class="card-text"><?php echo $order["fatura_address"]['tel']; ?></p>
                            <p class="card-text"><?php echo $order["fatura_address"]['county']." / ".$order["teslimat_address"]["city"]; ?></p>
                            <h6 class="card-text " ><?php echo $order["fatura_address"]['address'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["fatura_address"]['company_name'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["fatura_address"]['tax_administration'] ?></h6>
                            <h6 class="card-text " ><?php echo $order["fatura_address"]['tax_number'] ?></h6>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <h4 class="mt-4">Sipariş Edilen Ürünler</h4>
            <hr>
            <table id="adrestablosu" method="get" class="table table-striped display nowrap table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>Ürün Başlık</th>
                    <th>Sipariş Edilen Varyant</th>
                    <th>Ürün Adeti</th>
                    <th>Ürün Birim Fiyat</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order["product_variants"] as $key => $product) :?>
                    <tr>
                        <td><a href="<?php echo url(seoUrl($product["title"])."-p-".$product["barcode"]); ?>" name="musteri-sec"  ><?php echo $product["title"]; ?></a></td>
                        <td><?php echo $product["combine"]; ?></td>
                        <td><?php echo $product["quantity"]; ?></td>
                        <td><?php echo $product["unit_price"]; ?> ₺</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <h4 class="mt-4">Durum Güncellemesi</h4>
            <hr>
            <div class="mt-3">
              <?php if($order["status"] == -1){ ?>
                <div class="alert alert-danger text-center" role="alert">
                  <strong>Bu sipariş iptal edildi</strong>
                </div>
              <?php } else {?>
                <form class="" action="" method="post">
                    <input type="hidden" name="order_no" value="<?php echo $order["order_no"]; ?>">
                    <div class="form-group">
                        <label for=""><strong>Durum</strong></label>
                        <select  class="form-control col-6" name="status" id="">
                          <option value="-1" <?php if($order["status"]==-1) echo "selected"; ?> >İptal Edildi</option>
                          <option value="0" <?php  if($order["status"]==0) echo  "selected"; ?> >Onay Bekliyor</option>
                          <option value="1" <?php  if($order["status"]==1) echo  "selected"; ?> >Onaylandı</option>
                          <option value="2" <?php  if($order["status"]==2) echo  "selected"; ?> >Kargoya Verildi</option>
                          <option value="3" <?php  if($order["status"]==3) echo  "selected"; ?> >Teslim Edildi</option>
                        </select>
                    </div>
                    <label for=""><strong>Kargo Bilgileri</strong></label>
                    <div class="form-row">
                        <div class="form-group  col-6">
                            <label for="">Kargo Bilgileri:</label>

                            <input type="text" class=" form-control" name="cargo_company" placeholder="Kargo Şirketi Adı" value="<?php echo $order["cargo_company"];?>">
                        </div>
                        <div class="form-group col-6 ">
                            <label for="">Kargo Takip No:</label>

                            <input type="text" class="  form-control" name="cargo_no" placeholder="Kargo Takip No" value="<?php echo $order["cargo_no"];?>">
                        </div>
                    </div>
                    <input type="submit" class="btn btn-primary" name="update" value="Durumu Güncelle" >
                </form>
              <?php } ?>
            </div>

    </div>
    </div>
  </div>
</div>
