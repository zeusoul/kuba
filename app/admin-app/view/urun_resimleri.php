<div class="col-md-12">
  <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
    <div class="alert alert-info" role="alert">
      <?php echo $pageMessage; ?>
    </div>
  <?php } ?>
  <div class="card">
    <div class="card-header">
      <strong>Seçilen Ürün : </strong>
      <a target="_blank" href="<?php echo url("urun/".seoUrl($productDetails["title"])."-p-".$productDetails["product_id"]); ?>"><?php echo $productDetails["title"]; ?></a>
    </div>
    <div class="card-body card-block">

      <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Resim Yükle</h3></label>
          </div>
          <div class="col col-md-3">
            <label for="image" class=" form-control-label">Resim Seçiniz</label>
          </div>
          <div class="col-12 col-md-9">
            <input type="file" id="image" name="image">
            <button type="submit" name="insertProductImage" class="btn btn-primary btn-sm">
              <i class="fa fa-dot-circle-o"></i> Yükle
            </button>
          </div>
        </div>
        <!-- input -->
      </form>
      <hr>
      <form action="" method="post" class="form-horizontal">
        <!-- input -->
        <div class="row form-group">
          <div class="col col-md-12">
            <label for="image" class=" form-control-label"><h3>Yüklü Resimler</h3></label>
          </div>
          <?php if(count($productImages) == 0) { ?>
            <div class="col col-md-12">
              <p>Yüklü Resim Bulunamadı.</p>
            </div>
          <?php } else { ?>
            <?php foreach($productImages as $productImage){ ?>
              <div class="col col-md-2">
                <img width="200" src="<?php echo publicUrl("img/product-images/".$productImage["product_image"]); ?>" alt="">
                <input type="hidden" name="product_image_id" value="<?php echo $productImage["product_image_id"]; ?>">
                <button style="margin-top:-75px; width:75px; background: red; margin-left: 60px;" type="submit" name="deleteProductImage" class="btn btn-primary btn-sm">
                  <i class="fas fa-trash-alt"></i> Sil
                </button>
              </div>
            <?php } ?>
          <?php } ?>
        </div>
        <!-- input -->
      </form>


    </div>
  </div>
</div>
