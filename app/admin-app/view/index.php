           <div class="container">

             <?php if(isset($pageMessage) && trim($pageMessage) != ""){ ?>
               <div class="alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>UYARI!</strong> <?php echo $pageMessage; ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
             <?php } ?>

            <section class="statistic">
                <div class="section__content section__content-">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-8 col-lg-3">
                                <div class="statistic__item">
                                    <h2 class="number"><?php echo $numberOfMember; ?></h2>
                                    <span class="desc">Kayıtlı Müşteri Sayısı</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-account-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-3">
                                <div class="statistic__item">
                                    <h2 class="number"><?php echo $getItemPriceInAllCarts; ?>₺</h2>
                                    <span class="desc">Tüm Sepetlerdeki Ürünler</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-shopping-cart"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-3">
                                <div class="statistic__item">
                                    <h2 class="number"><?php echo $getMonthlyEarnings; ?>₺</h2>
                                    <span class="desc">Aylık Kazanç</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-calendar-note"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-lg-3">
                                <div class="statistic__item">
                                    <h2 class="number"><?php echo $getTotalEarnings; ?>₺</h2>
                                    <span class="desc">Toplam Kazanç</span>
                                    <div class="icon">
                                        <i class="zmdi zmdi-money"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

           </div>

            <div class="container">
                <h4>En Çok incelenen Ürünler</h4>
                <div class="table-responsive table-responsive-data2">

                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Başlık</th>
                                <th>Ürün Kodu</th>
                                <th>Durum</th>
                                <th>Fiyat</th>
                                <th>Görüntülenme Sayısı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!isset($mostViewedProducts) || !is_array($mostViewedProducts) || count($mostViewedProducts) <= 0){ ?>
                                <tr class="tr-shadow">
                                <td colspan="5" class="desc">Ürün Bulunamadı</td>
                                </tr>
                            <?php } else { ?>
                                <?php foreach ($mostViewedProducts as $key => $product) { ?>
                                <tr class="tr-shadow">
                                    <td class="desc">
                                    <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                    </td>
                                    <td>
                                    <?php echo $product["product_code"]; ?>
                                    </td>
                                    <td>
                                    <?php if((int)$product["release_status"] == 1) { ?>
                                        <span class="status--process">
                                        Yayında
                                        </span>
                                    <?php } else { ?>
                                        <span class="status--process" style="color:red;">
                                        Gizli
                                        </span>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $product["price"]; ?></td>
                                    <td><?php echo $product["views"]; ?></td>
                                    <td>
                                      <div class="table-data-feature">
                                          <a class="item btn " href="<?php echo adminUrl("urun-duzenle/".$product["product_id"]);?>"><i class="zmdi zmdi-edit"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-bilgisi-ekle/".$product["product_id"]);?>"><i class="zmdi zmdi-plus"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-sil/".$product["product_id"]); ?>"><i class="zmdi zmdi-delete"></i></a>
                                      </div>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
            </div>
            <br>
            <div class="container">
                <h4>En Çok Sepete Eklenen Ürünler</h4>
                <div class="table-responsive table-responsive-data2">
                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Başlık</th>
                                <th>Ürün Kodu</th>
                                <th>Durum</th>
                                <th>Fiyat</th>
                                <th>Eklenme Sayısı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!isset($mostAddedProductsToTheCart) || !is_array($mostAddedProductsToTheCart) || count($mostAddedProductsToTheCart) <= 0){ ?>
                                <tr class="tr-shadow">
                                <td colspan="5" class="desc">Ürün Bulunamadı</td>
                                </tr>
                            <?php } else { ?>
                                <?php foreach ($mostAddedProductsToTheCart as $key => $product) { ?>
                                <tr class="tr-shadow">
                                    <td class="desc">
                                    <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                    </td>
                                    <td>
                                    <?php echo $product["product_code"]; ?>
                                    </td>
                                    <td>
                                    <?php if((int)$product["release_status"] == 1) { ?>
                                        <span class="status--process">
                                        Yayında
                                        </span>
                                    <?php } else { ?>
                                        <span class="status--process" style="color:red;">
                                        Gizli
                                        </span>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $product["price"]; ?></td>
                                    <td><?php echo $product["add_to_basket"]; ?></td>
                                    <td>
                                      <div class="table-data-feature">
                                          <a class="item btn " href="<?php echo adminUrl("urun-duzenle/".$product["product_id"]);?>"><i class="zmdi zmdi-edit"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-bilgisi-ekle/".$product["product_id"]);?>"><i class="zmdi zmdi-plus"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-sil/".$product["product_id"]); ?>"><i class="zmdi zmdi-delete"></i></a>
                                      </div>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
            </div>

            <br>
            <div class="container">
                <h4>En Çok Sepetten Çıkarılan Ürünler</h4>
                <div class="table-responsive table-responsive-data2">

                        <table class="table table-data2">
                            <thead>
                            <tr>
                                <th>Başlık</th>
                                <th>Ürün Kodu</th>
                                <th>Durum</th>
                                <th>Fiyat</th>
                                <th>Çıkarılma Sayısı</th>
                                <th>İşlemler</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!isset($mostItemsOutOfTheBasket) || !is_array($mostItemsOutOfTheBasket) || count($mostItemsOutOfTheBasket) <= 0){ ?>
                                <tr class="tr-shadow">
                                <td colspan="5" class="desc">Ürün Bulunamadı</td>
                                </tr>
                            <?php } else { ?>
                                <?php foreach ($mostItemsOutOfTheBasket as $key => $product) { ?>
                                <tr class="tr-shadow">
                                    <td class="desc">
                                    <a target="_blank" href="<?php echo url(seoUrl($product["title"])."-p-".$product["variants"][0]["barcode"]); ?>"><?php echo $product["title"]; ?></a>
                                    </td>
                                    <td>
                                    <?php echo $product["product_code"]; ?>
                                    </td>
                                    <td>
                                    <?php if((int)$product["release_status"] == 1) { ?>
                                        <span class="status--process">
                                        Yayında
                                        </span>
                                    <?php } else { ?>
                                        <span class="status--process" style="color:red;">
                                        Gizli
                                        </span>
                                    <?php } ?>
                                    </td>
                                    <td><?php echo $product["price"]; ?></td>
                                    <td><?php echo $product["get_out_the_basket"]; ?></td>
                                    <td>
                                      <div class="table-data-feature">
                                          <a class="item btn " href="<?php echo adminUrl("urun-duzenle/".$product["product_id"]);?>"><i class="zmdi zmdi-edit"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-bilgisi-ekle/".$product["product_id"]);?>"><i class="zmdi zmdi-plus"></i></a>
                                          <a class="item btn " href="<?php echo adminUrl("urun-sil/".$product["product_id"]); ?>"><i class="zmdi zmdi-delete"></i></a>
                                      </div>
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                    </table>
                    </div>
            </div>
