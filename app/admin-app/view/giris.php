
<!DOCTYPE html>
<html lang="tr">
  <head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title><?php echo (!isset($title) || trim($title)==null) ? "Yönetim" : $title; ?></title>

    <!-- Fontfaces CSS-->
    <link href="css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/font-awesome-4.7/css/font-awesome.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/font-awesome-5/css/fontawesome-all.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/mdi-font/css/material-design-iconic-font.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo publicAdminUrl("vendor/bootstrap-4.1/bootstrap.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Vendor CSS-->

    <!--<link href="<?php echo publicAdminUrl("vendor/animsition/animsition.min.css"); ?>" rel="stylesheet" media="all">-->

    <link href="<?php echo publicAdminUrl("vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css"); ?>" rel="stylesheet" media="all">

    <link href="<?php echo publicAdminUrl("vendor/wow/animate.css"); ?>" rel="stylesheet" media="all">


    <link href="<?php echo publicAdminUrl("vendor/css-hamburgers/hamburgers.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/slick/slick.css"); ?>" rel="stylesheet" media="all">


    <link href="<?php echo publicAdminUrl("vendor/select2/select2.min.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/perfect-scrollbar/perfect-scrollbar.css"); ?>" rel="stylesheet" media="all">
    <link href="<?php echo publicAdminUrl("vendor/vector-map/jqvmap.min.css"); ?>" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?php echo publicAdminUrl("css/theme.css"); ?>" rel="stylesheet" media="all">
    <script src="https://cdn.ckeditor.com/4.14.1/standard/ckeditor.js"></script>
  </head>


<body class="animsition">
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">

                        <div class="login-logo">
                        <h2>Yönetim Paneli</h2>
                            <a href="#">
                              <?php if(isset($companyInformation["company_logo"]) && trim($companyInformation["company_logo"]) != ""){ ?>
                                  <img src="<?php echo publicUrl('img/'.$companyInformation["company_logo"]); ?>" alt="">
                              <?php } else { ?>
                                <img src="<?php echo publicUrl('img/ex_logo.png'); ?>" alt="">
                              <?php } ?>
                            </a>
                        </div>
                        <div class="login-form">
                            <form action="" method="post">
                                <div class="form-group">
                                    <label>Kullanıcı Adı</label>
                                    <input class="au-input au-input--full" type="text" name="uname" placeholder="Kullanıcı adı" value="<?php echo $_POST['uname']; ?>">
                                </div>
                                <div class="form-group">
                                    <label>Şifre</label>
                                    <input class="au-input au-input--full" type="password" name="pass" placeholder="Şifre" value="">
                                </div>
                                <div class="login-checkbox">

                                    <label>
                                        <a href="#">Şifremi Unuttum?</a>
                                    </label>
                                </div>
                                <button class="au-btn au-btn--block au-btn--green m-b-20" name="logIn" type="submit">Giriş Yap</button>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


        <!-- Jquery JS-->
        <script src="<?php echo publicAdminUrl("vendor/jquery-3.2.1.min.js")   ?>"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo publicAdminUrl("vendor/bootstrap-4.1/popper.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/bootstrap-4.1/bootstrap.min.js")   ?>"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo publicAdminUrl("vendor/slick/slick.min.js")   ?>">
    </script>
    <script src="<?php echo publicAdminUrl("vendor/wow/wow.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/animsition/animsition.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/bootstrap-progressbar/bootstrap-progressbar.min.js")   ?>">
    </script>
    <script src="<?php echo publicAdminUrl("vendor/counter-up/jquery.waypoints.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/counter-up/jquery.counterup.min.js")   ?>">
    </script>
    <script src="<?php echo publicAdminUrl("vendor/circle-progress/circle-progress.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/perfect-scrollbar/perfect-scrollbar.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/chartjs/Chart.bundle.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("/vendor/select2/select2.min.js")   ?>">
    </script>
    <script src="<?php echo publicAdminUrl("vendor/vector-map/jquery.vmap.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/vector-map/jquery.vmap.min.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/vector-map/jquery.vmap.sampledata.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl("vendor/vector-map/jquery.vmap.world.js")   ?>"></script>

    <!-- Main JS-->
    <script src="<?php echo publicAdminUrl("js/main.js")   ?>"></script>
    <script src="<?php echo publicAdminUrl('js/select-category.js'); ?>"></script>
</body>

</html>
<!-- end document-->
