<?php

  if(!isset($_GET["variantValueId"])){
    header("location:".adminUrl(""));
    exit;
  }
  else{
    $variantValueObj = new VariantValue();
    $variantValueObj->setVariantValueId($_GET["variantValueId"]);
    $variantValue = $variantValueObj->getVariantValueById();
    if(!is_array($variantValue) || count($variantValue) == 0){
      header("location:".adminUrl(""));
      exit;
    }
    else {
      $platform = new Platform();
      $platforms = $platform->getPlatforms();

      $platformVariantValue = new PlatformVariantValue();
      $platformVariantValue->setVariantValueId($_GET["variantValueId"]);

      if(isset($_POST["matchVariantValue"])){
        $platformVariantValue->setPlatformId($_POST["platform_id"]);
        $platformVariantValue->setVariantValueId($_POST["variant_value_id"]);
        $platformVariantValue->setPlatformVariantValueCode($_POST["platform_variant_value_code"]);
        $insert = $platformVariantValue->insertPlatformVariantValue();
        $pageMessage = $insert ? "Varyant Değeri Eşleştirildi" : "Varyant Değeri Eşleştirilemedi !";
      }

      $platformVariantValues = $platformVariantValue->getPlatformVariantValuesByVariantValueId();

      if(isset($_GET["platform_id"])){
        $platformInfoObj = new PlatformInformation();
        $platformInfoObj->setPlatformId($_GET["platform_id"]);
        $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
        if(!is_array($platformInfo) || count($platformInfo) == 0){
          $pageMessage = "Lütfen İlgili Platforma Ait API Bilgilerini Giriniz.";
        }
        else{
          if($_GET["platform_id"] == "1"){
            //TRENDYOL
            $variantId = $variantValue["variant_id"];

            $platformVariantObj = new PlatformVariant();
            $platformVariantObj->setVariantId($variantId);
            $platformVariantObj->setPlatformId($_GET["platform_id"]);
            $platformVariant = $platformVariantObj->getPlatformVariantByVariantIdAndPlatformId();
            if(!is_array($platformVariant) || count($platformVariant) == 0){
              $pageMessage = "Lütfen önce varyantı eşleştiriniz<br>";
              $pageMessage .= "<a target='_blank' href='".adminUrl("varyant-eslestir?variantId=".$variantId)."&platform_id=".$_GET["platform_id"]."'> Varyantı eşleştirmek için tıklayınız </a>";
            }
            else{
              $platformVariantCode = $platformVariant["platform_variant_code"];
              $platformCategoryCode = $platformVariant["platform_category_code"];

              $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
              $variantValues = $trendyol->getVariantsValues($platformCategoryCode, $platformVariantCode);
            }
          }
        }
      }

      $title = "Varyant Değeri Eşleştir";
      $pageTitle = "Varyant Değeri Eşleştir";
      $map = adminMap("Anasayfa,Varyant İşlemleri,Varyant Değerleri,Varyant Değeri Eşleştir", "index,varyant-islemleri,varyant-degerleri?variantId=$variantId,varyant-degeri-eslestir?variantValueId=".$_GET["variantValueId"]);
    }
  }

?>
