<?php


  if(isset($_POST["insertPlatformInfo"])){
    $platformInfo = new PlatformInformation();
    $platformInfo->setMarketName($_POST["market_name"]);
    $platformInfo->setPlatformId($_POST["platform_id"]);
    $insert = $platformInfo->insertPlatformInformation();
    $pageMessage = $insert ? "Platform Bilgisi Kaydedildi" : "Platform Bilgisi Kaydedilemedi !";
  }
  $platform = new Platform();
  $platforms = $platform->getPlatforms();

  $title = "Platform Bilgisi Ekle";
  $pageTitle = "Platform Bilgisi Ekle";
  $map = adminMap("Ana Sayfa,Platform Bilgileri,Platform Bilgisi Ekle","index,platform-bilgileri,platform-bilgisi-ekle");

?>
