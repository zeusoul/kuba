<?php
  if(!isset($_GET["id"])){
    header("location:".adminUrl("kategori-islemleri"));
    exit;
  }
  else{
    $objCategory = new Category();
    $objCategory->setCategoryId($_GET["id"]);

    if(isset($_POST["updateCategory"])){
      $objCategory->setCategoryImage($_FILES["category_image"]);
      $objCategory->setCategoryName($_POST["category_name"]);
      $res = $objCategory->updateCategory();
      $pageMessage = $res == "Success" ? "Kategori Güncellendi" : $res;
    }

    $category = $objCategory->getCategoryById();

    $title = "Kategori Düzenle";
    $pageTitle = "Kategori Düzenle";
    $map = adminMap("Anasayfa,Kategori İşlemleri,Kategori Düzenle", "index,kategori-islemleri,kategori-duzenle?id=".$_GET["id"]);
  }


?>
