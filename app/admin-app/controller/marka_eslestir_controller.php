<?php
  if(!isset($_GET["id"])){
    header("location:".adminUrl("kategori-islemleri"));
    exit;
  }
  else{
    $objBrand = new Brand();
    $objBrand->setBrandId($_GET["id"]);
    $brand = $objBrand->getBrand();

    $platform = new Platform();
    $platforms = $platform->getPlatforms();

    $platformBrand = new PlatformBrand();
    $platformBrand->setBrandId($_GET["id"]);

    if(isset($_POST["matchBrand"])){
      $platformBrand->setPlatformId($_POST["platform_id"]);
      $platformBrand->setPlatformBrandCode($_POST["platform_brand_code"]);
      $platformBrand->setBrandId($_POST["brand_id"]);
      $insert = $platformBrand->insertPlatformBrand();
      $pageMessage = $insert ? "Marka Eşleştirildi" : "Marka Eşleştirilemedi !";
    }
    else if(isset($_POST["deleteMatch"])){
      $platformBrand->setPlatformBrandId($_POST["platform_brand_id"]);
      $delete = $platformBrand->deletePlatformBrand();
      $pageMessage = $delete ? "Eşleştirme Silindi" : "Eşleştirme Silinemedi !";
    }

    $platformBrands = $platformBrand->getPlatformBrandsByBrandId();

    if(isset($_GET["platform_id"])){
      $platformInfoObj = new PlatformInformation();
      $platformInfoObj->setPlatformId($_GET["platform_id"]);
      $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
      if(!is_array($platformInfo) || count($platformInfo) == 0){
        $pageMessage = "Lütfen İlgili Platforma Ait API Bilgilerini Giriniz.";
      }
      else{
        if($_GET["platform_id"] == "1" && isset($_GET["brand_name"])){
          $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
          $brands = $trendyol->getSearchBrands(15,1,$_GET["brand_name"]);
          $brands = json_decode($brands, true);
        }
      }
    }

    $title = "Marka Eşleştir";
    $pageTitle = "Marka Eşleştir";
    $map = adminMap("Ana Sayfa,Marka İşlemleri,Marka Eşleştir","index,markalar,marka-eslestir?id=".$_GET["id"]);
  }

?>
