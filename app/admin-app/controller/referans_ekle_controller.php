<?php
  if(!adminLoginState()) {
    header("location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET["url"]);
    if(count($url) != 3) {
      header("location:".adminUrl("referans-kategori-sec/referans-ekle"));
      exit;
    }
    else {
      $title = "Referans Ekle";
      $selectedReferenceCategoryId = (int)$url[2];

      $referenceCategoryOb = new ReferenceCategory();
      $referenceCategoryOb->setReferenceCategoryId($selectedReferenceCategoryId);
      $selectedReferenceCategory = $referenceCategoryOb->getSelectedReferenceCategory();
      $selectedReferenceCategoryName = $selectedReferenceCategory["reference_category_name"];


      if(isset($_POST["insertReference"])){
        $referenceObject = new References();
        $referenceObject->setReferenceCategoryId($selectedReferenceCategoryId);
        $referenceObject->setReferenceImage($_FILES["reference_image"]);

        $insert = $referenceObject->insertReference();
        if($insert === "Success"){
          $insertedProduct = $referenceObject->getReferences();
          $insertedProductId = (int)$insertedProduct["reference_id"];
        }
        else $pageMessage = "Referans Eklendi ";
      }
    }
  }




?>
