<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
     $url = explode('/',$_GET["url"]);
     if(count($url) == 2) header("location:".adminUrl("urun-sec/urun-ozellik-ekle"));
     else if(count($url) == 3){
       $productId = (int)$url[2];
       $productObject = new Product();
       $productObject->setProductId($productId);

       $infoObject = new ProductInformation();
       $infoObject->setProductId($productId);

       if(isset($_POST["insertProductInfo"])){
         $infoObject->setInfo($_POST["info"]);
         $infoObject->setValue($_POST["value"]);

         $insert = $infoObject->insertProductInfo();
         $pageMessage = ($insert === "Success") ? "Bilgi Eklendi" : $insert;

       }
       else if(isset($_POST["updateProductInfo"])){
         $infoObject->setInfoId($_POST["info_id"]);
         $infoObject->setinfo($_POST["info"]);
         $infoObject->setValue($_POST["value"]);

         $update = $infoObject->updateProductInfo();

         $pageMessage = ($update === "Success") ? "Bilgi Güncellendi" : $update;
       }
       else if(isset($_POST["deleteProductInfo"])){
         $infoObject->setInfoId($_POST["info_id"]);

         $delete = $infoObject->deleteProductInfo();
         $pageMessage = (is_bool($delete) && $delete) ? "Bilgi Silindi" : $delete;
       }

       $productDetails = $productObject->getProductById();
       $informations = $infoObject->getProductInformations();

       if(is_bool($productDetails) && !$productDetails) header("location:".adminUrl(""));

       $title = "Ürün Bilgisi Ekle";
       $pageTitle = "Ürün Bilgisi Ekle";
       $map = adminMap("Anasayfa,Ürün İşlemleri, Yeni Ürün Ekle, Ürün Bilgisi Ekle", "index,urun-islemleri,urun-ekle,urun-bilgisi-ekle/$productId");
     }
     else header("location:".adminUrl(""));
  }

?>
