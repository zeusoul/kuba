<?php

  if(!isset($_GET["id"]) || (int)$_GET["id"] <= 0){
    header("location:".adminUrl("platform-bilgileri"));
    exit;
  }
  else{
    $platformInfoObj = new PlatformInformation();
    $platformInfoObj->setPlatformInformationId($_GET["id"]);
    if(isset($_POST["updateApi"])){
      $platformInfoObj->setKey1($_POST["username"]);
      $platformInfoObj->setKey2($_POST["password"]);
      $platformInfoObj->setKey3($_POST["supplier_id"]);
      $update = $platformInfoObj->updatePlatformInformation();
      $pageMessage = $update ? "API Bilgileri Güncellendi" : "API Bilgileri Güncellenemedi !";
    }

    $platformInfo = $platformInfoObj->getPlatformInformationById();
    if(!is_array($platformInfo) || count($platformInfo) == 0){
      header("location:".adminUrl("platform-bilgileri"));
      exit;
    }
    else{

    }
    $title = "API Bilgisi Ekle";
    $pageTitle = "API Bilgisi Ekle";
    $map = adminMap("Ana Sayfa,Platform Bilgileri,API Bilgisi Ekle","index,platform-bilgileri,api-bilgisi?id=".$_GET["id"]);
  }

?>
