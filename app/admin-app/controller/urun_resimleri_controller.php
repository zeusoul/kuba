<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(count($url) != 3) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $productId = (int)$url[2];

      $productObject = new Product();
      $productObject->setProductId($productId);
      $productDetails = $productObject->getProductById();

      $productImgsObj = new ProductImages();
      $productImgsObj->setProductId($productId);
      if(isset($_POST["insertProductImage"])){
        $productImgsObj->setProductImage($_FILES["image"]);
        $insert = $productImgsObj->insertProductImage();
        $state = $insert ? "insertSuccess" : "insertFailed";
        header("location:".adminUrl("urun-resimleri/$productId?$state"));
      }
      else if(isset($_POST["deleteProductImage"])){
        $productImgsObj->setProductImageId($_POST["product_image_id"]);
        $delete = $productImgsObj->deleteProductImage();
        $state = $delete ? "deleteSuccess" : "deleteFailed";
        header("location:".adminUrl("urun-resimleri/$productId?$state"));
      }

      if(isset($_GET["insertSuccess"])) $pageMessage = "Resim Yüklendi";
      else if(isset($_GET["insertFailed"])) $pageMessage = "Resim Yüklenemedi";
      else if(isset($_GET["deleteSuccess"])) $pageMessage = "Resim Silindi";
      else if(isset($_GET["deleteFailed"])) $pageMessage = "Resim Silinemedi";
      $productImages = $productImgsObj->getProductImages();

      $title = "Ürün Resimleri";
      $pageTitle = "Ürün Resimleri";
      $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Resimleri", "index,urun-islemleri,urun-resimleri/$productId");
    }
  }
?>
