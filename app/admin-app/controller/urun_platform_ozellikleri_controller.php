<?php

  $productId = $_GET["id"];
  $product = new Product();
  $product->setProductId($productId);
  if(!$product->isProduct()){
    header("location:".adminUrl("urun-listesi"));
    exit;
  }
  else{
    $platform = new Platform();
    $platforms = $platform->getPlatforms();

    $productDetails = $product->getProductDetails();

    if(!is_array($productDetails) || count($productDetails) == 0){
      $fatalError = "Ürün bilgileri bulunamadı !";
    }
    else{
      $categoryId = $productDetails["category_id"];

      if(isset($_GET["platform_id"])){
        $platformProductAttribute = new PlatformProductAttribute();
        $platformProductAttribute->setProductId($_GET["id"]);
        $platformProductAttribute->setPlatformId($_GET["platform_id"]);

        $platformCategory = new PlatformCategory();
        $platformCategory->setCategoryId($categoryId);
        $platformCategory->setPlatformId($_GET["platform_id"]);
        $result = $platformCategory->getPlatformCategoryByCategoryIdAndPlatformId();
        if(!is_array($result) || count($result) == 0){
          $fatalError = "Seçmiş olduğunuz platforma ait ürün özelliklerini girebilmeniz için önce kategori eşleştirmesi yapmanız gerekmektedir.";
          $fatalError .= "<br/> Eşleştirilmesi gereken kategori : ".$productDetails["name"];
          $fatalError .= "<br/> Eşleştirmek için <a href='".adminUrl("kategori-eslestir?id=".$productDetails["category_id"]."&platform_id=".$_GET["platform_id"])."'>Tıklayınız</a>";
        }
        else if($_GET["platform_id"] == "1"){
          $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
          $attributes = $trendyol->getCategoryAttributes($result["platform_category_code"]);
          if(!is_array($attributes) || count($attributes) == 0 || count($attributes["categoryAttributes"]) == 0){
            $fatalError = "Bu kategoriye ait özellik bulunamadı.";
          }
        }
        if(isset($_POST["insertPlatformAttributes"])){
          if(isset($_POST["attribute_values"]) && is_array($_POST["attribute_values"])){
            $delete = $platformProductAttribute->deletePlatformAttributesByProductIdAndPlatformId();
            if(!$delete) {
              $pageMessage = "Ürün özellikleri kaydedilemedi !";
            }
            else{
              $error=0;
              foreach ($_POST["attribute_values"] as $key => $attributeValue) {
                if(trim($attributeValue) != ""){
                  $attributeValue = explode("-",$attributeValue);
                  $attributeValueCode = $attributeValue[0];
                  $attributeValue = $attributeValue[1];

                  $attribute = $_POST["attributes"][$key];
                  $attribute = explode("-",$attribute);
                  $attributeCode = $attribute[0];
                  $attribute = $attribute[1];

                  $platformProductAttribute->setPlatformAttributeCode($attributeCode);
                  $platformProductAttribute->setPlatformAttributeValueCode($attributeValueCode);
                  $platformProductAttribute->setAttribute($attribute);
                  $platformProductAttribute->setAttributeValue($attributeValue);
                  $insert = $platformProductAttribute->insertPlatformProductAttribute();
                  if(!$insert) $error++;
                }
              }
              $pageMessage = ($error == 0) ? "Ürünün platform özellikleri kaydedildi" : "$error Özellik kaydedilemedi !";
            }
          }
        }
        $platformProductAttributes = $platformProductAttribute->getPlatformProductAttributesByProductIdAndPlatformId();
      }
    }
  }
?>
