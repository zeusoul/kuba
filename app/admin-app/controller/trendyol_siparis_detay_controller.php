<?php
  if(!isset($_GET["orderNo"])) {
    header("location:".adminUrl());
    exit;
  }

  $orderNo =  $_GET["orderNo"];
  $platformInfoObj = new PlatformInformation();
  $platformInfoObj->setPlatformId(1);
  $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
  if(!is_array($platformInfo) || count($platformInfo) == 0){
    $pageMessage = "Lütfen Trendyol Platformuna Ait API Bilgilerini Giriniz";
  }
  else{
    $trendyolPageNumber = isset($_GET["trendyolPage"]) ? (int)$_GET["trendyolPage"] : 1;
    $objTrendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
    $order = $objTrendyol->getOrders(1,$orderNo)["content"][0];
  }

  if(isset($_POST["update"])) {
    $orderObj->setOrderNo($_POST["order_no"]);
    $orderObj->setStatus($_POST["status"]);
    $orderObj->setCargoNo($_POST["cargo_no"]);
    $orderObj->setCargoCompany($_POST["cargo_company"]);

    if((int)$_POST["status"] != -1){
      $update = $orderObj->updateOrder();
      $pageMessage = ($update) ? "Güncelleme Başarılı" : $update;
    }
    else {
      $cancel = $orderObj->cancelOrder();
      $pageMessage = ($cancel === "Success") ? "Sipariş iptal edildi" : $cancel;
    }
  }

  $title = "Sipariş Detayı";
  $pageTitle = "Sipariş Detayı";
  $map = adminMap("Anasayfa,Siparişler,Sipariş Detayı", "index,siparisler,trendyol-siparis-detay?orderNo=$orderNo");

?>
