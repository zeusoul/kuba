<?php

  if(!adminLoginState()){
    header("location:".adminUrl('giris'));
  }
  else{
    $category = new Category();
    $url = explode("/",$_GET["url"]);
    if(isset($_POST["insertCategory"])){
      $category->setCategoryName($_POST["categoryName"]);
      $category->setParentId($_POST["parentId"]);
      $category->setCategoryImage($_FILES["categoryImage"]);
      $insert = $category->insertCategory();

      $pageMessage = ($insert == "Success")  ? "Kategori Eklendi" : $insert;
    }
    else if(isset($_POST["deleteCategory"])){
      $category->setCategoryId($_POST["categoryId"]);
      $delete = $category->deleteCategory();
      $pageMessage = (is_bool($delete) && !$delete) ? "Kategori Silinemedi : HATA!" : "Kategori Silindi";
    }

    $categories = $category->getAllCategories();
    $category->setParentId(0);
    $menu = $category->getCategoriesAndSubCategories();


    $title = "Kategori İşlemleri";
    $pageTitle = "Kategori İşlemleri";
    $map = adminMap("Anasayfa,Kategori İşlemleri","index,kategori-islemleri");
  }
?>
