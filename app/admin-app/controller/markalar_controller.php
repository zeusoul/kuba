<?php

  $brand = new Brand();
  if(isset($_POST["insertBrand"])){
    $brand->setBrandName($_POST["brand_name"]);
    $insert = $brand->insertBrand();
    $pageMessage = $insert ? "Marka Eklendi." : "Marka Eklenemedi !";
  }
  else if(isset($_POST["deleteBrand"])){
    $brand->setBrandId($_POST["brand_id"]);
    $delete = $brand->deleteBrand();
    $pageMessage = $delete ? "Marka Silindi." : "Marka Silinemedi !";
  }
  $brands = $brand->getBrands();

  $pageTitle = "Marka İşlemleri";
  $map = adminMap("Anasayfa,Marka İşlemleri", "index,markalar");

?>
