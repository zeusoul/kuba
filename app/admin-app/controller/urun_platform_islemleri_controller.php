<?php

  if(!isset($_GET["id"])){
    header("location:".adminUrl("urun-islemleri"));
    exit;
  }
  else{
    $productId = $_GET["id"];
    $product = new Product();
    $product->setProductId($productId);
    if(!$product->isProduct()){
      header("location:".adminUrl("urun-islemleri"));
      exit;
    }
    else{
      $productDetail = $product->getProductDetails();

      $platform = new Platform();
      $platforms = $platform->getPlatforms();
      if(isset($_POST["sendTrendyol"]) || isset($_POST["updateTrendyol"])){
        $admin = new Admin();
        $admin->setUserName($_SESSION[sessionPrefix()."admin_username"]);
        $admin->setPass($_POST["password"]);

        if(!$admin->isAdmin()) $pageMessage = "Şifre hatalı !";
        else{
          if(isset($_POST["sendTrendyol"])){
            // Trendyol'a ürün gönderme
            $ch = curl_init('https://istanbulelektromatik.com/modamahur/api/send-product-trendyol');
            curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array(
              "productId" => $_GET["id"]
            )));
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'username:'.$_SESSION[sessionPrefix()."admin_username"],
              'password:'.$_POST["password"],
              'Content-Type:application/json'
            ));
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);

            if(isset($result["state"])) {
              if($result["state"] == "success") $pageMessage = "İşlem Başarılı !  ";
              else $pageMessage = "İşlem Başarısız ! ";
              $pageMessage .= "<br>İşlem Sonucu : ".$result["message"];
            }
          }
          else if(isset($_POST["updateTrendyol"])){
            // Trendyol'da ürün bilgilerini güncelleme
            $ch = curl_init('https://istanbulelektromatik.com/modamahur/api/update-product-trendyol');
            curl_setopt($ch,CURLOPT_POSTFIELDS,json_encode(array(
              "productId" => $_GET["id"]
            )));
            curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'username:'.$_SESSION[sessionPrefix()."admin_username"],
              'password:'.$_POST["password"],
              'Content-Type:application/json'
            ));
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
            $result = curl_exec($ch);
            curl_close($ch);
            $result = json_decode($result, true);

            if(isset($result["state"])) {
              if($result["state"] == "success") $pageMessage = "İşlem Başarılı !  ";
              else $pageMessage = "İşlem Başarısız ! ";
              $pageMessage .= "<br>İşlem Sonucu : ".$result["message"];
            }
          }
        }
      }

      $title = "Ürün Platform İşlemleri";
      $pageTitle = "Ürün Platform İşlemler";
      $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Platform İşlemleri",
                      "index,urun-islemleri,urun-platform-islemleri?id=".$_GET["id"]."&platform_id=".$_GET["platform_id"]);

    }
  }


?>
