<?php

  if(!isset($_GET["productId"])){
    header("location:".adminUrl("urun-islemleri"));
  }
  else{
    $productId = (int)$_GET["productId"];
    $product = new Product();
    $product->setProductId($productId);

    if(!$product->isProduct()){
      header("location:".adminUrl("urun-islemleri"));
    }
    else{
      $variantObj = new Variant();
      $variants = $variantObj->getVariants();
      if(!is_array($variants) || count($variants) == 0){
        header("location:".adminUrl("urun-varyant-ekle?productId=".$_GET["productId"]));
        exit;
      }

      $title = "Ürün Variantları Seç";
      $pageTitle = "Ürün Varyantları Seç";
      $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Ekle,Ürün Bilgisi Ekle,Ürün Variantları Seç", "index,urun-islemleri,urun-ekle,urun-bilgisi-ekle/$productId,urun-varyant-sec?productId=$productId");
    }
  }

?>
