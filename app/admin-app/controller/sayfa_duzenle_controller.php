<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $url = explode("/",$_GET["url"]);
    $pageId = $url[count($url)-1];

    $pageObj = new Pages();
    $pageObj->setPageId($pageId);

    if (isset($_POST["deletePage"]) || isset($_GET["deletePage"])) {
      $delete = $pageObj->deletePage();
      if($delete) {
        header("location:".adminUrl("sayfa-islemleri?silindi"));
        exit;
      }
      else $pageMessage =  "Sayfa silinemedi!";
    }
    if (isset($_POST["updatePage"])) {
      $pageObj->setContent($_POST["content"]);
      $pageObj->setMenuTitle($_POST["menu-title"]);
      $pageObj->setStatus($_POST["status"]);
      $update = $pageObj->updatePage();
      $pageMessage = ($update) ? "Sayfa Düzenlendi." : "Sayfa düzenlenemedi!";
    }

    $pageDetails = $pageObj->getSelectedPages();

    $title = "Sayfa Düzenle";
    $pageTitle = "Sayfa Düzenle";
    $map = adminMap("Anasayfa,Sayfa İşlemleri,Sayfa Düzenle", "index,sayfa-islemleri,sayfa-duzenle/$pageId");
  }
?>
