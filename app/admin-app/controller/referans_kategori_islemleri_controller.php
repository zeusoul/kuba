<?php

  if(!adminLoginState()){
    header("location:".adminUrl('giris'));
  }
  else{
    $referenceCategory = new ReferenceCategory();
    $url = explode("/",$_GET["url"]);
    if(isset($_POST["referenceInsertCategory"])){
      $referenceCategoryName = $_POST["referenceCategoryName"];
      $referenceCategory->setReferenceCategoryName($referenceCategoryName);
      $insertReference = $referenceCategory->insertReferenceCategory();
      if($insertReference != "Kategori Eklendi") $pageMessage = "Kategori Eklendi";
      else $pageMessage = $insertReference;
    }
    else if(isset($_POST["deleteReferenceCategory"])){
      $referenceCategory->setReferenceCategoryId($_POST["referenceCategoryId"]);
      $delete = $referenceCategory->deleteReferenceCategory();
      $pageMessage = (is_bool($delete) && !$delete) ? "Kategori Silinemedi : HATA!" : "Kategori Silindi";
    }
    else if(isset($_POST["updateReferenceCategory"])){
      $referenceCategory->setReferenceCategoryId($_POST["referenceCategoryId"]);
      $referenceCategory->setReferenceCategoryName($_POST["referenceCategoryName"]);
      $update = $referenceCategory->updateReferenceCategory();
      $pageMessage = ($update) ? "Kategori Güncellendi" : "Kategori Güncellenemedi : HATA!";
    }

    $categories = $referenceCategory->getAllReferenceCategories();
    $menu = $referenceCategory->getReferenceCategoriesWithReferences();


    $title = "Referans Kategori İşlemleri";

  }
?>
