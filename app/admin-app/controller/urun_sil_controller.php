<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $url = explode('/',$_GET["url"]);
    if(count($url) != 3 || trim($url[2]) == "" || (int)$url[2] == 0) {
      header("location:".adminUrl("urun-islemleri"));
    }
    else{
      $productId = (int)$url[2];
      $productObject = new Product();
      $productObject->setProductId($productId);
      if(isset($_POST["confirm"]) || isset($_POST["cancel"])){
        if(isset($_POST["cancel"])){
          header("refresh:4;url=".adminUrl("urun-islemleri"));
          die("<h1>Ürün silme işlemi iptal edildi.<br/>Sayfaya yönlendiriliyorsunuz...</h1>");
        }
        else{
          $delete = $productObject->deleteProduct();
          $pageMessage = $delete;

          header("refresh:4;url=".adminUrl("urun-islemleri"));
          die("<h1>$pageMessage<br/>Sayfaya yönlendiriliyorsunuz...</h1>");
        }
        exit;
      }
      else {
        $product = $productObject->getProductById();
      }
    }

    $title = "Ürün Sil";
    $pageTitle = "Ürün Sil";
    $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Sil", "index,urun-islemleri,urun-sil/$productId");
  }


?>
