<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $productObject = new Product();
    $mostViewedProducts = $productObject->getTheMostViewedProducts();
    $mostAddedProductsToTheCart = $productObject->getTheMostAddedProductsToTheCart();
    $mostItemsOutOfTheBasket = $productObject->getTheMostItemsOutOfTheBasket();

    $memberObject = new Member();
    $numberOfMember = $memberObject->getNumberOfMember();

    $cartObject = new Cart();
    $getItemPriceInAllCarts = $cartObject->getItemPriceInAllCarts();

    $earningObject = new Earning();
    $getMonthlyEarnings = $earningObject->getMonthlyEarnings();

    $getTotalEarnings = $earningObject->getTotalEarnings();

    $title = "Yönetim";
  }

?>
