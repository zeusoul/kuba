<?php

  #Onaylanmamış ürün yorumlarını çekelim
  $commentsObject = new ProductComments();
  $numberOfComments = (int)$commentsObject->getNumberOfComments();

  #Teslim edilmeyen siparişler
  $orderObject = new Order();
  $numberOfOrders = (int)$orderObject->getNumberOfNewOrders();

  $companyObject = new Company();
  $companyInformation = $companyObject->getCompanyInformation();

  $messageObject = new Message();
  $messageObject->setRecipientId(-1);
  $newMessageStatus = $messageObject->getUnvisitedMessages();

?>
