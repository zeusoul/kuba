<?php
  if(!isset($_GET["variantId"])){
    header("location:".adminUrl("variant-islemleri"));
    exit;
  }
  else{
    $objVariant = new Variant();
    $objVariant->setVariantId($_GET["variantId"]);
    $variant = $objVariant->getVariantById();

    $platform = new Platform();
    $platforms = $platform->getPlatforms();

    $platformVariant = new PlatformVariant();
    $platformVariant->setVariantId($_GET["variantId"]);

    if(isset($_POST["matchVariant"])){
      $platformVariant->setPlatformId($_POST["platform_id"]);
      $platformVariant->setVariantId($_POST["variant_id"]);
      $platformVariant->setPlatformVariantCode($_POST["platform_variant_code"]);
      $platformVariant->setPlatformCategoryCode($_POST["platform_category_code"]);
      $insert = $platformVariant->insertPlatformVariant();
      $pageMessage = $insert ? "Varyant Eşleştirildi" : "Varyant Eşleştirilemedi !";
    }
    else if(isset($_POST["deleteMatch"])){
      $platformVariant->setPlatformVariantId($_POST["platform_variant_id"]);
      $delete = $platformVariant->deletePlatformVariant();
      $pageMessage = $delete ? "Eşleştirme Silindi" : "Eşleştirme Silinemedi !";
    }

    $platformVariants = $platformVariant->getPlatformVariantsByVariantId();

    if(isset($_GET["platform_id"])){
      $platformInfoObj = new PlatformInformation();
      $platformInfoObj->setPlatformId($_GET["platform_id"]);
      $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
      if(!is_array($platformInfo) || count($platformInfo) == 0){
        $pageMessage = "Lütfen İlgili Platforma Ait API Bilgilerini Giriniz.";
      }
      else{
        $platformCategory = new PlatformCategory();
        $platformCategory->setPlatformId($_GET["platform_id"]);
        $categories = $platformCategory->getPlatformCategoriesByPlatformId();

        if($_GET["platform_id"] == "1" && isset($_GET["category_code"])){
          $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
          $variants = $trendyol->getVariants($_GET["category_code"]);
        }
      }
    }

    $title = "Varyant Eşleştir";
    $pageTitle = "Varyant Eşleştir";
    $map = adminMap("Ana Sayfa,Varyant İşlemleri,Varyant Eşleştir","index,varyant-islemleri,varyant-eslestir?variantId=".$_GET["variantId"]);
  }

?>
