<?php
  if(!adminLoginState()) {
    header("location:".adminUrl("giris"));
    exit;
  }
  else {
    if(isset($_POST["insertNew"])){
      $newsObj = new News();
      $newsObj->setNewTitle($_POST["title"]);
      $newsObj->setNewDesc($_POST["description"]);
      $newsObj->setNewImage($_FILES["image"]);
      $newsObj->setNewContent($_POST["content"]);

      $insert = $newsObj->insertNew();
      if($insert == "Success") header("location:".adminUrl("haber-ekle?success"));
      else header("location:".adminUrl("haber-ekle?failed=$insert"));
    }
    if(isset($_GET["success"])) $pageMessage = "Haber Eklendi";
    else if(isset($_GET["failed"])) $pageMessage = $_GET["failed"];

    $title = "Yeni Haber Ekle";
    $pageTitle = "Yeni Haber Ekle";
    $map = adminMap("Anasayfa,Haberler,Yeni Haber Ekle", "index,haberler,haber-ekle");
  }
?>
