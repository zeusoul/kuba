<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    if(isset($_POST["insertPage"])){
      $pageObj= new Pages();
      $pageObj->setContent($_POST["content"]);
      $pageObj->setMenuTitle($_POST["menu-title"]);
      $pageObj->setStatus($_POST["status"]);
      $insert = $pageObj->insertPage();

      if($insert){
        header("location:".adminUrl("sayfa-islemleri?eklendi"));
        exit;
      }
      else $pageMessage =  "Sayfa eklenemedi!";
    }
    $title = "Sayfa Ekle";
    $pageTitle = "Sayfa Ekle";
    $map = adminMap("Anasayfa,Sayfa İşlemleri,Sayfa Ekle", "index,sayfa-islemleri,sayfa-ekle");
  }

?>
