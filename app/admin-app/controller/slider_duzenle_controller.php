<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(count($url) != 3) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $sliderId = (int)$url[2];
      $sliderObject = new Slider();
      $sliderObject->setSliderId($sliderId);

      if(isset($_POST['updateSlider'])) {
        $sliderObject->setSliderImage($_FILES["image"]);
        $sliderObject->setSliderYear($_POST["year"]);
        $sliderObject->setSliderTitle($_POST["title"]);
        $sliderObject->setSliderDescription($_POST["description"]);
        $sliderObject->setSliderUrl($_POST["url"]);
        $sliderObject->setSliderPrice($_POST["price"]);

        $update = $sliderObject->updateSlider();

        $pageMessage = ($update === "Success") ? "Slider Güncellendi" : $update;
      }

      $sliderDetails = $sliderObject->getSliderDetails();
      if(count($sliderDetails) == 0){
        header("Location:".adminUrl());
        exit;
      }

      $title = "Slider Düzenle";
      $pageTitle = "Slider Düzenle";
      $map = adminMap("Anasayfa,Slider İşemleri,Slider Düzenle", "index,slider-islemleri,slider-duzenle/$sliderId");
    }
  }
?>
