<?php

  if(!isset($_GET["variantId"])){
    header("location:".adminUrl("varyant-islemleri"));
    exit;
  }
  else{
    $variantId = (int)$_GET["variantId"];
    $variant = new Variant();
    $variant->setVariantId($variantId);
    if(!$variant->isVariant()){
      header("location:".adminUrl("varyant-islemleri"));
      exit;
    }
    else{
      $variantValue = new VariantValue();
      $variantValue->setVariantId($variantId);
      if(isset($_POST["insertVariantValue"])){
        $variantValue->setVariantValueName($_POST["variant_value"]);
        $insert = $variantValue->insertVariantValue();
        $pageMessage = $insert == "Success" ? "Varyant değeri kaydedildi." : $insert;
      }
      else if(isset($_POST["delete_variant_value"])){
        $variantValue->setVariantValueId($_POST["variant_value_id"]);
        $delete = $variantValue->deleteVariantValue();
        $pageMessage = $delete ? "Varyant Değeri Silindi" : "Varyant Değeri Silinemedi";
      }
      $variantValues = $variantValue->getVariantValuesByVariantId();

      $title = "Varyant Değerleri";
      $pageTitle = "Varyant Değerleri";
      $map = adminMap("Anasayfa,Varyant İşlemleri,Varyant Değerleri", "index,varyant-islemleri,varyant-degerleri?variantId=$variantId");
    }
  }

?>
