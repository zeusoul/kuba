<?php

  $variant = new Variant();

  if(isset($_POST["insertVariant"])){
    $variant->setVariantCode($_POST["variant_code"]);
    $variant->setVariantName($_POST["variant"]);
    $insert = $variant->insertVariant();
    $pageMessage = $insert == "Success" ? "Varyant Eklendi" : $insert;
  }
  else if(isset($_POST["delete_variant"])){
    $variant->setVariantId($_POST["variant_id"]);
    $delete = $variant->deleteVariant();
    $pageMessage = $delete ? "Varyant Silindi" : "Varyant Silinemedi";
  }

  $variants = $variant->getVariants();

  $title = "Varyant İşlemleri";
  $pageTitle = "Varyant İşlemleri";
  $map = adminMap("Anasayfa,Varyant İşlemleri", "index,varyant-islemleri");

?>
