<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(count($url) != 3) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $productId=(int)$url[2];
      $category=new Category();
      $product= new Product();
      $filterObject = new Filter();
      $filterObject->setProductId($productId);
      $product->setProductId($productId);

      if(isset($_POST['updateProduct'])) {
        $product->setCategoryId($_POST["category_id"]);
        $product->setProductImage($_FILES["image"]);
        $product->setTitle($_POST["title"]);
        $product->setSubTitle($_POST["subTitle"]);
        $product->setBrandId($_POST["brand_id"]);
        $product->setModel($_POST["model"]);
        $product->setProductCode($_POST["productCode"]);
        $product->setContent($_POST["content"]);
        $product->setStatus($_POST["status"]);

        #foreach ile post dizisindeki tüm elemanları kontrol ediyoruz.
        #Sadece key'i int olanları properties dizisine aktarıyoruz.
        #Bu elemanlar ürünün özelliklerini taşıyor
        $selectedFilters = array();
        foreach ($_POST as $key => $value) {
          $keyArray = explode("-",$key);
          $keyInt = $keyArray[count($keyArray)-1];
          if(intval($key)){
            if (trim($_POST["txt-$keyInt"]) == "") {
              $selectedFilters[$keyInt] = $value;
            }
            else{
              $valueName = $_POST["txt-$keyInt"];
              $values = new FilterValues();
              $values->setValueName($valueName);
              $values->setFilterId($keyInt);
              $insertValue = $values->insertFilterValue();

              if($insertValue){
                $lastValue = $values->getLastInsertedFilterValue();
                $valueId = $lastValue["filter_value_id"];
                $selectedFilters[$keyInt] = $valueId;
              }
            }
          }
        }

        $update = $product->updateProduct();

        if($update === "Success"){
          $updateFilters = $filterObject->updateProductFilters($selectedFilters);
          if($updateFilters){
            header("location:".adminUrl("urun-bilgisi-ekle/$productId?updateSuccess"));
            exit;
          }
          else $pageMessage = "Ürün Filtresi Güncellemede Hata";
        }
        else $pageMessage = $update;

        $pageMessage = $update == "Success" ? "Ürün bilgleri güncellendi" : $update;
      }

      $categories = $category->getAllCategories();
      $productDetails = $product->getProductById();
      $productFiltersAndValues = $product->getProductFiltersAndValues();

      $selectedCategoryId = $productDetails['category_id'];


      $filterObject->setCategoryId($selectedCategoryId);
      $filters = $filterObject->getCategoryFilters();

      $brand = new Brand();
      $brands = $brand->getBrands();

    }

    $title = "Ürün Düzenle";
    $pageTitle = "Ürün Düzenle";
    $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Düzenle", "index,urun-islemleri,urun-duzenle/$productId");
  }
?>
