<?php
if (!adminLoginState()) {
  header("Location:".adminUrl("giris"));

}
else {
  if (isset($_POST['insertBank'])){

    $bank = new Bank();
    $bank->setBankName($_POST["bank_name"]);
    $bank->setBankImage($_FILES["bank_image"]);
    $bank->setBranchCode($_POST["branch_code"]);
    $bank->setAccountNo($_POST["account_no"]);
    $bank->setIbanNo($_POST["iban"]);
    $bank->setAccountName($_POST["account_name"]);


    $insert=$bank->insertBankInformation();

    $pageMessage = ($insert === "Success") ? "Banka Bilgileri Kayıt Edildi" : $insert;

    $title = "Banka Bilgisi Ekle";
    $pageTitle = "Banka Bilgisi Ekle";
    $map = adminMap("Anasayfa,Banka Bilgileri,Yeni Banka Bilgisi", "index,banka-bilgileri,banka-bilgisi-ekle");
  }
}



 ?>
