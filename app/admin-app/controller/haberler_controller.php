<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $newsObj = new News();

    if(isset($_POST["delete_new"])){
      $newsObj->setNewId($_POST["new_id"]);
      $delete = $newsObj->deleteNew();
      $pageMessage = ($delete) ? "Haber Silindi" : "Haber Silinemedi!";
    }

    $news = $newsObj->getNews();

    $number_of_pages = (int)(ceil(count($news)/20));
    $page_number = 1;
    if(isset($_GET["page"])) $page_number = (int)$_GET["page"];
    if($page_number <= 0) $page_number = 1;
    else if($page_number > $number_of_pages) $page_number=$number_of_pages;

    $offset = ($page_number-1)*20;
    $limit = $offset+20;
  }

  $title = "Haberler";
  $pageTitle = "Haberler";
  $map = adminMap("Anasayfa,Haberler", "index,haberler");


?>
