<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $url = explode('/',$_GET["url"]);
    if(count($url) <= 2 || trim($url[2]) == "") header("location:".adminUrl("kategori-sec/kategori-filtre-ekle"));
    else{
      $categoryId = (int)$url[2];

      $filter = new Filter();
      $filter->setCategoryId($categoryId);

      if(isset($_POST["insertCategoryFilter"])){
        $filter->setFilterId($_POST["filterId"]);
        if(!isset($_POST["sub_categories"])){
          $insert = $filter->insertCategoryFilter();
          $pageMessage = ($insert === "Success") ? "Kategoriye Filtre Eklendi" : "Kategoriye Filtre Eklenemedi";
        }
        else{
          $insert = $filter->insertCategoryFilterWithSubCategories();
          $pageMessage = ($insert === "Success") ? "Kategoriye ve Tüm Alt Kategorilerine Filtre Eklendi" : "Kategoriye Filtre Eklenemedi";
        }
      }
      else if(isset($_POST["deleteCategoryFilter"])){
        $filter->setFilterId($_POST["filterId"]);
        if(!isset($_POST["sub_categories"])){
          $delete = $filter->deleteCategoryFilter();
          $pageMessage = ($delete) ? "Filtre Kategoriden Kaldırıldı" : "Filtre Kaldırılamadı";
        }
        else{
          $delete = $filter->deleteCategoryFilterWithSubCategories();
          $pageMessage = ($delete) ? "Filtre Kategoriden ve Tüm Alt Kategorilerinden Kaldırıldı" : "Filtre Kaldırılamadı";
        }
      }
      $categoryFilters = $filter->getCategoryFilters();

      $categoryObject = new Category();
      $categoryObject->setCategoryId($categoryId);
      $categoryInfo = $categoryObject->getSelectedCategory();
      $filters = $filter->getAllFilters();

      $title = "Kategoriye Filtre Ekle";
      $pageTitle = "Kategoriye Filtre Ekle";
      $map = adminMap("Anasayfa,Kategori İşlemleri,Kategoriye Filtre Ekle", "index,kategori-islemleri,kategori-filtre-ekle/$categoryId");
    }
  }
?>
