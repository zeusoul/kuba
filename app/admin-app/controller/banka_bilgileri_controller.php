<?php

  if(!adminLoginState()){
    header("location:".adminUrl('giris'));
  }
  else{
    $bank = new Bank();

    if(isset($_POST["deleteBank"])){
      $bank->setBankId($_POST["bank_info_id"]);
      $delete = $bank->deleteBankInformation();

      $pageMessage = ($delete) ? "Banka Bilgileri Silindi" : "Banka Bilgileri Silinemedi";
      if(!is_bool($pageMessage)) $pageMessage = $delete;
    }
    $bankInformations = $bank->getBanks();

    $title = "Banka Bilgileri";
    $pageTitle = "Banka Bilgileri";
    $map = adminMap("Anasayfa,Banka Bilgileri", "index,banka-bilgileri");
  }

?>
