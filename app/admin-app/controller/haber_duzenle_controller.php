<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(!isset($_GET["new_id"])) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $newId = (int)$_GET["new_id"];
      $newsObj = new News();
      $newsObj->setNewId($newId);

      if(isset($_POST['updateNew'])) {
        $newsObj->setNewImage($_FILES["image"]);
        $newsObj->setNewTitle($_POST["title"]);
        $newsObj->setNewDesc($_POST["description"]);
        $newsObj->setNewContent($_POST["content"]);

        $update = $newsObj->updateNew();;

        if($update == "Success") header("location:".adminUrl("haber-duzenle?new_id=$newId&Success"));
        else header("location:".adminUrl("haber-duzenle?new_id=$newId&Failed=$update"));
      }
      $newDetails = $newsObj->getNewDetails();
      if(count($newDetails) == 0){
        header("Location:".adminUrl());
        exit;
      }
      if(isset($_GET["Success"])) $pageMessage = "Haber güncellendi";
      else if(isset($_GET["Failed"])) $pageMessage = $_GET["Failed"];

      $title = "Haber";
      $pageTitle = "Haber Düzenle";
      $map = adminMap("Anasayfa,Haberler,Haber Düzenle", "index,haberler,haber-duzenle?new_id=$newId");
    }
  }
?>
