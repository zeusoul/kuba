<?php
  if(!adminLoginState()) {
    header("location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET["url"]);
    if(count($url) != 3) {
      header("location:".adminUrl("kategori-sec/urun-ekle"));
      exit;
    }
    else {
      $selectedCategoryId = (int)$url[2];

      $categoryOb = new Category();
      $categoryOb->setCategoryId($selectedCategoryId);
      $selectedCategory = $categoryOb->getSelectedCategory();
      $selectedCategoryName = $selectedCategory["name"];


      $filterObject = new Filter();
      $filterObject->setCategoryId($selectedCategoryId);
      $filters = $filterObject->getCategoryFilters();

      $brand = new Brand();
      $brands = $brand->getBrands();

      if(isset($_POST["insertProduct"])){
        $productObject = new Product();
        $productObject->setCategoryId($selectedCategoryId);
        $productObject->setProductImage($_FILES["image"]);
        $productObject->setTitle($_POST["title"]);
        $productObject->setSubTitle($_POST["subTitle"]);
        $productObject->setBrandId($_POST["brand_id"]);
        $productObject->setModel($_POST["model"]);
        $productObject->setProductCode($_POST["productCode"]);
        $productObject->setContent($_POST["content"]);
        $productObject->setStatus($_POST["status"]);

        #foreach ile post dizisindeki tüm elemanları kontrol ediyoruz.
        #Sadece key'i int olanları properties dizisine aktarıyoruz.
        #Bu elemanlar ürünün özelliklerini taşıyor
        $selectedFilters = array();
        foreach ($_POST as $key => $value) {
          $keyArray = explode("-",$key);
          $keyInt = $keyArray[count($keyArray)-1];
          if(intval($key)){
            if (trim($_POST["txt-$keyInt"]) == "") {
              $selectedFilters[$keyInt] = $value;
            }
            else{
              $valueName = $_POST["txt-$keyInt"];
              $values = new FilterValues();
              $values->setValueName($valueName);
              $values->setFilterId($keyInt);
              $insertValue = $values->insertFilterValue();

              if($insertValue){
                $lastValue = $values->getLastInsertedFilterValue();
                $valueId = $lastValue["filter_value_id"];
                $selectedFilters[$keyInt] = $valueId;
              }
            }
          }
        }


        $insert = $productObject->insertProduct();
        if($insert === "Success"){
          $insertedProduct = $productObject->getLastProduct();
          $insertedProductId = (int)$insertedProduct["product_id"];
          if(count($selectedFilters) > 0){
            $filterObject->setProductId($insertedProductId);
            $insertFilters = $filterObject->insertProductFilters($selectedFilters);
            if(is_bool($insertFilters) && $insertFilters){
              header("location:".adminUrl("urun-bilgisi-ekle/$insertedProductId?success"));
              exit;
            }
            else $pageMessage = "Ürün Filtresi Eklemede Hata ";
          }
          else {
            header("location:".adminUrl("urun-bilgisi-ekle/$insertedProductId?success"));
            exit;
          }
        }
        else $pageMessage = "Ürün Eklemede Hata : $insert";
      }

      $title = "Ürün Ekle";
      $pageTitle = "Ürün Ekle";
      $map = adminMap("Anasayfa,Ürün İşlemleri, Ürün Ekle", "index,urun-islemleri,urun-ekle/$selectedCategoryId");
    }
  }




?>
