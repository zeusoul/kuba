<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $page_number = 1;
    if(isset($_GET["page"])) $page_number = (int)$_GET["page"];
    if($page_number <= 0) $page_number = 1;

    $offset = ($page_number-1)*20;
    $limit = $offset+20;

    $products = array();
    $productObject = new Product();
    if(isset($_GET["search"])){
      $filters = array(
        $_GET['type'] =>  $_GET["value"]
      );
      $result = $productObject->getSearchProducts($filters);
    }
    else{
      $result = $productObject->getAllProducts($page_number, 20);
    }

    if(isset($result["products"])) $products = $result["products"];
    $number_of_pages = (int)(ceil(count($result["total_products_count"])/20));
    if($page_number > $number_of_pages) $page_number = $number_of_pages;

    $title = "Ürün İşlemleri";
    $pageTitle = "Ürün İşlemleri";
    $map = adminMap("Anasayfa,Ürün İşlemleri", "index,urun-islemleri");
  }


?>
