<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(count($url) != 2) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $title = "Videolar";

      $productId = (int)$url[2];

      $videosObj = new Videos();

      if(isset($_POST["insertVideo"])){
        $videoUrl = $_POST["video_url"];
        $videoTitle = $_POST["video_title"];
        $videoDesc = $_POST["video_desc"];
        $videosObj->setVideoUrl($videoUrl);
        $videosObj->setVideoTitle($videoTitle);
        $videosObj->setVideoDesc($videoDesc);
        $insert = $videosObj->insertVideo();
        $state = $insert ? "insertSuccess" : "insertFailed";
        header("location:".adminUrl("video-islemleri?$state"));
      }
      else if(isset($_POST["deleteVideo"])){
        $videosObj->setVideoId($_POST["video_id"]);
        $delete = $videosObj->deleteVideo();
        $state = $delete ? "deleteSuccess" : "deleteFailed";
        header("location:".adminUrl("video-islemleri?$state"));
      }

      if(isset($_GET["insertSuccess"])) $pageMessage = "Video Yüklendi";
      else if(isset($_GET["insertFailed"])) $pageMessage = "Video Yüklenemedi";
      else if(isset($_GET["deleteSuccess"])) $pageMessage = "Video Silindi";
      else if(isset($_GET["deleteFailed"])) $pageMessage = "Video Silinemedi";
      $videos = $videosObj->getVideos();

    }
  }
?>
