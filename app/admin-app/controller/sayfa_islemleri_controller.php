<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    if(isset($_GET["silindi"])) $pageMessage = "Sayfa Silindi.";
    if(isset($_GET["eklendi"])) $pageMessage = "Sayfa Eklendi.";
    $pagesObject = new Pages();
    $pages = $pagesObject->getAllPages();

    $title = "Sayfa İşlemleri";
    $pageTitle = "Sayfa İşemleri";
    $map = adminMap("Anasayfa,Sayfa İşlemleri", "index,sayfa-islemleri");
  }

?>
