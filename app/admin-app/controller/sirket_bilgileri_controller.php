<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $companyObj = new Company();
    if(isset($_POST["updateCompanyName"])){
      $companyObj->setCompanyName($_POST["company_name"]);
      $update = $companyObj->updateCompanyName();
      $pageMessage = $update ? "Şirket Adı Güncellendi" : "Şirket Adı Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyTel"])){
      $companyObj->setCompanyTel($_POST["company_tel"]);
      $update = $companyObj->updateCompanyTel();
      $pageMessage = $update ? "Telefon Numarası Güncellendi" : "Telefon Numarası Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyWp"])){
      $companyObj->setCompanyWp($_POST["company_wp"]);
      $update = $companyObj->updateCompanyWp();
      $pageMessage = $update ? "Whatsapp Numarası Güncellendi" : "Whatsapp Numarası Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyEmail"])){
      $companyObj->setCompanyEmail($_POST["company_email"]);
      $update = $companyObj->updateCompanyEmail();
      $pageMessage = $update ? "Email Adresi Güncellendi" : "Email Adresi Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyLogo"])){
      $companyObj->setCompanyLogo($_FILES["company_logo"]);
      $update = $companyObj->updateCompanyLogo();
      $pageMessage = $update ? "Şirket Logosu Güncellendi" : "Şirket Logosu Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyHeaderLogo"])){
      $companyObj->setCompanyHeaderLogo($_FILES["company_header_logo"]);
      $update = $companyObj->updateCompanyHeaderLogo();
      $pageMessage = $update ? "Site Başlığı İçin Logo Güncellendi" : "Site Başlığı İçin Logo Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyAddress"])){
      $companyObj->setCompanyAddress($_POST["company_address"]);
      $update = $companyObj->updateCompanyAddress();
      $pageMessage = $update ? "Adres Bilgisi Güncellendi" : "Adres Bilgisi Güncellemede Hata!";
    }
    else if(isset($_POST["updateCompanyAbout"])){
      $companyObj->setCompanyAbout($_POST["company_about"]);
      $update = $companyObj->updateCompanyAbout();
      $pageMessage = $update ? "Şirket Hakkında Bilgisi Güncellendi" : "Şirket Hakkında Bilgisi Güncellemede Hata!";
    }

    $companyInformation = $companyObj->getCompanyInformation();
    $title = "Şirket Bilgileri";
    $pageTitle = "Şirket Bilgileri";
    $map = adminMap("Anasayfa,Şirket Bilgileri", "index,sirket-bilgileri");
  }

?>
