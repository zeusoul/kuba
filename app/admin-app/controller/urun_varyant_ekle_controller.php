<?php
  if(!isset($_GET["productId"])){
    header("location:".adminUrl("urun-islemleri"));
    exit;
  }
  else{
    $productId = (int)$_GET["productId"];
    $product = new Product();
    $product->setProductId($productId);

    if(!$product->isProduct()){
      header("location:".adminUrl("urun-islemleri"));
      exit;
    }
    else{
      $productVariant = new ProductVariant();
      $productVariant->setProductId($productId);
      $variants = array();
      $productVariants = $productVariant->getProductVariantsByProductId();

      if(isset($productVariants[0]["json_combine"])){
        $jsonArray = json_decode($productVariants[0]["json_combine"], true);
        foreach ($jsonArray as $key => $value) {
          $variantId = (int)$value["variantId"];
          $variantObj = new Variant();
          $variantObj->setVariantId($variantId);
          $variants[] = $variantObj->getVariantWithValues();
        }
      }
      else if(isset($_GET["selectVariants"])){
        $getVariants = $_GET["variants"];
        foreach ($getVariants as $key => $variant) {
          $variantId = (int)$variant;
          $variantObj = new Variant();
          $variantObj->setVariantId($variantId);
          $variants[] = $variantObj->getVariantWithValues();
        }
      }
      else {
        header("location:".adminUrl("urun-varyant-sec?productId=$productId"));
        exit;
      }
      if(isset($_POST["insertVariant"])){
        $JSONcombine = array();
        if(isset($_POST["variantsAndValues"])){
          foreach ($_POST["variantsAndValues"] as $variantId => $valueId) {
            $JSONcombine[] = [
              "variantId" => $variantId,
              "valueId"   => $valueId
            ];
          }
        }
        if(count($JSONcombine) == 0) $JSONcombine = "";
        else $JSONcombine = json_encode($JSONcombine);

        $productVariant->setJSONcombine($JSONcombine);
        $productVariant->setBarcode($_POST["barcode"]);
        $productVariant->setStockCode($_POST["stock_code"]);
        $productVariant->setStock($_POST["stock"]);
        $productVariant->setPrice($_POST["price"]);
        $productVariant->setDiscountPrice($_POST["discount_price"]);
        $productVariant->setCurrency($_POST["currency"]);
        $insert = $productVariant->insertProductVariant();

        $pageMessage = $insert == "Success" ? "Varyant Eklendi" : $insert;
      }
      else if(isset($_POST["delete_product_variant"])){
        $productVariant->setProductVariantId($_POST["product_variant_id"]);
        $delete = $productVariant->deleteProductVariant();
        $pageMessage = $delete ? "Ürün Varyantı Silindi" : "Ürün Varyantı Silinemedi";
      }
      else if(isset($_POST["update_product_variant"])){
        $productVariant->setBarcode($_POST["barcode"]);
        $productVariant->setStockCode($_POST["stock_code"]);
        $productVariant->setStock($_POST["stock"]);
        $productVariant->setPrice($_POST["price"]);
        $productVariant->setDiscountPrice($_POST["discount_price"]);
        $productVariant->setCurrency($_POST["currency"]);
        $productVariant->setProductVariantId($_POST["product_variant_id"]);
        $update = $productVariant->updateProductVariant();
        $pageMessage = $update == "Success" ? "Ürün Varyantı Güncellendi" : $update;
      }
      $productVariants = $productVariant->getProductVariantsByProductId();

      $title = "Ürün Varyantları Ekle";
      $pageTitle = "Ürün Varyantları Ekle";
      $map = adminMap("Anasayfa,Ürün İşlemleri,Ürün Ekle,Ürün Bilgisi Ekle,Ürün Varyantları Seç,Ürün Varyantları Ekle",
                      "index,urun-islemleri,urun-ekle,urun-bilgisi-ekle/$productId,urun-varyant-sec?productId=$productId,urun-varyant-ekle?productId=$productId&selectVariants=".$_GET["selectVariants"]);
    }
  }

?>
