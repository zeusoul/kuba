<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $settingsOb = new Settings();

    if(isset($_POST["update_yayin_durumu"])){
      $settingsOb = new Settings();
      $settingsOb->setValue($_POST["site_yayin_durumu"]);
      $update = $settingsOb->updateSiteYayinDurumu();
      $pageMessage = ($update) ? "Site Yayın Durumu Ayarınız Güncellendi" : $update;
    }
    else if(isset($_POST["updateEmailDogrulama"])){
      $settingsOb = new Settings();
      $settingsOb->setValue($_POST["email_dogrulama"]);
      $update = $settingsOb->updateEmailDogrulama();
      $pageMessage = ($update) ? "Email Doğrulama Ayarınız Güncellendi" : $update;
    }
    else if(isset($_POST["updateKargoUcreti"])){
      $settingsOb = new Settings();
      $settingsOb->setValue($_POST["kargo_ucreti"]);
      $update = $settingsOb->updateKargoUcreti();
      $pageMessage = ($update === "Success") ? "Kargo Ücreti Güncellendi" : $update;
    }
    else if(isset($_POST["updateUcretsizKargoSiniri"])){
      $settingsOb = new Settings();
      $settingsOb->setValue($_POST["ucretsiz_kargo_siniri"]);
      $update = $settingsOb->updateUcretsizKargoSiniri();
      $pageMessage = ($update === "Success") ? "Ücretsiz Kargo Sınırı Güncellendi" : $update;
    }

    $settings = $settingsOb->getSettings();

    $title="Genel Ayarlar";
    $pageTitle = "Genel Ayarlar";
    $map = adminMap("Anasayfa,Genel Ayarlar", "index,genel-ayarlar");
  }

?>
