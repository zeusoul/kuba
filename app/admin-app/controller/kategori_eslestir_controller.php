<?php
  if(!isset($_GET["id"])){
    header("location:".adminUrl("kategori-islemleri"));
    exit;
  }
  else{
    $objCategory = new Category();
    $objCategory->setCategoryId($_GET["id"]);
    $category = $objCategory->getCategoryById();

    $platform = new Platform();
    $platforms = $platform->getPlatforms();

    $platformCategory = new PlatformCategory();
    $platformCategory->setCategoryId($_GET["id"]);

    if(isset($_POST["matchCategory"])){
      $platformCategory->setPlatformId($_GET["platform_id"]);
      $platformCategory->setPlatformCategoryCode($_POST["platform_category_code"]);
      $platformCategory->setCategoryId($_POST["category_id"]);
      $insert = $platformCategory->insertPlatformCategory();
      $pageMessage = $insert ? "Kategori Eşleştirildi" : "Kategori Eşleştirilemedi !";
      unset($_GET["platform_id"]);
    }
    else if(isset($_POST["deleteMatch"])){
      $platformCategory->setPlatformCategoryId($_POST["platform_category_id"]);
      $res = $platformCategory->deletePlayformCategory();
      $pageMessage = $res ? "Eşleştirme Silindi" : "Eşleştirme Silinemedi !";
    }

    $platformCategories = $platformCategory->getPlatformCategoriesByCategoryId();

    if(isset($_GET["platform_id"])){
      $platformInfoObj = new PlatformInformation();
      $platformInfoObj->setPlatformId($_GET["platform_id"]);
      $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
      if(!is_array($platformInfo) || count($platformInfo) == 0){
        $pageMessage = "Lütfen İlgili Platforma Ait API Bilgilerini Giriniz.";
      }
      else{
        if($_GET["platform_id"] == "1"){
          $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
          $categories = $trendyol->getCategoriesList();
        }
        else if($_GET["platform_id"] == "2"){
          $objHb = new Hepsiburada($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
          $categories = $objHb->getAuthToken();
          exit;

          $categories = $objHb->getCategories();
          printr($categories);
        }
      }
    }

    $title = "Kategori Eşleştir";
    $pageTitle = "Kategori Eşleştir";
    $map = adminMap("Ana Sayfa,Kategori İşlemleri,Kategori Eşleştir","index,kategori-islemleri,kategori-eslestir?id=".$_GET["id"]);
  }

?>
