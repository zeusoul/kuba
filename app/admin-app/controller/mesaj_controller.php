<?php
  if(!adminLoginState()){
    header("location:".adminUrl());
    exit;
  }
  else{
    $url = explode("/",$_GET["url"]);
    if(count($url) != 4) {
      header("location:".adminUrl("destek-mesaj"));
      exit;
    }
    else{
      $messageId = (int)$url[2];
      $senderId = (int)$url[3];

      $messageObject = new Message();
      $messageObject->setUserId(-1);
      $messageObject->setMessageId($messageId);

      #ajax kontrolü
      if(isset($_POST["message_list"])){
        $messages = $messageObject->getMessages();
        foreach ($messages as $key => $message) {
          if((int)$message["sender_id"] == -1) {
            echo '
            <div class="outgoing_msg">
              <div class="sent_msg">
                <span class="time_date"> <strong>Siz : </strong></span>
                <p> '.$message["content"].' </p>
                <span class="time_date"> '.$message["posting_date"].' </span>
              </div>
            </div>';
          }
          else {
            echo '
            <div class="incoming_msg">
              <div class="incoming_msg_img">
                <img src="'.publicUrl("img/user.png").'" alt="sunil">
              </div>
              <div class="received_msg">
                <span class="time_date">';
                if((int)$message["sender_id"] > 0){
                  echo "<a target='_blank' href='".adminUrl("musteri-detay/".$message["sender_id"])."'>";
                }
                echo '<strong>'.$message["name_surname"].'</strong>';
                if((int)$message["sender_id"] > 0){
                  echo "</a>";
                }
                echo '
                </span>
                <div class="received_withd_msg">
                  <p>'.$message["content"].'</p>
                  <span class="time_date">'.$message["posting_date"].'</span>
                </div>
              </div>
            </div>';
          }
        }
        exit;
      }

      #ajax ile Mesaj gönderme kontrolü
      if(isset($_POST["send_message"])){
        $messageObject->setSubject($messageId." id'li mesaja cevap");
        $messageObject->setContent($_POST["message"]);
        $messageObject->setName($_SESSION[sessionPrefix()."admin_name"]);
        $messageObject->setSurname($_SESSION[sessionPrefix()."admin_surname"]);
        $messageObject->setParentMessageId($messageId);
        $messageObject->setEmail("Admin");
        $messageObject->setTel("111");
        $messageObject->setSenderId(-1);
        $messageObject->setRecipientId($senderId);
        $sendMessage = $messageObject->insertMessage();

        $member = new Member();
        $member->setUserId($senderId);
        $memberInfo = $member->getUserInformation();
        $email = $memberInfo["email"];

        $mail = new Mail();
        $subject = "Mesaj";
        $name = $memberInfo["name"]." ".$memberInfo["surname"];
        $content = "Merhaba $name, <br/>
        Yeni Mesajınız Var : <br/><br/>
        ".$_POST["message"];
        $sendMail = $mail->sendMail($email,"İstanbul","Elektromatik",$subject,$content);

        echo $sendMessage;
        exit;
      }

      $messages = $messageObject->getMessages();

      if(count($messages) <= 0) {
        header("location:".adminUrl("destek-mesaj"));
        exit;
      }
      else{
        $messageObject->updateMessageDisplay();
      }

      $title = "Mesajlaşmanız";
      $pageTitle = "";
      $map = adminMap("Anasayfa,", "index,");

    }
  }

?>
