<?php
  if(adminLoginState()){
    header("location:".url("yonetim"));
  }
  else {
    if(isset($_POST["logIn"])){
      $admin = new Admin();
      $admin->setUserName($_POST["uname"]);
      $admin->setPass($_POST["pass"]);
      $logIn = $admin->logIn();
      if($logIn) header("location:".adminUrl());
      else $pageMessage = "Kullanıcı Adı Veya Şifre Hatalı";
    }

    $title = "Yönetici Girişi";
    $page = "giris";
  }


?>
