<?php
  if(!adminLoginState()) {
    header("location:".adminUrl('giris'));
    exit;
  }
  else {
    #Burası sayfaya ajax bağlantısı.
    if(isset($_POST['id'])){
      $id = $_POST['id'][0];
      $category = new Category();
      $category->setCategoryId($id);
      $categories=$category->getSubCategories();
      echo json_encode($categories);
      exit;
    }

    $url = explode("/",$_GET["url"]);
    if(count($url) != 3) {
      header("location:".adminUrl());
      exit;
    }
    else {
      if (isset($_POST['select-category'])) {
        $selectedCategoryId = 0;
        for ($i=1; $i < $i+1; $i++) {
          if (isset($_POST["category-$i"])) $selectedCategoryId = $_POST["category-$i"];
          else break;
        }
        $page = $url[2];
        header("location:".adminUrl($page."/".$selectedCategoryId));
        exit;
      }
      else {
        $category = new Category();
        $category->setCategoryId(0);
        $categories = $category->getSubCategories();
      }
      $title = "Kategori Seç";
      $pageTitle = "Kategori Seç";
    }
  }
?>
