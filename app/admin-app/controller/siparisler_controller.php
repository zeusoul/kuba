<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $pageNumber = isset($_GET["pageNumber"]) ? (int)$_GET["pageNumber"] : 1;
    if($pageNumber < 1) $pageNumber = 1;

    $ordersObj = new Order();
    $orders = array();
    if(isset($_GET["search"])){
      $filters = array(
        "order_headers.order_no" => $_GET["order_no"],
        "users.name" => $_GET["name"],
        "users.surname" => $_GET["surname"],
        "users.tel" => $_GET["phone"],
        "order_headers.order_date" => $_GET["order_date"],
        "order_headers.order_amount" => $_GET["order_price"],
        "order_headers.status" => $_GET["status"]
      );
      $orders = $ordersObj->getAllOrders($pageNumber, $filters);
    }
    else $orders = $ordersObj->getAllOrders($pageNumber);

    $offset = (($pageNumber- 1) * 20) + 1;
    $limit = $offset + 19;
    $numberOfPages = (int)(ceil($orders["total_orders"]/20));

    // Trendyol siparişleri
    $platformInfoObj = new PlatformInformation();
    $platformInfoObj->setPlatformId(1);
    $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
    if(!is_array($platformInfo) || count($platformInfo) == 0){
      $trendyolMessage = "Lütfen Trendyol Platformuna Ait API Bilgilerini Giriniz";
    }
    else{
      $trendyolPageNumber = isset($_GET["trendyolPage"]) ? (int)$_GET["trendyolPage"] : 1;
      $objTrendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
      $trendyolOrders = $objTrendyol->getOrders($trendyolPageNumber-1);

      $trendyolOffset = (($trendyolPageNumber- 1) * 20) + 1;
      $trendyolLimit = $trendyolOffset + 19;
      $trendyolNumberOfPages = (int)(ceil($trendyolOrders["totalElements"]/20));
    }

    $title="Siparişler";
    $pageTitle = "Siparişler";
    $map = adminMap("Anasayfa,Siparişler", "index,siparisler");
  }

?>
