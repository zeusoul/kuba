<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $url = explode('/',$_GET["url"]);
    if(count($url) != 3){
      header("location:".adminUrl());
      exit;
    }
    else{
      $returnId = (int)$url[2];

      $returnObject = new ProductReturn();
      $returnObject->setReturnId($returnId);

      if(isset($_POST["cancelTheReturn"])){
        $returnObject->setUserId($_POST["user_id"]);
        $returnObject->setReasonForRejection($_POST["reason"]);
        $cancel = $returnObject->cancelTheReturn();

        $pageMessage = ($cancel === "Success") ? "İade Talebi İptal Edildi" : $cancel;
      }
      else if(isset($_POST["confirmTheReturn"])){
        $confirm = $returnObject->confirmTheReturn();
        $pageMessage = ($confirm) ? "İade Talebi Onaylandı" : "İade Talebi Onaylanamadı!";
      }
      $returnDetails = $returnObject->getReturnDetails();

      $orderNo = (int)$returnDetails["order_no"];
      $orderObject = new Order();
      $orderObject->setOrderNo($orderNo);
      $orderDetails = $orderObject->getOrderDetails();

      $userDetails = $orderDetails["user_information"];

      $title = "İade Detayı";
      $pageTitle = "İade Detayı";
      $map = adminMap("Anasayfa,İade Talepleri,İade Detayı", "index,iadeler,iade-detay/$returnId");
    }
  }


?>
