<?php

  if(!adminLoginState()){
      header("Location:". adminUrl());
  }
  else {
    $users = array();
    $user = new Member();
    if(isset($_GET["search"])){
      $filters = array(
        $_GET['type'] =>  $_GET["value"]
      );
      $users = $user->getSearchUsers($filters);
    }
    else {
      $users=$user->getUsers();
    }

    $title = "Müşteri Listesi";
    $pageTitle = "Müşteri Listesi";
    $map = adminMap("Anasayfa,Müşteri Listesi", "index,musteri-listesi");
  }
?>
