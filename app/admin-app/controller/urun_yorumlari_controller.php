<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $comments = new ProductComments();
    $member = new Member();

    if(isset($_POST["deleteComment"])){
      $comments->setCommentId($_POST["commentId"]);
      $delete = $comments->deleteProductComment();
      $pageMessage = ($delete === "Success") ? "Yorum Silindi" : $delete;
    }
    else if(isset($_POST["confirmComment"])){
      $comments->setCommentId($_POST["commentId"]);
      $confirm = $comments->confirmProductComment();
      $pageMessage = ($confirm === "Success") ? "Yorum Onaylandı" : $confirm;
    }

    $productComments="";
    if(isset($_GET["search"])){
      $set = array (
        $_GET["type"] => $_GET["value"]
      );
      $productComments = $comments->getSearchComments($set);
    }
    else $productComments = $comments->getAllProductComments();

    $title = "Ürün Yorumları";
    $pageTitle = "Ürün Yorumları";
    $map = adminMap("Anasayfa,", "index,urun-yorumlari");
  }
?>
