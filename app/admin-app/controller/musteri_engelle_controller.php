<?php
  if(!adminLoginState()){
    header("location:".adminUrl('giris'));
    exit;
  }
  else{
    $url = explode("/",$_GET["url"]);
    if(count($url) < 3) header("location:".adminUrl());
    else{
      $userId = $url[2];
      $user = new Member();
      $user->setUserId($userId);
      $userDetails = $user->getUserInformation();

      if(isset($_POST["engelle"])){
        $user->setUserId($_POST["userId"]);
        $ban = $user->banUser();
        if($ban === "Success") {
          if(isset($url[3]) && trim($url[3]) != "") header("Location:".adminUrl($url[3]."/".$_POST["userId"]));
          else header("Location:".adminUrl("musteri-listesi"));
        }
        else $pageMessage = $ban;
      }
      else if(isset($_POST["vazgec"])){
        if(isset($url[3]) && trim($url[3]) != "")  header("Location:".adminUrl($url[3]."/".$_POST["userId"]));
        else header("Location:".adminUrl("musteri-listesi"));
      }

      $title = "Müşteri Engelle";
      $pageTitle = "Müşteri Engelle";
      $map = adminMap("Anasayfa,Müşteri Listesi,Müşteri Bilgileri,Müşteri Engelle", "index,musteri-listesi,musteri-detay/$userId,musteri-engelle/$userId/".$url[3]);
    }
  }
?>
