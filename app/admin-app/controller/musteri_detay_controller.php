<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $url= explode("/",$_GET["url"]);
    if(count($url) != 3 || (int)$url[2] <= 0) {
      header("location:".adminUrl());
    }
    else{
      $userId = (int)$url[2];
      $user = new Member();
      $user->setUserId($userId);
      $userDetails= $user->getUserInformation();

      if(count($userDetails) > 0){
        $adres = new Address();
        $adres->setUserId($userId);
        $adresBilgileri=$adres->getAddressBooks();

        $cartObject = new Cart();
        $cart = $cartObject->getUserCart($userId);

        $orderObject = new Order();
        $orderObject->setUserId($userId);
        $orders = $orderObject->getOrders();
      }
      $title = "Müşteri Bilgileri";
      $pageTitle = "Müşteri Bilgileri";
      $map = adminMap("Anasayfa,Müşteri Listesi,Müşteri Bilgileri", "index,musteri-listesi,musteri-detay/$userId");
    }
  }

?>
