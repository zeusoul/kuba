<?php

  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
  }
  else{
    $selectedFilterId=0;
    $url = explode('/',$_GET["url"]);
    if(count($url) == 3)$selectedFilterId = (int)$url[2];

    $filter = new Filter();
    $filterValues = new FilterValues();


    if(isset($_POST["insertFilterValue"])){
      $filterValues->setValueName($_POST["valueName"]);
      $filterValues->setFilterId($selectedFilterId);

      $insert = $filterValues->insertFilterValue();
      $pageMessage = ($insert) ? "Değer Eklendi" : "Değer Eklenemedi";
    }
    else if(isset($_POST["updateFilterValue"])){
      $filterValues->setValueName($_POST["valueName"]);
      $filterValues->setFilterId($_POST["filterId"]);
      $filterValues->setValueId($_POST["filterValueId"]);
      $update = $filterValues->updateFilterValue();
      $pageMessage = ($update) ? "Değer Güncellendi" : "Değer Güncellenemedi";
    }
    else if(isset($_POST["deleteFilterValue"])){
      $filterValues->setValueId($_POST["filterValueId"]);
      $delete = $filterValues->deleteFilterValue();
      $pageMessage = ($delete) ? "Değer Silindi" : "Değer Silinemedi";
    }


    $filter->setFilterId($selectedFilterId);
    $Selectedfilter =$filter->getSelectedFilter();



    $filterValues->setFilterId($selectedFilterId);
    $values=$filterValues->getSelectedFilterValues();

    $title = "Filtre Değeri Ekle";
    $pageTitle = "Filtre Değeri Ekle";
    $map = adminMap("Anasayfa,Filtre Ekle,Filtre Değeri Ekle", "index,filtre-ekle,filtre-deger-ekle/$selectedFilterId");
  }

?>
