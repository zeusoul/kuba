<?php
  if(!adminLoginState()) {
    header("location:".adminUrl("giris"));
    exit;
  }
  else {
    if(isset($_POST["insertSlider"])){
      $sliderObj = new Slider();
      $sliderObj->setSliderYear($_POST["year"]);
      $sliderObj->setSliderTitle($_POST["title"]);
      $sliderObj->setSliderDescription($_POST["description"]);
      $sliderObj->setSliderImage($_FILES["image"]);
      $sliderObj->setSliderUrl($_POST["url"]);
      $sliderObj->setSliderPrice($_POST["price"]);

      $insert = $sliderObj->insertSlider();
      $pageMessage = ($insert == "Success") ? "Slider Kaydedildi" : $insert;
    }

    $title = "Slider Ekle";
    $pageTitle = "Slider Ekle";
    $map = adminMap("Anasayfa,Slider İşlemleri,Slider Ekle", "index,slider-islemleri,slider-ekle");
  }
?>
