<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $title = "Referanslar";

    $referencesObj = new References();


    if(isset($_POST["insertReference"])){
      $referencesObj->setReferenceImage($_FILES["image"]);
      $videoOrder = $_POST["video_order"];
      $referencesObj->setReferenceOrder($referenceOrder);
      $insert = $referencesObj->insertReference();
      $state = $insert ? "insertSuccess" : "insertFailed";
      header("location:".adminUrl("referanslar?$state"));
    }
    else if(isset($_POST["deleteReference"])){
      $referencesObj->setReferenceId($_POST["reference_id"]);
      $delete = $referencesObj->deleteReference();
      $state = $delete ? "deleteSuccess" : "deleteFailed";
      header("location:".adminUrl("referanslar?$state"));
    }
    /*if(isset($_POST["referenceOrder"])){
      $referencesObj->setReferenceId($_POST["reference_id"]);
      $referencesObj->setReferenceOrder($_POST["reference_order"]);
      $update = $referencesObj->updateReferenceByOrderNumber();
      $state = $update ? "updateSuccess" : "updateFailed";
      header("location:".adminUrl("video-islemleri?$state"));
    }
    */
    if(isset($_GET["insertSuccess"])) $pageMessage = "Referans Yüklendi";
    else if(isset($_GET["insertFailed"])) $pageMessage = "Referans Yüklenemedi";
    else if(isset($_GET["deleteSuccess"])) $pageMessage = "Referans Silindi";
    else if(isset($_GET["deleteFailed"])) $pageMessage = "Referans Silinemedi";

    $references = $referencesObj->getReferences();

  }
?>
