<?php
  if(!adminLoginState()) {
    header("Location:".adminUrl("giris"));
    exit;
  }
  else {
    $url = explode("/",$_GET['url']);
    if(count($url) != 3) {
      header("location:".adminUrl());
      exit;
    }
    else {
      $title = "Ürün Videoları";

      $productId = (int)$url[2];

      $productObject = new Product();
      $productObject->setProductId($productId);
      $productDetails = $productObject->getProductDetails();

      $productVideosObj = new ProductVideos();
      $productVideosObj->setProductId($productId);

      if(isset($_POST["insertProductVideo"])){
        $productVideoUrl = $_POST["video_url"];
        $productVideosObj->setProductVideoUrl($productVideoUrl);
        $insert = $productVideosObj->insertProductVideo();
        $state = $insert ? "insertSuccess" : "insertFailed";
        header("location:".adminUrl("urun-videolari/$productId?$state"));
      }
      else if(isset($_POST["deleteProductVideo"])){
        $productVideosObj->setProductVideoId($_POST["product_video_id"]);
        $delete = $productVideosObj->deleteProductVideo();
        $state = $delete ? "deleteSuccess" : "deleteFailed";
        header("location:".adminUrl("urun-videolari/$productId?$state"));
      }

      if(isset($_GET["insertSuccess"])) $pageMessage = "Video Yüklendi";
      else if(isset($_GET["insertFailed"])) $pageMessage = "Video Yüklenemedi";
      else if(isset($_GET["deleteSuccess"])) $pageMessage = "Video Silindi";
      else if(isset($_GET["deleteFailed"])) $pageMessage = "Video Silinemedi";
      $productVideos = $productVideosObj->getProductVideos();

    }
  }
?>
