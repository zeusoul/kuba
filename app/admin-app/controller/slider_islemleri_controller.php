<?php
  if(!adminLoginState()){
    header("location:".adminUrl("giris"));
    exit;
  }
  else{
    $sliderObj = new Slider();

    if(isset($_POST["delete_slider"])){
      $sliderObj->setSliderId($_POST["slider_id"]);
      $delete = $sliderObj->deleteSlider();
      $pageMessage = ($delete) ? "Slider Silindi" : "Slider Silinemedi";
    }

    $sliders = $sliderObj->getSliders();

    $number_of_pages = (int)(ceil(count($sliders)/20));
    $page_number = 1;
    if(isset($_GET["page"])) $page_number = (int)$_GET["page"];
    if($page_number <= 0) $page_number = 1;
    else if($page_number > $number_of_pages) $page_number=$number_of_pages;

    $offset = ($page_number-1)*20;
    $limit = $offset+20;

    $title = "Slider İşlemleri";
    $pageTitle = "Slider İşlemleri";
    $map = adminMap("Anasayfa,Slider İşlemleri", "index,slider-islemleri");
  }


?>
