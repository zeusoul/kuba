<?php
  if(!adminLoginState()){
    header("location:".url('giris'));
  }
  else{
    $filter = new Filter();
    if(isset($_POST["insertFilter"])){
      $filter->setFilterName($_POST["filterName"]);
      $insert = $filter->insertFilter();
      $pageMessage = ($insert) ? "Filtre Eklendi" : "Filtre Eklenemedi";
    }
    else if(isset($_POST["updateFilter"])){
      $filter->setFilterId($_POST["filterId"]);
      $filter->setFilterName($_POST["filterName"]);
      $update = $filter->updateFilter();

      $pageMessage = ($update) ? "Filtre Güncellendi" : "Filtre Güncellenemedi";
    }
    else if(isset($_POST["deleteFilter"])){
      $filter->setFilterId($_POST["filterId"]);
      $delete = $filter->deleteFilter();

      $pageMessage = ($delete) ? "Filtre Silindi" : "Filtre Silinemedi";
    }


    $filters = $filter->getAllFilters();
    $title = "Filtre Ekle";
    $pageTitle = "Filtre Ekle";
    $map = adminMap("Anasayfa,", "index,filtre-ekle");
  }


?>
