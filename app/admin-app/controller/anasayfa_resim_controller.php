<?php
  if(!adminLoginState()){
    header("location:".adminUrl('giris'));
  }
  else{

    $mainImage = new MainImage();

    if(isset($_POST["firstImage"])){
      $mainImage->setImage($_FILES["image"]);
      $mainImage->setProductId($_POST["product_id"]);
      $mainImage->setImageOrder(1);
      $result = $mainImage->insertMain();
      $pageMessage = $result["message"];
    }
    else if(isset($_POST["secondImage"])){
      $mainImage->setImage($_FILES["image2"]);
      $mainImage->setProductId($_POST["product_id"]);
      $mainImage->setImageOrder(2);
      $result = $mainImage->insertMain();
      $pageMessage = $result["message"];
    }
    else if(isset($_POST["thirdImage"])){
      $mainImage->setImage($_FILES["image3"]);
      $mainImage->setProductId($_POST["product_id"]);
      $mainImage->setImageOrder(3);
      $result = $mainImage->insertMain();
      $pageMessage = $result["message"];
    }
    else if(isset($_POST["fourthImage"])){
      $mainImage->setImage($_FILES["image4"]);
      $mainImage->setProductId($_POST["product_id"]);
      $mainImage->setImageOrder(4);
      $result = $mainImage->insertMain();
      $pageMessage = $result["message"];
    }
    else if(isset($_POST["fiveImage"])){
      $mainImage->setImage($_FILES["image5"]);
      $mainImage->setProductId($_POST["product_id"]);
      $mainImage->setImageOrder(5);
      $result = $mainImage->insertMain();
      $pageMessage = $result["message"];
    }


    $product = new Product();
    $products = $product->getAllProducts();


    $title = "Anasayfa Resim İşlemleri";
    $pageTitle = "Anasayfa Resimleri";
    $map = adminMap("Anasayfa, Anasayfa Resimleri", "index,anasayfa-resim");
  }
?>
