<?php

  include "./vendor/autoload.php";

  class Hepsiburada{
    protected $username,$password,$merchantId,$guzzle,$authToken;
    protected $categories;
    function __construct($username,$password,$merchantId){
        $this->merchantId = $merchantId;
        $this->username = $username;
        $this->password = $password;
        $this->getAuthToken($username,$password);
        $options['headers'] = array(
          'Accept' => 'application/json',
          'Authorization' =>'Bearer '.$this->authToken
        );
        $this->guzzle = new GuzzleHttp\Client($options);
    }
    public  function getAuthToken($username,$password){
      $postData = array(
        'username' => $username,
        'password' => $password,
        'authenticationType' => 'INTEGRATOR');
      $json = json_encode($postData);
      printr($json);
      exit;
      $ch = curl_init('https://mpop-sit.hepsiburada.com/api/authenticate');
      curl_setopt($ch,CURLOPT_POSTFIELDS,$json);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'Content-Type:application/json'
      ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);

      $arr = json_decode($result,true);
      $this->authToken= $arr['id_token'];

    }
    public function getBarer(){
      return $this->authToken;
    }
    public function getAttributes($categoryCode){
      $attributes=array();
      //JSON KAYDET
      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/categories/'.$categoryCode.'/attributes');
      $get=json_decode($data->getBody(),true);
      foreach ($get['data']['attributes'] as $attribute) {
        $attributes[] = $attribute;
      }
      return $attributes;
    }
    public function getVariants($categoryCode){
      $attributes=array();
      //JSON KAYDET
      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/categories/'.$categoryCode.'/attributes');
      $get = json_decode($data->getBody(),true);
      if(isset($get['data']['variantAttributes'])){
        foreach ($get['data']['variantAttributes'] as $attribute) {
          $attributes[] = $attribute;
        }
        return $attributes;
      }
      else return array();
    }
    public function getVariantValues($categoryCode,$variantCode){
      $variantValues=array();
      //JSON KAYDET
      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/categories/'.$categoryCode.'/attribute/'.$variantCode);
      $get=json_decode($data->getBody(),true);
      if(isset($get["data"]) && is_array($get["data"])){
        foreach ($get['data'] as $attributeValue) {
          $variantValues[] =array(
            'id' => $attributeValue['id'],
            'name' => $attributeValue['value']
          );
        }
      }
      return $variantValues;
    }
    public function getCategoryAttributeValues($categoryCode,$attributeId){

      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/categories/'.$categoryCode.'/attribute/'.$attributeId);
      $get=json_decode($data->getBody(),true);
      $attributeValues=array();
      if(isset($get["data"]) && is_array($get["data"])){
        foreach ($get['data'] as $attributeValue) {
          $attributeValues[] =array(
              'id' => $attributeValue['id'],
              'name' => $attributeValue['value']
          );
        }
      }
      return $attributeValues;
    }
    public function getCategoryAttributes($categoryCode){
      $attributes=array();
      //JSON KAYDET
      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/categories/'.$categoryCode.'/attributes/');
      $get=json_decode($data->getBody(),true);

      foreach ($get['data']['attributes'] as $attribute) {
        $attributes[] =array(
          'attribute' => array(
             'id'=> $attribute['id'],
             'name'=> $attribute['name']
          ),
          'required' =>$attribute['mandatory'],
          'attributeValues' =>$this->getCategoryAttributeValues($categoryCode,$attribute['id']),
          'allowCustom' => ($attribute['type'] == 'enum') ? false : true
        );
      }
      return $attributes;
    }
    public function getCategories(){
      $categories=array();
      //JSON KAYDET
      for ($i=0; $i < 3; $i++) {
        $data = $this->guzzle->get('https://mpop.hepsiburada.com/product/api/categories/get-all-categories?page='.$i.'&size=2000');
        $get=json_decode($data->getBody(),true);
        foreach ($get['data'] as $category) {
          $categories[$category['categoryId']] =$category['name'];
        }
      }
      return $categories;
    }
    public function addProducts($jsonPath=""){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://mpop-sit.hepsiburada.com/product/api/products/import');    // Define target site
      curl_setopt($ch, CURLOPT_POST,1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array (
        'Content-Type:multipart/form-data',
        'Authorization:Bearer '.$this->authToken
      )); // No http head
      //curl_setopt($ch, CURLOPT_REFERER, $ref);
      curl_setopt($ch, CURLOPT_NOBODY, FALSE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);      // Return page in string

      //curl_setopt($ch, CURLOPT_USERAGENT, $agent);
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, array (
        'file' => new CURLFile ($jsonPath)));
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);      // Follow redirects
      curl_setopt($ch, CURLOPT_MAXREDIRS, 4);

       # Execute the PHP/CURL session and echo the downloaded page
      $page = curl_exec($ch);
      $err = curl_error($ch);
      $info =curl_getinfo($ch);
       # Close the cURL session
      curl_close($ch);
      return $page;
    }
    public function getLog($trackingId){
      $data = $this->guzzle->get('https://mpop-sit.hepsiburada.com/product/api/products/status/'.$trackingId);
      $get=json_decode($data->getBody(),true);
      return $get;
    }
    public function getOrders(){
      $options['headers'] = array(
        'Content-Type' => 'application/json',
        'Authorization' =>'Basic '. base64_encode($this->username.':'.$this->password)
      );
      $this->guzzle = new GuzzleHttp\Client($options);
      $data = $this->guzzle->get('https://oms-external-sit.hepsiburada.com/orders/merchantid/'.$this->merchantId.'?offset=0&limit=10');
      $get=json_decode($data->getBody(),true);
      return $get;
    }

  }

?>
