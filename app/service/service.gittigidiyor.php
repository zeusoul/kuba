<?php
  /**
   *
   */
  class Gittigidiyor{
    protected $apiKey,$secretKey,$login,$password,$sign;
    function __construct($apiKey,$secretKey,$login,$password){
      $this->apiKey    = $apiKey;
      $this->secretKey = $secretKey;
      $this->login     = $login;
      $this->password  = $password;
    }
    public function gg_connect($action,array $params,$soap_url){
        $client = new SoapClient($soap_url, array('login' => $this->login, 'password' => $this->password , 'authentication' => SOAP_AUTHENTICATION_BASIC));
        $response = $this->object_to_array($client->__soapCall($action,$params));
        return $response;
    }
    public function object_to_array($data){
        if (is_array($data) || is_object($data)){
            $result = array();
            foreach ($data as $key => $value){
                $result[$key] =  $this->object_to_array($value);
            }
            return $result;
        }
        return $data;
    }
    public function getProduct($productId=0,$itemId=0){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array(
        "apiKey" => $this->apiKey,
        "sign" => $sign ,
        "time"=>$time,
        "productId"=>$productId,
        "itemId"=>$itemId,
        "lang"=> "tr"
      );
      $response = $this->gg_connect('getProduct',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");
      return $response;
    }
    public function getOrders(){
      $status="A";
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign,
                      "time" => $time,
                      "withData" => true,
                      "byStatus" => $status,
                      "byUser" => "A",
                      "orderBy" => "A",
                      "orderType" => "A",
                      "pageNumber" => 1,
                      "pageSize" => 100,
                      "lang" => 'tr');
      $response = $this->gg_connect('getPagedSales',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualSaleService?wsdl");
      return $response;
    }
    public function getCategoryVariantSpecs($categoryCode){
      $status="A";
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("categoryCode" => $categoryCode,
                      "lang" => 'tr');
      $response = $this->gg_connect('getCategoryVariantSpecs',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/CategoryService?wsdl");
      $attributes = $response['specs']['spec'];
      $variants = array();
      $i=0;
      foreach ($attributes as $key => $attribute) {
          $variants[$i]['name'] = $attribute["name"];
          $variants[$i]['id'] = $attribute["nameId"];
          $i++;
      }
      return $variants;
    }
    public function getCategoryVariantSpecValues($categoryCode,$variantCode){
      $status="A";
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("categoryCode" => $categoryCode,
                      "lang" => 'tr');
      $response = $this->gg_connect('getCategoryVariantSpecs',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/CategoryService?wsdl");
      $values = array();
      foreach ($response["specs"]["spec"] as $key => $spec) {
        if($spec["nameId"] == $variantCode){
            foreach ($spec["specValues"]["specValue"] as $value) {
              $values[] = array(
                "id" => $value["valueId"],
                "name" => $value["value"]
              );
            }
          break;
        }
      }
      return $values;
    }
    public function insertProductWithNewCargoDetail($productId,$product){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign ,
                      "time"=>$time,
                      "itemId"=>$productId,
                      "product"=>$product,
                      "forceToSpecEntry"=>false,
                      "nextDateOption"=> false,
                      "lang"=> "tr");
      $response = $this->gg_connect('insertProductWithNewCargoDetail',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");
      return $response;
    }
    public function insertAndActivateProduct($productId,$product){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign ,
                      "time"=>$time,
                      "itemId"=>$productId,
                      "product"=>$product,
                      "forceToSpecEntry"=>false,
                      "nextDateOption"=> false,
                      "lang"=> "tr");
      $response = $this->gg_connect('insertAndActivateProduct',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");
      return $response;
    }
    public function updateProductVariants($productId,$productVariant){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign ,
                      "time"=>$time,
                      "productId"=>$productId,
                      "itemId"=>"",
                      "productVariant"=>$productVariant,
                      "lang"=> "tr");
      $response = $this->gg_connect('updateProductVariants',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");
      return $response;
    }
    public function updateProductWithNewCargoDetail($productId,$itemId,$product){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign ,
                      "time"=>$time,
                      "itemId"=>$itemId,
                      "productId"=>$productId,
                      "product"=>$product,
                      "onSale"=>false,
                      "forceToSpecEntry"=>false,
                      "nextDateOption"=> false,
                      "lang"=> "tr");
      $response = $this->gg_connect('updateProductWithNewCargoDetail',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualProductService?wsdl");
      return $response;
    }
    public function getCategorySpecsWithDetail($categoryCode){
        $time = round(microtime(1) * 1000);
        $sign = md5($this->apiKey.$this->secretKey.$time);
        $params = array("categoryCode" => $categoryCode,
                        "lang" => 'tr');
        $response = $this->gg_connect('getCategorySpecsWithDetail',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/CategoryService?wsdl");
        $values = array();
        $i=0;
        foreach ($response["specs"]["spec"] as $key => $spec) {
              $values[$i]['attribute'] = array(
                'id' =>$spec['specId'],
                'name'=>$spec['name']
              );
              $values[$i]['required']=$spec["required"];
              $values[$i]['allowCustom']=false;
              foreach ($spec["values"]["value"] as $value) {
                $values[$i]["attributeValues"][] = array(
                  "id" => $value["specId"],
                  "name" => $value["name"]
                );
              }
              $i++;
        }
        return $values;
      }
    public function getCities(){
      $params = array(
        "startOffSet" => 0,
        "rowCount" => 82,
        "lang" => 'tr'
      );
      $response = $this->gg_connect('getCities',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/CityService?wsdl");
      return $response;
    }
    public function getCargoCompany(){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array(
        "apiKey" => $this->apiKey,
        "sign" => $sign ,
        "time"=>$time,
        "lang" => 'tr'
      );
      $response = $this->gg_connect('getCargoCompany',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualCargoService?wsdl");
      return $response;
    }
    public function getCategories(){
      $ctgArr = array();
      for ($i=0; $i < 53 ; $i++) {
        $params = array(
                "startOffSet" => $i*100,
                "rowCount" => 100,
                "withSpecs" => false,
                "withDeepest" => true,
                "withCatalog" => false,
                "lang" => 'tr'
          );
        $response = $this->gg_connect('getCategories',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/CategoryService?wsdl");
        $categories = array();
        if(!isset($response['categories']['category'])) break;
        else{
          foreach ($response['categories']['category'] as $key => $value) {
            $categories[$value['categoryCode']] = $value['categoryName'];
          }
          $ctgArr =$ctgArr + $categories;
        }
      }
      return $ctgArr;
    }
    /**
     * Kargolama aşamasındaki siparişi iptal eder.
     *
     * @access public
     * @param string $saleCode (Sipariş kodu)
     * @param int    $reasonId (İptal sebebi ID'si)
     * @return object
     */
    public function cancelSale($saleCode="", $reasonId=0){
      $time = round(microtime(1) * 1000);
      $sign = md5($this->apiKey.$this->secretKey.$time);
      $params = array("apiKey" => $this->apiKey,
                      "sign" => $sign ,
                      "time"=>$time,
                      "saleCode"=>$saleCode,
                      "reasonId"=>$reasonId,
                      "lang"=> "tr");
      $response = $this->gg_connect('cancelSale',$params,"https://dev.gittigidiyor.com:8443/listingapi/ws/IndividualSaleService?wsdl");
      return $response;
    }
  }
?>
