<?php

  include "./vendor/autoload.php";
  class Trendyol{
    private $username,$password,$supplierId,$guzzle,$categories,$options;
    function __construct($username="",$password="",$supplierId="") {
        $this->supplierId = $supplierId;
        $this->username =$username;
        $this->password = $password;
        if ($this->password !="") {
          $this->options['headers'] = array(
              'Authorization' => 'Basic ' . base64_encode($this->username . ':' . $this->password),
              'Content-Type' => 'application/json',
              'User-Agent' => $this->supplierId.' - SelfIntegration'
          );
        }
        else {
          $this->options['headers'] = array(
              'Content-Type' => 'application/json'
          );
        }
        $this->options['http_errors'] = false;
        //$this->guzzle = new GuzzleHttp\Client($this->options);
    }
    public function getOrders($page=1, $orderNumber=""){
      if($orderNumber != "") $url = "https://api.trendyol.com/sapigw/suppliers/$this->supplierId/orders?orderNumber=$orderNumber";
      else $url = "https://api.trendyol.com/sapigw/suppliers/$this->supplierId/orders?page=$page&size=20&orderByDirection=DESC";

      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
        'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
        'Content-Type:application/json',
        'User-Agent:'.$this->supplierId.' - SelfIntegration'
      ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $data = curl_exec($ch);
      curl_close($ch);
      $attributes = json_decode($data,true);
      return $attributes;
    }
    public function getCategories() {
      $data = $this->guzzle->get('https://api.trendyol.com/sapigw/product-categories');
      return json_decode($data->getBody(),true);
    }
    public function getAttributes($platformCategoryCode=0) {
      $ch = curl_init("https://api.trendyol.com/sapigw/product-categories/$platformCategoryCode/attributes");
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $data = curl_exec($ch);
      curl_close($ch);
      $attributes = json_decode($data,true);
      return $attributes;
    }
    /**
     * Kargo takip numarası bildirmeye yarar.
     */
    public function updateTrackingNumber($supplierId,$shipmentPackageId,$trackingNumber){
      $ch = curl_init('https://api.trendyol.com/sapigw/suppliers/'.$supplierId.'/'.$shipmentPackageId.'/update-tracking-number');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch,CURLOPT_POSTFIELDS, $trackingNumber);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
              'Content-Type:application/json'
          ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
    }
    /**
     * Paket Statü Bildirimi
     *
     * NOT: Statü beslemelerini yaparken önce "Picking" sonra "Invoiced" statü beslemesi yapmanız gerekmektedir.
     * "status": "Picking" (Toplanmaya Başlandı Bildirimi)                - params {}
     * "status": "Invoiced" (Fatura Kesme Bildirimi)                      - params {"invoiceNumber": "EME2018000025208"}
     * "status": "UnSupplied" İptal Bildirimi (Tedarik Edememe Bildirimi) - params {}
     *
     *
     */
    public function updatePackage($supplierId, $request, $id){
      $params = json_encode($request,JSON_UNESCAPED_UNICODE);
      $ch = curl_init('https://api.trendyol.com/sapigw/suppliers/'.$supplierId.'/shipment-packages/'.$id);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch,CURLOPT_POSTFIELDS, $params);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
        'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
        'Content-Type:application/json'
      ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
    }
    public function addProducts($supplierid,$product) {
      $ch = curl_init('https://api.trendyol.com/sapigw/suppliers/'.$supplierid.'/v2/products');
      curl_setopt($ch,CURLOPT_POSTFIELDS,$product);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
              'Content-Type:application/json',
              'User-Agent:'.$supplierid.' - SelfIntegration'
      ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
    }
    public function updateProducts($supplierId,$product) {
      $ch = curl_init('https://api.trendyol.com/sapigw/suppliers/'.$supplierId.'/v2/products');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch,CURLOPT_POSTFIELDS,$product);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
              'Content-Type:application/json'
          ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;
    }
    public function updatePriceAndInventory($supplierid,$product) {
      $ch = curl_init('https://api.trendyol.com/sapigw/suppliers/'.$supplierid.'/products/price-and-inventory');
      curl_setopt($ch,CURLOPT_POSTFIELDS,$product);
      curl_setopt($ch,CURLOPT_HTTPHEADER,array(
              'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
              'Content-Type:application/json'
          ));
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $result = curl_exec($ch);
      curl_close($ch);
      return $result;

    }
    public function getCategoryAttributes($platformCategoryCode=0) {
      $attributes = $this->getAttributes($platformCategoryCode);
       $attributes = $attributes["categoryAttributes"];
       $categoryAttributes = array();
       foreach ($attributes as $key => $attribute) {
         if($attribute["varianter"] == false && $attribute["slicer"] == false){
           $categoryAttributes["categoryAttributes"][] = $attribute;
         }
       }
       return $categoryAttributes;
     }
    public function getVariants($platformCategoryCode=0) {
      $attributes = $this->getAttributes($platformCategoryCode);
      $attributes = $attributes["categoryAttributes"];
      $variants = array();
      foreach ($attributes as $key => $attribute) {
        $attribute["attribute"]["allowCustom"] = $attribute["allowCustom"];
        $variants[] = $attribute["attribute"];
      }
      return $variants;
    }
    public function getVariantByPlatformCode($platformCategoryCode=0, $platformCode=0){
      $variants = $this->getVariants($platformCategoryCode);
      $returnedVariant=array();
      foreach ($variants as $key => $variant) {
        if($variant["id"] == $platformCode){
          $returnedVariant = $variant;
          break;
        }
      }
      return $returnedVariant;
    }
    public function getVariantsValues($platformCategoryCode=0,$platformVariantCode=0) {
       $attributes = $this->getAttributes($platformCategoryCode);
       $attributes = $attributes["categoryAttributes"];
       $variantValues = array();
       foreach ($attributes as $key => $attribute) {
         if($attribute['attribute']['id'] == $platformVariantCode){
            $variantValues = $attribute["attributeValues"];
            break;
          }
       }
       return $variantValues;
    }
    public function getLog($batchRequestId) {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,"https://api.trendyol.com/sapigw/suppliers/".$this->supplierId."/products/batch-requests/".$batchRequestId);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $headers = array(
        'Authorization:Basic ' . base64_encode($this->username . ':' . $this->password),
        'Content-Type:application/json',
        'User-Agent:'.$this->supplierId.' - SelfIntegration'
      );
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      $server_output = curl_exec ($ch);
      curl_close ($ch);
      return json_decode($server_output,true);
    }
    public function getBrands($size=15,$page=1) {
      $data = $this->guzzle->get("https://api.trendyol.com/sapigw/brands?size=".$size."&page=".$page);
      $brands = json_decode($data->getBody(),true);
      return $brands;
    }
    public function getSearchBrands($size=15,$page=1,$name="") {
      $ch = curl_init("https://api.trendyol.com/sapigw/brands/by-name?name=$name");
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
      curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
      $brands = curl_exec($ch);
      curl_close($ch);
      return $brands;
    }
    public function getCargoProviders() {
      $data = $this->guzzle->get("https://api.trendyol.com/sapigw/shipment-providers");
      $brands = json_decode($data->getBody(),true);
      return $brands;
    }
    public function getCategoriesList($subCategories=array()) {
      if (count($subCategories)<=0) {
        $ch = curl_init('https://api.trendyol.com/sapigw/product-categories');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $data = curl_exec($ch);
        curl_close($ch);
        $categories = json_decode($data,true);
        $subCategories= $categories['categories'];
      }
      foreach ($subCategories as $category) {
        $categoryList[$category['id']] = $category['name'];
        if (count($category['subCategories'])>0) {
          foreach ($category['subCategories'] as $category) {
            $categoryList[$category['id']] = $category['name'];
            if (count($category['subCategories'])>0) {
              foreach ($category['subCategories'] as $category) {
                $categoryList[$category['id']] = $category['name'];
                if (count($category['subCategories'])>0) {
                  foreach ($category['subCategories'] as $category) {
                    $categoryList[$category['id']] = $category['name'];
                    if (count($category['subCategories'])>0) {
                      foreach ($category['subCategories'] as $category) {
                        $categoryList[$category['id']] = $category['name'];
                        if (count($category['subCategories'])>0) {
                          foreach ($category['subCategories'] as $category) {
                            $categoryList[$category['id']] = $category['name'];
                            if (count($category['subCategories'])>0) {
                              foreach ($category['subCategories'] as $category) {
                                $categoryList[$category['id']] = $category['name'];
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      return $categoryList;
    }
  }
?>
