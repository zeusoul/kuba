<?php

  if(!isset($header["api_key"]) || !isset($header["api_secret"]) || !isset($header["supplier_id"])){
    response(array(
      "error" => "API",
      "errorMessage" => "API Bilgileri eksik."
    ));
    exit;
  }
  $trendyolAPI = new Trendyol($header['api_key'],$header['api_secret'],$header['supplier_id']);
  $orders = $trendyolAPI->getOrders(1,"");

  // Siparişler
  foreach ($orders['content'] as $order) {
    if ($order["shipmentAddress"]["fullName"][0] == "*") {
      continue;
    }
    $orderDate = '';
    foreach ($order['packageHistories'] as $value) {
      if ($value['status'] == 'Created') {
        date_default_timezone_set('Europe/Istanbul');
        $orderDate = date("Y-m-d H:i:s", $value['createdDate']/1000);
      }
    }
      $totalCount = 0;
      $linesToSend = array();
      foreach ($order['lines'] as $key => $line) {
          $totalCount += (int)$line['quantity'];
          // stok takibi
          $productVariantsObj = new ProductVariants();
          $productVariantsObj->setBarcode($line['barcode']);
          $productVariantsObj->reduceStock((int)$line["quantity"]);
          // statüler için line bilgisi
          $linesToSend .='{
              "lineId": '.$line['id'].',
              "quantity": '.$line['quantity'].'
          },';
      }// end line
      $linesToSend = rtrim($linesToSend,',');

      // kargo entegrasyonu yok
      $updateOrderParams ='{
          "lines": ['.$linesToSend.'],
          "params": {},
          "status": "Picking"
      }';
      $result = $trendyolAPI->updatePackage($data['key3'], $updateOrderParams, $order["id"]);
  }
  // END:Siparişler

  exit;

?>
