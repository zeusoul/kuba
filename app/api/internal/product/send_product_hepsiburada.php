<?php

  $platformId = 5;
  require('./app/api/system/helper.php');

  $hepsiburadaArr = array();

  foreach ($variants as $key => $variant) {
    $attributes=array();
    foreach ($variant["variant_codes"] as $variantCode => $valueCode) {
      if (isset($valueCode["platform_id"]) && $valueCode['platform_id']==$platformId) {
        $attributes[]=array(
          "attributeId" => (string)$variantCode,
          "attributeValueId" => $valueCode['platform_code']
        );
      }
    }


    $titleToSend="";
    foreach ($titles as $key => $title) {
      if($title["is_main"] == 1)$titleToSend=$title["product_name"];
    }
    if(trim($titleToSend)=="") {
      $hepsiburadaArr['BOŞ'] = "BOŞ";
    }
    else{
      $descriptionToSend ="";
      if(count($descriptions) == 0) {
        response(["error" => "Açıklama yok"]);
        exit;
      }
      foreach ($descriptions as $key => $description) {
        if($description["is_main"] == 1)$descriptionToSend=$description["product_description"];
      }
      //ürün açıklaması kotrolü yap boş ? : ;
      $imagesToSend = array();
      foreach ($images as $key => $image) {
        if($key == 5)break;
        $imagesToSend[] = $image["picture_url"];
      }

      //kategori kontrolü yap eşleşmiş mi?
      $hepsiburadaArr['categoryId']= $platformCategories[0]["platform_code"];
      $hepsiburadaArr['merchant']= $companyPlatform['key3'];
      $hepsiburadaArr['attributes'] = array(
        "merchantSku" => $product["product_code"],
        "VaryantGroupID" => $product["product_code"],
        "Barcode" => $variant["barcode"],
        "UrunAdi" => $titleToSend,
        "UrunAciklamasi" => $descriptionToSend,
        "Marka" => $product["brand_name"],
        "tax_vat_rate" => (int)$product["kdv"],
        "price" => $variant["price"],
        "stock" => $variant["stock"],
        "Image1" => (isset($imagesToSend[0])) ? $imagesToSend[0] : "",
        "Image2" => (isset($imagesToSend[1])) ? $imagesToSend[1] : "",
        "Image3" => (isset($imagesToSend[2])) ? $imagesToSend[2] : "",
        "Image4" => (isset($imagesToSend[3])) ? $imagesToSend[3] : "",
        "Image5" => (isset($imagesToSend[4])) ? $imagesToSend[4] : "",
      );

      foreach ($productAttributes as $key => $attribute) {
        $hepsiburadaArr['attributes'][$attribute["attribute_code"]] = $attribute["attribute_value_code"];
      }
      foreach ($attributes as $key => $attribute) {
        $hepsiburadaArr['attributes'][$attribute["attributeId"]] = $attribute["attributeValueId"];
      }


    }
    $items[] = $hepsiburadaArr;
  }

  $productJson = json_encode($hepsiburadaArr);

  $file = fopen(publicPath('hb-json/product.json'),'w+') or die("File not found");
  fwrite($file, $productJson);
  fclose($file);

  $hepsiburadaObj = new Hepsiburada($companyPlatform['key1'],$companyPlatform['key2'],$companyPlatform['key3']);
  $result=$hepsiburadaObj->addProducts(publicPath('hb-json/product.json'));
  response($result);
  /*
  $test = $hepsiburadaObj->getBarer();
  response($test);
  exit;
  $log = $hepsiburadaObj->getLog($resultArr['data']['trackingId']);
  response($log);
  exit;*/
  $resultArr = json_decode($result,true);
  if (isset($resultArr['data']['trackingId'])) {
    $log = $hepsiburadaObj->getLog($resultArr['data']['trackingId']);
    response($log);
    $platformProductObj = new PlatformProducts();
    $platformProductObj->setPlatformId($platformId);
    $platformProductObj->setProductId($productId);
    $platformProductObj->setJobCode(1);
    $platformProductObj->setLogMessage($log);
    $platformProductObj->updateLogMessageProduct();
    /*$companyObj = new Company();
    $companyObj->setCompanyId($companyId);
    $users = $companyObj->getCompanyUsers();
    $notificationObj = new Notifications();
    $logArr = json_decode($log,true);
    foreach ($users as $user) {
      $notificationObj->setUserId($user['user_id']);
      $message = $logArr['items']['status'] =="SUCCESS" ? "ÜRÜN BAŞARIYLA EKLENDİ":"ÜRÜN EKLENİRKEN HATA OLUŞTU";
      $notificationObj->setContent($message);
      $notificationObj->setStatus(0);
    }*/
  }else {
      $platformProductObj = new PlatformProducts();
      $platformProductObj->setPlatformId($platformId);
      $platformProductObj->setProductId($productId);
      $platformProductObj->setJobCode(1);
      $platformProductObj->setLogMessage($resultArr);
      $platformProductObj->updateLogMessageProduct();
  }



/*
response([
    'result' =>$productId,
    'companyId' => $companyId,
    'product' => $product,
    'variants' =>$variants,
    'title' =>$title,
    'platformCategories'=>$platformCategories
    ]);*/
