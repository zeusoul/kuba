<?php
  require("./app/api/internal/helpers/helper.product.php");
  $productObj = new Product();
  $productObj->setProductId($body["productId"]);
  $productDetail = $productObj->getProductDetails();

  $platformCategoryObj = new PlatformCategory();
  $platformCategoryObj->setPlatformId(2);
  $platformCategoryObj->setCategoryId($productDetail["category_id"]);
  $platformCategory = $platformCategoryObj->getPlatformCategoryByCategoryIdAndPlatformId();

  if(!is_array($platformCategory) || count($platformCategory) == 0){
    response(array(
      "error" => "failed",
      "errorMessage" => "Lütfen kategori eşleştiriniz"
    ));
    exit;
  }
  else{
    $mainPrice = 0.00;
    $mainDiscountPrice = 0.00;
    $currencyType = "1";
    foreach ($productDetail["variants"] as $key => $variant) {
      if((float)$variant["discount_price"] > $mainPrice){
        $mainPrice = (float)$variant["price"];
        $mainDiscountPrice = (float)$variant["discount_price"];
        if($variant["currency"] == 'USD') {
          $currencyType = '2';
        }
        else if($variant["currency"] == 'EUR') {
          $currencyType = '3';
        }
      }
    }

    $platformProductAttribute = new PlatformProductAttribute();
    $platformProductAttribute->setProductId($productDetail["product_id"]);
    $platformProductAttribute->setPlatformId(2);
    $attributes = $platformProductAttribute->getPlatformProductAttributesByProductIdAndPlatformId();
    $attributesN11 = array();
    foreach ($attributes as $key => $attribute) {
      $attributesN11[] = array(
        "name" => $attribute["attribute"],
        "value" => $attribute["attribute_value"]
      );
    }

    $productVariants = array();
    $variants = $productDetail["variants"];
    if(count($variants) > 0 && strlen($variants[0]["json_combine"]) > 0){
      foreach ($variants as $key => $variant) {
        $productVariantObj = new ProductVariant();
        $productVariantObj->setProductVariantId($variant["product_variant_id"]);
        $variantValues = $productVariantObj->getProductVariantNamesAndValuesById();

        if($discount <= 0){
          $optionPrice = $variant['price'];
        }
        else{
          $optionPrice = $variant['discount_price'] + $discount;
        }
        $productVariants[] = array(
          "bundle" => "",
          "mpn"=>"",
          "gtin"=>"",
          "oem" => "",
          "n11CatalogId" => "",
          "quantity" => $variant["stock"],
          "sellerStockCode" => $variant["stock_code"],
          "attributes" => array(
            "comment" => "",
            "attribute" => $variantValues
          ),
          "optionPrice"=> $optionPrice
        );
      }
    }

    $product = array(
      "productSellerCode" => $productDetail["product_code"],
      "title" => $productDetail["title"],
      "subtitle" => $productDetail["sub_title"],
      "description" => $productDetail["content"],
      "domestic" => "",
      "category" => array(
        "id" => $platformCategory["platform_category_code"]
      ),
      "price" => $mainPrice,
      "currencyType" => $currencyType,
      "images" => array(
        "comment" => "",
        "image" => array(
          "order" => "1",
          "url" => publicUrl("img/product-images/".$productDetail["image"])
        )
      ),
      "approvalStatus" => 1,
      "attributes" => array(
        "comment" => "",
        "attribute" => $attributesN11
      ),
      "saleStartDate" => "",
      "saleEndDate" => "",
      "productionDate" => "",
      "expirationDate" => "",
      "unitInfo" => "",
      "maxPurchaseQuantity" => "",
      "saleStartDate" => "",
      "saleEndDate" => "",
      "productionDate" => "",
      "expirationDate" => "",
      "productCondition" => "",
      "preparingDay" => "",
      "shipmentTemplate" => "",//CARGO TEMPLATE DEN AL
      "groupAttribute" => "",
      "groupItemCode" => "",
      "itemName" => "",
      "stockItems" => $productVariants
    );
    if($mainPrice > $mainDiscountPrice) {
      $discount = (float)$mainPrice - (float)$mainDiscountPrice;
      $product["discount"] = array(
        "startDate" => "",
        "endDate" => "",
        "type" => "1",
        "value" => $discount
      );
    }

    $platformInfoObj = new PlatformInformation();
    $platformInfoObj->setPlatformId(2);
    $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
    if(!is_array($platformInfo) || count($platformInfo) == 0){
      response(array(
        "error" => "failed",
        "errorMessage" => "Lütfen N11 Platformuna Ait API Bilgilerini Giriniz"
      ));
      exit;
    }
    else{
      $n11Obj = new N11(
        array(
          'appKey'=>$platformInfo['key1'],
          'appSecret'=>$platformInfo['key2']
        )
      );
      /*$result = $n11Obj->SaveProduct($items);
      $result = json_decode(json_encode($result),true);
      response($result);*/
      response(array("end"));
      exit;
    /*  if (isset($resultArr["result"]['status'])) {
        $platformProductObj = new PlatformProducts();
        $platformProductObj->setPlatformId($platformId);
        $platformProductObj->setProductId($productId);
        $platformProductObj->setJobCode(1);
        $platformProductObj->setLogMessage("Ürün n11 platformuna eklendi.");
        $platformProductObj->updateLogMessageProduct();

        $platformProductCodeObj = new PlatformProductCodes();
        $platformProductCodeObj->setCompanyId($companyId);
        $platformProductCodeObj->setProductId($product['product_id']);
        $platformProductCodeObj->setPlatformProductCode($resultArr['product']['id']);
        $platformProductCodeObj->setPlatformId($platformId);
        $result = $platformProductCodeObj->insertPlatformProductCode();
      }
      else {
        response($result);
        $resultArr = json_decode($result,true);
        $platformProductObj = new PlatformProducts();
        $platformProductObj->setPlatformId($platformId);
        $platformProductObj->setProductId($productId);
        $platformProductObj->setJobCode(1);
        $platformProductObj->setLogMessage($resultArr['result']['errorMessage']);
        $platformProductObj->updateLogMessageProduct();
      }*/
    }
  }

?>
