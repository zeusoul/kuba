<?php

  require("./app/api/internal/helpers/helper.product.php");
  $productObj = new Product();
  $productObj->setProductId($body["productId"]);
  $productDetail = $productObj->getProductDetails();

  $platformCategoryObj = new PlatformCategory();
  $platformCategoryObj->setPlatformId(1);
  $platformCategoryObj->setCategoryId($productDetail["category_id"]);
  $platformCategory = $platformCategoryObj->getPlatformCategoryByCategoryIdAndPlatformId();

  if(!is_array($platformCategory) || count($platformCategory) == 0){
    response(array(
      "state" => "failed",
      "message" => "Lütfen kategori eşleştiriniz"
    ));
    exit;
  }
  else{
    $platformBrandObj = new PlatformBrand();
    $platformBrandObj->setPlatformId(1);
    $platformBrandObj->setBrandId($productDetail["brand_id"]);
    $platformBrand = $platformBrandObj->getPlatformBrandByPlatformIdAndBrandId();
    if(!is_array($platformBrand) || count($platformBrand) == 0){
      response(array(
        "state" => "failed",
        "message" => "Lütfen marka eşleştiriniz"
      ));
      exit;
    }
    else{
      $product = array(
        "items" => array()
      );
      $productVariants = $productDetail["variants"];

      $i=0;
      foreach ($productVariants as $key => $productVariant){
        $product["items"][$i]["barcode"] = $productVariant["barcode"];
        $product["items"][$i]["title"] = $productDetail["title"];
        $product["items"][$i]["productMainId"] = $productDetail["product_id"];
        $product["items"][$i]["brandId"] = $platformBrand["platform_brand_code"];
        $product["items"][$i]["categoryId"] = $platformCategory["platform_category_code"];
        $product["items"][$i]["quantity"] = $productVariant["stock"];
        $product["items"][$i]["stockCode"] = $productVariant["stock_code"];
        $product["items"][$i]["description"] = $productDetail["content"];
        $currencyType = "";
        if($productVariant["currency"] == "TL") $currencyType = "TRY";
        else $currencyType = $productVariant["currency"];
        $product["items"][$i]["currencyType"] = $currencyType;
        $product["items"][$i]["listPrice"] = $productVariant["price"];
        $product["items"][$i]["salePrice"] = $productVariant["discount_price"];
        $product["items"][$i]["vatRate"] = 8;
        $product["items"][$i]["dimensionalWeight"] = 1;
        $product["items"][$i]["cargoCompanyId"] = 10;
        $product["items"][$i]["images"][] = array(
          "url" => publicUrl("img/product-images/".$productDetail["image"])
        );


        $jsonCombine = $productVariant["json_combine"];
        $combineArray = json_decode($jsonCombine, true);
        $combine = $productVariant["combine"];
        foreach ($combineArray as $key => $value) {
          $variantId = $value["variantId"];
          $platformVariantObj = new PlatformVariant();
          $platformVariantObj->setVariantId($variantId);
          $platformVariantObj->setPlatformId(1);
          $platformVariant = $platformVariantObj->getPlatformVariantByVariantIdAndPlatformId();
          if(!is_array($platformVariant) || count($platformVariant) == 0){
            response(array(
              "state" => "failed",
              "message" => "Lütfen varyantı eşleştiriniz $variantId"
            ));
            exit;
          }
          else{
            $variantValueId = $value["valueId"];
            $platformVariantValueObj = new PlatformVariantValue();
            $platformVariantValueObj->setVariantValueId($variantValueId);
            $platformVariantValueObj->setPlatformId(1);
            $platformVariantValue = $platformVariantValueObj->getPlatformVariantValueByVariantValueIdAndPlatformId();
            if(!is_array($platformVariantValue) || count($platformVariantValue) == 0){
              response(array(
                "state" => "failed",
                "message" => "Lütfen varyant değerini eşleştiriniz"
              ));
              exit;
            }
            else{
              if((int)$platformVariantValue["platform_variant_value_code"] <= 0){
                $product["items"][$i]["attributes"][] = array(
                  "attributeId" => $platformVariant["platform_variant_code"],
                  "customAttributeValue" => $platformVariantValue["platform_variant_value_code"]
                );
              }
              else{
                $product["items"][$i]["attributes"][] = array(
                  "attributeId" => $platformVariant["platform_variant_code"],
                  "attributeValueId" => $platformVariantValue["platform_variant_value_code"]
                );
              }
            }
          }
        }
        $platformProductAttribute = new PlatformProductAttribute();
        $platformProductAttribute->setProductId($productDetail["product_id"]);
        $platformProductAttribute->setPlatformId(1);
        $attributes = $platformProductAttribute->getPlatformProductAttributesByProductIdAndPlatformId();
        foreach ($attributes as $key => $attribute) {
          if((int)$attribute["platform_attribute_value_code"] <= 0){
            $product["items"][$i]["attributes"][] = array(
              "attributeId" => $attribute["platform_attribute_code"],
              "customAttributeValue" => $attribute["platform_attribute_value_code"]
            );
          }
          else{
            $product["items"][$i]["attributes"][] = array(
              "attributeId" => $attribute["platform_attribute_code"],
              "attributeValueId" => $attribute["platform_attribute_value_code"]
            );
          }
        }
        $i++;
      }

      $platformInfoObj = new PlatformInformation();
      $platformInfoObj->setPlatformId(1);
      $platformInfo = $platformInfoObj->getPlatformInformationByPlatformId();
      if(!is_array($platformInfo) || count($platformInfo) == 0){
        response(array(
          "state" => "failed",
          "message" => "Lütfen Trendyol Platformuna Ait API Bilgilerini Giriniz"
        ));
        exit;
      }
      else{
        $trendyol = new Trendyol($platformInfo["key1"], $platformInfo["key2"], $platformInfo["key3"]);
        $result = $trendyol->addProducts($platformInfo["key3"], json_encode($product, JSON_UNESCAPED_UNICODE));
        if(!is_array($result)){
          $result = json_decode($result, true);
        }
        if(!is_array($result)){
           $result = json_decode($result, true);
        }
        if($result["batchRequestId"]){
          $resultLog = $trendyol->getLog($result["batchRequestId"]);
          if(!is_array($resultLog)) {
            $resultLog = json_decode($resultLog, true);
          }
          if($resultLog["status"] == "COMPLETE"){
            foreach ($resultLog["items"] as $key => $item) {
              if($item["status"] == "FAILED"){
                response(array(
                  "state" => "failed",
                  "message" => $item["failureReasons"]
                ));
                exit;
              }
              else{
                response(array(
                  "state" => "success",
                  "message" => "SUCCESS"
                ));
                exit;
              }
            }
          }
          else{
            response(array(
              "state" => "success",
              "message" => $resultLog["status"]
            ));
            exit;
          }
          exit;
        }
        else{
          if(isset($result["errors"]) && count($result["errors"]) > 0){
            $errors = "";
            foreach ($result["errors"] as $key => $error) {
              $errors .= $error["message"]."<br>";
            }
            response(array(
              "state" => "failed",
              "message" => $errors
            ));
            exit;
          }
          else{
            response(array(
              "state" => "failed",
              "message" => "İstisna oluştu"
            ));
            exit;
          }
        }
        exit;
      }
      exit;
    }
  }
  exit;
?>
