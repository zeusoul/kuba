<?php
  if(isset($body) && isset($header)){
    if(isset($body["productId"])){
      $product = new Product();
      $product->setProductId($body["productId"]);
      if(!$product->isProduct()){
        response(array(
          "error" => "productNotFound",
          "errorMessage" => "Ürün bulunamadı"
        ));
        exit;
      }
      else{
        $productDetail = $product->getProductDetails();
        if(!is_array($productDetail["variants"]) || count($productDetail["variants"]) == 0){
          response(array(
            "error" => "productVariantNotFound",
            "errorMessage" => "Ürüne ait herhangi bir barkod bulunamadı"
          ));
          exit;
        }
      }
    }
    else{
      response(array(
        "error" => "productIdNotFound",
        "errorMessage" => "productId Bilgisi eksik"
      ));
      exit;
    }
  }
  else{
    response(array(
      "error" => "headerAndBody",
      "errorMessage" => "Header ve body bilgisi eksik"
    ));
    exit;
  }

?>
