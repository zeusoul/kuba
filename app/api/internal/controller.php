<?php
  header('Content-Type: application/json');
  $body = json_decode(file_get_contents('php://input'), true);
  $header = getallheaders();

  if(!isset($header["username"])){
    response(array(
      "error" => "usernameNotFound",
      "errorMessage" => "Kullanıcı adı eksik"
    ));
    exit;
  }
  else if(!isset($header["password"])){
    response(array(
      "error" => "passwordNotFound",
      "errorMessage" => "Şifre eksik"
    ));
    exit;
  }
  else{
    $admin = new Admin();
    $admin->setUserName($header["username"]);
    $admin->setPass($header["password"]);
    if(!$admin->isAdmin()){
      response(array(
        "error" => "usernameOrPassword",
        "errorMessage" => "Kullanıcı adı veya şifre hatalı"
      ));
      exit;
    }
  }

?>
