<?php
  /**
   * Mail sınıfı, Mail ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Mail sınıfı,
   * Mail göndermeye yarar.
   *
   * Example usage:
   * if (Mail::sendMail("example@exapmle.com","Name","Surname","Subject","Content")) {
   *   print "E Mail sent";
   * }
   *
   * @package Mail
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Mail{
    /**
     * E Mail gönderir
     *
     * @access public
     * @return boolean
     * @param string $email
     * @param string $name
     * @param string $surname
     * @param string $subject
     * @param string $content
     */
    function sendMail($email,$name,$surname,$subject,$content){
      $tempContent = $content;
      $content = "
        <div style='width:100%; height:100px;'>
          <a target='_blank' href='".URL."'>
            <img src='".publicUrl("img/mail-images/mail_header.png")."' style='height:100px;'/>
          </a>
        </div>
        <br/>
        <br/>
        <br/>
        ";
      $content .= "
        <p>
          $tempContent
        </p>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
      ";
      $content .= "
        Bu e-mail, herhangi bir yasal kanıt olarak sunulamaz veya herhangi bir mali değeri bulunmamaktadır.<br/>
        İyi Çalışmalar,<br/>
        İstanbul Elektromatik Mak. Paz. San. Tic. Ltd. Şti.<br/>
        www.istanbulelektromatik.com";

        $content .= "
          <br/>
          <br/>
          <div style='width:100%; height:200px;'>
            <a target='_blank' href='".URL."'>
              <img src='".publicUrl("img/mail-images/mail_footer.png")."' style='height:200px;'/>
            </a>
          </div>
          <br/>
          ";

      $mail = new PHPMailer();
      $mail->IsSMTP();
      $mail->SMTPDebug = 1;
      $mail->SMTPSecure = 'ssl';
      $mail->Host = 'smtp.gmail.com';
      $mail->Port = 465;
      $mail->SMTPAuth = true;
      $mail->IsHTML(true);
      $mail->SetLanguage("tr", "phpmailer/language");
      $mail->CharSet = 'UTF-8';
      $mail->Username = 'phpilemailgonderme@gmail.com';
      $mail->Password = '159753er';
      $mail->SetFrom($mail->Username, 'istanbulelektromatik');
      $mail->AddAddress($email, $name.' '.$surname);
      $mail->Subject = $subject;
      $mail->MsgHTML($content);
      return $mail->Send();
    }
  }

?>
