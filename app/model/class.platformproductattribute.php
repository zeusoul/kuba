<?php

  class PlatformProductAttribute extends Product{
    protected $platformProductAttributeId, $platformAttributeCode, $platformAttributeValueCode, $attribute, $attributeValue;
    public function setPlatformProductAttributeId($platformProductAttributeId){
      $this->platformProductAttributeId = $platformProductAttributeId;
    }
    public function setPlatformAttributeCode($platformAttributeCode){
      $this->platformAttributeCode = $platformAttributeCode;
    }
    public function setPlatformAttributeValueCode($platformAttributeValueCode){
      $this->platformAttributeValueCode = $platformAttributeValueCode;
    }
    public function setAttribute($attribute){
      $this->attribute = $attribute;
    }
    public function setAttributeValue($attributeValue){
      $this->attributeValue = $attributeValue;
    }

    public function insertPlatformProductAttribute(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "INSERT INTO platform_product_attributes
                SET product_id=$this->productId,
                    platform_id=$this->platformId,
                    platform_attribute_code='$this->platformAttributeCode',
                    platform_attribute_value_code='$this->platformAttributeValueCode',
                    attribute='$this->attribute',
                    attribute_value='$this->attributeValue'";
        $query = mysqli_query($connection, $sql);
        return $query;
      }
    }
    public function deletePlatformAttributesByProductIdAndPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_product_attributes
                WHERE product_id=$this->productId
                AND platform_id=$this->platformId";
        $query = mysqli_query($connection, $sql);
        return $query;
      }
    }
    public function deletePlatformProductAttributesByProductId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_product_attributes
                WHERE product_id=$this->productId";
        $query = mysqli_query($connection, $sql);
        return $query;
      }
    }

    public function getPlatformProductAttributesByProductIdAndPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM platform_product_attributes
                WHERE product_id=$this->productId
                AND platform_id=$this->platformId";
        $query = mysqli_query($connection,$sql);
        $attributes = array();
        while($read = mysqli_fetch_array($query, MYSQLI_ASSOC)){
          $attributes[] = $read;
        }
        return $attributes;
      }
    }
  }

?>
