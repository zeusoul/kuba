<?php 
class Documenties{
  /**
   * Web sitesi içine pdf dökümanı
   * yüklemeye yarar
   */
  protected $documentId;
  //döküman Url linkini barındıracak değer
  protected $documentUrl;
  //döküman adını sahiplenecek değer
  protected $documentName;
  public function setDocumentId($documentId){
    $this->documentId = (int)$documentId;
  }
  public function setDocumentUrl($documentUrl){
    $this->documentUrl = trim(strip_tags($documentUrl));
  }
  public function setDocumentName($documentName){
    $this->documentName = trim(strip_tags($documentName));
  }
  public function __construct() {
    $this->documentId     = 0;
    $this->documentUrl  = "";
    $this->documentName = "";
  }
  //döküman ekle
  public function insertDocument(){
    $db = new Database();
    $connection = $db->MySqlConnection();
    if(!$connection) return false;
    else{
      $documentUrl = $this->documentUrl;
      $documentName = $this->documentName;

      $insertSql = "INSERT INTO documents
                    VALUES('','$documentUrl','$documentName')";
      $insertQuery = mysqli_query($connection,$insertSql);
      return $insertQuery;
    }
  }
  public function deleteDocument(){
    $db = new Database();
    $connection = $db->MySqlConnection();
    if(!$connection) return false;
    else{
      $documentId = $this->documentId;
      $sql = "DELETE FROM documents
              WHERE document_id=$documentId";
      $query = mysqli_query($connection,$sql);
      return $query;
    }
  }
  public function getDocument(){
    $db = new Database();
    $connection = $db->MySqlConnection();
    if(!$connection) return array();
    else{
      $documentId = $this->documentId;
      $sql = "SELECT * FROM documents ORDER BY 
              document_id=$documentId DESC LIMIT 1";
      $query = mysqli_query($connection,$sql);
      if($query->num_rows <= 0) return array();
      else{
        $documents = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $documents[] = $read;
        }
        return $documents;
      }
    }
  }
}
?>