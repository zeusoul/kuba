<?php
  /**
   * ProductInformation sınıfı, Ürün bilgileriyle ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * ProductInformation sınıfı,
   * Yeni ürün bilgisi kaydetmeye,
   * Kayıtlı ürün bilgisini güncellemeye, silmeye
   * ve ürün bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (ProductInformation::insertProductInfo() === "Success") {
   *   print "Product info Inserted";
   * }
   *
   * @package ProductInformation
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class ProductInformation extends Product{
    /**
     * Ürün bilgisi id'si
     *
     * @var int
     * @access protected
     */
    protected $infoId;
    /**
     * Ürün bilgisi
     *
     * @var string
     * @access protected
     */
    protected $info;
    /**
     * Ürün bilgisinin değeri (karşılığı)
     *
     * @var string
     * @access protected
     */
    protected $value;
    /**
     * Set the $infoId var
     *
     * @access public
     * @param int $infoId
     */
    public function setInfoId($infoId){
      $this->infoId = (int)$infoId;
    }
    /**
     * Set the $info var
     *
     * @access public
     * @param string $info
     */
    public function setInfo($info){
      $this->info = trim(strip_tags($info));
    }
    /**
     * Set the $value var
     *
     * @access public
     * @param string $value
     */
    public function setValue($value){
      $this->value = trim(strip_tags($value));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductInformation
     */
    public function __construct() {
      $this->infoId = 0;
      $this->info   = "";
      $this->value  = "";
    }

    /**
     * Ürün bilgisi ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertProductInfo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $info = mysqli_real_escape_string($connection,$this->info);
        $value = mysqli_real_escape_string($connection,$this->value);
        $productId = (int)$this->productId;

        $sql = "SELECT * FROM product_informations
                WHERE product_id=$productId
                AND info='$info'
                AND value='$value'";
        $query = mysqli_query($connection,$sql);

        if($query->num_rows > 0) return "Bu Ürüne Ait Bu Bilgi Zaten Mevcut";
        else if(trim($info) != "" && trim($value) != "" && $productId <= 0){
          return "Lütfen Boş Değer Girmeyiniz";
        }
        else{
          $insertSql = "INSERT INTO product_informations
                        VALUES('',$productId,'$info','$value')";
          $insertQuery = mysqli_query($connection,$insertSql);
          return ($insertQuery) ? "Success" : "Bilgi Eklenemedi : HATA!";
        }
      }
    }
    /**
     * Kayıtlı ürün bilgisini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductInfo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $infoId = (int)$this->infoId;
        $sql = "DELETE FROM product_informations
                WHERE product_information_id=$infoId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Ürüne ait tüm bilgileri siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductInformations(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return false;
      else{
        $productId = (int)$this->productId;
        $sql = "DELETE FROM product_informations
                WHERE product_id=$productId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Ürün bilgisini günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateProductInfo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $productId = (int)$this->productId;
        $infoId = (int)$this->infoId;
        $info = mysqli_real_escape_string($connection,$this->info);
        $value = mysqli_real_escape_string($connection,$this->value);
        $getSql = "SELECT * FROM product_informations
                   WHERE product_id=$productId
                   AND info='$info'
                   AND value='$value'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu Ürüne Ait Bu Bilgi Ve Değer Zaten Mevcut";
        else {
          $updateSql = "UPDATE product_informations
                        SET info='$info', value='$value'
                        WHERE product_information_id=$infoId";
          $updateQuery = mysqli_query($connection,$updateSql);
          return ($updateQuery) ? "Success" : "Güncelleme işlemi başarısız";
        }
      }
    }
    /**
     * Belirtilen ürünün bilgilerini döndürür
     *
     * @access public
     * @return array product's informations
     */
    public function getProductInformations(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return array();
      else{
        if(!is_int($this->productId) || (int)$this->productId == 0) return array();
        else{
          $productId = (int)$this->productId;
          $sql = "SELECT * FROM product_informations
                  WHERE product_id=$productId";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows <= 0) return array();
          else{
            $informations = array();
            while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
              $informations[] = $read;
            }
            return $informations;
          }
        }
      }
    }

  }

?>
