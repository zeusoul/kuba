<?php
  /**
   * Product sınıfı, Ürünler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Product sınıfı,
   * Yeni ürün kaydetmeye,
   * Kayıtlı ürünü güncellemeye, silmeye
   * ve ürün bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Product::insertPage() === "Success") {
   *   print "Product Inserted";
   * }
   *
   * @package Product
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class ProductVariant extends Product{
    /**
     * Ürün Varyant ID'si
     *
     * @var int
     * @access protected
     */
    protected $productVariantId;
    /**
     * Varyant kombin json
     *
     * @var string
     * @access protected
     */
    protected $JSONcombine;
    /**
     * ürün barkodu
     *
     * @var string
     * @access protected
     */
    protected $barcode;
    /**
     * ürün stok kodu
     *
     * @var string
     * @access protected
     */
    protected $stockCode;
    /**
     * Ürünün fiyatı
     *
     * @var float
     * @access protected
     */
    protected $price;
    /**
     * Ürünün stok sayısı
     *
     * @var int
     * @access protected
     */
    protected $stock;
    /**
     * Ürün fiyatının para birimi (Tl, euro, usd)
     *
     * @var string
     * @access protected
     */
    protected $currency;
    /**
     * Ürünün indirimli fiyatı
     *
     * @var float
     * @access protected
     */
    protected $discountPrice;

    /**
     * Set the $productVariantId var
     *
     * @access public
     * @param int $productVariantId
     */
    public function setProductVariantId($productVariantId){
      $this->productVariantId = (int)$productVariantId;
    }
    /**
     * Set the $barcode var
     *
     * @access public
     * @param string $barcode
     */
    public function setBarcode($barcode){
      $this->barcode = trim(strip_tags($barcode));
    }
    /**
     * Set the $stockCode var
     *
     * @access public
     * @param string $stockCode
     */
    public function setStockCode($stockCode){
      $this->stockCode = trim(strip_tags($stockCode));
    }
    /**
     * Set the $JSONcombine var
     *
     * @access public
     * @param string $JSONcombine
     */
    public function setJSONcombine($JSONcombine){
      $this->JSONcombine = $JSONcombine;
    }
    /**
     * Set the $price var
     *
     * @access public
     * @param float $price
     */
    public function setPrice($price){
      $this->price = (float)$price;
    }
    /**
     * Set the $stock var
     *
     * @access public
     * @param int $stock
     */
    public function setStock($stock){
      $this->stock = (int)$stock;
    }
    /**
     * Set the $currency var
     *
     * @access public
     * @param string $currency
     */
    public function setCurrency($currency){
      $this->currency = trim(strip_tags($currency));
    }
    /**
     * Set the $discountPrice var
     *
     * @access public
     * @param float $discountPrice
     */
    public function setDiscountPrice($discountPrice){
      $this->discountPrice = (float)$discountPrice;
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductVariant
     */
    public function __construct() {
      $this->price            = 0.00;
      $this->stock            = 0;
      $this->currency         = "";
      $this->discountPrice    = 0.00;
    }
    /**
     * Ürün varyantını kaydeder
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertProductVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı hatası";
      else{
        $productId = (int)$this->productId;
        $JSONcombine = $this->JSONcombine;
        $barcode = mysqli_real_escape_string($connection,$this->barcode);
        $stockCode = mysqli_real_escape_string($connection,$this->stockCode);
        $stock = (int)$this->stock;
        $price = (float)$this->price;
        $discountPrice = (float)$this->discountPrice;
        $currency = mysqli_real_escape_string($connection,$this->currency);

        $getSql = "SELECT * FROM product_variants
                   WHERE barcode='$barcode'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu barkoda ait zaten bir varyant var. Lütfen başka barkod giriniz.";
        else{
          $combine = "";
          $combineArray = json_decode($JSONcombine,true);
          foreach ($combineArray as $key => $value) {
            $valueId = (int)$value["valueId"];
            $variantValue = new VariantValue();
            $variantValue->setVariantValueId($valueId);
            $v_value = $variantValue->getVariantValueById();
            $combine .= $v_value["variant_value_name"]."/";
          }
          $combine = rtrim($combine,"/");
          $sql = "INSERT INTO product_variants
                  SET product_id=$this->productId, json_combine='$JSONcombine', combine='$combine', barcode='$barcode',
                  stock_code='$stockCode', stock=$stock, price=$price, discount_price=$discountPrice, currency='$currency'";
          $insert = mysqli_query($connection,$sql);
          return $insert ? "Success" : "Varyant Eklenemedi";
        }
      }
    }
    /**
     * Ürün varyantını günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateProductVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı hatası";
      else{
        $barcode = mysqli_real_escape_string($connection,$this->barcode);
        $stockCode = mysqli_real_escape_string($connection,$this->stockCode);
        $stock = (int)$this->stock;
        $price = (float)$this->price;
        $discountPrice = (float)$this->discountPrice;
        $currency = mysqli_real_escape_string($connection,$this->currency);

        $thisVariant = $this->getProductVariantById();

        $getSql = "SELECT * FROM product_variants
                   WHERE barcode='$barcode'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0 && $barcode != $thisVariant["barcode"]) return "Bu barkoda ait zaten bir varyant var. Lütfen başka barkod giriniz.";
        else{
          $sql = "UPDATE product_variants
                  SET barcode='$barcode', stock_code='$stockCode', stock=$stock, price=$price, discount_price=$discountPrice, currency='$currency'
                  WHERE product_variant_id=$this->productVariantId";
          $update = mysqli_query($connection,$sql);
          return $update ? "Success" : "Varyant Güncellenemedi";
        }
      }
    }
    /**
     * Ürün stoğunu parametrede gönderilen miktar kadar düşürür.
     *
     * @access public
     * @return boolean
     */
    public function reduceStock($quantity){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $barcode = mysqli_real_escape_string($connection,$this->barcode);
        $sql = "UPDATE product_variants
                SET stock = stock - $quantity
                WHERE barcode=$this->barcode";
        $update = mysqli_query($connection,$sql);
        return $update;
      }
    }
    /**
     * Ürün varyantını siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM product_variants
                WHERE product_variant_id=$this->productVariantId";
        $delete = mysqli_query($connection,$sql);
        return $delete;
      }
    }
    /**
     * Ürün ID'sine göre ürüne ait tüm varyantları siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductVariantsByProductId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM product_variants
                WHERE product_id=$this->productId";
        $delete = mysqli_query($connection,$sql);
        return $delete;
      }
    }
    /**
     * Ürüne ait varyantları döndürür
     *
     * @access public
     * @return array
     */
    public function getProductVariantsByProductId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM product_variants
                WHERE product_id=$this->productId";
        $query = mysqli_query($connection,$sql);
        $productVariants = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $price = (float)$read["price"];
          $discountPrice = (float)$read["discount_price"];
          $fark = $price-$discountPrice;
          $rate = ($price <= 0) ? 0 : (int)(($fark * 100) / $price);
          $read["rate"] = $rate;
          $productVariants[] = $read;
        }
        return $productVariants;
      }
    }
    /**
     * varyantın bilgilerini döndürür
     *
     * @access public
     * @return array
     */
    public function getProductVariantById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM product_variants
                WHERE product_variant_id=$this->productVariantId";
        $query = mysqli_query($connection,$sql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
    /**
     * varyantın bilgilerini döndürür
     *
     * @access public
     * @return array
     */
    public function getProductVariantByBarcode(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM product_variants
                INNER JOIN products
                ON product_variants.product_id=products.product_id
                WHERE product_variants.barcode='$this->barcode'";
        $query = mysqli_query($connection,$sql);

        $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
        return $read;
      }
    }
    public function getProductVariantNamesAndValuesById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM product_variants
                WHERE product_variant_id = $this->productVariantId";
        $query = mysqli_query($connection, $sql);
        $result = mysqli_fetch_array($query, MYSQLI_ASSOC);

        $variantsAndValues = array();
        $jsonCombine = json_decode($result["json_combine"], true);

        foreach ($jsonCombine as $combine) {
          $variantValueCodes = array();
          $variantId = $combine["variantId"];
          $valueId = $combine["valueId"];

          $variant = new Variant();
          $variant->setVariantId($variantId);
          $getVariant = $variant->getVariantById();

          $variantValue = new VariantValue();
          $variantValue->setVariantValueId($valueId);
          $getVariantValue = $variantValue->getVariantValueById();

          $variantsAndValues[] = array(
            "name"=>$getVariant["variant_name"],
            "value"=>$getVariantValue["variant_value_name"]
          );
        }

        return $variantsAndValues;
      }
    }
  }


?>
