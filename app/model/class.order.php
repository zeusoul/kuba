<?php
  /**
   * Order sınıfı, Siparişler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Order sınıfı,
   * Yeni sipariş kaydetmeye,
   * Kayıtlı siparişi güncellemeye, silmeye
   * ve gerekli sipariş bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Order::insertOrder() === "Success") {
   *   print "Order Inserted";
   * }
   *
   * @package Order
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Order extends ProductVariant{
    /**
     * Sipariş id'si
     *
     * @var int
     * @access protected
     */
    protected $orderId;
    /**
     * Sipariş numarası
     *
     * @var int
     * @access protected
     */
    protected $orderNo;
    /**
     * Teslimat adresi id'si
     *
     * @var int
     * @access protected
     */
    protected $teslimatAddressBookId;
    /**
     * Fatura adresi id'si
     *
     * @var int
     * @access protected
     */
    protected $faturaAddressBookId;
    /**
     * Ödeme tipi (0 = credit card, 1 = transfer/eft)
     *
     * @var int
     * @access protected
     */
    protected $paymentType;
    /**
     * Banka id'si
     *
     * @var int
     * @access protected
     */
    protected $bankId;
    /**
     * Siparişin durumu
     * (-1 = iptal edildi, 0 = onay bekliyor, 1 = onaylandı, 2 = kargoda, 3 = teslim edildi)
     *
     * @var int
     * @access protected
     */
    protected $status;
    /**
     * Kullanıcı id'si
     *
     * @var int
     * @access protected
     */
    protected $userId;
    /**
     * Sipariş tarihi
     *
     * @var string
     * @access protected
     */
    protected $orderDate;
    /**
     * Kargo şirketi ismi
     *
     * @var string
     * @access protected
     */
    protected $cargoCompany;
    /**
     * Kargo takip numarası
     *
     * @var string
     * @access protected
     */
    protected $cargoNo;
    /**
     * Set the $orderId var
     *
     * @access public
     * @param int $orderId
     */
    public function setOrderId($orderId){
      $this->orderId = (int)$orderId;
    }
    /**
     * Set the $orderNo var
     *
     * @access public
     * @param int $orderNo
     */
    public function setOrderNo($orderNo){
      $this->orderNo = (int)$orderNo;
    }
    /**
     * Set the $teslimatAddressBookId var
     *
     * @access public
     * @param int $teslimatAddressBookId
     */
    public function setTeslimatAddressBookId($teslimatAddressBookId){
      $this->teslimatAddressBookId = (int)$teslimatAddressBookId;
    }
    /**
     * Set the $faturaAddressBookId var
     *
     * @access public
     * @param int $faturaAddressBookId
     */
    public function setFaturaAddressBookId($faturaAddressBookId){
      $this->faturaAddressBookId = (int)$faturaAddressBookId;
    }
    /**
     * Set the $paymentType var
     *
     * @access public
     * @param int $paymentType
     */
    public function setPaymentType($paymentType){
      $this->paymentType = (int)$paymentType;
    }
    /**
     * Set the $bankId var
     *
     * @access public
     * @param int $bankId
     */
    public function setBankId($bankId){
      $this->bankId = (int)$bankId;
    }
    /**
     * Set the $status var
     *
     * @access public
     * @param int $status
     */
    public function setStatus($status){
      $this->status = (int)$status;
    }
    /**
     * Set the $userId var
     *
     * @access public
     * @param int $userId
     */
    public function setUserId($userId){
      $this->userId = (int)$userId;
    }
    /**
     * Set the $orderDate var
     *
     * @access public
     * @param string $orderDate
     */
    public function setOrderDate($orderDate){
      $this->orderDate = strip_tags(trim($orderDate));
    }
    /**
     * Set the $cargoCompany var
     *
     * @access public
     * @param string $cargoCompany
     */
    public function setCargoCompany($cargoCompany){
      $this->cargoCompany = strip_tags(trim($cargoCompany));
    }
    /**
     * Get the $cargoCompany var
     *
     * @access public
     * @return string $cargoCompany
     */
    public function getCargoCompany(){
      return $this->cargoCompany;
    }
    /**
     * Set the $cargoNo var
     *
     * @access public
     * @param string $cargoNo
     */
    public function setCargoNo($cargoNo){
      $this->cargoNo = strip_tags(trim($cargoNo));
    }
    /**
     * Get the $cargoNo var
     *
     * @access public
     * @return string $cargoNo
     */
    public function getCargoNo(){
      return $this->cargoNo;
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Order
     */
    public function __construct() {
      $this->orderId               = 0;
      $this->orderNo               = 0;
      $this->teslimatAddressBookId = 0;
      $this->faturaAddressBookId   = 0;
      $this->paymentType           = -1;
      $this->bankId                = 0;
      $this->status                = -2;
      $this->userId                = 0;
      $this->orderDate             = "";
      $this->cargoCompany          = "";
      $this->cargoNo               = "";
    }
    /**
     * Siparişi kaydeder
     *
     * @access public
     * @return string "Success" or error message
     * @param array $cartInfo -> Sepet bilgilerini içerir (Toplam tutar vs.)
     * @param array $userCart -> Kullanıcının sepetini içerir
     */
    public function insertOrder($cartInfo = array(), $userCart = array()){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $teslimatId = (int)$this->teslimatAddressBookId;
        $faturaId = (int)$this->faturaAddressBookId;
        $paymentType = (int)$this->paymentType;
        $bankId = (int)$this->bankId;
        $userId = (int)$this->userId;
        $orderDate = date('d.m.Y H:i:s');

        $bankObject = new Bank();
        $bankObject->setBankId($bankId);
        $bankControl = $bankObject->getBankDetails();

        $memberObject = new Member();
        $memberObject->setUserId($userId);
        $userControl = $memberObject->getUserInformation();

        $addressObject = new Address();
        $addressObject->setUserId($userId);
        $addressObject->setAddressBookId($teslimatId);

        $teslimatAddressControl = $addressObject->getAddressBook();

        $addressObject->setAddressBookId($faturaId);
        $faturaAddressControl = $addressObject->getAddressBook();

        if(!is_array($userCart) || count($userCart) == 0 || !isset($userCart["cart"]) || !is_array($userCart["cart"]) || count($userCart["cart"]) == 0) {
          return "Sipariş Vermeden Önce Lütfen Sepetinize Ürün Ekleyiniz.";
        }
        #Güncel kurları çekelim. eğer kurlar gelmezse sipariş başarısız
        /*$exchange = new Exchange();
        $usd_selling = $exchange->usd_selling;
        $usd_buying = $exchange->usd_buying;
        $euro_selling = $exchange->euro_selling;
        $euro_buying = $exchange->euro_buying;

        if(!is_float($usd_selling) || !is_float($euro_selling) || $usd_selling <= 0 || $euro_selling <= 0){
          #Kur bilgileri çekilemez ise;
          return "Kur bilgileri alınamadığı için sipariş tamamlanamadı.";
        }*/

        $cart = $userCart["cart"];

        /*Kontrollerimizi yaptıktan sonra kayıt işlemimizi gerçekleştirebiliriz.*/
        #Önce sipariş no oluşturalım.
        $orderNo = 0;
        $getSql = "SELECT * FROM order_headers
                   ORDER BY order_header_id DESC
                   LIMIT 1";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0){
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $orderNo=(int)$read["order_header_id"];
        }
        $orderNo++;

        $orderAmount= $cartInfo["odenecek_tutar"];

        $status = ($paymentType == 0) ? 1 : 0;

        $insertSql = "INSERT INTO order_headers
                      VALUES ('',$orderNo,$teslimatId,$faturaId,$paymentType,$bankId,
                              $status,$userId,'$orderDate',$orderAmount,'','')";
        $insertQuery = mysqli_query($connection,$insertSql);
        if(!$insertQuery) return "Siparişiniz Alınırken Bir Hata Oluştu <br> SQL : $insertSql";
        else {
          $queries = array();
          foreach ($cart as $key => $productVariant) {
            $barcode = $productVariant["barcode"];
            $quantity = (int)$productVariant["quantity"];
            $unitPrice = (float)$productVariant["product_details"]["discount_price"];
            $currency = $productVariant["product_details"]["currency"];
            if($currency == "USD") $unitPrice *= $usd_selling;
            else if($currency == "EURO") $unitPrice *= $euro_selling;
            else if($currency != "TL") return "EURO, USD VE TL HARİCİ PARA BİRİMİ İLE SATIŞIMIZ OLMADIĞI İÇİN SİPARİŞ İPTAL EDİLDİ.";

            $insertSql = "INSERT INTO order_lines
                          VALUES ('',$orderNo,'$barcode',$unitPrice,$quantity)";
            $insertQuery = mysqli_query($connection,$insertSql);
            if(!$insertQuery) return "Siparişiniz Alınırken Bir Hata Oluştu";
            else{
              #Sipariş miktarını ürünün stok sayısından düşen sql sorgularını diziye alalım.
              #Diziye alma sebebim foreach bittikten sonra sorguyu çalıştırmamdır.
              #Foreach bittikten sonra çalışıtıryorum çünkü; foreach bittiğinde bir hata yok demektir.
              $queries[$i++] = "UPDATE product_variants
                                SET stock = stock-$quantity
                                WHERE barcode='$barcode'";
            }
          }
          #bu foreach de sorguları çalıştırıyorum.
          #sorgular siparişi verilen ürünlerin stok sayısını güncelliyor.
          foreach ($queries as $key => $query) {
            mysqli_query($connection,$query);
          }
          $cartObject = new Cart();
          $cartObject->emptyCart($userId);
          return "Success";
        }
      }
    }
    /**
     * Sipariş bilgilerini kontrol eder.
     * Siparişe hazır mı? değil mi?
     *
     * @access public
     * @return string "Success" or error message
     * @param array $cartInfo -> Sepet bilgilerini içerir (Toplam tutar vs.)
     * @param array $userCart -> Kullanıcının sepetini içerir
     * @param int $countTheCart -> Sepetteki toplam ürün sayısıdır
     */
    public function orderControl($cartInfo = array(), $userCart=array(), $countTheCart=0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else if(!is_array($cartInfo) || count($cartInfo) <= 0) return "Sipariş vermeden önce lütfen sepetinize ürün ekleyiniz.";
      else{
        $teslimatId = (int)$this->teslimatAddressBookId;
        $faturaId = (int)$this->faturaAddressBookId;
        $paymentType = (int)$this->paymentType;
        $bankId = (int)$this->bankId;
        $userId = (int)$this->userId;
        $orderDate = date('d.m.Y H:i:s');

        $bankObject = new Bank();
        $bankObject->setBankId($bankId);
        $bankControl = $bankObject->getBankDetails();

        $memberObject = new Member();
        $memberObject->setUserId($userId);
        $userControl = $memberObject->getUserInformation();


        $addressObject = new Address();
        $addressObject->setUserId($userId);
        $addressObject->setAddressBookId($teslimatId);

        $teslimatAddressControl = $addressObject->getAddressBook();

        $addressObject->setAddressBookId($faturaId);
        $faturaAddressControl = $addressObject->getAddressBook();

        if(!is_array($teslimatAddressControl) || count($teslimatAddressControl) == 0){
          return "Lütfen Kayıtlı Bir Teslimat Adresi Seçiniz.";
        }
        else if(!is_array($faturaAddressControl) || count($faturaAddressControl) == 0){
          return "Lütfen Kayıtlı Bir Fatura Adresi Seçiniz.";
        }
        else if(!is_array($userControl) || count($userControl) == 0){
          return "Kullanıcı Bulunamadı";
        }
        else if($paymentType == 1 && (!is_array($bankControl) || count($bankControl) == 0) ){
          return "Lütfen Kayıtlı Banka Bilgilerinden Birini Seçiniz";
        }
        else if((int)$countTheCart <= 0){
          return "Sipariş Vermeden Önce Lütfen Sepetinize Ürün Ekleyiniz.";
        }
        else{

          if(!is_array($userCart) || count($userCart) == 0 || !isset($userCart["cart"]) || !is_array($userCart["cart"]) || count($userCart["cart"]) == 0) {
            return "Sipariş Vermeden Önce Lütfen Sepetinize Ürün Ekleyiniz.";
          }
          else{
            #Güncel kurları çekelim. eğer kurlar gelmezse sipariş başarısız
            /*$exchange = new Exchange();
            $usd_selling = $exchange->usd_selling;
            $usd_buying = $exchange->usd_buying;
            $euro_selling = $exchange->euro_selling;
            $euro_buying = $exchange->euro_buying;

            if(!is_float($usd_selling) || !is_float($euro_selling) || $usd_selling <= 0 || $euro_selling <= 0){
              #Kur bilgileri çekilemez ise;
              return "Kur bilgileri alınamadığı için sipariş tamamlanamadı.";
            }*/

            $cart = $userCart["cart"];
            $returnedValue = "";
            foreach ($cart as $key => $productVariant) {
              $barcode = $productVariant["barcode"];
              $quantity = (int)$productVariant["quantity"];
              $stock = (int)$productVariant["product_details"]["stock"];
              $title = $productVariant["product_details"]["title"];
              $discount_price = (float)$productVariant["product_details"]["discount_price"];
              $cartObject = new Cart();
              $cartObject->setBarcode($barcode);
              if($discount_price <= 0) {
                $delete = $cartObject->deleteProductInCart($userId);

                $returnedValue .= "Sipariş vermek istediğiniz bir ürün satıştan kaldırıldığı için sepetinizden çıkardık. <br>
                Sepetinizden çıkarılan ürün : $title <br>
                Lütfen siparişinizi şimdi tamamlayınız <br/>";
              }
              if($quantity <= 0){
                $returnedValue .= "Sipariş verebilmeniz için, her ürün için en az 1 adet vermelisiniz.";
              }
              else if($stock <= 0) {
                $delete = $cartObject->deleteProductInCart($userId);

                $returnedValue .= "Sipariş vermek istediğiniz bir ürün stokta kalmadığı için sepetinizden çıkardık. <br>
                Sepetinizden çıkarılan ürün : $title <br>
                Lütfen siparişinizi şimdi tamamlayınız <br/>";
              }
              else if($quantity > $stock) {
                $cartObject->setQuantity($stock);
                $cartObject->updateCart($userId);

                $returnedValue .= "Sipariş vermek istediğiniz bir ürün stokta istediğiniz adet sayısı kadar kalmadığı için sepetinizi güncelledik. <br>
                Sepetinizden güncellenen ürün : $title <br>
                Eski miktarı : $quantity  <br>
                Yeni Miktarı : $stock     <br>
                Lütfen siparişinizi şimdi tamamlayınız<br/>";
              }
            }
            if(trim($returnedValue) != "") return $returnedValue;
            return "Success";
          }
        }
      }
    }
    /**
     * Siparişi iptal eder
     *
     * @access public
     * @return string "Success" or error message
     */
    public function cancelOrder(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $orderNo = (int)$this->orderNo;
        $getSql = "SELECT * FROM order_headers
                   WHERE order_no=$orderNo";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "İptal Edilmesi İstenen Sipariş Bulunamadı";
        else{
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $orderStatus = (int)$read["status"];
          if($orderStatus == -1) return "Bu sipariş zaten iptal edilmiş durumda.";
          else if($orderStatus == 1) return "Siparişiniz onaylandığı için iptal edilemez.";
          else if($orderStatus == 2) return "Siparişiniz kargoda olduğu için iptal edilemez.";
          else if($orderStatus == 3) return "Teslim edilen sipariş, iptal edilemez.";
          else if($orderStatus != 0) return "Siparişiniz iptal edilemedi.";
          else {
            $updateHeader = "UPDATE order_headers
                             SET status=-1,
                                 cargo_company='',
                                 cargo_no=''
                             WHERE order_no=$orderNo";
            $updateHeaderQuery = mysqli_query($connection,$updateHeader);
            if(!$updateHeaderQuery) return "Sipariş İptal İşlemi Başarısız Oldu";
            else{
              $getSql = "SELECT * FROM order_lines
                         WHERE order_no=$orderNo";
              $getQuery = mysqli_query($connection,$getSql);
              while ($read = mysqli_fetch_array($getQuery)) {
                $barcode = $read["barcode"];
                $quantity = (int)$read["quantity"];

                $update = "UPDATE product_variants
                           SET stock = stock+$quantity
                           WHERE barcode='$barcode'";
                $updateQuery = mysqli_query($connection,$update);
              }
              return "Success";
            }
          }
        }
      }
    }
    /**
     * Siparişin durumunu günceller
     *
     * @access public
     * @return boolean
     */
    public function updateOrder(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $orderNo = (int)$this->orderNo;
        $status = (int)$this->status;
        $cargoCompany = $this->cargoCompany;
        $cargoNo = $this->cargoNo;

        $getSql="SELECT * FROM order_headers
                 WHERE order_no=$orderNo";
        $getQuery=mysqli_query($connection,$getSql);
        if(!$getQuery){
          return "Güncellemek İstediğiniz Sipariş Bulunamadı";
        }
        else{
          $sql = "UPDATE order_headers
                  SET status=$status,
                      cargo_company='$cargoCompany',
                      cargo_no='$cargoNo'
                  WHERE order_no=$orderNo";
          $query = mysqli_query($connection,$sql);
          return $query;
        }
      }
    }
    /**
     * Kullanıcıya ait siparişleri döndürür
     *
     * @access public
     * @return array user's orders
     */
    public function getOrders(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $userId = (int)$this->userId;
        $getSql = "SELECT * FROM order_headers
                   WHERE user_id=$userId
                   ORDER BY order_header_id DESC";
        $getQuery = mysqli_query($connection,$getSql);
        $orders = array();
        while ($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)) {
          $orderNo = (int)$read["order_no"];
          $sql = "SELECT * FROM order_lines
                  WHERE order_no = $orderNo";
          $query = mysqli_query($connection,$sql);
          $read["count_cart"] = $query->num_rows;
          $orders[] = $read;
        }
        return $orders;
      }
    }
    /**
     * Tüm kayıtlı siparişleri döndürür
     *
     * @access public
     * @return array all orders
     */
    public function getAllOrders($pageNumber=1, $filters = array()){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $offset = (($pageNumber - 1) * 20);
        $limit = $offset + 20;
        if(count($filters) == 0){
          $sql = "SELECT * FROM users
                  INNER JOIN order_headers
                  ON order_headers.user_id=users.user_id
                  ORDER BY order_headers.order_header_id DESC";

          $allSql = $sql;
          $sql .= " LIMIT $offset, $limit";
        }
        else {
          $sql = "SELECT * FROM users
                  INNER JOIN order_headers
                  ON order_headers.user_id=users.user_id
                  WHERE ";
          $i=0;
          foreach ($filters as $filter => $value) {
            if($i++ > 0) $sql .= " AND ";
            if($filter == "order_headers.status" && $value != "") $sql .= " $filter = $value ";
            else $sql .= " $filter LIKE '%$value%' ";
          }
          $allSql = $sql;
          $sql .= "ORDER BY order_headers.order_header_id DESC
                   LIMIT $offset, $limit";
        }
        $query = mysqli_query($connection,$sql);
        $orders = array("orders" => []);
        $orders["sql"] = $sql;
        while ($read = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
          $orders["orders"][] = $read;
        }
        $query = mysqli_query($connection,$allSql);
        $ordersCount = $query->num_rows;
        $orders["total_orders"] = $ordersCount;
        return $orders;
      }
    }
    /**
     * Arama sonucuna göre bulunan siparişleri döndürür
     *
     * @access public
     * @return array search orders
     */
    public function getSearchOrders($get=null){
      if(is_null($get) || !is_array($get)) {
        return $this->getAllOrders();
      }
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
          $getSql = "SELECT * FROM users
                     INNER JOIN order_headers
                     ON order_headers.user_id=users.user_id";
          foreach ($get as $key => $value) {
            if($key == "order_no"){
              $getSql .= " WHERE order_headers.order_no=$value ";
            }
            else if($key == "name"){
              $getSql .= " WHERE users.name LIKE '%$value%' ";
            }
            else if($key == "surname"){
              $getSql .= " WHERE users.surname LIKE '%$value%' ";
            }
            break;
          }
          $getSql .= " ORDER BY order_headers.order_date DESC";
          $getQuery = mysqli_query($connection,$getSql);
          $orders = array();
          while ($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)) {
            $orders[] = $read;
          }
          return $orders;
        }
      }
    }
    /**
     * Onaylanmamış siparişlerin sayısını döndürür
     *
     * @access public
     * @return int
     */
    public function getNumberOfNewOrders(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return 0;
      else {
        $sql = "SELECT * FROM order_headers
                WHERE status = 0
                OR status = 1";
        $query = mysqli_query($connection,$sql);
        return $query->num_rows;
      }
    }
    /**
     * Yeni sipariş için kaydedilecek sipariş no'yu döndürür
     *
     * @access public
     * @return int
     */
    public function getNewOrderNo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return 0;
      else {
        $orderNo = 0;
        $getSql = "SELECT * FROM order_headers
                   ORDER BY order_header_id DESC
                   LIMIT 1";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0){
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $orderNo = (int)$read["order_header_id"];
        }
        $orderNo++;
        return $orderNo;
      }
    }

    /**
     * Kayıtlı siparişin detaylarını döndürür
     *
     * @access public
     * @return array
     */
    public function getOrderDetails(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $order = array();
        $orderNo = (int)$this->orderNo;
        $getSql = "SELECT * FROM order_headers
                   WHERE order_headers.order_no=$orderNo";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        $userId = (int)$read["user_id"];
        $member = new Member();
        $member->setUserId($userId);
        $userInformation = $member->getUserInformation();
        $order = $read;
        $order["user_information"] = $userInformation;
        if($read["payment_type"] == 1){
          $bankId = (int)$read["bank_info_id"];
          $bank = new Bank();
          $bank->setBankId($bankId);
          $order["bank_details"] = $bank->getBankDetails();
        }
        $teslimatId = (int)$read["teslimat_address_id"];
        $teslimatSql = "SELECT * FROM address_book
                        WHERE address_book_id=$teslimatId";
        $teslimatQuery = mysqli_query($connection,$teslimatSql);
        $order["teslimat_address"] = mysqli_fetch_array($teslimatQuery,MYSQLI_ASSOC);

        $faturaId = (int)$read["fatura_address_id"];
        $faturaSql = "SELECT * FROM address_book
                      WHERE address_book_id=$faturaId";
        $faturaQuery = mysqli_query($connection,$faturaSql);
        $order["fatura_address"] = mysqli_fetch_array($faturaQuery,MYSQLI_ASSOC);
        $linesSql = "SELECT * FROM order_lines
                     INNER JOIN product_variants
                     ON order_lines.barcode=product_variants.barcode
                     INNER JOIN products
                     ON products.product_id=product_variants.product_id
                     WHERE order_lines.order_no=$orderNo";
        $linesQuery = mysqli_query($connection,$linesSql);
        while ($read = mysqli_fetch_array($linesQuery,MYSQLI_ASSOC)) {
          $order["product_variants"][] = $read;
        }
        return $order;
      }
    }
    /**
     * Aylık verilen sipariş tutarını döndürür
     *
     * @access public
     * @return float
     */
    public function getMonthlyOrderAmount(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return -1;
      else{
        $orderSql = "SELECT * FROM order_headers
                     WHERE status = 1
                     OR status = 2
                     OR status = 3";
        $orderQuery = mysqli_query($connection,$orderSql);
        $gain = (float)0.00;
        while($read = mysqli_fetch_array($orderQuery,MYSQLI_ASSOC)){
          $orderDate = (string)$read["order_date"];
          $orderDate = explode(" ",$orderDate);
          $orderDate = $orderDate[0];
          $orderDate = explode('.',$orderDate);
          $orderMounth = (string)$orderDate[1];
          $orderYear = (string)$orderDate[2];
          $nowMounth = (string)date("m");
          $nowYear = (string)date("Y");
          if($orderYear == $nowYear && $orderMounth == $nowMounth){
            $amount = (float)$read["order_amount"];
            $gain += $amount;
          }
        }
        return $gain;
      }
    }
    /**
     * Toplam verilen sipariş tutarını döndürür
     *
     * @access public
     * @return float
     */
    public function getTotalOrderAmount(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return -1;
      else{
        $sql = "SELECT * FROM order_headers
                WHERE status = 1
                OR    status = 2
                OR    status = 3";
        $query = mysqli_query($connection,$sql);

        $gain = (float)0.00;
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $amount = (float)$read["order_amount"];
          $gain += $amount;
        }
        return $gain;
      }
    }
  }

?>
