<?php

  class PlatformVariant extends Variant{
    protected $platformVariantId, $platformVariantCode, $platformCategoryCode;
    public function setPlatformVariantId($platformVariantId) {
      $this->platformVariantId = $platformVariantId;
    }
    public function setPlatformVariantCode($platformVariantCode) {
      $this->platformVariantCode = $platformVariantCode;
    }
    public function setPlatformCategoryCode($platformCategoryCode) {
      $this->platformCategoryCode = $platformCategoryCode;
    }

    public function insertPlatformVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variants
                   WHERE variant_id=$this->variantId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if(is_array($read) && count($read) > 0){
          //UPDATE
          $sql = "UPDATE platform_variants
                  SET platform_variant_code='$this->platformVariantCode',
                      platform_category_code='$this->platformCategoryCode'
                  WHERE variant_id=$this->variantId
                  AND platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
        else{
          //INSERT
          $sql = "INSERT INTO platform_variants
                  SET platform_variant_code='$this->platformVariantCode',
                      platform_category_code='$this->platformCategoryCode',
                      variant_id=$this->variantId,
                      platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    public function deletePlatformVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_variants
                WHERE platform_variant_id=$this->platformVariantId";
        return mysqli_query($connection,$sql);
      }
    }
    public function deletePlatformVariantsByVariantId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_variants
                WHERE variant_id=$this->variantId";
        return mysqli_query($connection,$sql);
      }
    }

    public function getPlatformVariantsByVariantId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variants
                   INNER JOIN platforms ON platform_variants.platform_id = platforms.platform_id
                   WHERE platform_variants.variant_id=$this->variantId";
        $getQuery = mysqli_query($connection,$getSql);
        $platformVariants = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $platformVariants[] = $read;
        }
        return $platformVariants;
      }
    }
    public function getPlatformVariantByVariantIdAndPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variants
                   WHERE variant_id=$this->variantId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
      }
    }
  }

?>
