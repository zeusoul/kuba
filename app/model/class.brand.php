<?php

  class Brand extends Platform{
    protected $brandId, $brandName;
    public function setBrandId($brandId){
      $this->brandId = (int)$brandId;
    }
    public function setBrandName($brandName){
      $this->brandName = $brandName;
    }
    public function insertBrand(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM brands
                   WHERE brand_name='$this->branName'";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if(is_array($read) || count($read) > 0) return false;
        else{
          $sql = "INSERT INTO brands
                  SET brand_name='$this->brandName'";
          return mysqli_query($connection,$sql);
        }
      }
    }
    public function deleteBrand(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM brands
                WHERE brand_id='$this->brandId'";
        $query = mysqli_query($connection,$sql);
        if($query){
          # Marka silme işlemi başarılıysa eşleşmelerini de silelim.
          $objPlatformBrand = new PlatformBrand();
          $objPlatformBrand->setBrandId($this->brandId);
          $objPlatformBrand->deletePlatformBrandsByBrandId();
        }
        return $query;
      }
    }
    public function getBrands(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM brands";
        $getQuery = mysqli_query($connection,$getSql);
        $brands = array();
        while($read = mysqli_fetch_array($getQuery)){
          $brands[] = $read;
        }
        return $brands;
      }
    }
    public function getBrand(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM brands
                   WHERE brand_id=$this->brandId";
        $getQuery = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
      }
    }
  }


?>
