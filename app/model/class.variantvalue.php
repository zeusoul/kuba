<?php

  class VariantValue extends Variant{
    protected $variantValueId;
    protected $variantValueName;
    public function setVariantValueId($variantValueId){
      $this->variantValueId = (int)$variantValueId;
    }
    public function setVariantValueName($variantValueName){
      $this->variantValueName = trim(strip_tags($variantValueName));
    }

    public function insertVariantValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $variantValueName = mysqli_real_escape_string($connection,$this->variantValueName);
        $getSql = "SELECT * FROM variant_values
        WHERE variant_id=$this->variantId AND variant_value_name='$variantValueName'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows>0) return "Bu varyanta ait '$variantValueName' isminde varyant değeri zaten var. Lütfen başka isim giriniz.";
        else{
          $sql = "INSERT INTO variant_values
          SET variant_id=$this->variantId, variant_value_name='$variantValueName'";
          return mysqli_query($connection,$sql) ? "Success" : "Varyant Değeri Kaydedilemedi";
        }
      }
    }
    public function deleteVariantValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM variant_values
        WHERE variant_value_id=$this->variantValueId";
        return mysqli_query($connection,$sql);
      }
    }
    public function deleteVariantValuesByVariantId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM variant_values
                WHERE variant_id=$this->variantId";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
          $sql = "DELETE FROM variant_values
                  WHERE variant_value_id=".$read["variant_value_id"];
          $query = mysqli_query($connection,$sql);
          if($query){
            # Eğer silme işlemi başarılıysa eşleşmeleri de silelim.
            $objPlatformVariantValue = new PlatformVariantValue();
            $objPlatformVariantValue->setVariantValueId($read["variant_value_id"]);
            $objPlatformVariantValue->deletePlatformVariantValuesByVariantValueId();
          }          
        }
      }
    }

    public function getVariantValuesByVariantId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variant_values WHERE variant_id=$this->variantId";
        $query = mysqli_query($connection,$sql);
        $variantValues = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $variantValues[] = $read;
        }
        return $variantValues;
      }
    }
    public function getVariantValueById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variant_values
                WHERE variant_value_id=$this->variantValueId";
        $query = mysqli_query($connection,$sql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
  }

?>
