<?php
  /**
   * Database sınıfı, Veritabanı ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Database sınıfı,
   * Veritabanı bağlantısı kurmaya yarar.
   *
   * Example usage:
   * if (Database::MySqlConnection()) {
   *   print "Database connection established";
   * }
   *
   * @package Database
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Database{
    /**
     * Server (Sunucu)
     *
     * @var string
     * @access private
     */
    private $host;
    /**
     * Kullanıcı adı
     *
     * @var string
     * @access private
     */
    private $uname;
    /**
     * Şifre
     *
     * @var string
     * @access private
     */
    private $pass;
    /**
     * Veritabanı adı
     *
     * @var string
     * @access private
     */
    private $db;
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Database
     */
    public function __construct(){
      $this->host   = "localhost";
      $this->uname  = "root";
      $this->pass   = "";
      $this->db     = "modamahur";

     /*  $this->host   = "localhost";
      $this->uname  = "insoftya_moda";
      $this->pass   ="btaiPT}aTH_m";
      $this->db     = "insoftya_modamahur";*/

      /*
      $this->host   = "79.98.129.215:3306";
      $this->uname  = "muratko1";
      $this->pass   = "Lu8F566bhp";
      $this->db  = "muratko1_iem";
      */
      /*
      $this->host   = "89.252.185.52:3306";
      $this->uname  = "istan306";
      $this->pass   = "4rg3Wo5g4O";
      $this->db     = "istan306_iem";
      */

      /*$this->host   = "localhost";
      $this->uname  = "istan306";
      $this->pass   = "4rg3Wo5g4O";
      $this->db     = "istan306_cng";*/

    }
    /**
     * Veri tabanı bağlantısını sağlar.
     *
     * @access public
     * @return object
     */
    public function MySqlConnection(){
      $connection = @mysqli_connect($this->host,$this->uname,$this->pass,$this->db);
      mysqli_set_charset($connection, "utf8");
      return $connection;
    }
  }

?>
