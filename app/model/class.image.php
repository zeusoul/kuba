<?php
  /**
   * Image sınıfı, Resim ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Image sınıfı,
   * Sunucuya (Server) resim yüklemeye yarar.
   *
   * Example usage:
   * if (Image::uploadImage()) {
   *   print "Image uploaded";
   * }
   *
   * @package Image
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Image extends Product{
    /**
     * Resim ismi
     *
     * @var string
     * @access protected
     */
    protected $imageName;
    /**
     * Resim
     *
     * @var array
     * @access protected
     */
    protected $image;
    /**
     * Resmin dosya yolu
     *
     * @var string
     * @access protected
     */
    protected $imageLocation;
    /**
     * Set the $imageName var
     *
     * @access public
     * @param string $imageName
     */
    public function setImageName($imageName){
      $this->imageName = trim(strip_tags($imageName));
    }
    /**
     * Set the $image var
     *
     * @access public
     * @param array $image
     */
    public function setImage($image){
      if(is_array($image)){
        if(isset($image["name"]) && isset($image["type"]) && isset($image["tmp_name"]) && isset($image["size"])){
          $this->image = $image;
        }
        else $this->image = array();
      }
      else {
        $this->image = trim(strip_tags($image));
      }
    }
    /**
     * Set the $imageLocation var
     *
     * @access public
     * @param string $imageLocation
     */
    public function setImageLocation($imageLocation){
      $this->imageLocation = trim(strip_tags($imageLocation));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Image
     */
    public function __construct(){
      $this->imageName     = 0;
      $this->imageLocation = "";
    }
    /**
     * Seçilen resmi sunucuya yükler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function uploadImage(){
      $imageName = $this->imageName;
      $imageLocation = $this->imageLocation;
      $_IMAGE = (!is_array($this->image) || count($this->image) == 0) ? $this->productImage : $this->image;
      if(is_null($imageName) || $imageName == "") return "Resim isim boş olamaz";
      else if(is_null($imageLocation) || $imageLocation == "") return "Resim yolu giriniz";
      else if(!is_array($_IMAGE) || count($_IMAGE) == 0)return "Resim dosyası bulunamadı";
      else {
        $error = $_IMAGE["error"];
        if($error != 0) return "Resim yüklenirken hata oluştu";
        else {
          $size = $_IMAGE["size"];
          #eğer boyut 10 mb'dan fazlaysa;
          if($size > (1024*1024*30)) return "Resmin boyutu 30 MB'dan küçük olmalı";
          else {
            $type = $_IMAGE["type"];//dosyanın tipi
            $fileName = $_IMAGE["name"];//dosyanın ismi
            $extension = explode('.',$fileName);//dosyanın ismini  .  ile ayırıyoruz (deneme.jpg)
            //uzantının elemanları deneme ve jpg oluyor
            $extension = $extension[count($extension)-1];//uzanti dizi olarak geldiği için en son indisi alıyoruz (yani uzantısını)
            $extension = (string)trim($extension);
            if($extension == "jpg" || $extension == "JPG" || $extension == "png" || $extension == "PNG"){
              $file = $_IMAGE['tmp_name'];
              $copy = copy($file,  "$imageLocation/$imageName.jpg");
              // Dosya upload edildi! ""Success değerini döndürebiliriz
              return ($copy) ? "Success" : "Resim yüklenirken hata oluştu";
            }
            else return "Lütfen jpg veya png dosyası seçiniz";
          }
        }
      }
    }
  }

?>
