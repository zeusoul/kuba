<?php

  class PlatformVariantValue extends VariantValue{
    protected $platformVariantValueId, $platformVariantValueCode;
    public function setPlatformVariantValueId($platformVariantValueId){
      $this->platformVariantValueId = $platformVariantValueId;
    }
    public function setPlatformVariantValueCode($platformVariantValueCode){
      $this->platformVariantValueCode = $platformVariantValueCode;
    }

    public function insertPlatformVariantValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variant_values
                   WHERE variant_value_id=$this->variantValueId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if(is_array($read) && count($read) > 0){
          //UPDATE
          $sql = "UPDATE platform_variant_values
                  SET platform_variant_value_code='$this->platformVariantValueCode'
                  WHERE variant_value_id=$this->variantValueId
                  AND platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
        else{
          //INSERT
          $sql = "INSERT INTO platform_variant_values
                  SET platform_variant_value_code='$this->platformVariantValueCode',
                      variant_value_id=$this->variantValueId,
                      platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    public function deletePlatformVariantValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_variant_values
                WHERE platform_variant_value_id=$this->platformVariantValueId";
        return mysqli_query($connection,$sql);
      }
    }
    public function deletePlatformVariantValuesByVariantValueId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_variants
                WHERE variant_value_id=$this->variantValueId";
        return mysqli_query($connection,$sql);
      }
    }

    public function getPlatformVariantValuesByVariantValueId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variant_values
                   INNER JOIN platforms ON platform_variant_values.platform_id = platforms.platform_id
                   WHERE platform_variant_values.variant_value_id=$this->variantValueId";
        $getQuery = mysqli_query($connection,$getSql);
        $platformVariantValues = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $platformVariantValues[] = $read;
        }
        return $platformVariantValues;
      }
    }
    public function getPlatformVariantValueByVariantValueIdAndPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_variant_values
                   WHERE variant_value_id=$this->variantValueId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
      }
    }
  }

?>
