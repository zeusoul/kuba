<?php
  /**
   * Category sınıfı, Kategori ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Category sınıfı,
   * Yeni kategori eklemeye,
   * Kayıtlı kategoriyi silmeye
   * ve kategori ile ilgili gerekli bilgileri döndürmeye yarar.
   *
   * Example usage:
   * if (Category::deleteCategory()) {
   *   print "Category has been deleted";
   * }
   *
   * @package Category
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
 */
  class Category extends Platform{
    /**
     * Kategorinin id'si
     *
     * @var int
     * @access protected
     */
    protected $categoryId;
    /**
     * Üst kategorinin id'si
     *
     * @var int
     * @access protected
     */
    protected $parentId;
    /**
     * Kategori ismi
     *
     * @var string
     * @access protected
     */
    protected $categoryName;
    /**
     * Kategori resmi
     *
     * @var array
     * @access protected
     */
    protected $categoryImage;
    /**
     * Set the $categoryId var
     *
     * @access public
     * @param int $categoryId
     */
    public function setCategoryId($categoryId){
      $this->categoryId = (int)$categoryId;
    }
    /**
     * Set the $parentId var
     *
     * @access public
     * @param int $parentId
     */
    public function setParentId($parentId){
      $this->parentId = (int)$parentId;
    }
    /**
     * Set the $categoryName var
     *
     * @access public
     * @param string $categoryName
     */
    public function setCategoryName($categoryName){
      $this->categoryName = trim(strip_tags($categoryName));
    }
    /**
     * Set the $categoryImage var
     *
     * @access public
     * @param array $categoryImage
     */
    public function setCategoryImage($categoryImage){
      if(is_array($categoryImage)){
        if(isset($categoryImage["name"]) && isset($categoryImage["type"]) && isset($categoryImage["tmp_name"]) && isset($categoryImage["size"])){
          if($categoryImage["name"] == "" || $categoryImage["type"] == "" || $categoryImage["tmp_name"] == "" ||$categoryImage["size"] <= 0){
            $this->categoryImage = array();
          }
          else $this->categoryImage = $categoryImage;
        }
        else $this->categoryImage = array();
      }
      else {
        $this->categoryImage = trim(strip_tags($categoryImage));
      }
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Category
     */
    public function __construct(){
      $this->categoryId   = 0;
      $this->parentId     = 0;
      $this->categoryName = "";
      $this->categoryImage     = array();
    }
    /**
     * Kategori ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $name = mysqli_real_escape_string($connection,$this->categoryName);
        $parentId = (int)$this->parentId;
        $getSql = "SELECT * FROM categories
                   WHERE name='$name'
                   AND parent_id=$parentId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu İsimde Kategori Zaten Mevcut.";
        else {
          $insertSql = "INSERT INTO categories
                        VALUES('','$name',$parentId,'')";
          $insertQuery = mysqli_query($connection,$insertSql);
          if(!$insertQuery) return "Kategori Kaydedilmedi";
          else if(!is_array($this->categoryImage) || count($this->categoryImage) <= 0) return "Success";
          else {
            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/category-images";
            $imageName = seoUrl($name);
            $imageObject->setImageName($imageName);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setImage($this->categoryImage);

            $upload = $imageObject->uploadImage();
            if($upload != "Success") return "Kategori kaydedildi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
            else {
              $updateSql = "UPDATE categories
                            SET category_image='$imageName.jpg'
                            WHERE name='$name'
                            AND parent_id=$parentId";
              $updateQuery = mysqli_query($connection,$updateSql);
              return ($updateQuery) ? "Success" : "Kategori kaydedildi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
            }
          }
        }
      }
    }
    /**
     * Kayıtlı kategoriyi siler
     *
     * @access public
     * @return boolean
     */
    public function deleteCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $categoryId = (int)$this->categoryId;
        $getSql = "SELECT * FROM categories
                   WHERE category_id=$categoryId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $deleteSql = "DELETE FROM categories
                        WHERE category_id=$categoryId";
          $delete = mysqli_query($connection,$deleteSql);
          if(!$delete) return false;
          else{
            // Kategori eşleştirmelerinide silelim.
            $objPlatformCategory = new PlatformCategory();
            $objPlatformCategory->setCategoryId($this->categoryId);
            $res = $objPlatformCategory->deletePlaformCategoriesByCategoryId();

            $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
            $imageName = $read["category_image"];
            if($imageName != ""){
              $path = PATH."/app/user-app/view/public/img/category-images";
              $deleteImage = unlink("$path/$imageName");
            }

            $filter = new Filter();
            $filter->setCategoryId($categoryId);
            $deleteFilters = $filter->deleteAllCategoryFilter();
            if(!$deleteFilters) return false;
            else{
              $getSql = "SELECT * FROM categories
                         WHERE parent_id=$categoryId";
              $getQuery = mysqli_query($connection,$getSql);
              if($getQuery->num_rows > 0){
                while($read = mysqli_fetch_array($getQuery)){
                  $this->categoryId = (int)$read["category_id"];
                  $this->deleteCategory();
                }
              }
              else return $delete;
            }
          }
        }
      }
    }
    /**
     * Kayıtlı kategoriyi günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $categoryName = mysqli_real_escape_string($connection,$this->categoryName);
        $categoryId = (int)$this->categoryId;
        if(trim($categoryName) == "" || trim($categoryName) == null) return false;
        else {
          $sql = "UPDATE categories SET name='$categoryName'
                  WHERE category_id=$categoryId";
          $query = mysqli_query($connection,$sql);
          if($query && is_array($this->categoryImage) && count($this->categoryImage)){
            $category = $this->getCategoryById();
            $imageLocation = PATH."/app/user-app/view/public/img/category-images/".$category["category_image"];
            unlink($imageLocation);

            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/category-images";
            $imageName = seoUrl($categoryName);
            $imageObject->setImageName($imageName);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setImage($this->categoryImage);

            $upload = $imageObject->uploadImage();
            if($upload = "Success") {
              $sql = "UPDATE categories
                      SET category_image='$imageName.jpg'
                      WHERE category_id=$this->categoryId";
              $query = mysqli_query($connection, $sql);
              return $query ? "Success" : "Kategori bilgileri güncellendi fakat resim güncellenemedi !";
            }
            else return "Kategori bilgileri güncellendi fakat resim güncellenemedi !";
          }
          return $query ? "Success" : "Kategori Bilgileri Güncellenemedi !";
        }
      }
    }
    /**
     * $parentId' ye göre kategorileri ve alt kategorlerini döndürür
     *
     * @access public
     * @return array categories and sub categories for parent id
     */
    public function getCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array("false");
      else {
        $parentId = (int)$this->parentId;
        $mainSql = "SELECT * FROM categories
                    WHERE parent_id=$parentId";

        $mainQuery = mysqli_query($connection,$mainSql);
        $categories = array();

        while($mainRead = mysqli_fetch_array($mainQuery,MYSQLI_ASSOC)){
          $categoryId = (int)$mainRead["category_id"];
          $categories[$categoryId] = $mainRead;
          $subSql = "SELECT * FROM categories
                     WHERE parent_id=$categoryId";
          $subQuery = mysqli_query($connection,$subSql);
          if($subQuery->num_rows > 0){
            while($subRead = mysqli_fetch_array($subQuery,MYSQLI_ASSOC)){
              $this->parentId = $categoryId;
              $categories[$categoryId]["sub"] = $this->getCategories();
            }
          }
        }
        return $categories;
      }
    }
    /**
     * $categoryId' ye göre kategoriyi ve alt kategorlerini döndürür
     *
     * @access public
     * @return array categories and sub categories for parent id
     */
    public function getCategoryById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array("false");
      else {
        $mainSql = "SELECT * FROM categories
                    WHERE category_id=$this->categoryId";
        $query = mysqli_query($connection,$mainSql);
        $category = mysqli_fetch_array($query,MYSQLI_ASSOC);

        $subSql = "SELECT * FROM categories
                   WHERE parent_id=$this->categoryId";
        $subQuery = mysqli_query($connection,$subSql);
        if($subQuery->num_rows > 0){
          while($subRead = mysqli_fetch_array($subQuery,MYSQLI_ASSOC)){
            $this->parentId = $this->categoryId;
            $category["sub"] = $this->getCategories();
          }
        }
        return $category;
      }
    }
    /**
     * Tüm kategorileri döndürür
     *
     * @access public
     * @return array All categories
     */
    public function getAllCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM categories
                ORDER BY parent_id ASC";
        $query = mysqli_query($connection,$sql);
        $categories = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          if((int)$read["parent_id"] > 0){
            $sql2 = "SELECT * FROM categories
                     WHERE category_id=".$read["parent_id"];
            $query2 = mysqli_query($connection, $sql2);
            $read["parent"] = mysqli_fetch_array($query2, MYSQLI_ASSOC);
          }
          $categories[] = $read;
        }
        return $categories;
      }
    }
    /**
     * Seçilen kategorinin bilgilerini döndürür
     *
     * @access public
     * @return array Selected category
     */
    public function getSelectedCategory(){
      $db = new Database();
      $connection= $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM categories
                WHERE category_id = $this->categoryId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else{
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $category = $read;
          return $category;
        }
      }

    }
    /**
     * Tüm alt kategorileri döndürür
     *
     * @access public
     * @return array Sub categories
     */
    public function getSubCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM categories
                WHERE parent_id=$this->categoryId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else {
          $subCategories = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $subCategories[] = $read;
          }
          return $subCategories;
        }
      }
    }
    /**
     * Tüm kategorileri ve alt kategorilerini döndürür
     *
     * @access public
     * @return array All categories and sub categories
     */
    public function getCategoriesAndSubCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $parentId = 0;
        $categories = array();
        if(is_int($this->parentId) && $this->parentId > 0){
          $parentId = (int)$this->parentId;
        }
        $getSql = "SELECT * FROM categories
                   WHERE parent_id=$parentId";
        $getQuery = mysqli_query($connection,$getSql);

        while($read = mysqli_fetch_array($getQuery)){
          $this->parentId = (int)$read["category_id"];
          $subCategories = $this->getCategoriesAndSubCategories();
          $read["subCategories"] = $subCategories;
          $categories[] = $read;
        }
        return $categories;
      }
    }
       /**
     * Kategori ve o kategoriye ait ürünleri döndürür.
     *
     * @access public
     * @return array categories and sub categories for parent id
     */
    public function getCategoriesWithProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM categories
                ORDER BY category_id DESC";
        $query = mysqli_query($connection,$sql);
        $categories = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $categories[$read["category_id"]] = $read;
          $getSql = "SELECT * FROM products WHERE category_id=".$read["category_id"];
          $getQuery = mysqli_query($connection,$getSql);
          $categories[$read["category_id"]]["products"] = array();
          while($productRead = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
            $categories[$read["category_id"]]["products"][] = $productRead;
          }
        }
        return $categories;
      }
    }
  }

?>
