<?php
  /**
   * Exchange sınıfı, Kurlar ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Exchange sınıfı,
   * Dolar ve Euro kurlarını çekemeye yarar.
   *
   * Example usage:
   * $exchange = new Exchange();
   * $usd_selling = $exchange->usd_selling;
   * print "1₺ = $usd_selling $";
   *
   * @package Earning
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Exchange{
    /**
     * Dolar alış fiyatı
     *
     * @var float
     * @access public
     */
    public $usd_buying;
    /**
     * Dolar satış fiyatı
     *
     * @var float
     * @access public
     */
    public $usd_selling;
    /**
     * Euro alış fiyatı
     *
     * @var float
     * @access public
     */
    public $euro_buying;
    /**
     * Euro satış fiyatı
     *
     * @var float
     * @access public
     */
    public $euro_selling;
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Exchange
     */
    public function __construct(){
      $connect_web        = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');
      $this->usd_buying   = (float)round((float)$connect_web->Currency[0]->BanknoteBuying,2);
      $this->usd_selling  = (float)round((float)$connect_web->Currency[0]->BanknoteSelling,2);
      $this->euro_buying  = (float)round((float)$connect_web->Currency[3]->BanknoteBuying,2);
      $this->euro_selling = (float)round((float)$connect_web->Currency[3]->BanknoteSelling,2);
    }
  }

?>
