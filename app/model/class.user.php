<?php
  /**
   * User sınıfı, kullanıcıyla işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * User sınıfı,
   * Kullanıcı yeni kaydı yapmaya yarar.
   *
   *
   * Example usage:
   * if (User::signUp() === "Success") {
   *   print "User registration successful";
   * }
   *
   * @package User
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class User extends Person{
    /**
     * Kullanıcı kaydı yapar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function signUp(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $email = mysqli_real_escape_string($connection,$this->email);
        $name = mysqli_real_escape_string($connection,$this->name);
        $surname = mysqli_real_escape_string($connection,$this->surname);
        $tel = mysqli_real_escape_string($connection,$this->tel);
        $pass = mysqli_real_escape_string($connection,$this->pass);
        $str = new Str();
        if($str->IsNullOrEmptyString(array($email,$name,$surname,$tel,$pass))) return "Lütfen Boş Değer Girmeyiniz";
        else if(strlen($tel) < 10) return "Lütfen Geçerli Bir Telefon Numarası Giriniz";
        else if(strlen($pass) < 8) return "Şifre en az 8 haneli olmalıdır";
        else if(strstr($pass," ")) return "Şifrenizde boşluk olmamalıdır.";
        else {
          $getSql = "SELECT * FROM users
                     WHERE email ='$email'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows > 0) return "Bu Email Adresine Ait Kullanıcı Mevcut";
          else {
            $date = date('d.m.Y');
            $sql = "INSERT INTO users
                    VALUES('','$name','$surname','$email','$tel',md5('$pass'),'$date',0,1)";
            $query = mysqli_query($connection,$sql);
            return ($query) ? "Success" : "Kayıt Başarısız!";
          }
        }
      }
    }
  }

?>
