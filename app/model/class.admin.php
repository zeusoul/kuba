<?php
  /**
   * Admin sınıfı, Yönetici ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Admin sınıfı,
   * Yönetici için giriş yapmaya
   * ve çıkış yapmaya yarar
   *
   * Example usage:
   * if (Admin::logIn()) {
   *   print "Logged in";
   * }
   *
   * @package Admin
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Admin extends Person{
   /**
    * Admin için giriş yaptıtır.
    * @access public
    * @return boolean
    */
    public function logIn(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $userName = mysqli_real_escape_string($connection,$this->userName);
        $pass = mysqli_real_escape_string($connection,$this->pass);
        $sql = "SELECT * FROM admin
                WHERE uname='$userName'
                AND pass=md5('$pass')";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows==1){
          $read = mysqli_fetch_array($query);
          $_SESSION[sessionPrefix()."admin_login"]    = "true";
          $_SESSION[sessionPrefix()."admin_name"]     = $read["name"];
          $_SESSION[sessionPrefix()."admin_surname"]  = $read["surname"];
          $_SESSION[sessionPrefix()."admin_id"]       = $read["admin_id"];
          $_SESSION[sessionPrefix()."admin_username"] = $read["uname"];
          return true;
        }
        else return false;
      }
    }
    /**
     * Kullanıcı adı ve şifreye göre admin bilgisi olup olmadıgını kontrol eder
     * @access public
     * @return boolean
     */
     public function isAdmin(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return false;
       else {
         $userName = mysqli_real_escape_string($connection,$this->userName);
         $pass = mysqli_real_escape_string($connection,$this->pass);
         $sql = "SELECT * FROM admin
                 WHERE uname='$userName'
                 AND pass=md5('$pass')";
         $query = mysqli_query($connection,$sql);
         if($query->num_rows == 1){
           return true;
         }
         else return false;
       }
     }
    /**
     * Admin için çıkış yaptıtır.
     * @access public
     */
    public function signOut(){
      $_SESSION[sessionPrefix()."admin_login"]    = null;
      $_SESSION[sessionPrefix()."admin_name"]     = null;
      $_SESSION[sessionPrefix()."admin_surname"]  = null;
      $_SESSION[sessionPrefix()."admin_id"]       = null;
      $_SESSION[sessionPrefix()."admin_username"] = null;
    }
  }

?>
