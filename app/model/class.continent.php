<?php
  /**
   * continent sınıfı, Kategori ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * continent sınıfı,
   * Yeni kategori eklemeye,
   * Kayıtlı kategoriyi silmeye
   * ve kategori ile ilgili gerekli bilgileri döndürmeye yarar.
   *
   * Example usage:
   * if (continent::deletecontinent()) {
   *   print "continent has been deleted";
   * }
   *
   * @package continent
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
 */
  class Continent extends Platform{
    /**
     * Kategorinin id'si
     *
     * @var int
     * @access protected
     */
    protected $continentId;
    /**
     * Üst kategorinin id'si
     *
     * @var int
     * @access protected
     */
    protected $parentId;
    /**
     * Kategori ismi
     *
     * @var string
     * @access protected
     */
    protected $continentName;
    /**
     * Kategori resmi
     *
     * @var array
     * @access protected
     */
    protected $continentImage;
    /**
     * Set the $continentId var
     *
     * @access public
     * @param int $continentId
     */
    public function setContinentId($continentId){
      $this->continentId = (int)$continentId;
    }
    /**
     * Set the $parentId var
     *
     * @access public
     * @param int $parentId
     */
    public function setParentId($parentId){
      $this->parentId = (int)$parentId;
    }
    /**
     * Set the $continentName var
     *
     * @access public
     * @param string $continentName
     */
    public function setContinentName($continentName){
      $this->continentName = trim(strip_tags($continentName));
    }
    /**
     * Set the $continentImage var
     *
     * @access public
     * @param array $continentImage
     */
    public function setContinentImage($continentImage){
      if(is_array($continentImage)){
        if(isset($continentImage["name"]) && isset($continentImage["type"]) && isset($continentImage["tmp_name"]) && isset($continentImage["size"])){
          if($continentImage["name"] == "" || $continentImage["type"] == "" || $continentImage["tmp_name"] == "" ||$continentImage["size"] <= 0){
            $this->continentImage = array();
          }
          else $this->continentImage = $continentImage;
        }
        else $this->continentImage = array();
      }
      else {
        $this->continentImage = trim(strip_tags($continentImage));
      }
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return continent
     */
    public function __construct(){
      $this->continentId   = 0;
      $this->parentId     = 0;
      $this->continentName = "";
      $this->continentImage     = array();
    }
    /**
     * Kategori ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertContinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $name = mysqli_real_escape_string($connection,$this->continentName);
        $parentId = (int)$this->parentId;
        $getSql = "SELECT * FROM continent
                   WHERE name='$name'
                   AND parent_id=$parentId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu İsimde Kategori Zaten Mevcut.";
        else {
          $insertSql = "INSERT INTO continent
                        VALUES('','$name',$parentId,'')";
          $insertQuery = mysqli_query($connection,$insertSql);
          if(!$insertQuery) return "Kategori Kaydedilmedi";
          else if(!is_array($this->continentImage) || count($this->continentImage) <= 0) return "Success";
          else {
            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/continent-images";
            $imageName = seoUrl($name);
            $imageObject->setImageName($imageName);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setImage($this->continentImage);

            $upload = $imageObject->uploadImage();
            if($upload != "Success") return "Kategori kaydedildi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
            else {
              $updateSql = "UPDATE continent
                            SET continent_image='$imageName.jpg'
                            WHERE name='$name'
                            AND parent_id=$parentId";
              $updateQuery = mysqli_query($connection,$updateSql);
              return ($updateQuery) ? "Success" : "Kategori kaydedildi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
            }
          }
        }
      }
    }
    /**
     * Kayıtlı kategoriyi siler
     *
     * @access public
     * @return boolean
     */
    public function deleteContinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $continentId = (int)$this->continentId;
        $getSql = "SELECT * FROM continent
                   WHERE continent_id=$continentId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $deleteSql = "DELETE FROM continent
                        WHERE continent_id=$continentId";
          $delete = mysqli_query($connection,$deleteSql);
          if(!$delete) return false;
          else{
            // Kategori eşleştirmelerinide silelim.
            $objPlatformContinent = new PlatformContinent();
            $objPlatformContinent->setContinentId($this->continentId);
            $res = $objPlatformContinent->deletePlaformContinentBycontinentId();

            $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
            $imageName = $read["continent_image"];
            if($imageName != ""){
              $path = PATH."/app/user-app/view/public/img/continent-images";
              $deleteImage = unlink("$path/$imageName");
            }

            $filter = new Filter();
            $filter->setContinentId($continentId);
            $deleteFilters = $filter->deleteAllContinentFilter();
            if(!$deleteFilters) return false;
            else{
              $getSql = "SELECT * FROM continent
                         WHERE parent_id=$continentId";
              $getQuery = mysqli_query($connection,$getSql);
              if($getQuery->num_rows > 0){
                while($read = mysqli_fetch_array($getQuery)){
                  $this->continentId = (int)$read["continent_id"];
                  $this->deleteContinent();
                }
              }
              else return $delete;
            }
          }
        }
      }
    }
    /**
     * Kayıtlı kategoriyi günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateContinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $continentName = mysqli_real_escape_string($connection,$this->continentName);
        $continentId = (int)$this->continentId;
        if(trim($continentName) == "" || trim($continentName) == null) return false;
        else {
          $sql = "UPDATE continent SET name='$continentName'
                  WHERE continent_id=$continentId";
          $query = mysqli_query($connection,$sql);
          if($query && is_array($this->continentImage) && count($this->continentImage)){
            $continent = $this->getcontinentById();
            $imageLocation = PATH."/app/user-app/view/public/img/continent-images/".$continent["continent_image"];
            unlink($imageLocation);

            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/continent-images";
            $imageName = seoUrl($continentName);
            $imageObject->setImageName($imageName);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setImage($this->continentImage);

            $upload = $imageObject->uploadImage();
            if($upload = "Success") {
              $sql = "UPDATE continent
                      SET continent_image='$imageName.jpg'
                      WHERE continent_id=$this->continentId";
              $query = mysqli_query($connection, $sql);
              return $query ? "Success" : "Kategori bilgileri güncellendi fakat resim güncellenemedi !";
            }
            else return "Kategori bilgileri güncellendi fakat resim güncellenemedi !";
          }
          return $query ? "Success" : "Kategori Bilgileri Güncellenemedi !";
        }
      }
    }
    /**
     * $parentId' ye göre kategorileri ve alt kategorlerini döndürür
     *
     * @access public
     * @return array continent and sub continent for parent id
     */
    public function getContinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array("false");
      else {
        $parentId = (int)$this->parentId;
        $mainSql = "SELECT * FROM continent
                    WHERE parent_id=$parentId";

        $mainQuery = mysqli_query($connection,$mainSql);
        $continent = array();

        while($mainRead = mysqli_fetch_array($mainQuery,MYSQLI_ASSOC)){
          $continentId = (int)$mainRead["continent_id"];
          $continent[$continentId] = $mainRead;
          $subSql = "SELECT * FROM continent
                     WHERE parent_id=$continentId";
          $subQuery = mysqli_query($connection,$subSql);
          if($subQuery->num_rows > 0){
            while($subRead = mysqli_fetch_array($subQuery,MYSQLI_ASSOC)){
              $this->parentId = $continentId;
              $continent[$continentId]["sub"] = $this->getcontinent();
            }
          }
        }
        return $continent;
      }
    }
    /**
     * $continentId' ye göre kategoriyi ve alt kategorlerini döndürür
     *
     * @access public
     * @return array continent and sub continent for parent id
     */
     public function getContinentById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array("false");
      else {
        $mainSql = "SELECT * FROM continent
                    WHERE continent_id=$this->continentId";
        $query = mysqli_query($connection,$mainSql);
        $continent = mysqli_fetch_array($query,MYSQLI_ASSOC);

        $subSql = "SELECT * FROM continent
                   WHERE parent_id=$this->continentId";
        $subQuery = mysqli_query($connection,$subSql);
        if($subQuery->num_rows > 0){
          while($subRead = mysqli_fetch_array($subQuery,MYSQLI_ASSOC)){
            $this->parentId = $this->continentId;
            $continent["sub"] = $this->getcontinent();
          }
        }
        return $continent;
      }
    }
    /**
     * Tüm kategorileri döndürür
     *
     * @access public
     * @return array All continent
     */
    public function getAllcontinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM continent
                ORDER BY parent_id ASC";
        $query = mysqli_query($connection,$sql);
        $continent = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          if((int)$read["parent_id"] > 0){
            $sql2 = "SELECT * FROM continent
                     WHERE continent_id=".$read["parent_id"];
            $query2 = mysqli_query($connection, $sql2);
            $read["parent"] = mysqli_fetch_array($query2, MYSQLI_ASSOC);
          }
          $continent[] = $read;
        }
        return $continent;
      }
    }
    /**
     * Seçilen kategorinin bilgilerini döndürür
     *
     * @access public
     * @return array Selected continent
     */
    public function getSelectedcontinent(){
      $db = new Database();
      $connection= $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM continent
                WHERE continent_id = $this->continentId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else{
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $continent = $read;
          return $continent;
        }
      }

    }
    /**
     * Tüm alt kategorileri döndürür
     *
     * @access public
     * @return array Sub continent
     */
    public function getSubcontinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM continent
                WHERE parent_id=$this->continentId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else {
          $subcontinent = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $subcontinent[] = $read;
          }
          return $subcontinent;
        }
      }
    }
    /**
     * Tüm kategorileri ve alt kategorilerini döndürür
     *
     * @access public
     * @return array All continent and sub continent
     */
    public function getcontinentAndSubcontinent(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $parentId = 0;
        $continent = array();
        if(is_int($this->parentId) && $this->parentId > 0){
          $parentId = (int)$this->parentId;
        }
        $getSql = "SELECT * FROM continent
                   WHERE parent_id=$parentId";
        $getQuery = mysqli_query($connection,$getSql);

        while($read = mysqli_fetch_array($getQuery)){
          $this->parentId = (int)$read["continent_id"];
          $subcontinent = $this->getcontinentAndSubcontinent();
          $read["subcontinent"] = $subcontinent;
          $continent[] = $read;
        }
        return $continent;
      }
    }
       /**
     * Kategori ve o kategoriye ait ürünleri döndürür.
     *
     * @access public
     * @return array continent and sub continent for parent id
     */
    public function getcontinentWithProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM continent
                ORDER BY continent_id DESC";
        $query = mysqli_query($connection,$sql);
        $continent = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $continent[$read["continent_id"]] = $read;
          $getSql = "SELECT * FROM products WHERE continent_id=".$read["continent_id"];
          $getQuery = mysqli_query($connection,$getSql);
          $continent[$read["continent_id"]]["products"] = array();
          while($productRead = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
            $continent[$read["continent_id"]]["products"][] = $productRead;
          }
        }
        return $continent;
      }
    }
  }

?>
