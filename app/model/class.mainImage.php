<?php

/**
 * Sayfa içine eklenecek url'i bulunan resimlerin
 * işlemlerinin yapıldığı class
 */

    class MainImage extends Image{
        /**
         * Resimlerin ıdsi
         *
         *
         *  @var int
         *  @access protected
         */
        protected $mainId;
        /**
         * Resimlerin ıdsi
         *
         *
         *  @var int
         *  @access protected
         */
        protected $imageOrder;
        /**
         * resim1'in resmi
         *
         *
         *  @var string
         *  @access protected
         */
        protected $image;
        /**
         * set the $mainId var
         *
         * @access public
         * @param string $mainId
         */
        public function setMainId($mainId){
            $this->mainId = (int)$mainId;
        }
        /**
         * set the $image var
         *
         * @access public
         * @param array $image
         *
         */
        public function setImage($image){
            if(is_array($image)){
                if(isset($image["name"]) && isset($image["type"])
                && isset($image["tmp_name"]) && isset($image["size"])){
                    $this->image = $image;
                }
                else $this->image = array();
            }
            else{
                $this->image = trim(strip_tags($image));
            }
        }
        /**
         * set the $imageOrder var
         *
         * @access public
         * @param int
         */
        public function setImageOrder($imageOrder){
            $this->imageOrder = (int)$imageOrder;
        }
        /**
         * Tüm sınıf verilerini  veri tabanına kaydeder
         *
         * @access public
         * @return boolean
         */
        public function insertMain(){
            $db= new Database();
            $connection = $db->MySqlConnection();
            if(!connection) return false;
            else{
              $getSql = "SELECT * FROM main_images
                          WHERE image_order=$this->imageOrder";
              $getQuery = mysqli_query($connection,$getSql);
              if($getQuery->num_rows == 1){
                $updateSql = "UPDATE main_images
                              SET product_id = $this->productId";
                $update = mysqli_query($connection,$updateSql);
                if(!$update) return false;
                else{
                  $mainImage = new Image();
                  $mainLocation = PATH."/app/user-app/view/public/img/main-images";
                  $mainImage->setImageName("main-image-$this->imageOrder");
                  $mainImage->setImageLocation($mainLocation);
                  $mainImage->setProductImage($this->image);

                  $upload = $mainImage->uploadImage();
                  return $upload == "Success";
                }
              }
              else{
                $imageName = "main-image-$this->imageOrder";
                $insertSQL = "INSERT INTO main_images
                              SET image_order=$this->imageOrder,
                                  product_id=$this->productId,
                                  image_name='$imageName.jpg'";
                $insert = mysqli_query($connection,$insertSQL);

                if(!$insert) return false;
                else{
                  $mainImage = new Image();
                  $mainLocation = PATH."/app/user-app/view/public/img/main-images";
                  $mainImage->setImageName("main-image-$this->imageOrder");
                  $mainImage->setImageLocation($mainLocation);
                  $mainImage->setProductImage($this->image);
                  $upload = $mainImage->uploadImage();
                  return "Success";
                }
              }
          }
        }
        public function getFirstMainImage(){
          $db= new Database();
          $connection = $db->MySqlConnection();
          if(!$connection) return array();
          else{
            $getSql = "SELECT * FROM main_images
                        WHERE image_order=1";
            $getQuery = mysqli_query($connection,$getSql);
            $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
            return $read;
          }
        }
        public function getSecondMainImage(){
          $db= new Database();
          $connection = $db->MySqlConnection();
          if(!$connection) return array();
          else{
            $getSql = "SELECT * FROM main_images
                        WHERE image_order=2";
            $getQuery = mysqli_query($connection,$getSql);
            $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
            return $read;
          }
        }
        public function getThirdMainImage(){
          $db= new Database();
          $connection = $db->MySqlConnection();
          if(!$connection) return array();
          else{
            $getSql = "SELECT * FROM main_images
                        WHERE image_order=3";
            $getQuery = mysqli_query($connection,$getSql);
            $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
            return $read;
          }
      }
      public function getFourthMainImage(){
        $db= new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
          $getSql = "SELECT * FROM main_images
                      WHERE image_order=4";
          $getQuery = mysqli_query($connection,$getSql);
          $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
          return $read;
        }
    }
    public function getFiveMainImage(){
      $db= new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM main_images
                    WHERE image_order=5";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC);
        return $read;
      }
  }
      
    }
?>
