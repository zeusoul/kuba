<?php
  /**
   * ProductComments sınıfı, Ürün yorumlarıyla ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * ProductComments sınıfı,
   * Yeni yorum kaydetmeye,
   * Kayıtlı yorumu güncellemeye, silmeye
   * ve yorum bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (ProductComments::insertProductComment() === "Success") {
   *   print "Product comment Inserted";
   * }
   *
   * @package ProductComments
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class ProductComments extends Product{
    /**
     * Yorum id'si
     *
     * @var int
     * @access protected
     */
    protected $commentId;
    /**
     * Ürün Yorumu
     *
     * @var int
     * @access protected
     */
    protected $productComment;
    /**
     * Set the $commentId var
     *
     * @access public
     * @param int $commentId
     */
    public function setCommentId($commentId){
      $this->commentId = (int)$commentId;
    }
    /**
     * Set the $productComment var
     *
     * @access public
     * @param string $productComment
     */
    public function setProductComment($productComment){
      $this->productComment = trim(strip_tags($productComment));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductComments
     */
    public function __construct() {
      $this->commentId      = 0;
      $this->productComment = "";
    }

    /**
     * Onaysız yorumların sayısını döndürür
     *
     * @access public
     * @return string "Success" or error message
     * @param int $userId user's id
     */
    public function insertProductComment($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $userId = (int)$userId;
        $productId = (int)$this->productId;
        $comment = mysqli_real_escape_string($connection,$this->productComment);
        $rating = (int)$this->productRatePoint;

        $userGetSql = "SELECT * FROM users
                       WHERE user_id=$userId";
        $userGetQuery = mysqli_query($connection,$userGetSql);
        if($userGetQuery->num_rows != 1) return "Kullanıcı Bulunamadı";
        else if(trim($comment) == null || trim($comment) == "" || $rating < 1 || $rating > 5) {
          return "Yorum ve değerlendirme alanları boş bırakılamaz";
        }
        else {
          $insertSql = "INSERT INTO product_comments
                        VALUES('',$userId,$rating,'$comment',$productId,0)";
          $insertQuery = mysqli_query($connection,$insertSql);
          return ($insertQuery) ? "Success" : "Yorum Eklenemedi";
        }
      }
    }
    /**
     * Yorumun durumunu onaylar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function confirmProductComment(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $commentId = (int)$this->commentId;
        $getSql = "SELECT * FROM product_comments
                   WHERE product_comment_id = $commentId";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if($getQuery->num_rows != 1) return "Yorum Bulunamadı";
        else if((int)$read["status"] == 1) return "Yorum Zaten Onaylanmış Durumda";
        else{
          $updateSql = "UPDATE product_comments
                        SET status=1
                        WHERE product_comment_id=$commentId";
          $query = mysqli_query($connection,$updateSql);
          if(!$query) return "Yorum Onaylanamadı";
          else {
            $rating = (int)$read["user_rating"];
            $productId = (int)$read["product_id"];
            $update = "UPDATE products
                       SET rating = ((voting_user * rating + $rating) / (voting_user + 1)) ,
                           voting_user = voting_user + 1
                       WHERE product_id=$productId";
            $updateQuery = mysqli_query($connection,$update);
            return ($updateQuery) ? "Success" : "Yorum Onaylandı Fakat Kullanıcının Oyu Ürüne Yansımadı";
          }
        }
      }
    }
    /**
     * Ürün Yorumunu siler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function deleteProductComment(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $commentId = (int)$this->commentId;
        $getSql = "SELECT * FROM product_comments
                   WHERE product_comment_id=$commentId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "Yorum Bulunamadı";
        else{
          $read = mysqli_fetch_array($getQuery);
          $status = (int)$read["status"];
          if($status == 1){
            $productId = (int)$read["product_id"];
            $rating = (int)$read["user_rating"];
            $update = "UPDATE products
                       SET rating = (((rating * voting_user) - $rating) / (voting_user-1)),
                           voting_user = voting_user - 1
                       WHERE product_id=$productId";
            $updateQuery = mysqli_query($connection,$update);
            if(!$updateQuery) return "Ürünün değerlendirmesine yansımadığı için yorum silinmedi";
          }
          $sql = "DELETE FROM product_comments
                  WHERE product_comment_id=$commentId";
          $query = mysqli_query($connection,$sql);
          return ($query) ? "Success" : "Yorum Silinemedi";
        }
      }
    }
    /**
     * Ürüne ait tüm yorumları siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductComments(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productId = (int)$this->productId;
        $sql = "DELETE FROM product_comments
                WHERE product_id=$productId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Kullanıcının yorumununu günceller
     *
     * @access public
     * @return boolean
     * @param int $userId user's id
     */
    public function updateProductComment($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else if(trim($this->productComment) == "" || trim($this->productComment) == null || is_null($this->productComment)) return false;
      else{
        $commentId = (int)$this->commentId;
        $comment = mysqli_real_escape_string($connection,$this->productComment);
        $rating = (int)$this->productRatePoint;
        if(trim($comment) == "" || trim($comment) == null || is_null($comment)) return false;
        else {
          $userId = (int)$userId;
          $getSql = "SELECT * FROM product_comments
                     WHERE product_comment_id=$commentId
                     AND user_id=$userId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows != 1) return false;
          else {
            $read = mysqli_fetch_array($getQuery);
            $oldRating = (int)$read["user_rating"];
            $oldComment = $read["comment"];
            $status=(int)$read["status"];
            if($oldRating == $rating && $oldComment == $comment) return false;
            else{
              $updateSql = ($status == 1) ? "UPDATE product_comments
                                             INNER JOIN products
                                             ON product_comments.product_id=products.product_id
                                             SET product_comments.comment='$comment',
                                                 product_comments.user_rating=$rating,
                                                 product_comments.status=0,
                                                 products.rating = ((products.rating*products.voting_user)-$oldRating)/(products.voting_user-1),
                                                 products.voting_user=products.voting_user-1
                                              WHERE product_comments.product_comment_id=$commentId"
                                            : "UPDATE product_comments
                                               SET comment='$comment',
                                                   user_rating=$rating
                                              WHERE product_comment_id=$commentId";

              $updateQuery = mysqli_query($connection,$updateSql);
              return $updateQuery;
            }
          }
        }
      }
    }
    /**
     * Kayıtlı tüm ürün yorumlarını döndürür
     *
     * @access public
     * @return array All product comments
     */
    public function getAllProductComments(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT *,product_comments.status AS 'comment_status' FROM product_comments
                   INNER JOIN users
                   ON product_comments.user_id=users.user_id
                   INNER JOIN products
                   ON products.product_id=product_comments.product_id
                   ORDER BY product_comment_id DESC";
        $getQuery = mysqli_query($connection,$getSql);
        $productComments = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $read["name"] =  $read["name"]." ".$read["surname"];
          $productComments[] = $read;
        }
        return $productComments;
      }
    }
    /**
     * Ürün'e yorumları döndürür
     *
     * @access public
     * @return array product's comments
     */
    public function getProductComments(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM product_comments
                INNER JOIN users
                ON product_comments.user_id = users.user_id
                WHERE product_comments.product_id=$this->productId
                AND product_comments.status = 1
                ORDER BY product_comments.user_rating DESC";
        $query = mysqli_query($connection,$sql);
        $comments = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $read["name"] = $read["name"]." ".$read["surname"];
          $comments[] = $read;
        }
        return $comments;
      }
    }
    /**
     * Arama sonucna göre bulunana yorumları döndürür
     *
     * @access public
     * @return array search product comments
     * @param array $get filters
     */
    public function getSearchComments($get = array()){
      if(!is_array($get) || count($get) <= 0){
        return array();
      }
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else {
          $getSql = "SELECT * FROM product_comments
                     INNER JOIN users
                     ON product_comments.user_id = users.user_id
                     INNER JOIN products
                     ON product_comments.product_id = products.product_id
                     WHERE ";
          $i=0;
          foreach ($get as $key => $value) {
            if($i++>0) $getSql.=" AND ";
            $getSql .= "$key LIKE '%$value%'";
          }
          $getSql.=" ORDER BY product_comment_id DESC";
          $getQuery = mysqli_query($connection,$getSql);
          $comments = array();
          while($read = mysqli_fetch_array($getQuery)){
            $comments[] = array(
              "product_comment_id"  =>  $read["product_comment_id"],
              "name"                =>  $read["name"]." ".$read["surname"],
              "rating"              =>  $read["user_rating"],
              "comment"             =>  $read["comment"]
            );
          }
          return $comments;
        }
      }
    }
    /**
     * Kullanıcıya ait yorumları döndürür
     *
     * @access public
     * @return array user's comments
     * @param int $userId user's id
     */
    public function getUserComments($userId){
      $userId = (int)$userId;
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM product_comments
                INNER JOIN  products
                ON product_comments.product_id = products.product_id
                WHERE product_comments.user_id=$userId
                ORDER BY product_comments.product_comment_id DESC";
        $query = mysqli_query($connection,$sql);
        $comments = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $comments[] = $read;
        }
        return $comments;
      }
    }
    /**
     * Onaysız yorumların sayısını döndürür
     *
     * @access public
     * @return int
     */
    public function getNumberOfComments(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return 0;
      else{
        $sql = "SELECT * FROM product_comments
                WHERE status=0";
        $query = mysqli_query($connection,$sql);
        return $query ? $query->num_rows : 0;
      }
    }
  }

?>
