<?php
  /**
   * Settings sınıfı, Ayarlar ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Settings sınıfı,
   * Yeni ayarlar kaydetmeye,
   * Kayıtlı ayarları güncellemeye, silmeye
   * ve ayarları döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Settings::updateEmailDogrulama() === "Success") {
   *   print "Email dogrulama Updated";
   * }
   *
   * @package Settings
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Settings{
    /**
     * Ayar id'si
     *
     * @var int
     * @access protected
     */
    protected $settingId;
    /**
     * Ayar ismi
     *
     * @var string
     * @access protected
     */
    protected $setting;
    /**
     * Ayarın değeri
     *
     * @var string
     * @access protected
     */
    protected $value;
    /**
     * Set the $settingId var
     *
     * @access public
     * @param int $settingId
     */
    public function setSettingId($settingId){
      $this->settingId = (int)$settingId;
    }
    /**
     * Set the $setting var
     *
     * @access public
     * @param string $setting
     */
    public function setSetting($setting){
      $this->setting = trim(strip_tags($setting));
    }
    /**
     * Set the $value var
     *
     * @access public
     * @param string $value
     */
    public function setValue($value){
      $this->value = trim(strip_tags($value));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Settings
     */
    public function __construct() {
      $this->settingId = 0;
      $this->setting   = "";
      $this->value     = "";
    }

    /**
     * Sitenin yayın ayarının değerini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateSiteYayinDurumu(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $value = (int)$this->value;
        if($value == 1 || $value == 0){
          $getSql = "SELECT * FROM settings
                     WHERE setting='site_yayin_durumu'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows <= 0){
            $sql = "INSERT INTO settings
                    VALUES ('','site_yayin_durumu','$value')";
            $insert = mysqli_query($connection,$sql);
            return $insert;
          }
          else{
            $sql = "UPDATE settings
                    SET value='$value'
                    WHERE setting='site_yayin_durumu'";
            $update = mysqli_query($connection,$sql);
            return $update;
          }
        }
        else return false;
      }
    }
    /**
     * Email doğrulama ayarının değerini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateEmailDogrulama(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $value = (int)$this->value;
        if($value == 1 || $value == 0){
          $getSql = "SELECT * FROM settings
                     WHERE setting='email_dogrulama'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows <= 0){
            $sql = "INSERT INTO settings
                    VALUES ('','email_dogrulama','$value')";
            $insert = mysqli_query($connection,$sql);
            return $insert;
          }
          else{
            $sql = "UPDATE settings
                    SET value='$value'
                    WHERE setting='email_dogrulama'";
            $update = mysqli_query($connection,$sql);
            return $update;
          }
        }
        else return false;
      }
    }
    /**
     * Kargo ücreti ayarının değerini günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateKargoUcreti(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $value = (int)$this->value;
        if($value < 0) return "Kargo ücreti 0'dan küçük olamaz";
        else{
          $getSql = "SELECT * FROM settings
                     WHERE setting='kargo_ucreti'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows <= 0){
            $sql = "INSERT INTO settings
                    VALUES('','kargo_ucreti','$value')";
            $insert = mysqli_query($connection,$sql);
            return ($insert) ? "Success" : "Kargo ücreti güncellenemedi : HATA!";
          }
          else{
            $sql = "UPDATE settings
                    SET value='$value'
                    WHERE setting='kargo_ucreti'";
            $update = mysqli_query($connection,$sql);
            return $update ? "Success" : "Kargo ücreti güncellenemedi : HATA!";
          }
        }
      }
    }
    /**
     * Ücretsiz kargo sınırı ayarının değerini günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateUcretsizKargoSiniri(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $value=(int)$this->value;
        if($value < 0) return "Ücretsiz kargo sınırı 0'dan küçük olamaz";
        else{
          $getSql = "SELECT * FROM settings
                     WHERE setting='ucretsiz_kargo_siniri'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows <= 0){
            $sql = "INSERT INTO settings
                    VALUES('','ucretsiz_kargo_siniri','$value')";
            $insert = mysqli_query($connection,$sql);
            return ($insert) ? "Success" : "Ücretsiz kargo sınırı güncellenemedi : HATA!";
          }
          else{
            $sql = "UPDATE settings
                    SET value='$value'
                    WHERE setting='ucretsiz_kargo_siniri'";
            $update = mysqli_query($connection,$sql);
            return $update ? "Success" : "Ücretsiz kargo sınırı güncellenemedi : HATA!";
          }
        }
      }
    }
    /**
     * Kayıtlı tüm ayarları döndürür
     *
     * @access public
     * @return array settings
     */
    public function getSettings(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM settings";
        $query= mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else{
          $settings = array();
          while($read = mysqli_fetch_array($query)){
            $settings["".$read["setting"]]  =  $read["value"];
          }
          return $settings;
        }
      }
    }
  }

?>
