<?php
  /**
   * Pages sınıfı, Sayfalar ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Pages sınıfı,
   * Yeni sayfa kaydetmeye,
   * Kayıtlı sayfayı güncellemeye, silmeye
   * ve gerekli sayfa bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Pages::insertPage()) {
   *   print "Page Inserted";
   * }
   *
   * @package Pages
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */

  class Pages {
    /**
     * Sayfa id'si
     *
     * @var int
     * @access protected
     */
    protected $pageId;
    /**
     * Menüde görünecek olan başlığı
     *
     * @var string
     * @access protected
     */
    protected $menuTitle;
    /**
     * Sayfa içeriği
     *
     * @var string
     * @access protected
     */
    protected $content;
    /**
     * Sayfanın durumu (1 = Yayında, 0 = Gizli)
     *
     * @var int
     * @access protected
     */
    protected $status;

    /**
     * Set the $pageId var
     *
     * @access public
     * @param int $pageId
     */
    public function setPageId($pageId){
      $this->pageId = (int)$pageId;
    }
    /**
     * Set the $menuTitle var
     *
     * @access public
     * @param string $menuTitle
     */
    public function setMenuTitle($menuTitle){
      $this->menuTitle = trim(strip_tags($menuTitle));
    }
    /**
     * Set the $content var
     *
     * @access public
     * @param string $content
     */
    public function setContent($content){
      $this->content = trim($content);
    }
    /**
     * Set the $status var
     *
     * @access public
     * @param int $status
     */
    public function setStatus($status){
      $this->status = (int)$status;
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Pages
     */
    public function __construct() {
      $this->pageId    = 0;
      $this->menuTitle = "";
      $this->content   = "";
      $this->status    = -1;
    }

    /**
     * Sayfa ekler
     *
     * @access public
     * @return boolean
     */
    public function insertPage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $content = $this->content;
        $menuTitle = $this->menuTitle;
        $status = $this->status;

        $insertSql = "INSERT INTO pages
                      VALUES('','$menuTitle','$content',$status)";
        $insertQuery = mysqli_query($connection,$insertSql);
        return $insertQuery;
      }
    }
    /**
     * Kayıtlı sayfayı günceller
     *
     * @access public
     * @return boolean
     */
    public function updatePage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $content = $this->content;
        $menuTitle = $this->menuTitle;
        $status = $this->status;
        $pageId = $this->pageId;
        $sql = "UPDATE pages
                SET content='$content',
                    menu_title='$menuTitle',
                    status=$status
                WHERE page_id=$pageId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Kayıtlı sayfayı siler
     *
     * @access public
     * @return boolean
     */
    public function deletePage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $pageId = $this->pageId;
        $sql = "DELETE FROM pages
                WHERE page_id=$pageId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Kayıtlı olan bütün sayfaları döndürür
     *
     * @access public
     * @return array all pages
     */
    public function getAllPages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM pages
                ORDER BY page_id ASC ";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else{
          $pages = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $pages[] = $read;
          }
          return $pages;
        }
      }
    }
    /**
     * Kayıtlı olan seçilen sayfayı döndürür
     *
     * @access public
     * @return array selected pages
     */
    public function getSelectedPages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $pageId = $this->pageId;
        $sql = "SELECT * FROM pages
        WHERE page_id=$pageId";
        $query = mysqli_query($connection,$sql);
        $page = mysqli_fetch_array($query,MYSQLI_ASSOC);
        return $page;
      }
    }
  }
