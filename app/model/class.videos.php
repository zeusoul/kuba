<?php
  /**
   * Videos sınıfı, videolar ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Videos sınıfı,
   * Video yükleneye ve silmeye yarar.
   *
   * Example usage:
   * if (Videos::insertVideo()) {
   *   echo "video inserted";
   * }
   *
   * @package Videos
  */
  class Videos{
    /**
     * Video idsi
     *
     * @var int
     * @access protected
     */
    protected $videoId;
    /**
     * Video url'i
     *
     * @var string
     * @access protected
     */
    protected $videoUrl;
    /**
     * Video başlığı
     *
     * @var string
     * @access protected
     */
    protected $videoTitle;
    /**
     * Video açıklaması
     *
     * @var string
     * @access protected
     */
    protected $videoDesc;
    /**
     * Set the $videoId var
     *
     * @access public
     * @param int $videoId
     */
    public function setVideoId($videoId){
      $this->videoId = (int)$videoId;
    }
    /**
     * Set the $videoUrl var
     *
     * @access public
     * @param string $videoUrl
     */
    public function setVideoUrl($videoUrl){
      $videoUrl = explode("?",$videoUrl);
      $videoUrl = $videoUrl[1];
      $videoUrl = explode("&",$videoUrl);
      $url = "https://www.youtube.com/embed/";
      foreach ($videoUrl as $get) {
        $get = explode("=",$get);
        if($get[0] == "v") $url .= $get[1];
        else if($get[0] == "list") $url .= "?list=".$get[1];
      }
      $this->videoUrl = trim(strip_tags($url));
    }
    /**
     * Set the $videoTitle var
     *
     * @access public
     * @param string $videoTitle
     */
    public function setVideoTitle($videoTitle){
      $this->videoTitle = trim(strip_tags($videoTitle));
    }
    /**
     * Set the $videoDesc var
     *
     * @access public
     * @param string $videoDesc
     */
    public function setVideoDesc($videoDesc){
      $this->videoDesc = trim(strip_tags($videoDesc));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductVideos
     */
    public function __construct() {
      $this->videoId  = 0;
      $this->videoUrl = "";
      $this->videoTitle = "";
      $this->videoDesc = "";
    }
    /**
     * Videoyu veri tabanına kaydeder.
     *
     * @access public
     * @return boolean
     */
    public function insertVideo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $videoUrl = $this->videoUrl;
        $videoTitle = $this->videoTitle;
        $videoDesc = $this->videoDesc;
        if($videoUrl == "") return false;
        else{
          $insertSql = "INSERT INTO videos
                        VALUES('','$videoUrl','$videoTitle','$videoDesc')";
          $insertQuery = mysqli_query($connection,$insertSql);
          return $insertQuery;
        }
      }
    }
    /**
     * Videoyu veri tabanından siler.
     *
     * @access public
     * @return boolean
     */
    public function deleteVideo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $videoId = $this->videoId;

        $deleteSql = "DELETE FROM videos
                      WHERE video_id=$videoId";
        $deleteQuery = mysqli_query($connection,$deleteSql);
        return $deleteQuery;
      }
    }
    /**
     * Kayıtlı videoların listesini döndürür.
     *
     * @access public
     * @return array
     */
    public function getVideos(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "SELECT * FROM videos";
        $query = mysqli_query($connection,$sql);
        $videos = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $videos[] = $read;
        }
        return $videos;
      }
    }
  }

?>
