<?php
  /**
   * Message sınıfı, Mesajlar ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Message sınıfı,
   * Mesaj göndermeye ve gelen mesajları çekmeye yarar.
   *
   *
   *
   * Example usage:
   * if (Message::insertMessage() === "Success") {
   *   print "Message inserted";
   * }
   *
   * @package Message
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Message extends Person{
    /**
     * Mesaj id'si
     *
     * @var int
     * @access protected
     */
    protected $messageId;
    /**
     * Ebeveyn mesaj id'si
     *
     * @var int
     * @access protected
     */
    protected $parentMessageId;
    /**
     * Mesajı gönderenin id'si
     *
     * @var int
     * @access protected
     */
    protected $senderId;
    /**
     * Mesajı alanın id'si
     *
     * @var int
     * @access protected
     */
    protected $recipientId;
    /**
     * Mesajın konusu
     *
     * @var string
     * @access protected
     */
    protected $subject;
    /**
     * Mesajın içeriği
     *
     * @var string
     * @access protected
     */
    protected $content;
    /**
     * Set the $messageId var
     *
     * @access public
     * @param int $messageId
     */
    public function setMessageId($messageId){
      $this->messageId = (int)$messageId;
    }
    /**
     * Set the $parentMessageId var
     *
     * @access public
     * @param int $parentMessageId
     */
    public function setParentMessageId($parentMessageId){
      $this->parentMessageId = (int)$parentMessageId;
    }
    /**
     * Set the $senderId var
     *
     * @access public
     * @param int $senderId
     */
    public function setSenderId($senderId){
      $this->senderId = (int)$senderId;
    }
    /**
     * Set the $recipientId var
     *
     * @access public
     * @param int $recipientId
     */
    public function setRecipientId($recipientId){
      $this->recipientId = (int)$recipientId;
    }
    /**
     * Set the $subject var
     *
     * @access public
     * @param string $subject
     */
    public function setSubject($subject){
      $this->subject = trim(strip_tags($subject));
    }
    /**
     * Set the $content var
     *
     * @access public
     * @param string $content
     */
    public function setContent($content){
      $this->content = trim(strip_tags($content));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Message
     */
    public function __construct() {
      $this->messageId       = 0;
      $this->parentMessageId = 0;
      $this->senderId        = 0;
      $this->recipientId     = 0;
      $this->subject         = "";
      $this->content         = "";
    }
    /**
     * Yeni mesaj kaydı yapar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertMessage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $parentMessageId = (int)$this->parentMessageId;
        $senderId = (int)$this->senderId;
        $recipientId = (int)$this->recipientId;
        $name = mysqli_real_escape_string($connection,$this->name);
        $surname = mysqli_real_escape_string($connection,$this->surname);
        $email = mysqli_real_escape_string($connection,$this->email);
        $tel = mysqli_real_escape_string($connection,$this->tel);
        $subject = mysqli_real_escape_string($connection,$this->subject);
        $content = mysqli_real_escape_string($connection,$this->content);
        $date = date('d.m.Y H:i:s');

        $getSql = "SELECT * FROM messages
                   WHERE name_surname='$name $surname'
                   AND email='$email'
                   AND tel='$tel'
                   AND subject='$subject'
                   AND content='$content'
                   AND posting_date='$date'";
        $getQuery = mysqli_query($connection,$getSql);
        $str = new Str();
        if(!stripos(strtolower($content), ' ') && strlen($content) > 50) return "uzun";
        if($getQuery->num_rows > 0) return "Bu ürüne ait fiyat talebiniz zaten önceden alınmıştır.";
        else if($str->IsNullOrEmptyString(array($name,$surname,$email,$tel,$subject,$content))) return "Lütfen boş değer girmeyiniz.";
        else{
          $sql = "INSERT INTO messages
                  VALUES('',$senderId,$recipientId,'$subject','$content',
                         '$name $surname','$email','$tel',
                         '$date',0,$parentMessageId)";
          $insert = mysqli_query($connection,$sql);
          return $insert ? "Success" : "Mesaj Gönderilemedi!";
        }
      }
    }
    /**
     * Mesajlaşmayı döndürür
     *
     * @access public
     * @return array messages
     */
    public function getMessages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $messageId = (int)$this->messageId;
        $sql = "SELECT * FROM messages
                WHERE message_id=$messageId
                OR parent_message_id=$messageId
                ORDER BY posting_date ASC";

        $query = mysqli_query($connection,$sql);
        $messages = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $messages[] = $read;
        }
        return $messages;
      }
    }
    /**
     * Tüm mesajları getirir
     *
     * @access public
     * @return array all messages
     */
    public function getMyAllMessages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $userId = (int)$this->userId;
        $sql = "SELECT * FROM messages
                WHERE (sender_id=$userId
                OR recipient_id=$userId)
                AND parent_message_id=0
                ORDER BY posting_date ASC";
        $query = mysqli_query($connection,$sql);
        $messages = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $messageId = $read["message_id"];
          $getSql = "SELECT * FROM messages
                     WHERE recipient_id=$userId
                     AND parent_message_id=$messageId
                     AND display_status=0";
          $getQuery = mysqli_query($connection,$getSql);

          $read["new_message"] = ($getQuery->num_rows > 0) ? "true" : "false";
          $messages[] = $read;
        }
        return $messages;
      }
    }
    /**
     * Alıcı id'sine ait olan mesajları döndürür
     *
     * @access public
     * @return array recipient's messages
     */
    public function getMyMessages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $messageId = (int)$this->messageId;
        $userId = (int)$this->userId;
        $sql = "SELECT * FROM messages
                WHERE (message_id=$messageId
                OR parent_message_id=$messageId)
                AND (sender_id = $userId
                OR recipient_id=$userId)
                ORDER BY posting_date ASC";

        $query = mysqli_query($connection,$sql);
        $messages = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $messages[] = $read;
        }
        return $messages;
      }
    }
    /**
     * Mesajın görütülenme durumunu günceller
     *
     * @access public
     * @return boolean
     */
    public function updateMessageDisplay(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $messageId = (int)$this->messageId;
        $userId = (int)$this->userId;
        $sql = "UPDATE messages
                SET display_status = 1
                WHERE (message_id=$messageId
                OR parent_message_id=$messageId)
                AND   (recipient_id=$userId)";
        return mysqli_query($connection,$sql);
      }
    }
    /**
     * Görüntülenmemiş mesaj var mı? yok mu? kontrol eder
     * ve durumunu döndürür.
     *
     * @access public
     * @return boolean
     */
    public function getUnvisitedMessages(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $recipientId = (int)$this->recipientId;
        $sql = "SELECT * FROM messages
                WHERE recipient_id=$recipientId
                AND display_status=0";
        $query = mysqli_query($connection,$sql);
        return ($query->num_rows > 0) ? true : false;
      }
    }
  }

?>
