<?php
  /**
   * Person sınıfı, Kişiyi temsil etmesi için açılan abstract(soyut) bir sınıftır
   *
   * Person sınıfı,
   * abstract sınıftır. Kişiden miras alabilmeleri için açılan sınıfır.
   * User, member, admin gibi sınıflar bu sınıftan miras alırlar.
   *
   * NOTE Bu sınıfa ait nesne oluşturulamaz
   *
   * @package Person
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  abstract class Person{
    /**
     * Kullanıcı id'si
     *
     * @var int
     * @access protected
     */
    protected $userId;
    /**
     * Kişinin ismi
     *
     * @var string
     * @access protected
     */
    protected $name;
    /**
     * Kişini soyadı
     *
     * @var string
     * @access protected
     */
    protected $surname;
    /**
     * Email adresi
     *
     * @var string
     * @access protected
     */
    protected $email;
    /**
     * Kişinin kullanıcı adı
     *
     * @var string
     * @access protected
     */
    protected $userName;
    /**
     * Geçerli şifresi
     *
     * @var string
     * @access protected
     */
    protected $pass;
    /**
     * Değiştirilmesi istenen yeni şifresi
     *
     * @var string
     * @access protected
     */
    protected $newPass;
    /**
     * Kişinin telefon numarası
     *
     * @var string
     * @access protected
     */
    protected $tel;

    /**
     * Set the $userId var
     *
     * @access public
     * @param string $userId
     */
    public function setUserId($userId){
      $this->userId = (int)$userId;
    }
    /**
     * Set the $name var
     *
     * @access public
     * @param string $name
     */
    public function setName($name){
      $this->name = trim(strip_tags($name));
    }
    /**
     * Set the $surname var
     *
     * @access public
     * @param string $surname
     */
    public function setSurname($surname){
      $this->surname = trim(strip_tags($surname));
    }
    /**
     * Set the $email var
     *
     * @access public
     * @param string $email
     */
    public function setEmail($email){
      $this->email = trim(strip_tags($email));
    }
    /**
     * Set the $userName var
     *
     * @access public
     * @param string $userName
     */
    public function setUserName($userName){
      $this->userName = trim(strip_tags($userName));
    }
    /**
     * Set the $pass var
     *
     * @access public
     * @param string $pass
     */
    public function setPass($pass){
      $this->pass = trim(strip_tags($pass));
    }
    /**
     * Set the $newPass var
     *
     * @access public
     * @param string $newPass
     */
    public function setNewPass($newPass){
      $this->newPass = trim(strip_tags($newPass));
    }
    /**
     * Set the $tel var
     *
     * @access public
     * @param string $tel
     */
    public function setTel($tel){
      $this->tel = (int)$tel;
      $this->tel = (string)$tel;
      $this->tel = trim(strip_tags($tel));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Person
     */
    public function __construct() {
      $this->userId   = 0;
      $this->name     = "";
      $this->surname  = "";
      $this->email    = "";
      $this->userName = "";
      $this->pass     = "";
      $this->newPass  = "";
      $this->tel      = "";
    }

    /**
     * T.C. Kimlik no. kontrolü yapar
     *
     * @access protected
     * @return boolean
     * @param string $tcNo T.C. Kimlik no.
     */
    protected function tcNoControl($tcNo = ""){
      if(is_null($tcNo) || trim($tcNo) == "" || trim($tcNo) == null) return false;
      else {
        $tcNo = (string)$tcNo;
        if(strlen($tcNo) != 11) return false;
        else if(intval(($tcNo[strlen($tcNo)-1])) % 2 != 0) return false;
        else {
          $sum = 0;
          for($i=0;$i<10;$i++){
            if(!intval($tcNo[$i])) return false;
            else $sum += (int)$tcNo[$i];
          }
          $sum = (string)$sum;
          if($sum[strlen($sum)-1] == $tcNo[strlen($tcNo)-1])return true;
          else return false;
        }
      }
    }
  }

?>
