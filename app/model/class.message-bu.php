<?php
    class Messages{
        /**
         * Mesaj id'si
         *
         * @var int
         * @access protected
         */
        protected $messageId;
        /**
         * Mesaj'ı Gönderen
         * Kişinin Adı
         * @var string
         * @access protected
         */
        protected $name;
        /**
         * Mesaj'ı gönderenin Soyadı
         * 
         * @var string
         * @access protected
         */
        protected $surname;
        /**
         * Mesaj'ın başlığı
         * 
         * @var string
         * @access protected
         */
        protected $title;
        /**
         * Mesaj'ın açıklaması
         * 
         * @var string
         * @access protected
         */
        protected $description;
        /**
         * Mesaj'ı gönderen
         * kişinin telefonu
         * 
         * @var string
         * @access protected
         */
        protected $phone;
        /**
         * Mesaj'ı gönderen
         * kişinin maili
         * 
         * @var string
         * @access protected
         */
        protected $email;
        /**
         * Mesaj'ın gönderildiği
         * tarih 
         * 
         * @var string
         * @access protected
         */
        protected $messageDate;
        /**
         * Set the $messageId var
         *
         * @access public
         * @param int $messageId
         */
        public function setMessageId($messageId){
            $this->messageId = (int)$messageId;
        }
        public function setName($name){
            $this->name = trim(strip_tags($name));
        }
        public function setSurname($surname){
            $this->surname = trim(strip_tags($surname));
        }
        public function setTitle($title){
            $this->title = trim(strip_tags($title));
        }
        public function setDescription($description){
            $this->description = trim(strip_tags($description));
        }
        public function setPhone($phone){
            $this->phone = trim(strip_tags($phone));
        }
        public function setEmail($email){
            $this->email = trim(strip_tags($email));
        }
        public function setMessageDate($messageDate){
            $this->messageDate = trim(strip_tags($messageDate));
        }
        public function __construct() {
            $this->messageId        = 0;
            $this->name             = "";
            $this->surname          = "";
            $this->title            = "";
            $this->description      = "";
            $this->phone            = "";
            $this->email            = "";
            $this->messageDate      = "";
        }
        public function insertMessage(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return "Bağlantı Hatası";
            else{
                    $name = mysqli_real_escape_string($connection,$this->name);
                    $surname = mysqli_real_escape_string($connection,$this->surname);
                    $title = mysqli_real_escape_string($connection,$this->title);
                    $description=mysqli_real_escape_string($connection,$this->description);
                    $phone = mysqli_real_escape_string($connection,$this->phone);
                    $email = mysqli_real_escape_string($connection,$this->email);
                    $messageDate = date('d.m.Y H:i:s');
                    if(!stripos(strtolower($description), ' ') && strlen($description) > 50) return "uzun";
                    else{
                        $sql = "INSERT INTO messages
                        VALUES('','$name','$surname','$title','$description',
                                '$phone','$email','$messageDate')";
                        $insert = mysqli_query($connection,$sql);
                        return $insert ? "Success" : "Mesaj Gönderilemedi!";
                        }
                }
            }
        public function getMessage(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return "Bağlantı Hatası";
            else{
                $messageId = (int)$this->messageId;
                $sql="SELECT * FROM messages
                    ORDER BY message_id ASC";
                $query = mysqli_query($connection,$sql);
                $messages = array();
                while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
                $messages[] = $read;
                }
                return $messages;
            }
        }    
    }
    ?>