<?php
  /**
   * Member sınıfı, Üye ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Member sınıfı,
   * Üye girişi,çıkışı yapmaya,
   * Üye bilgilerini güncellemeye,
   * Üyeyi engellemeye, üyenin engelini kaldırmaya,
   * Onaysız üyenin onaylanmasına
   * ve üyenin gerekli bilgilerini döndürmeye yarar.
   *
   * Example usage:
   * if (Member::logIn()) {
   *   print "Login Successfull";
   * }
   *
   * @package Member
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Member extends Person{
    /**
     * Kullanıcı girişi yapar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function logIn(){
        $db = new Database();
        $connection = $db->MySqlConnection();
        if($connection){
          $sql = "";
          $userId = (int)$this->userId;
          if($userId > 0){
            $sql = "SELECT * FROM users
                    WHERE user_id='$userId'";
          }
          else{
            $email = mysqli_real_escape_string($connection, $this->email);
            $pass = mysqli_real_escape_string($connection, $this->pass);
            $sql = "SELECT * FROM users
                    WHERE BINARY email='$email'
                    AND pass=md5('$pass')";
          }
          $query = mysqli_query($connection,$sql);
          if($query->num_rows == 1){
            $read = mysqli_fetch_array($query);
            $status = (int)$read["status"];
            if($status == 1){
              $confirm = (int)$read["confirm"];
              $settingOb = new Settings();
              $settings = $settingOb->getSettings();
              if(isset($settings["email_dogrulama"]) && (int)$settings["email_dogrulama"] == 1 && $confirm == 0){
                return "Giriş Yapabilmeniz İçin Lütfen Hesabınızı Mailinizden Onaylayınız.";
              }
              else{
                $_SESSION[sessionPrefix()."login"]   = "true";
                $_SESSION[sessionPrefix()."name"]    = $read["name"];
                $_SESSION[sessionPrefix()."surname"] = $read["surname"];
                $_SESSION[sessionPrefix()."email"]   = $read["email"];
                $_SESSION[sessionPrefix()."tel"]     = $read["tel"];
                $_SESSION[sessionPrefix()."user_id"] = $read["user_id"];
                return "Success";
              }
            }
            else return "Hesabınız Askıya Alındığı İçin Giriş Yapamazsınız.";
          }
          else return "Email Adresiniz veya Şifreniz Hatalı";
        }
        else {
          return "Bağlantı Hatası";
        }
    }
    /**
     * Kullanıcı çıkışı yapar
     *
     * @access public
     */
    public function signOut(){
      $_SESSION[sessionPrefix()."login"]   = null;
      $_SESSION[sessionPrefix()."name"]    = null;
      $_SESSION[sessionPrefix()."surname"] = null;
      $_SESSION[sessionPrefix()."email"]   = null;
      $_SESSION[sessionPrefix()."user_id"] = null;
      $_SESSION[sessionPrefix()."tel"]     = null;
    }
    /**
     * Kullanıcı bilgilerini günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateUserInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $userId=(int)$this->userId;
        $pass = mysqli_real_escape_string($connection,$this->pass);
        $name = mysqli_real_escape_string($connection,$this->name);
        $surname = mysqli_real_escape_string($connection,$this->surname);
        $tel = mysqli_real_escape_string($connection,$this->tel);
        $newPass = mysqli_real_escape_string($connection,$this->newPass);
        $str = new Str();
        if($str->IsNullOrEmptyString($name) && $str->IsNullOrEmptyString($surname) && $str->IsNullOrEmptyString($tel) && $str->IsNullOrEmptyString($newPass)){
          return "En Az Bir Alanı Doldurunuz.";
        }
        else{
          $sql = "SELECT * FROM users
                  WHERE user_id=$userId
                  AND pass=md5('$pass')";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows != 1) return "Şifrenizi Hatalı Girdiniz";
          else {
            $count=0;
            $updateSql = "UPDATE users
                          SET ";
            if(!$str->IsNullOrEmptyString($name)){
              if($count++ > 0) $updateSql.=" , ";
              $updateSql .= " name='$name' ";
            }
            if(!$str->IsNullOrEmptyString($surname)){
              if($count++ > 0) $updateSql.=" , ";
              $updateSql .= " surname='$surname' ";
            }
            if(!$str->IsNullOrEmptyString($tel)){
              if($count++ > 0) $updateSql.=" , ";
              $updateSql .= " tel='$tel' ";
            }
            if(!$str->IsNullOrEmptyString($newPass)){
              if(strlen($newPass) < 8) return "Şifreniz En Az 8 Haneli Olmalıdır.";
              else if(strstr($newPass," ")) return "Şifrenizde boşluk olmamalıdır.";
              else if($count++ > 0) $updateSql.=" , ";
              $updateSql .= " pass=md5('$newPass') ";
            }

            $updateSql .= " WHERE user_id=$userId ";
            $updateQuery = mysqli_query($connection,$updateSql);
            $_SESSION[sessionPrefix()."name"] = $name;
            $_SESSION[sessionPrefix()."surname"] = $surname;
            $_SESSION[sessionPrefix()."tel"] = $tel;

            return ($updateQuery) ? "Success" : "Bilgileriniz Güncellenemedi";
          }
        }
      }
    }
    /**
     * Kullanıcıyı engeller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function banUser(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $userId = (int)$this->userId;
        $getSql="SELECT * FROM users
                 WHERE user_id=$userId";
        $getQuery=mysqli_query($connection,$getSql);
        if ($getQuery->num_rows <= 0) return "Kullanıcı Bulunamadı";
        else {
          $read=mysqli_fetch_array($getQuery);
          $status=(int)$read['status'];
          if ($status==0) return "Kullancı Zaten Engellenmiş";
          else {
            $sql = "UPDATE users
                    SET status=0
                    WHERE user_id=$userId";
            $query = mysqli_query($connection,$sql);
            return ($query) ? "Success" : "Kullanıcı Engellemede Hata Oluştu";
          }
        }
      }
    }
    /**
     * Kullanıcının engelini kaldırır
     *
     * @access public
     * @return boolean
     */
    public function unBanUser(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $userId = (int)$this->userId;
        $sql = "UPDATE users
                SET status=1
                WHERE user_id=$userId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Kullanıcıyı onaylı kullanıcı yapar
     *
     * @access public
     * @return string "Success" or error message
     * @param string $md5UserId md5 ile kriptolanmış kullanıcı id'si
     */
    public function confirmUser($md5UserId = ""){
      if(strlen($md5UserId) != 32) return "Kullanıcı Bulunamadı";
      else {
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return "Bağlantı Başarısız";
        else{
          $md5UserId = mysqli_real_escape_string($connection,$md5UserId);
          if(strlen($md5UserId) != 32) return "Kullanıcı Bulunamadı";
          else{
            $getSql = "SELECT * FROM users
                       WHERE md5(user_id)='$md5UserId'";
            $getQuery = mysqli_query($connection,$getSql);
            if($getQuery->num_rows != 1) return "Kullanıcı Bulunamadı";
            else{
              $updateSql = "UPDATE users
                            SET confirm=1
                            WHERE md5(user_id)='$md5UserId'";
              $updateQuery = mysqli_query($connection,$updateSql);
              return ($updateQuery) ? "Success" : "Hesap Onaylanamadı";
            }
          }
        }
      }
    }
    /**
     * Kullanıcıyı onaylı kullanıcı yapar
     *
     * @access public
     * @return string new password or "false"
     * @param string $md5Email md5 ile kriptolanmış email adresi
     */
    public function resetPassword(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "false";
      else{
        $userId = (int)$this->userId;
        $getSql = "SELECT * FROM users
                   WHERE user_id=$userId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "false";
        else{
          $newPass = "";
          $array = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
          for ($i = 0; $i < 8; $i++){
            $randomIndex = rand(0,strlen($array)-1);
            $newPass .= $array[$randomIndex];
          }
          $updateSql = "UPDATE users
                        SET pass=md5('$newPass')
                        WHERE user_id=$userId";
          $updateQuery = mysqli_query($connection,$updateSql);
          return ($updateQuery) ? $newPass : "false";
        }
      }

    }
    /**
     * Tüm kayıtlı kullanıcıları döndürür
     *
     * @access public
     * @return array users
     */
    public function getUsers(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM users";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else {
          $userInformation;
          while($read = mysqli_fetch_array($query)){
            $userInformation[] = $read;
          }
          return $userInformation;
        }
      }
    }
    /**
     * Kayıtlı kullanıcı sayısını döndürür
     *
     * @access public
     * @return int
     */
    public function getNumberOfMember(){
      $db = new Database();
      $connection =$db->MySqlConnection();
      if(!$connection)return -1;
      else {
        $sql = "SELECT * FROM users";
        $query = mysqli_query($connection,$sql);
        return $query->num_rows;
      }

    }
    /**
     * Arama sonucuna göre bulunan kullanıcıları döndürür
     *
     * @access public
     * @return array users
     * @param array $get filters
     */
    public function getSearchUsers($get = array()){
      if(!is_array($get) || count($get) <= 0) return false;
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else {
          $sql = "SELECT * FROM users
                  WHERE ";
          $i=0;
          foreach ($get as $key => $value) {
            if($i++>0) $getSql.=" AND ";
            $sql .= " $key LIKE '%$value%' ";
          }
          $query = mysqli_query($connection,$sql);
          if($query->num_rows <= 0) return array();
          else {
            $userInformation;
            while($read = mysqli_fetch_array($query)){
              $userInformation[] = $read;
            }
            return $userInformation;
          }
        }
      }
    }
    /**
     * Kayıtlı kullanıcının bilgilerini döndürür
     *
     * @access public
     * @return array users
     * @param string $md5Email md5 ile kriptolanmış kullanıcının email adresi
     */
    public function getUserInformation($md5Email = ""){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "";
        $userId = (int)$this->userId;
        $email = mysqli_real_escape_string($connection,$this->email);
        if((int)$userId > 0) {
          $sql = "SELECT * FROM users
                  WHERE user_id=$userId";
        }
        else if(strlen($md5Email) != 32) {
          $sql = "SELECT * FROM users
                  WHERE email='$email'";
        }
        else{
           $sql = "SELECT * FROM users
                   WHERE md5(email)='$md5Email'";
         }
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else {
          $userInformation = mysqli_fetch_array($query,MYSQLI_ASSOC);;
          return $userInformation;
        }
      }
    }
  }

?>
