<?php
  /**
   * Filter sınıfı, Filtre ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Filter sınıfı,
   * Yeni filtre eklemeye,
   * Kayıtlı filtreyi silmeye, güncellemeye,
   * Kategoriye filtre eklemeye, kategoriden filtre silmeye,
   * Ürüne filtre eklemeye, üründen filtre silmeye
   * ve gerekli filtre bilgilerini döndürmeye yarar.
   *
   * Example usage:
   * if (Filter::insertFilter()) {
   *   print "Filter added";
   * }
   *
   * @package Filter
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Filter extends Product{
    /**
     * Filtre id'si
     *
     * @var int
     * @access protected
     */
    protected $filterId;
    /**
     * Filtre ismi
     *
     * @var string
     * @access protected
     */
    protected $filterName;
    /**
     * Set the $filterId var
     *
     * @access public
     * @param int $filterId
     */
    public function setFilterId($filterId){
      $this->filterId = (int)$filterId;
    }
    /**
     * Set the $filterName var
     *
     * @access public
     * @param string $filterName
     */
    public function setFilterName($filterName){
      $this->filterName = trim(strip_tags($filterName));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Filter
     */
    public function __construct(){
      $this->filterId   = 0;
      $this->filterName = "";
    }
    /**
     * Yeni filtre kaydı yapar
     *
     * @access public
     * @return boolean
     */
    public function insertFilter(){
      $db=new Database();
      $connection =$db->MySqlConnection();
      if (!$connection) return false;
      else{
        $filterName = mysqli_real_escape_string($connection,$this->filterName);
        $getSql = "SELECT * FROM filters
                   WHERE name='$filterName'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return false;
        else {
          if(trim($filterName) == "" || trim($filterName) == null) return false;
          else{
            $sql="INSERT INTO filters
                  VALUES ('','$filterName')";
            $query=mysqli_query($connection,$sql);
            return $query;
          }
        }
      }
    }
    /**
     * Kategoriye filtre ekler
     *
     * @access public
     * @return boolean
     */
    public function insertCategoryFilter(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM  category_filters
                   WHERE category_id=$this->categoryId
                   AND filter_id=$this->filterId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return false;
        else{
          $insertSql = "INSERT INTO category_filters
                        VALUES('',$this->categoryId,$this->filterId)";
          $insertQuery = mysqli_query($connection,$insertSql);
          return $insertQuery;
        }
      }
    }
    /**
     * Kategoriye ve o kategoriye ait tüm alt kategorilerine filtre ekler
     *
     * @access public
     * @return boolean
     */
    public function insertCategoryFilterWithSubCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $insert = $this->insertCategoryFilter();
        if(!$insert) return false;
        else{
          $categoryId = (int)$this->categoryId;
          $sql = "SELECT * FROM categories
                  WHERE parent_id=$categoryId";
          $query = mysqli_query($connection,$sql);
          while($read = mysqli_fetch_array($query)){
            $categoryId = (int)$read["category_id"];
            $this->categoryId = $categoryId;
            $insert = $this->insertCategoryFilterWithSubCategories();
            if(!$insert) return false;
          }
          return true;
        }
      }
    }
    /**
     * Ürün filtrelerini ekler
     *
     * @access public
     * @return boolean
     * @param array $filters
     */
    public function insertProductFilters($filters = array()){
      if(!is_array($filters) || count($filters) <= 0){
        return false;
      }
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return false;
        else{
          $productId = (int)$this->productId;
          if(!is_int($productId) || $productId == 0) return false;
          foreach ($filters as $filter_id => $filter_value_id) {
            $insertSql = "INSERT INTO product_filters
                          VALUES('',$productId,$filter_id,$filter_value_id)";
            $insertQuery = mysqli_query($connection,$insertSql);
            if(!$insertQuery) return false;
          }
          return true;
        }
      }
    }
    /**
     * Filtreyi günceller
     *
     * @access public
     * @return boolean
     */
    public function updateFilter(){
      $db = new Database();
      $connection =$db->MySqlConnection();
      if (!$connection) return false;
      else{
        $filterName = mysqli_real_escape_string($connection,$this->filterName);
        $filterId = (int)$this->filterId;

        $getSql = "SELECT * FROM filters
                   WHERE filter_id=$filterId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows == 0) return false;
        else {
          if(trim($filterName) == "" || trim($filterName) == null) return false;
          else{
            $sql="UPDATE filters
                  SET name='$filterName'
                  WHERE filter_id=$filterId";
            $query=mysqli_query($connection,$sql);
            return $query;
          }
        }
      }
    }
    /**
     * Ürüne ait filtreleri günceller
     *
     * @access public
     * @return boolean
     * @param array $filters
     */
    public function updateProductFilters($filters = array()){
      if(!is_array($filters) || count($filters) <= 0){
        return false;
      }
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return false;
        else{
          $productId = (int)$this->productId;
          if(!is_int($productId) || $productId == 0) return false;
          foreach ($filters as $filter_id => $value_id) {
            $getSql = "SELECT * FROM product_filters
                       WHERE product_id=$productId
                       AND filter_id=$filter_id";
            $getQuery = mysqli_query($connection,$getSql);
            if($getQuery->num_rows != 1){
              $insert = $this->insertProductFilters(array($filter_id => $value_id));
              if(!$insert) return false;
            }
            else{
              $updateSql = "UPDATE product_filters
                            SET filter_value_id=$value_id
                            WHERE filter_id=$filter_id
                            AND product_id=$productId";
              $updateQuery = mysqli_query($connection,$updateSql);
              if(!$updateQuery) return false;
            }
          }
          return true;
        }
      }
    }
    /**
     * Kayıtlı filtreyi siler
     *
     * @access public
     * @return boolean
     */
    public function deleteFilter(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $filterId = (int)$this->filterId;
        $getSql = "SELECT * FROM filters
                   WHERE filter_id=$filterId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $deleteSql = "DELETE FROM filters
                        WHERE filter_id=$filterId";
          $query = mysqli_query($connection,$deleteSql);
          $filterValueObj = new FilterValues();
          $filterValueObj->setFilterId($filterId);
          $deleteFilterValues = $filterValueObj->deleteFilterValues();
          return ($query && $deleteFilterValues);
        }
      }
    }
    /**
     * Kategoriye uygulanan filtreyi siler
     *
     * @access public
     * @return boolean
     */
    public function deleteCategoryFilter(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return false;
      else{
        $deleteSql = "DELETE FROM category_filters
                      WHERE category_id=$this->categoryId
                      AND filter_id=$this->filterId";
        $deleteQuery = mysqli_query($connection,$deleteSql);
        return $deleteQuery;
      }
    }
    /**
     * Kategoriye uygulanmış bütün filtreleri siler
     *
     * @access public
     * @return boolean
     */
    public function deleteAllCategoryFilter(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $deleteSql = "DELETE FROM category_filters
                      WHERE category_id=$this->categoryId";
        $deleteQuery = mysqli_query($connection,$deleteSql);
        return $deleteQuery;
      }
    }
    /**
     * Ürüne uygulanan filtreyi siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductFilters(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $deleteSql = "DELETE FROM product_filters
                      WHERE product_id=$this->productId";
        $deleteQuery = mysqli_query($connection,$deleteSql);
        return $deleteQuery;
      }
    }
    /**
     * Kategoriye uygulanmış bütün filtreleri kategoriden
     * ve bütün alt kategorilerinden siler
     *
     * @access public
     * @return boolean
     */
    public function deleteCategoryFilterWithSubCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return false;
      else{
        $delete = $this->deleteCategoryFilter();
        if(!$delete) return false;
        else {
          $getSql = "SELECT * FROM categories
                     WHERE parent_id=$this->categoryId";
          $getQuery = mysqli_query($connection,$getSql);
          while ($read = mysqli_fetch_array($getQuery)) {
            $this->categoryId = (int)$read["category_id"];
            $delete = $this->deleteCategoryFilterWithSubCategories();
            if(!$delete) return false;
          }
          return true;
        }
      }
    }
    /**
     * Tüm filtreleri döndürür
     *
     * @access public
     * @return array All filters
     */
    public function getAllFilters(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM filters";
        $query = mysqli_query($connection,$sql);
        $filters = array();
        while($read = mysqli_fetch_array($query)){
          $getSql = "SELECT * FROM filter_values
                     WHERE filter_id=".$read["filter_id"];
          $getQuery = mysqli_query($connection,$getSql);
          $values = array();
          while($valuesRead = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
            $values[] = $valuesRead;
          }
          $filters[] = array(
            "filter_id"   =>  $read["filter_id"],
            "name"        =>  $read["name"],
            "values"      =>  $values
          );
        }
        return $filters;
      }
    }
    /**
     * Kategori'ye ait tüm filtreleri döndürür
     *
     * @access public
     * @return array All category filters
     */
    public function getCategoryFilters(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $getSql = "SELECT * FROM filters
                   INNER JOIN category_filters
                   ON filters.filter_id = category_filters.filter_id
                   WHERE category_filters.category_id=$this->categoryId";

        $getQuery = mysqli_query($connection,$getSql);
        $categoryFilters = array();
        while($read = mysqli_fetch_array($getQuery)){
          $filterId = (int)$read["filter_id"];
          $sql = "SELECT * FROM filter_values
                  WHERE filter_id=$filterId";
          $query = mysqli_query($connection,$sql);
          $values = array();
          while ($valuesRead = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
            $values[] = $valuesRead;
          }
          $categoryFilters[] = array(
            "category_filter_id"  =>  $read["category_filter_id"],
            "category_id"         =>  $read["category_id"],
            "filter_id"           =>  $read["filter_id"],
            "name"                =>  $read["name"],
            "values"              =>  $values
          );
        }
        return $categoryFilters;
      }
    }
    /**
     * Seçilen filtreyi döndürür
     *
     * @access public
     * @return array Selected filter
     */
    public function getSelectedFilter(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $getSql = "SELECT * FROM filters
                   WHERE filter_id=$this->filterId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return array();
        else {
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          return $read;
        }
      }
    }
  }

?>
