<?php
  /**
   * References sınıfı, Referans ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * References sınıfı,
   * Referans için resim yükleneye ve silmeye yarar.
   *
   * Example usage:
   * if (ProducReferencestImages::insertReference()) {
   *   print "Reference inserted";
   * }
   *
   * @package References
  */
  class References{
    /**
     * Referans idsi
     *
     * @var int
     * @access protected
     */
    protected $referenceId;
    /**
     * Referans Adı
     *
     * @var string
     * @access protected
     */
    protected $referenceName;
    /**
     * Referans Telefon Numarası
     *
     * @var string
     * @access protected
     */
    protected $referencePhone;
    /**
     * Referans mail adresi
     *
     * @var string
     * @access protected
     */
    protected $referenceMail;
    /**
     * Referans adresi
     *
     * @var string
     * @access protected
     */
    protected $referenceAddress;
    /**
     * Referans Görevi
     *
     * @var string
     * @access protected
     */
    protected $referenceRole;
      /**
     * Referans Resmi
     *
     * @var aarray
     * @access protected
     */
    protected $referenceImage;
    
    /**
     * Set the $referenceId var
     *
     * @access public
     * @param int $referenceId
     */
    public function setReferenceId($referenceId){
      $this->referenceId = (int)$referenceId;
    }
    /**
     * Set the $referenceName var
     *
     * @access public
     * @param string $referenceName
     */
    public function setReferenceName($referenceName){
      $this->referenceName = trim(strip_tags($referenceName));
    }
    /**
     * Set the $referencePhone var
     *
     * @access public
     * @param string $referencePhone
     */
    public function setReferencePhone($referencePhone){
      $this->referencePhone = trim(strip_tags($referencePhone));
    }
    /**
     * Set the $referenceMail var
     *
     * @access public
     * @param string $referenceMail
     */
    public function setReferenceMail($referenceMail){
      $this->referenceMail = trim(strip_tags($referenceMail));
    }
    /**
     * Set the $referenceAddress var
     *
     * @access public
     * @param string $referenceAddress
     */
    public function setReferenceAddress($referenceAddress){
      $this->referenceAddress = trim(strip_tags($referenceAddress));
    }
    /**
     * Set the $referenceRole var
     *
     * @access public
     * @param string $referenceRole
     */
    public function setReferenceRole($referenceRole){
      $this->referenceRole = trim(strip_tags($referenceRole));
    }
    /**
     * Set the $referenceImage var
     *
     * @access public
     * @param array $referenceImage
     */
    public function setReferenceImage($referenceImage){
      if(is_array($referenceImage)){
        if(isset($referenceImage["name"]) && isset($referenceImage["type"]) && isset($referenceImage["tmp_name"]) && isset($referenceImage["size"])){
          $this->referenceImage = $referenceImage;
        }
        else $this->referenceImage = array();
      }
      else {
        $this->referenceImage = trim(strip_tags($referenceImage));
      }
    }
    /**
     * Set the $referenceCategoryId var
     *
     * @access public
     * @param int $referenceCategoryId
     */
    public function setReferenceCategoryId($referenceCategoryId){
      $this->referenceCategoryId = (int)$referenceCategoryId;
    }
    
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return reference
     */
    public function __construct() {
      $this->referenceId        = 0;
      $this->referenceName           = "";
      $this->referencePhone        = ""; 
      $this->referenceMail            = "";
      $this->referenceAddress            = "";
      $this->referenceRole      = ""; 
      $this->referenceImage     = array();
      $this->referenceCategoryId         = 0;
    }
    /**
     * Referans ekler
     *
     * @access public
     * @return boolean
     */
    public function insertReference(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $referenceName = mysqli_real_escape_string($connection,$this->referenceName);
        $referencePhone = mysqli_real_escape_string($connection,$this->referencePhone);
        $referenceMail = mysqli_real_escape_string($connection,$this->referenceMail);
        $referenceAddress = mysqli_real_escape_string($connection,$this->referenceAddress);
        $referenceRole = mysqli_real_escape_string($connection,$this->referenceRole);
        $referenceCategoryId = (int)$this->referenceCategoryId;
        $referenceImage = $this->referenceImage;
        if(!isset($referenceImage["size"]) || (int)$referenceImage["size"] == 0) return false;
        else{
          $newId = 1;
          $getSql = "SELECT * FROM reference
          ORDER BY reference_id DESC
          LIMIT 1";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 1){
            $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
            $newId = (int)$read["reference_id"];
            $newId++;
          }
          $imageObject = new Image();
          $imageLocation = PATH."/app/user-app/view/public/img/reference-images";
          $imageObject->setImageName("reference-".$newId);
          $imageObject->setImageLocation($imageLocation);
          $imageObject->setProductImage($referenceImage);

          $upload = $imageObject->uploadImage();
          if($upload != "Success") return false;
          else{
            $insertSql = "INSERT INTO reference
                          VALUES('','$referenceName','$referencePhone',
                          '$referenceMail','$referenceAddress','$referenceRole','reference-$newId.jpg',$referenceCategoryId)";
            $insertQuery = mysqli_query($connection,$insertSql);
            if($insertQuery) return true;
            else{
              unlink("$imageLocation/reference-$newId.jpg");
              return false;
            }
          }
        }
      }
    }
    /**
    * Referansı siler
    *
    * @access public
    * @return boolean
    */
    public function deleteReference(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $referenceCategoryId = (int)$this->referenceCategoryId;
        $referenceId = (int)$this->referenceId;
        $getSql = "SELECT * FROM reference
                    WHERE reference_id=$referenceId
                    AND reference_category_id=$referenceCategoryId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $referenceImage = $read["reference_image"];
          $deleteSql = "DELETE FROM reference
                        WHERE reference_id=$referenceId
                        AND reference_category_id=$referenceCategoryId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          if(!$deleteQuery) return false;
          else{
            $imageLocation = PATH."/app/user-app/view/public/img/reference-images";
            unlink("$imageLocation/".$referenceImage);
            return true;
          }
        }
      }
    }
    /**
    * Referansları döndürür
    *
    * @access public
    * @return array
    */
      public function getReferences(){
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return false;
        else{
          $sql = "SELECT * FROM reference";
          $query = mysqli_query($connection,$sql);
          $references = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $references[] = $read;
        }
        return $references;
        }
      }
      /**
    * Ana Refereans Kategorisini ve o kategoriye ait referanslarını döndürür.
    *
    * @access public
    * @return array categories and sub categories
    */
    public function getReferenceCategoriesWithReferences(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM reference
                ORDER BY reference_category_id DESC";
        $query = mysqli_query($connection,$sql);
        $referencesWithCategories = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $referencesWithCategories[$read["reference_category_id"]] = $read;
          $getSql = "SELECT * FROM referenc_categories WHERE reference_category_id=".$read["reference_category_id"];
          $getQuery = mysqli_query($connection,$getSql);
          $referencesWithCategories[$read["reference_category_id"]]["reference"] = array();
          while($referenceRead = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
            $referencesWithCategories[$read["reference_category_id"]]["reference"][] = $referenceRead;
          }
        }
        return $referencesWithCategories;
      }
    }
    /**
    * referansın numarasını Günceller
    * 
    * @access public
    * @return string
    */
    /*public function updateReferenceByOrderNumber(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $referenceId = (int)$this->referenceId;
        $referenceOrder = (int)$this->referenceOrder;
        $updateSql = "UPDATE reference
                      SET reference_order=$referenceOrder
                      WHERE reference_id=$referenceId";
        $updateQuery = mysqli_query($connection,$updateSql);
        if(!$updateQuery) return "Reference Numarası Güncellenemedi";
          else return "Success";
        }
    } */
  }
  ?>
