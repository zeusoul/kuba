<?php
  /**
   * FilterValues sınıfı, Filtre Değerleri ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * FilterValues sınıfı,
   * Yeni filtre değeri eklemeye,
   * Kayıtlı filtre değerini silmeye, güncellemeye,
   * ve gerekli filtre değerlerini döndürmeye yarar.
   *
   * Example usage:
   * if (FilterValues::insertFilterValue()) {
   *   print "Filter value added";
   * }
   *
   * @package FilterValues
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class FilterValues extends Filter{
    /**
     * Filtre değeri id'si
     *
     * @var int
     * @access protected
     */
    protected $valueId;
    /**
     * Filtre değeri ismi
     *
     * @var string
     * @access protected
     */
    protected $valueName;
    /**
     * Set the $valueId var
     *
     * @access public
     * @param int $valueId
     */
    public function setValueId($valueId){
      $this->valueId = (int)$valueId;
    }
    /**
     * Set the $valueName var
     *
     * @access public
     * @param string $valueName
     */
    public function setValueName($valueName){
      $this->valueName = trim(strip_tags($valueName));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return FilterValues
     */
    public function __construct(){
      $this->valueId   = 0;
      $this->valueName = "";
    }
    /**
     * Yeni filtre değeri ekler
     *
     * @access public
     * @return boolean
     */
    public function insertFilterValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $valueName = mysqli_real_escape_string($connection,$this->valueName);
        if(trim($valueName) == "" || is_null($valueName)) return false;
        else {
          $filterId = (int)$this->filterId;
          $getSql = "SELECT * FROM filter_values
                     WHERE value='$valueName'
                     AND filter_id=$filterId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows>0) return false;
          else{
            $insertSql = "INSERT INTO filter_values
                          VALUES('','$valueName',$filterId)";
            $insertQuery = mysqli_query($connection,$insertSql);
            return $insertQuery;
          }
        }
      }
    }
    /**
     * Kaytlı filtre değerini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateFilterValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $valueId = (int)$this->valueId;
        $filterId = (int)$this->filterId;
        $valueName = mysqli_real_escape_string($connection,$this->valueName);

        if(trim($valueName) == "" || trim($valueName) == null) return false;
        else{
          $getSql = "SELECT * FROM filter_values
                     WHERE filter_id=$filterId
                     AND value='$valueName'";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows > 0) return false;
          else{
            $updateSql = "UPDATE filter_values
                          SET value='$valueName'
                          WHERE filter_value_id=$valueId";
            $update = mysqli_query($connection,$updateSql);
            return $update;
          }
        }
      }
    }
    /**
     * Kayıtlı filtreye ait filtrenin istenilen değerini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteFilterValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $valueId = (int)$this->valueId;
        $getSql = "SELECT * FROM filter_values
                   WHERE filter_value_id=$valueId";
        $getQuery=mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else {
          $deleteSql = "DELETE FROM filter_values
                        WHERE filter_value_id=$valueId";
          $delete = mysqli_query($connection,$deleteSql);
          return $delete;
        }
      }
    }
    /**
     * Kayıtlı filtreye ait filtrenin bütün değerlerini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteFilterValues(){
      $db = new Database();
      $connection =$db->MySqlConnection();
      if(!$connection) return false;
      else {
        $filterId = (int)$this->filterId;
        $deleteSql = "DELETE FROM filter_values
                      WHERE filter_id=$filterId";
        return mysqli_query($connection,$deleteSql);
      }
    }
    /**
     * En son eklenen filtre değerini döndürür
     *
     * @access public
     * @return array last inserted filter value
     */
    public function getLastInsertedFilterValue(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql="SELECT * FROM filter_values
              ORDER BY filter_value_id DESC
              LIMIT 1";
        $query= mysqli_query($connection,$sql);
        $value = array();
        $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
        $value = $read;
        return $value;
      }
    }
    /**
     * Seçilen filtreye ait bütün değerlerini döndürür
     *
     * @access public
     * @return array Selected filter's values
     */
    public function getSelectedFilterValues(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "SELECT * FROM filter_values
                WHERE filter_id=$this->filterId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else {
          $values = array();
          while($read = mysqli_fetch_array($query)){
            $values[] = array(
              "filter_value_id"  =>  $read["filter_value_id"],
              "value"            =>  $read["value"]
            );
          }
          return $values;
        }
      }
    }
  }
?>
