<?php

  class Variant extends Platform{
    protected $variantId;
    protected $variantName;
    protected $variantCode;
    public function setVariantId($variantId){
      $this->variantId = (int)$variantId;
    }
    public function setVariantName($variantName){
      $this->variantName = trim(strip_tags($variantName));
    }
    public function setVariantCode($variantCode){
      $this->variantCode = trim(strip_tags($variantCode));
    }
    //Success or error message
    public function insertVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $variantCode = mysqli_real_escape_string($connection,$this->variantCode);
        $variantName = mysqli_real_escape_string($connection,$this->variantName);
        $getSql = "SELECT * FROM variants
                   WHERE variant_code='$variantCode'
                   AND variant_name='$variantName'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu varyant kodu ve adına ait zaten bir varyant var. Lütfen başka bir varyant kodu veya varyant giriniz.";
        else{
          $sql = "INSERT INTO variants
                  SET variant_code='$variantCode', variant_name='$variantName'";
          return mysqli_query($connection,$sql) ? "Success" : "Varyant Eklenedi";
        }
      }
    }
    public function deleteVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM variants
                WHERE variant_id=$this->variantId";
        $delete = mysqli_query($connection,$sql);
        if($delete){
          // Varyant eşleştirmelerini de silelim.
          $objPlatformVariant = new PlatformVariant();
          $objPlatformVariant->setVariantId($this->variantId);
          $res = $objPlatformVariant->deletePlatformVariantsByVariantId();

          # Varyant değerlerini silelim
          $variantValue = new VariantValue();
          $variantValue->setVariantId($this->variantId);
          $deleteValue = $variantValue->deleteVariantValuesByVariantId();

          return $deleteValue;
        }
      }
    }
    public function isVariant(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variants
                WHERE variant_id=$this->variantId";
        $query = mysqli_query($connection,$sql);
        return $query->num_rows > 0;
      }
    }
    public function getVariants(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variants";
        $query = mysqli_query($connection,$sql);
        $variants=array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $variants[] = $read;
        }
        return $variants;
      }
    }
    public function getVariantById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variants
                WHERE variant_id=$this->variantId";
        $query = mysqli_query($connection,$sql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
    public function getVariantWithValues(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM variants
                WHERE variant_id=$this->variantId";
        $query = mysqli_query($connection,$sql);
        $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
        $variantValue = new VariantValue();
        $variantValue->setVariantId($this->variantId);
        $variantValues = $variantValue->getVariantValuesByVariantId();
        $read["values"] = $variantValues;
        return $read;
      }
    }
  }

?>
