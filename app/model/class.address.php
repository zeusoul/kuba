<?php
  /**
   * Address sınıfı, adres ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Address sınıfı, yeni adres eklemeye,
   * adres silmeye, var olan adresi güncellemeye
   * ve var olan adresleri liste halinde döndürmeye yarar.
   *
   * Example usage:
   * if (Address::deleteAddressBook()) {
   *   print "Address was deleted";
   * }
   *
   * @package Address
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Address extends Person{
    /**
     * Adres defteri id'si
     *
     * @var int
     * @access protected
     */
    protected $addressBookId;
    /**
     * Adres defterinin başlığı
     *
     * @var string
     * @access protected
     */
    protected $title;
    /**
     * Adres için şehir bilgisi
     *
     * @var string
     * @access protected
     */
    protected $city;
    /**
     * Adres için ilçe bilgisi
     *
     * @var string
     * @access protected
     */
    protected $county;
    /**
     * Adres bilgisi
     *
     * @var string
     * @access protected
     */
    protected $address;
    /**
     * Adres için posta kodu bilgisi
     *
     * @var string
     * @access protected
     */
    protected $postCode;
    /**
     * Kişisel bilgi için T.C. No. bilgisi
     *
     * @var string
     * @access protected
     */
    protected $tcNo;
    /**
     * Kurumsal bilgi için Şirket Adı bilgisi
     *
     * @var string
     * @access protected
     */
    protected $companyName;
    /**
     * Kurumsal bilgi için Vergi Dairesi bilgisi
     *
     * @var string
     * @access protected
     */
    protected $taxAdministration;
    /**
     * Kurumsal bilgi için Verigi Numarası bilgisi
     *
     * @var string
     * @access protected
     */
    protected $taxNumber;
    /**
     * Adres defteri tipi (0 = Kurumsal, 1 = Bireysel)
     *
     * @var int
     * @access protected
     */
    protected $type;
    /**
     * Set the $addressBookId var
     *
     * @access public
     * @param int $addressBookId
     */
    public function setAddressBookId($addressBookId){
      $this->addressBookId = (int)$addressBookId;
    }
    /**
     * Set the $title var
     *
     * @access public
     * @param string $title
     */
    public function setTitle($title){
      $this->title = trim(strip_tags($title));
    }
    /**
     * Set the $city var
     *
     * @access public
     * @param string $city
     */
    public function setCity($city){
      $this->city = trim(strip_tags($city));
    }
    /**
     * Set the $county var
     *
     * @access public
     * @param string $county
     */
    public function setCounty($county){
      $this->county = trim(strip_tags($county));
    }
    /**
     * Set the $address var
     *
     * @access public
     * @param string $address
     */
    public function setAddress($address){
      $this->address = trim(strip_tags($address));
    }
    /**
     * Set the $postCode var
     *
     * @access public
     * @param string $postCode
     */
    public function setPostCode($postCode){
      $this->postCode = (int)$postCode;
      $this->postCode = (string)$postCode;
      $this->postCode = trim(strip_tags($postCode));
    }
    /**
     * Set the $tcNo var
     *
     * @access public
     * @param string $tcNo
     */
    public function setTcNo($tcNo){
      $this->tcNo = (int)$tcNo;
      $this->tcNo = (string)$tcNo;
      $this->tcNo = trim(strip_tags($tcNo));
    }
    /**
     * Set the $companyName var
     *
     * @access public
     * @param string $companyName
     */
    public function setCompanyName($companyName){
      $this->companyName = trim(strip_tags($companyName));
    }
    /**
     * Set the $taxAdministration var
     *
     * @access public
     * @param string $taxAdministration
     */
    public function setTaxAdministration($taxAdministration){
      $this->taxAdministration = trim(strip_tags($taxAdministration));
    }
    /**
     * Set the $taxNumber var
     *
     * @access public
     * @param string $taxNumber
     */
    public function setTaxNumber($taxNumber){
      $this->taxNumber = (int)$taxNumber;
      $this->taxNumber = (string)$taxNumber;
      $this->taxNumber = trim(strip_tags($taxNumber));
    }
    /**
     * Set the $type var
     *
     * @access public
     * @param int $type
     */
    public function setType($type){
      $this->type = (int)$type;
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Address
     */
    public function __construct() {
      $this->addressBookId      = 0;
      $this->title              = "";
      $this->city               = "";
      $this->county             = "";
      $this->address            = "";
      $this->postCode           = "";
      $this->tcNo               = "";
      $this->companyName        = "";
      $this->taxAdministration  = "";
      $this->taxNumber          = "";
      $this->type               = 2;
    }
    /**
     * Kayıtlı adres defterini siler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function deleteAddressBook(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $id = (int)$this->addressBookId;
        $userId = (int)$this->userId;
        $sql = "SELECT * FROM address_book
                WHERE address_book_id = $id
                AND user_id = $userId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows!=1) return "Silinmesi istenilen adres bulunamadı.";
        else {
          $deleteSql = "DELETE FROM address_book
                        WHERE address_book_id=$id
                        AND user_id=$userId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          return ($deleteQuery) ? "Success" : "Adres Silinemedi";
        }
      }
    }
    /**
     * Yeni adres bilgisi kaydı yapar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertAddressBook(){
      $db= new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $userId = (int)$this->userId;
        $getSql = "SELECT * FROM users
                    WHERE user_id = $userId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "Kullanıcı Bulunamadı";
        else {
          $title=mysqli_real_escape_string($connection,$this->title);
          $nameSurname=mysqli_real_escape_string($connection,$this->name);
          $tel=mysqli_real_escape_string($connection,$this->tel);
          $city=mysqli_real_escape_string($connection,$this->city);
          $county=mysqli_real_escape_string($connection,$this->county);
          $address=mysqli_real_escape_string($connection,$this->address);
          $postCode=mysqli_real_escape_string($connection,$this->postCode);
          $tcNo=mysqli_real_escape_string($connection,$this->tcNo);
          $companyName=mysqli_real_escape_string($connection,$this->companyName);
          $taxAdministration=mysqli_real_escape_string($connection,$this->taxAdministration);
          $taxNumber=mysqli_real_escape_string($connection,$this->taxNumber);

          $type = (int)$this->type;
          if($type == 1) {
            $companyName = "";
            $taxAdministration = "";
            $taxNumber = "";
          }
          else if($type == 0)$tcNo = "";

          $getSql = "SELECT * FROM address_book
                     WHERE title='$title'
                     AND user_id=$userId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows > 0) return "Bu Adres Adına Ait Zaten Bir Adres Kayıtlı";
          else if($type != 1 && $type != 0) return "Yalnızca bireysel veya kurumsal kayıt türü seçebilirsiniz.";
          else {
            $str = new Str();
            if($str->IsNullOrEmptyString(array($title,$nameSurname,$tel,$city,$county,$address))) return "Lütfen Gerekli Bilgileri Boş Girmeyiniz";
            else {
              if($type == 1 && strlen($tcNo) != 11) return "TC No 11 Haneli Olmalıdır";
              else if($type == 1 && !$this->tcNoControl($tcNo)) return "Lütfen Geçerli Bir TC No giriniz";
              else if($type == 0 && strlen($taxNumber) != 10) return "Vergi No 10 Haneli Olmalıdır";
              else if($type == 0 && ($companyName == "" || $taxAdministration == "")) return "Lütfen Gerekli Bilgileri Boş Girmeyiniz";
              else{
                $insertSql = "INSERT INTO address_book
                              VALUES ('',$userId,'$title','$nameSurname','$tel','$city','$county',
                                      '$address','$postCode','$tcNo','$companyName','$taxAdministration',
                                      '$taxNumber',$type)";
                $insertQuery = mysqli_query($connection,$insertSql);
                return ($insertQuery) ? "Success" : "Adres Eklenemedi : HATA!";
              }
            }
          }
        }
      }
    }
    /**
     * Kayıtlı adres bilgisini günceller
     *
     * @access public
     * @return string "Success" or error message
     * @param string $md5AddressBookId md5 ile kriptolanmış adres id'si
     */
    public function updateAddressBook($md5AddressBookId = ""){
      if(strlen($md5AddressBookId) != 32){
        $md5AddressBookId = md5($this->addressBookId);
      }
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $userId = (int)$this->userId;
        $md5AddressBookId = mysqli_real_escape_string($connection,$md5AddressBookId);
        $getSql = "SELECT * FROM address_book
                   WHERE md5(address_book_id)='$md5AddressBookId'
                   AND user_id=$userId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "Adres Bulunamadığı İçin Güncellenemedi";
        else {
          $read = mysqli_fetch_array($getQuery);
          $oldTitle = $read["title"];

          $title = mysqli_real_escape_string($connection,$this->title);
          $nameSurname = mysqli_real_escape_string($connection,$this->name);
          $tel = mysqli_real_escape_string($connection,$this->tel);
          $city = mysqli_real_escape_string($connection,$this->city);
          $county = mysqli_real_escape_string($connection,$this->county);
          $address = mysqli_real_escape_string($connection,$this->address);
          $postCode = mysqli_real_escape_string($connection,$this->postCode);
          $tcNo=mysqli_real_escape_string($connection,$this->tcNo);
          $companyName=mysqli_real_escape_string($connection,$this->companyName);
          $taxAdministration=mysqli_real_escape_string($connection,$this->taxAdministration);
          $taxNumber=mysqli_real_escape_string($connection,$this->taxNumber);
          $type=(int)$this->type;
          if($type == 1) {
            $companyName = "";
            $taxAdministration = "";
            $taxNumber = "";
          }
          else if($type == 0)$tcNo = "";
          /*$str = new String();
          if($str->IsNullOrEmptyString(array($title,$nameSurname,$tel,$city,$county,$address))){
            return "Lütfen Gerekli Bilgileri Boş Girmeyiniz";
          }
          else*/ if($type != 1 && $type != 0) return "Lütfen kayıt türünü kurumsal veya bireysel seçiniz";
          else if($type == 1 && strlen($tcNo) != 11) return "TC No 11 Haneli Olmalıdır";
          else if($type == 1 && !$this->tcNoControl($tcNo)) return "Lütfen Geçerli Bir TC No giriniz";
          else if($type == 0 && strlen($taxNumber) != 10) return "Vergi No 10 Haneli Olmalıdır";
          else if($type == 0 && ($companyName == "" || $taxAdministration == "")) return "Lütfen Gerekli Bilgileri Boş Girmeyiniz";
          else{
            $getSql = "SELECT * FROM address_book
                       WHERE user_id = $userId
                       AND title = '$title'";

            $getQuery = mysqli_query($connection,$getSql);
            if($getQuery->num_rows > 0 && $oldTitle != $title) return "Bu Adres Adına Ait Zaten Bir Adres Kayıtlı";
            else {
              $updateSql = "UPDATE address_book
                            SET title = '$title', name_surname = '$nameSurname',
                                tel = '$tel', city = '$city', county = '$county',
                                address = '$address', post_code = '$postCode',
                                tc_no = '$tcNo', company_name = '$companyName',
                                tax_administration = '$taxAdministration',
                                tax_number = '$taxNumber', type = $type
                            WHERE md5(address_book_id) = '$md5AddressBookId'
                            AND user_id = $userId";
              $updateQuery = mysqli_query($connection,$updateSql);

              return ($updateQuery) ? "Success" : "Adres Güncellenemedi";
            }
          }
        }
      }
    }
    /**
     * Kullanıcıya ait kayıtlı adres bilgisini döndürür
     *
     * @access public
     * @return array adres bilgileri
     */
    public function getAddressBooks(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $userId = (int)$this->userId;
        $sql = "SELECT * FROM address_book
                WHERE user_id=$userId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else {
          $address = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $address[] = $read;
          }
          return $address;
        }
      }
    }
    /**
     * Kayıtlı adres defterinin bilgilerini döndürür.
     *
     * @access public
     * @return array adres defteri bilgileri
     * @param string $md5AddressBookId md5 ile kriptolanmış adres id'si
     */
    public function getAddressBook($md5AddressBookId = ""){
      if(strlen($md5AddressBookId) != 32){
        $md5AddressBookId = md5($this->addressBookId);
      }
      $db= new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $userId = (int)$this->userId;
        $md5AddressBookId = mysqli_real_escape_string($connection,$md5AddressBookId);
        $getSql = "SELECT * FROM address_book
                   WHERE md5(address_book_id)='$md5AddressBookId'
                   AND user_id=$userId";
        $getQuery=mysqli_query($connection,$getSql);
        if($getQuery->num_rows!=1) return false;
        else {
          return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        }
      }
    }
  }

?>
