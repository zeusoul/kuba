<?php

  class PlatformCategory extends Category{
    protected $platformCategoryId;
    protected $platformCategoryCode;
    public function setPlatformCategoryId($platformCategoryId){
      $this->platformCategoryId = $platformCategoryId;
    }
    public function setPlatformCategoryCode($platformCategoryCode){
      $this->platformCategoryCode = $platformCategoryCode;
    }

    public function insertPlatformCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_categories
                   WHERE category_id=$this->categoryId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if(is_array($read) && count($read) > 0){
          //UPDATE
          $sql = "UPDATE platform_categories
                  SET platform_category_code='$this->platformCategoryCode'
                  WHERE category_id=$this->categoryId
                  AND platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
        else{
          //INSERT
          $sql = "INSERT INTO platform_categories
                  SET platform_category_code='$this->platformCategoryCode',
                      category_id=$this->categoryId,
                      platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    public function deletePlaformCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_categories
                WHERE platform_category_id=$this->platformCategoryId";
        return mysqli_query($connection,$sql);
      }
    }
    public function deletePlaformCategoriesByCategoryId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_categories
                WHERE category_id=$this->categoryId";
        return mysqli_query($connection,$sql);
      }
    }

    public function getPlatformCategoriesByPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_categories
                   INNER JOIN categories ON platform_categories.category_id = categories.category_id
                   INNER JOIN platforms ON platform_categories.platform_id = platforms.platform_id
                   WHERE platform_categories.platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $platformCategories = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $platformCategories[] = $read;
        }
        return $platformCategories;
      }
    }
    public function getPlatformCategoryByCategoryIdAndPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM platform_categories
                   INNER JOIN platforms ON platform_categories.platform_id = platforms.platform_id
                   WHERE platform_categories.category_id=$this->categoryId
                   AND platform_categories.platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $result = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        if(!is_array($result) || count($result) == 0) return array();
        else return $result;
      }
    }
    public function getPlatformCategoriesByCategoryId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_categories
                   INNER JOIN platforms ON platform_categories.platform_id = platforms.platform_id
                   WHERE platform_categories.category_id=$this->categoryId";
        $getQuery = mysqli_query($connection,$getSql);
        $platformCategories = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $platformCategories[] = $read;
        }
        return $platformCategories;
      }
    }
  }

?>
