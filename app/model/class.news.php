<?php
  /**
   * News sınıfı, haberler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * News sınıfı,
   * Yenş haber kaydetmeye ve silmeye yarar.
   *
   * Example usage:
   * if (News::insertNew()) {
   *   print "New inserted";
   * }
   *
   * @package News
  */
  class News{
    /**
     * Haber idsi
     *
     * @var int
     * @access protected
     */
    protected $newId;
    /**
     * Haber başlığı
     *
     * @var string
     * @access protected
     */
    protected $newTitle;
    /**
     * Haber açıklaması
     *
     * @var string
     * @access protected
     */
    protected $newDesc;
    /**
     * Haber içeriği
     *
     * @var string
     * @access protected
     */
    protected $newContent;
    /**
     * Haber resmi
     *
     * @var array
     * @access protected
     */
    protected $newImage;
    /**
     * Set the $newId var
     *
     * @access public
     * @param int $newId
     */
    public function setNewId($newId){
      $this->newId = (int)$newId;
    }
    /**
     * Set the $newTitle var
     *
     * @access public
     * @param string $newTitle
     */
    public function setNewTitle($newTitle){
      $this->newTitle = trim(strip_tags($newTitle));
    }
    /**
     * Set the $newDesc var
     *
     * @access public
     * @param string $newDesc
     */
    public function setNewDesc($newDesc){
      $this->newDesc = trim(strip_tags($newDesc));
    }
    /**
     * Set the $newContent var
     *
     * @access public
     * @param string $newContent
     */
    public function setNewContent($newContent){
      $this->newContent = trim($newContent);
    }
    /**
     * Set the $newImage var
     *
     * @access public
     * @param array $newImage
     */
    public function setNewImage($newImage){
      if(is_array($newImage)){
        if(isset($newImage["name"]) && isset($newImage["type"]) && isset($newImage["tmp_name"]) && isset($newImage["size"])){
          $this->newImage = $newImage;
        }
        else $this->newImage = array();
      }
      else {
        $this->newImage = trim(strip_tags($newImage));
      }
    }

    /**
     * Haber ekler
     *
     * @access public
     * @return string Success or error message
     */
    public function insertNew(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $newImage = $this->newImage;
        $newTitle = $this->newTitle;
        $newDesc = $this->newDesc;
        $newContent = $this->newContent;

        $date = date("d.m.Y");
        $sql = "INSERT INTO news
                VALUES('','$newTitle','$newContent','not.jpg','$date','$newDesc')";
        $query = mysqli_query($connection,$sql);
        if(!$query) return "Haber kaydedilemedi";
        else if(!isset($newImage["size"]) || (int)$newImage["size"] == 0) return "Success";
        else{
          $sql = "SELECT * FROM news ORDER BY new_id DESC LIMIT 1";
          $query = mysqli_query($connection,$sql);
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $newId = $read["new_id"];
          $imageName = "new-image-$newId";

          $imageObject = new Image();
          $imageLocation = PATH."/app/user-app/view/public/img/new-images";
          $imageObject->setImageName($imageName);
          $imageObject->setImageLocation($imageLocation);
          $imageObject->setProductImage($newImage);

          $upload = $imageObject->uploadImage();
          if($upload != "Success") return "Haber kaydedildi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
          else{
            $updateSql = "UPDATE news
                          SET new_image='$imageName.jpg'
                          WHERE new_id=$newId";
            $updateQuery = mysqli_query($connection,$updateSql);
            return ($updateQuery) ? "Success" : "Haber kaydedildi, resim yüklendi fakat haberin resim alanı güncellenemedi";
          }
        }
      }
    }
    /**
     * Haberi ID'ye göre siler.
     *
     * @access public
     * @return boolean
     */
     public function deleteNew(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return false;
       else{
         $newId = $this->newId;
         $sql = "DELETE FROM news WHERE new_id=$newId";
         $query = mysqli_query($connection,$sql);
         if($query){
           $path = PATH."/app/user-app/view/public/img/new-images";
           $deleteImage = unlink("$path/new-image-$newId.jpg");
         }
         return $query;
       }
     }
     /**
      * Haberi günceller
      *
      * @access public
      * @return string "Success" or error message
      */
     public function updateNew(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return "Bağlantı Hatası";
       else {
         $newId = (int)$this->newId;
         $newImage = $this->newImage;
         $newTitle = mysqli_real_escape_string($connection,$this->newTitle);
         $newDesc = mysqli_real_escape_string($connection,$this->newDesc);
         $content = mysqli_real_escape_string($connection,$this->newContent);

         $getSql = "SELECT * FROM news
                    WHERE new_id=$newId";
         $getQuery = mysqli_query($connection,$getSql);
         $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);


         $updateSql = "UPDATE news
                       SET new_title='$newTitle', new_desc='$newDesc', new_content='$content'
                       WHERE new_id=$newId";
         $updateQuery = mysqli_query($connection,$updateSql);
         if(!$updateQuery) return "Haber Güncellenemedi";
         else{
           if(isset($newImage["size"]) && (int)$newImage["size"] > 0){
             $imageName = "new-image-$newId";
             $imageObject = new Image();
             $imageLocation = PATH."/app/user-app/view/public/img/new-images";
             $imageObject->setImageName("$imageName");
             $imageObject->setImageLocation($imageLocation);
             $imageObject->setProductImage($newImage);

             $upload = $imageObject->uploadImage();
             if($upload != "Success") return "Haber Güncellendi Fakat Resim şu nedenden dolayı yüklenemedi : $upload";
             else {
               $sql = "UPDATE news SET new_image='$imageName.jpg' WHERE new_id=$newId";
               $query = mysqli_query($connection,$sql);
               return "Success";
             }
           }
           else return "Success";
         }
       }
     }
    /**
     * Haberleri döndürür.
     *
     * @access public
     * @return array
     */
     public function getNews(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return array();
       else{
         $sql = "SELECT * FROM news ORDER BY new_id DESC";
         $query = mysqli_query($connection,$sql);
         $news = array();
         while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
           $news[] = $read;
         }
         return $news;
       }
     }
     /**
      * Haberin detaylarını döndürür.
      *
      * @access public
      * @return array
      */
      public function getNewDetails(){
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
          $newId = $this->newId;
          $sql = "SELECT * FROM news WHERE new_id=$newId";
          $query = mysqli_query($connection,$sql);
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          return $read;
        }
      }
  }

?>
