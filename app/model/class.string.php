<?php
  /**
   * Str sınıfı, Kelimelerle ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Str sınıfı,
   * Kelimenin veya kelimeler içeren dizinin içerisinde
   * boş değer olup olmadığını söyleyen sınıftır.
   *
   *
   * Example usage:
   * if (!Str::IsNullOrEmptyString("murat")) {
   *   print "String is not null or empty";
   * }
   *
   * @package Str
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Str{
    /**
     * Parametre ile gelen kelimenin yada
     * kelimeler içeren dizinin boş olmadığını kontrol eder
     * ve boş ise true boş değilse false değerini döndürür.
     *
     * @access public
     * @return boolean
     * @param string or @param array
     */
    public function IsNullOrEmptyString($str){
      if(is_array($str)){
        foreach ($str as $deger) {
          if(!isset($deger) || trim($deger) == '' || trim($deger) == '' || trim($deger) == null){
            return true;
          }
        }
        return false;
      }
      else return (!isset($str) || trim($str) == '');
    }
  }

?>
