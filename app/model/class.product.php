<?php
  /**
   * Product sınıfı, Ürünler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Product sınıfı,
   * Yeni ürün kaydetmeye,
   * Kayıtlı ürünü güncellemeye, silmeye
   * ve ürün bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Product::insertPage() === "Success") {
   *   print "Product Inserted";
   * }
   *
   * @package Product
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Product extends Category{
    /**
     * Ürün id'si
     *
     * @var int
     * @access protected
     */
    protected $productId;
    /**
     * Ürünün derecelendirme puanı (1-5 arası)
     *
     * @var int
     * @access protected
     */
    protected $productRatePoint;
    /**
     * Ürün resmi
     *
     * @var array
     * @access protected
     */
    protected $productImage;
    /**
     * Ürünün başlığı
     *
     * @var string
     * @access protected
     */
    protected $title;
    /**
     * Ürünün alt başlığı
     *
     * @var string
     * @access protected
     */
    protected $subTitle;
    /**
     * Ürünün markası
     *
     * @var string
     * @access protected
     */
    protected $brandId;
    /**
     * Ürünün modeli
     *
     * @var string
     * @access protected
     */
    protected $model;
    /**
     * Ürün kodu
     *
     * @var string
     * @access protected
     */
    protected $productCode;
    /**
     * Ürünün açıklaması
     *
     * @var string
     * @access protected
     */
    protected $content;
    /**
     * Ürünün durumu (0 = Gizli, 1 = Yayında)
     *
     * @var int
     * @access protected
     */
    protected $status;

    /**
     * Set the $productId var
     *
     * @access public
     * @param int $productId
     */
    public function setProductId($productId){
      $this->productId = (int)$productId;
    }
    /**
     * Set the $productRatePoint var
     *
     * @access public
     * @param int $productRatePoint
     */
    public function setProductRatePoint($productRatePoint){
      $this->productRatePoint = (int)$productRatePoint;
    }
    /**
     * Set the $productImage var
     *
     * @access public
     * @param array $productImage
     */
    public function setProductImage($productImage){
      if(is_array($productImage)){
        if(isset($productImage["name"]) && isset($productImage["type"]) && isset($productImage["tmp_name"]) && isset($productImage["size"])){
          if($productImage["name"] == "" || $productImage["type"] == "" || $productImage["tmp_name"] == "" ||$productImage["size"] <= 0){
            $this->productImage = array();
          }
          else $this->productImage = $productImage;
        }
        else $this->productImage = array();
      }
      else {
        $this->productImage = trim(strip_tags($productImage));
      }
    }
    /**
     * Set the $title var
     *
     * @access public
     * @param string $title
     */
    public function setTitle($title){
      $this->title = trim(strip_tags($title));
    }
    /**
     * Set the $subTitle var
     *
     * @access public
     * @param string $subTitle
     */
    public function setSubTitle($subTitle){
      $this->subTitle = trim(strip_tags($subTitle));
    }
    /**
     * Set the $brandId var
     *
     * @access public
     * @param int $brandId
     */
    public function setBrandId($brandId){
      $this->brandId = (int)$brandId;
    }
    /**
     * Set the $model var
     *
     * @access public
     * @param string $model
     */
    public function setModel($model){
      $this->model = trim(strip_tags($model));
    }
    /**
     * Set the $productCode var
     *
     * @access public
     * @param string $productCode
     */
    public function setProductCode($productCode){
      $this->productCode = trim(strip_tags($productCode));
    }
    /**
     * Set the $content var
     *
     * @access public
     * @param string $content
     */
    public function setContent($content){
      $this->content = trim($content);
    }
    /**
     * Set the $status var
     *
     * @access public
     * @param int $status
     */
    public function setStatus($status){
      $this->status = trim(strip_tags($status));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Product
     */
    public function __construct() {
      $this->productId        = 0;
      $this->productRatePoint = 0;
      $this->productImage     = array();
      $this->title            = "";
      $this->subTitle         = "";
      $this->brandId          = 0;
      $this->model            = "";
      $this->productCode      = "";
      $this->content          = "";
      $this->status           = -1;
    }

    /**
     * Ürün ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertProduct(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $productImage = $this->productImage;
        $title = mysqli_real_escape_string($connection,$this->title);
        $subTitle = mysqli_real_escape_string($connection,$this->subTitle);
        $brandId = $this->brandId;
        $model = mysqli_real_escape_string($connection,$this->model);
        $productCode = mysqli_real_escape_string($connection,$this->productCode);
        $content = mysqli_real_escape_string($connection,$this->content);
        $status = mysqli_real_escape_string($connection,$this->status);
        $categoryId = (int)$this->categoryId;
        $str = new Str();
        if($str->IsNullOrEmptyString(array($title,$subTitle,$model,$productCode,$status))){
          return "Zorunlu Alanlar Boş Girilemez ( * ile gösterilen alanlar)";
        }
        else{
          $getSql = "SELECT * FROM products
                     WHERE product_code='$productCode'
                     AND category_id=$categoryId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows > 0) return "Bu Ürün Zaten Mevcut. Lütfen Ürün İşlemleri Sayfasına Gidiniz.";
          else {
            $insertSql = "INSERT INTO products
                          VALUES ('','$title','$content',
                                  $brandId,'$model','',
                                  '$productCode',$categoryId,$status,
                                  'not.jpg','$subTitle',
                                  0,0,0,0,0)";
            $insertQuery = mysqli_query($connection,$insertSql);
            if(!$insertQuery) return "Ürün Kaydedilmedi";
            else if(!is_array($productImage) || count($productImage) <= 0) return "Success";
            else {
              $imageObject = new Image();
              $imageLocation = PATH."/app/user-app/view/public/img/product-images";
              $imageObject->setImageName($productCode);
              $imageObject->setImageLocation($imageLocation);
              $imageObject->setProductImage($productImage);

              $upload = $imageObject->uploadImage();
              if($upload != "Success") return "Ürün kaydedildi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
              else {
                $updateSql = "UPDATE products
                              SET image='$productCode.jpg'
                              WHERE product_code='$productCode'
                              AND category_id=$categoryId";
                $updateQuery = mysqli_query($connection,$updateSql);
                return ($updateQuery) ? "Success" : "Ürün kaydedildi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
              }
            }
          }
        }
      }
    }
    /**
     * Kayıtlı ürünü siler
     *
     * @access public
     * @return string
     */
    public function deleteProduct(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $productId = (int)$this->productId;
        $getSql = "SELECT * FROM products
                   WHERE product_id=$productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "Silmek istediğiniz ürün bulunamadı";
        else{
          $read = mysqli_fetch_array($getQuery);
          $productCode = $read["product_code"];
          $deleteSql = "DELETE FROM products
                        WHERE product_id=$productId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          if(!$deleteQuery) return "Ürün silinemedi";
          else{
            $returnedValue = "Ürün silindi <br>";

            $filterObj = new Filter();
            $filterObj->setProductId($productId);
            $deleteFilters = $filterObj->deleteProductFilters();
            if(!$deleteFilters) $returnedValue .= "Ürünün filtreleri silinemedi! <br>";
            #ürüne ait yorumları silelim
            $commentObj = new ProductComments();
            $commentObj->setProductId($productId);
            $deleteComments = $commentObj->deleteProductComments();
            if(!$deleteComments) $returnedValue .= "Ürüne ait youmlar silinemedi! <br>";
            #ürüne ait özellikleri silelim
            $infoObj = new ProductInformation();
            $infoObj->setProductId($productId);
            $deleteInfo = $infoObj->deleteProductInformations();
            if(!$deleteInfo) return $returnedValue .= "Ürünün özellikleri silinemedi! <br>";
            #ürüne ait varyantları silelim
            $productVariant = new ProductVariant();
            $productVariant->setProductId($productId);
            $deleteVariants = $productVariant->deleteProductVariantsByProductId();
            if(!$deleteVariants) return $returnedValue .= "Ürünün varyantları silinemedi! <br>";
            #ürünün platform eşleşmelerini silelim
            $platformProductAttribute = new PlatformProductAttribute();
            $platformProductAttribute->setProductId($this->productId);
            $result = $platformProductAttribute->deletePlatformProductAttributesByProductId();

            #Ürüne ait resimleri silelim
            $path = PATH."/app/user-app/view/public/img/product-images";
            $deleteImage = unlink("$path/$productCode.jpg");
            if(!$deleteImage) $returnedValue .= "Ürünün resimleri silinemedi! <br>";
            return $returnedValue;
          }
        }
      }
    }
    /**
     * Kayıtlı ürünü günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateProduct(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı hatası";
      else{
        $productImage = $this->productImage;
        $title = mysqli_real_escape_string($connection,$this->title);
        $subTitle = mysqli_real_escape_string($connection,$this->subTitle);
        $brandId = $this->brandId;
        $model = mysqli_real_escape_string($connection,$this->model);
        $productCode = mysqli_real_escape_string($connection,$this->productCode);
        $content = mysqli_real_escape_string($connection,$this->content);
        $status = mysqli_real_escape_string($connection,$this->status);
        $categoryId = (int)$this->categoryId;
        $productId=(int)$this->productId;
        if($productId == 0) return "Ürün bulunamadı!";
        else if($discountPrice > $price) return "Ürünün indirimli fiyatı, ürünün fiyatından büyük olamaz";
        else{
          $updateSql = "UPDATE products
                        SET title='$title', sub_title='$subTitle',
                            brand_id=$brandId, model_name='$model',
                            product_code='$productCode', content='$content',
                            release_status=$status, category_id=$categoryId
                        WHERE product_id=$productId";
          $updateQuery = mysqli_query($connection,$updateSql);
          if(!$updateQuery) return "Ürün güncellenemedi!";
          else{
            $productImage = $this->productImage;
            if(!is_array($productImage) || count($productImage) <= 0) return "Success";
            else {
              $imageObject = new Image();
              $imageLocation = PATH."/app/user-app/view/public/img/product-images";
              $imageObject->setImageName($productCode);
              $imageObject->setImageLocation($imageLocation);
              $imageObject->setProductImage($productImage);

              $upload = $imageObject->uploadImage();
              if($upload != "Success") return "Ürün güncellendi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
              else {
                $updateSql = "UPDATE products
                              SET image='$productCode.jpg'
                              WHERE product_code='$productCode'
                              AND category_id=$categoryId";
                $updateQuery = mysqli_query($connection,$updateSql);
                return ($updateQuery) ? "Success" : "Ürün güncellendi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
              }
            }
          }
        }
      }
    }
    /**
     * Ürünün görütülenme sayısını arttırır
     *
     * @access public
     * @return boolean
     */
    public function increaseProductViews(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productId = (int)$this->productId;
        $getSql = "SELECT * FROM products
                   WHERE product_id=$productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1)return false;
        else{
          $sql = "UPDATE products
                  SET views = views+1
                  WHERE product_id=$productId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    /**
     * Ürünün sepete eklenme sayısını arttırır
     *
     * @access public
     * @return boolean
     */
    public function increasingTheNumberOfAdditionsToTheBasket(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productId = (int)$this->productId;
        $getSql = "SELECT * FROM products
                   WHERE product_id=$productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $sql = "UPDATE products
                  SET add_to_basket = add_to_basket+1
                  WHERE product_id=$productId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    /**
     * Ürünün sepetten çıkarılma sayısını arttırır
     *
     * @access public
     * @return boolean
     */
    public function increasingTheNumberOfExitsFromTheBasket(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productId = (int)$this->productId;
        $getSql = "SELECT * FROM products
                   WHERE product_id=$productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $sql = "UPDATE products
                  SET get_out_the_basket = get_out_the_basket+1
                  WHERE product_id=$productId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    /**
     * Kayıtlı tüm ürünleri döndürür
     *
     * @access public
     * @return array all products
     */
    public function getAllProducts($pageNumber = 1, $limit = 20){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $offset = ($pageNumber-1)*20;
        $getSql = "SELECT * FROM products
                   LIMIT $offset, $limit";
        $getQuery = mysqli_query($connection,$getSql);
        $products = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          if(is_array($read["variants"]) && count($read["variants"]) > 0){
            $productDetails = $read;
          }
          $products["products"][] = $read;
        }
        $sql = "SELECT * FROM products";
        $query = mysqli_query($connection,$sql);
        $productsCount = $query->num_rows;
        $products["total_products_count"] = $productsCount;
        return $products;
      }
    }
    /**
     * Arama sonucuna göre bulunan tüm ürünleri döndürür
     *
     * @access public
     * @return array all products
     * @param array $get filters
     */
    public function getSearchProducts($get = array()){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        if(!is_array($get) || count($get) <= 0){
          if(trim($get) == "") return array();
          else {
            $value = trim(mysqli_real_escape_string($connection,$get));
            $sql = "SELECT * FROM products
                    INNER JOIN brands ON products.brand_id=brands.brand_id
                    WHERE (products.title LIKE '%$value%'
                    OR products.model_name LIKE '%$value%'
                    OR brands.brand_name LIKE '%$value%'
                    OR products.product_code LIKE '%$value%')";
            $values = explode(' ',$value);

            $sql = "SELECT * FROM products
                    INNER JOIN brands ON products.brand_id=brands.brand_id";
            $i=0;
            $products = array();
            foreach ($values as $key => $value) {
              $value = trim(mysqli_real_escape_string($connection,$value));
              if($value != ""){
                if($i++ == 0) $sql .= " WHERE ";
                else $sql.=" AND ";
                $sql .= "
                (
                  products.title LIKE '%$value%'
                  OR products.model_name LIKE '%$value%'
                  OR brands.brand_name LIKE '%$value%'
                  OR products.product_code LIKE '%$value%'
                ) ";
              }
            }
            $sql .= " AND (release_status = 1)";
            $query = mysqli_query($connection,$sql);
            while ($read = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
              $productVariant = new ProductVariant();
              $productVariant->setProductId($read["product_id"]);
              $read["variants"] = $productVariant->getProductVariantsByProductId();
              $products[] = $read;
            }
            return $products;
          }
        }
        else{
          $getSql = "SELECT * FROM products
                     WHERE ";
          $i=0;
          foreach ($get as $key => $value) {
            if($i++>0) $getSql.=" AND ";
            $getSql .= "$key LIKE '%$value%'";
          }
          $getQuery = mysqli_query($connection,$getSql);
          $products = array();
          $i=0;
          while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
            $price = (float)$read["price"];
            $discountPrice = (float)$read["discount_price"];
            $fark = $price-$discountPrice;
            $rate = (int)(($fark * 100) / $price);
            $products[$i] = $read;
            $products[$i++]["rate"] = $rate;

            $productVariant = new ProductVariant();
            $productVariant->setProductId($read["product_id"]);
            $read["variants"] = $productVariant->getProductVariantsByProductId();
          }
          return $products;
        }
      }
    }
    /**
     * Son eklenen ürünü döndürür
     *
     * @access public
     * @return array last products
     */
    public function getLastProduct(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $products = array();
        $getSql = "SELECT * FROM products
                   ORDER BY product_id DESC
                   LIMIT 1";
        $getQuery = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
      }
    }
    /**
     * Son eklenen 4 ürünü döndürür
     *
     * @access public
     * @return array last products
     */
    public function getLastProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $products = array();
        $getSql = "SELECT * FROM products
                   WHERE release_status = 1
                   ORDER BY product_id DESC
                   LIMIT 8";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $this->setProductId($read["product_id"]);
          $products[] = $this->getProductDetails();
        }
        return $products;
      }
    }
    /**
     * İstenilen kategoriye ait ürünleri döndürür
     *
     * @access public
     * @return array products
     */
    public function getProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $products = array();

        $getSql = "SELECT * FROM products
                   WHERE category_id=$this->categoryId
                   AND release_status=1";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $productImage = new ProductImages();
          $productImage->setProductId($read["product_id"]);
          $read["product_images"] = $productImage->getProductImages();

          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $variants = $productVariant->getProductVariantsByProductId();

          $read["product_price"] = $variants[0]["discount_price"];
          $read["variants"] = $variants;
          if(is_array($read["variants"]) && count($read["variants"]) > 0){
            if(loginState()){
              $favorite = new Favorite();
              $favorite->setProductId($read["product_id"]);
              $favorite->setUserId($_SESSION[sessionPrefix()."user_id"]);
              $favoriteState = $favorite->getFavoriteState();
              $read["favorite"] = $favoriteState ? 1 : 0;
            }
            else $read["favorite"] = 0;
            $products[] = $read;
          }
        }
        $sql = "SELECT * FROM categories
                WHERE parent_id=$this->categoryId";
        $query = mysqli_query($connection,$sql);
        while($read = mysqli_fetch_array($query)){
          $categoryId = (int)$read["category_id"];
          $productObject = new Product();
          $productObject->categoryId = $categoryId;
          $returnedValue = $productObject->getProducts();
          if(is_array($returnedValue) && count($returnedValue)>0){
            foreach ($returnedValue as $key => $value) {
              $products[] = $value;
            }
          }
        }
        return $products;
      }
    }
    /**
     * İstenilen kategoriye ait
     * Belirtilen productId haricindeki ilk 6 ürünü döndürür
     *
     * @access public
     * @return array products
     */
    public function getRelatedProducts($productId){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $products = array();
        $exchangeObject = new Exchange();
        $usd_selling = $exchangeObject->usd_selling;
        $euro_selling = $exchangeObject->euro_selling;

        $getSql = "SELECT * FROM products
                   WHERE category_id=$this->categoryId
                   AND release_status=1
                   AND product_id != $productId
                   LIMIT 6";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $productImage = new ProductImages();
          $productImage->setProductId($read["product_id"]);
          $read["product_images"] = $productImage->getProductImages();

          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          if(is_array($read["variants"]) && count($read["variants"]) > 0){
            $products[] = $read;
          }
        }
        $sql = "SELECT * FROM categories
                WHERE parent_id=$this->categoryId";
        $query = mysqli_query($connection,$sql);
        while($read = mysqli_fetch_array($query)){
          $categoryId = (int)$read["category_id"];
          $productObject = new Product();
          $productObject->categoryId = $categoryId;
          $returnedValue = $productObject->getProducts();
          if(is_array($returnedValue) && count($returnedValue)>0){
            foreach ($returnedValue as $key => $value) {
              $products[] = $value;
            }
          }
        }
        return $products;
      }
    }
    /**
     * Filtreleme sonucuna göre bulunan tüm ürünleri döndürür
     *
     * @access public
     * @return array filtered products
     * @param array $get filters
     */
    public function getFilteredProducts($get = array()){
      if(!is_array($get) || count($get) < 0) return array();
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
          $i = 0;
          $max = 0;
          $min = 0;
          $rating = 0;
          $products = array();
          $getIdSql = "SELECT DISTINCT products.product_id FROM products
                       INNER JOIN product_filters
                       ON products.product_id=product_filters.product_id
                       INNER JOIN product_variants
                       ON products.product_id = product_variants.product_id
                       WHERE products.category_id=$this->categoryId ";



          if(isset($get["min"]) ||isset($get["max"])){
            if(isset($get["min"])) $min = (int)$get["min"];
            if(isset($get["max"])) $max = (int)$get["max"];
            if($min > $max) {
              $temp = $max;
              $max = $min;
              $min = $temp;
            }
            if($min > 0) $getIdSql .= " AND product_variants.discount_price >= $min ";
            if($max > 0) $getIdSql .= " AND product_variants.discount_price <= $max ";
          }
          if(isset($get["rating"])){
            $rating = (float)$get["rating"];
            if($rating > 0 &&  $rating < 6) {
              $getIdSql .= " AND (Ceiling(products.rating) = $rating OR Ceiling(products.rating) = 0)";
            }
          }
          $orSql = "";
          if(isset($get["filters"]) && is_array($get["filters"]) && count($get["filters"]) > 0){
            $filters = $get["filters"];
            $orSql .= " AND ";
            $orSql .= "  ( ";
            $control = 0;
            foreach ($filters as $filter => $value) {
              $filterId = (int)$filter;
              $valueId = (int)$value;
                if($control++ > 0 ) $orSql .= " OR ";
                $orSql .= " (product_filters.filter_id=$filterId AND product_filters.filter_value_id=$valueId) ";
            }
            $orSql .= ")";
          }

          $getIdSql .= $orSql;
          $getIdQuery = mysqli_query($connection,$getIdSql);
          while ($read = mysqli_fetch_array($getIdQuery)) {
            $productId = (int)$read["product_id"];
            $getSql = "SELECT * FROM products
                       INNER JOIN product_filters
                       ON products.product_id = product_filters.product_id
                       WHERE products.product_id=$productId
                       AND release_status=1".$orSql;
            $getQuery = mysqli_query($connection,$getSql);
            if(!isset($get["filters"]) || $getQuery->num_rows == count($get["filters"])){
              $productObject = new Product();
              $productObject->setProductId($productId);
              $products[$i] = $productObject->getProductDetails();

              $exchange = new Exchange();
              $usd_selling = (float)$exchange->usd_selling;
              $euro_selling = (float)$exchange->euro_selling;
              $priceTL = (float)$products[$i]["discount_price"];

              if($products[$i]["currency"] == "EURO")$priceTL *= $euro_selling;
              else if($products[$i]["currency"] == "USD")$priceTL *= $usd_selling;
              $priceTL = (int)ceil($priceTL);
              $products[$i]["price_tl"] = $priceTL;
              ++$i;
            }
          }
          $sql = "SELECT * FROM categories
                  WHERE parent_id=$this->categoryId ";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows > 0){
            while($subRead = mysqli_fetch_array($query)){
              $categoryId = (int)$subRead["category_id"];
              $productObject = new Product();
              $productObject->setCategoryId($categoryId);
              $returnedProducts = $productObject->getFilteredProducts($get);

              foreach ($returnedProducts as $key => $returnedProduct){
                $price = (float)$returnedProduct["price"];
                $discountPrice = (float)$returnedProduct["discount_price"];
                $fark = $price-$discountPrice;
                $rate = ($price <= 0) ? 0 : (int)(($fark * 100) / $price);

                $products[$i] = $returnedProduct;
                $products[$i++]["rate"] = $rate;
              }
            }
          }
          return $products;
        }
      }
    }
    /**
     * İstenilen ürün var mı yok mu?
     *
     * @access public
     * @return boolean
     */
    public function isProduct(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM products
                WHERE product_id=$this->productId";
        $query = mysqli_query($connection,$sql);
        $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
        return (is_array($read) && count($read)>0);
      }
    }
    /**
     * İstenilen ürünün detaylarını döndürür
     *
     * @access public
     * @return array product details
     */
    public function getProductDetails(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql="";
        if((int)$this->productId > 0) {
          $sql = "SELECT * FROM products
                  INNER JOIN brands ON products.brand_id=brands.brand_id
                  INNER JOIN categories ON products.category_id = categories.category_id
                  WHERE products.product_id=$this->productId";
        }
        else if(!is_null(trim($this->productCode)) && trim($this->productCode) != "") {
          $productCode = trim(mysqli_real_escape_string($connection,$this->productCode));
          $sql = "SELECT * FROM products
                  INNER JOIN categories ON products.category_id = categories.category_id
                  WHERE products.product_code='$productCode'";
        }
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else {
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();

          $productImage = new ProductImages();
          $productImage->setProductId($read["product_id"]);
          $read["product_images"] = $productImage->getProductImages();

          if(loginState()){
            $favorite = new Favorite();
            $favorite->setProductId($read["product_id"]);
            $favorite->setUserId($_SESSION[sessionPrefix()."user_id"]);
            $favoriteState = $favorite->getFavoriteState();
            $read["favorite"] = $favoriteState ? 1 : 0;
          }
          else $read["favorite"] = 0;

          return $read;
        }
      }
    }
    /**
     * productId'ye göre ürünü döndürür
     *
     * @access public
     * @return array
     */
    public function getProductById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM products
                WHERE product_id=$this->productId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else {
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          return $read;
        }
      }
    }
    /**
     * İstenilen ürüne ait filtrelerini ve değerlerini döndürür
     *
     * @access public
     * @return array product filters and values
     */
    public function getProductFiltersAndValues(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $productId = (int)$this->productId;
        $getProductSql = "SELECT * FROM products
        WHERE product_id=$productId";
        $getProductQuery = mysqli_query($connection,$getProductSql);
        if($getProductQuery->num_rows != 1) return array();
        else {
          $getSql = "SELECT * FROM product_filters
                     INNER JOIN filter_values
                     ON product_filters.filter_value_id=filter_values.filter_value_id
                     INNER JOIN filters
                     ON filter_values.filter_id=filters.filter_id
                     WHERE product_filters.product_id=$productId";

          $getQuery = mysqli_query($connection,$getSql);
          $productFilters = array();
          while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
            $productFilters[] = $read;
          }
          return $productFilters;
        }
      }
    }
    /**
     * En çok satan 5 ürünü döndürür
     *
     * @access public
     * @return array most viewed products
     */
    public function getTheMostSellingProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM order_lines
                INNER JOIN product_variants
                ON order_lines.barcode=product_variants.barcode
                ORDER BY product_variants.product_id ASC";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          if(count($products) == 0 || !isset($products[$read["product_id"]])){
            $products[$read["product_id"]] = array(
              "product_id" => $read["product_id"],
              "quantity"   => $read["quantity"]
            );
          }
          else{
            $products[$read["product_id"]]["quantity"] += $read["quantity"];
          }
        }
        $products = $this->record_sort($products, "quantity", true);
        $returnedProducts = array();
        foreach ($products as $key => $product) {
          $this->setProductId($product["product_id"]);
          $productDetails = $this->getProductDetails();
          $returnedProducts[] = $productDetails;
        }
        return $returnedProducts;
      }
    }

    /**
     * En çok görütülenen 5 ürünü döndürür
     *
     * @access public
     * @return array most viewed products
     */
    public function getTheMostViewedProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY views DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * En çok sepete eklenen 5 ürünü döndürür
     *
     * @access public
     * @return array most added products to the cart
     */
    public function getTheMostAddedProductsToTheCart(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY add_to_basket DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * En çok sepetten çıkarılan 5 ürünü döndürür
     *
     * @access public
     * @return array most items out of the cart
     */
    public function getTheMostItemsOutOfTheBasket(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY get_out_the_basket DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * Dizinin elemanlarını sıralar
     *
     * @access public
     * @param array $records
     * @param string $field
     * @param boolean $reverse
     */
     public function record_sort($records, $field, $reverse = false){
       $hash = array();
       $c = 'a';
       foreach($records as $record){
         $index = $record[$field];
         $index_char = '';
         if(isset($hash[$index])) $index_char = $c++;
         $hash[$index.$index_char] = $record;
       }
       ($reverse) ? krsort($hash) : ksort($hash);
       $records = array();
       foreach($hash as $record){
         $records[] = $record;
       }
       return $records;
     }
  }

?>
