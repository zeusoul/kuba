<?php
  /**
   * Company sınıfı, Şirket ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Company sınıfı,
   * Şirket bilgilerini kaydetmeye,
   * Şirket bilgilerini güncellemeye
   * ve şirket bilgilerini çekmeye yarar.
   *
   * Example usage:
   * if (Company::updateCompanyName()) {
   *   print "Company name updated";
   * }
   *
   * @package Company
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Company{
    /**
     * Şirket ismi
     *
     * @var string
     * @access protected
     */
    protected $companyName;
    /**
     * Şirket Telefonu
     *
     * @var string
     * @access protected
     */
    protected $companyTel;
    /**
     * Şirket E-mail adresi
     *
     * @var string
     * @access protected
     */
    protected $companyEmail;
    /**
     * Şirket Whatsapp numarası
     *
     * @var string
     * @access protected
     */
    protected $companyWp;
    /**
     * Şirket Adresi
     *
     * @var string
     * @access protected
     */
    protected $companyAddress;
    /**
     * Şirket Hakkında
     *
     * @var string
     * @access protected
     */
    protected $companyAbout;
    /**
     * Şirket logosu
     *
     * @var array
     * @access protected
     */
    protected $companyLogo;
    /**
     * Şirket'in header da görünecek olan logosu
     *
     * @var array
     * @access protected
     */
    protected $companyHeaderLogo;
    /**
     * Set the $companyName var
     *
     * @access public
     * @param string $companyName
     */
    public function setCompanyName($companyName){
      $this->companyName = trim(strip_tags($companyName));
    }
    /**
     * Set the $companyTel var
     *
     * @access public
     * @param string $companyTel
     */
    public function setCompanyTel($companyTel){
      $this->companyTel = (int)$companyTel;
      $this->companyTel = (string)$companyTel;
      $this->companyTel = trim(strip_tags($companyTel));
    }
    /**
     * Set the $companyEmail var
     *
     * @access public
     * @param string $companyEmail
     */
    public function setCompanyEmail($companyEmail){
      $this->companyEmail = trim(strip_tags($companyEmail));
    }
    /**
     * Set the $companyWp var
     *
     * @access public
     * @param string $companyWp
     */
    public function setCompanyWp($companyWp){
      $this->companyWp = (int)$companyWp;
      $this->companyWp = (string)$companyWp;
      $this->companyWp = trim(strip_tags($companyWp));
    }
    /**
     * Set the $companyAddress var
     *
     * @access public
     * @param string $companyAddress
     */
    public function setCompanyAddress($companyAddress){
      $this->companyAddress = trim(strip_tags($companyAddress));
    }
    /**
     * Set the $companyAbout var
     *
     * @access public
     * @param string $companyAbout
     */
    public function setCompanyAbout($companyAbout){
      $this->companyAbout = trim(strip_tags($companyAbout));
    }
    /**
     * Set the $companyLogo var
     *
     * @access public
     * @param array $companyLogo
     */
    public function setCompanyLogo($companyLogo){
      if(is_array($companyLogo)){
        $this->companyLogo = $companyLogo;
        foreach ($companyLogo as $image) {
          if(trim($image) == "" || trim($image) == null) {
            $this->companyLogo = array();
            break;
          }
        }
      }
      else {
        $this->companyLogo = trim(strip_tags($companyLogo));
      }
    }
    /**
     * Set the $companyHeaderLogo var
     *
     * @access public
     * @param array $companyHeaderLogo
     */
    public function setCompanyHeaderLogo($companyHeaderLogo){
      if(is_array($companyHeaderLogo)){
        $this->companyHeaderLogo = $companyHeaderLogo;
        foreach ($companyHeaderLogo as $image) {
          if(trim($image) == "" || trim($image) == null) {
            $this->companyHeaderLogo = array();
            break;
          }
        }
      }
      else {
        $this->companyHeaderLogo = trim(strip_tags($companyHeaderLogo));
      }
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Company
     */
    public function __construct(){
      $this->companyName        = "";
      $this->companyTel         = "";
      $this->companyEmail       = "";
      $this->companyWp          = "";
      $this->companyAddress     = "";
      $this->companyAbout       = "";
      $this->companyLogo        = array();
      $this->companyHeaderLogo  = array();
    }
    /**
     * Şirket adını kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyName(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $companyName = mysqli_real_escape_string($connection,$this->companyName);
        $getSql = "SELECT * FROM company_information";
        if($getQuery->num_rows == 0){
          #Kayıt bulunamadı. İlk kaydı yapalım.
          $sql = "INSERT INTO company_information
                  VALUES('','$companyName','','','','','','','')";
          $query = mysqli_query($connection,$sql);
          return $query;
        }
        else{
          #Kayıt var. Güncelleme yapalım.
          return $this->updateCompanyName();
        }
      }
    }
    /**
     * Şirket adını günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyName(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyName) == "") return false;
        else{
          $companyName = mysqli_real_escape_string($connection,$this->companyName);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyName();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_name = '$companyName'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket telefonunu kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyTel(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyTel) == "") return false;
        else{
          $companyTel = mysqli_real_escape_string($connection,$this->companyTel);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','$companyTel','','','','','','')";
            return mysqli_query($connection,$sql);
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyTel();
          }
        }
      }
    }
    /**
     * Şirket telefonunu günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyTel(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyTel) == "") return false;
        else{
          $companyTel = mysqli_real_escape_string($connection,$this->companyTel);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyTel();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_tel = '$companyTel'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket email adresini kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyEmail(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyEmail) == "") return false;
        else{
          $companyEmail = mysqli_real_escape_string($connection,$this->companyEmail);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','','','$companyEmail','','','')";
            return mysqli_query($connection,$sql);
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyEmail();
          }
        }
      }
    }
    /**
     * Şirket email adresini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyEmail(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyEmail) == "") return false;
        else{
          $companyEmail = mysqli_real_escape_string($connection,$this->companyEmail);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyEmail();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_email = '$companyEmail'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket Whatsapp numarasını kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyWp(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyWp) == "") return false;
        else{
          $companyWp = mysqli_real_escape_string($connection,$this->companyWp);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','','','','$companyWp','','')";
            return mysqli_query($connection,$sql);
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyWp();
          }
        }
      }
    }
    /**
     * Şirket Whatsapp numarasını günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyWp(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyWp) == "") return false;
        else{
          $companyWp = mysqli_real_escape_string($connection,$this->companyWp);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyWp();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_wp = '$companyWp'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket adresini kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyAddress(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyAddress) == "") return false;
        else{
          $companyAddress = mysqli_real_escape_string($connection,$this->companyAddress);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','','','','','$companyAddress','')";
            return mysqli_query($connection,$sql);
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyAddress();
          }
        }
      }
    }
    /**
     * Şirket adresini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyAddress(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyAddress) == "") return false;
        else{
          $companyAddress = mysqli_real_escape_string($connection,$this->companyAddress);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyAddress();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_address = '$companyAddress'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket hakkında bilgisini kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyAbout(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyAbout) == "") return false;
        else{
          $companyAbout = mysqli_real_escape_string($connection,$this->companyAbout);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','','','','','','$companyAbout')";
            return mysqli_query($connection,$sql);
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyAbout();
          }
        }
      }
    }
    /**
     * Şirket hakında bilgisini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyAbout(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        if(trim($this->companyAbout) == "") return false;
        else{
          $companyAbout = mysqli_real_escape_string($connection,$this->companyAbout);
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyAbout();
          }
          else{
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_about = '$companyAbout'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket logosunu kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyLogo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $imageObject = new Image();
        $imageLocation = PATH."/app/user-app/view/public/img";
        $imageObject->setImageName("logo");
        $imageObject->setImageLocation($imageLocation);
        $imageObject->setProductImage($this->companyLogo);

        $upload = $imageObject->uploadImage();
        if($upload != "Success") return false;
        else{
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','logo.png','','','','','')";
            return mysqli_query($connection,$sql);
          }
          else {
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyLogo();
          }
        }
      }
    }
    /**
     * Şirket logosunu günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyLogo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $imageObject = new Image();
        $imageLocation = PATH."/app/user-app/view/public/img";
        $imageObject->setImageName("logo");
        $imageObject->setImageLocation($imageLocation);
        $imageObject->setProductImage($this->companyLogo);

        $upload = $imageObject->uploadImage();
        if($upload != "Success") return false;
        else{
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyLogo();
          }
          else {
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_logo = 'logo.jpg'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Header için Şirket logosunu kaydeder
     *
     * @access public
     * @return boolean
     */
    public function insertCompanyHeaderLogo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $imageObject = new Image();
        $imageLocation = PATH."/app/user-app/view/public/img";
        $imageObject->setImageName("header_logo");
        $imageObject->setImageLocation($imageLocation);
        $imageObject->setProductImage($this->companyHeaderLogo);

        $upload = $imageObject->uploadImage();
        if($upload != "Success") return false;
        else{
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            $sql = "INSERT INTO company_information
                    VALUES('','','','','header_logo.jpg','','','','')";
            return mysqli_query($connection,$sql);
          }
          else {
            #Kayıt var. Güncelleme yapalım.
            return $this->updateCompanyHeaderLogo();
          }
        }
      }
    }
    /**
     * Header için Şirket logosunu günceller
     *
     * @access public
     * @return boolean
     */
    public function updateCompanyHeaderLogo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $imageObject = new Image();
        $imageLocation = PATH."/app/user-app/view/public/img";
        $imageObject->setImageName("header_logo");
        $imageObject->setImageLocation($imageLocation);
        $imageObject->setProductImage($this->companyHeaderLogo);

        $upload = $imageObject->uploadImage();
        if($upload != "Success") return false;
        else{
          $getSql = "SELECT * FROM company_information";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 0){
            #Kayıt bulunamadı. İlk kaydı yapalım.
            return $this->insertCompanyHeaderLogo();
          }
          else {
            #Kayıt var. Güncelleme yapalım.
            $sql = "UPDATE company_information
                    SET company_header_logo = 'header_logo.jpg'";
            return mysqli_query($connection,$sql);
          }
        }
      }
    }
    /**
     * Şirket bilgilerini döndürür
     *
     * @access public
     * @return array company information
     */
    public function getCompanyInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM company_information";
        $query = mysqli_query($connection,$sql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
  }

?>
