<?php
  /**
   * Earning sınıfı, Kar-Kazanç ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Earning sınıfı,
   * Aylık veya toplam kazancı döndürmeye yarar.
   *
   * Example usage:
   * if (Earning::getMonthlyEarnings() == 0) {
   *   print "You didn't earn this month";
   * }
   *
   * @package Earning
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Earning{
    /**
     * Aylık kazancı hesaplar
     *
     * @access public
     * @return float
     */
    public function getMonthlyEarnings(){
      $orderObject = new Order();
      $returnObject = new ProductReturn();
      $orderAmount = (float)$orderObject->getMonthlyOrderAmount();
      $refundAmount = (float)$returnObject->getMonthlyRefundAmount();
      $gain = $orderAmount - $refundAmount;

      $gain = round($gain,2);
      return $gain;
    }
    /**
     * Toplam kazancı hesaplar
     *
     * @access public
     * @return float
     */
    public function getTotalEarnings(){
      $orderObject = new Order();
      $returnObject = new ProductReturn();
      $orderAmount = (float)$orderObject->getTotalOrderAmount();
      $refundAmount = (float)$returnObject->getTotalRefundAmount();
      $gain = $orderAmount - $refundAmount;

      $gain = round($gain,2);
      return $gain;
    }
  }

?>
