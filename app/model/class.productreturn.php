<?php
  /**
   * ProductReturn sınıfı, İade ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * ProductReturn sınıfı,
   * Yeni iade kaydetmeye,
   * Kayıtlı iade bilgisini güncellemeye, silmeye
   * ve iade bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (ProductReturn::insertReturn() === "Success") {
   *   print "Product return Inserted";
   * }
   *
   * @package ProductReturn
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class ProductReturn extends Order {
    /**
     * İade id'si
     *
     * @var int
     * @access protected
     */
    protected $returnId;
    /**
     * İade sebebi
     *
     * @var string
     * @access protected
     */
    protected $returnReason;
    /**
     * İade durumu
     * (-1 = İptal edildi, 0 = Onay Bekliyor, 1 = İade Onaylandı)
     *
     * @var int
     * @access protected
     */
    protected $returnStatus;
    /**
     * İade İptalinin Nedeni
     *
     * @var string
     * @access protected
     */
    protected $reasonForRejection;
    /**
     * İade edilecek ürün adedi
     *
     * @var int
     * @access protected
     */
    protected $returnedQuantity;
    /**
     * İade edilecek ürün fiyatı
     *
     * @var float
     * @access protected
     */
    protected $returnedPrice;
    /**
     * Set the $returnId var
     *
     * @access public
     * @param int $returnId
     */
    public function setReturnId($returnId){
      $this->returnId = (int)$returnId;
    }
    /**
     * Set the $returnReason var
     *
     * @access public
     * @param string $returnReason
     */
    public function setReturnReason($returnReason){
      $this->returnReason = trim(strip_tags($returnReason));
    }
    /**
     * Set the $returnStatus var
     *
     * @access public
     * @param int $returnStatus
     */
    public function setReturnStatus($returnStatus){
      $this->returnStatus = (int)$returnStatus;
    }
    /**
     * Set the $reasonForRejection var
     *
     * @access public
     * @param string $reasonForRejection
     */
    public function setReasonForRejection($reasonForRejection){
      $this->reasonForRejection = trim(strip_tags($reasonForRejection));
    }
    /**
     * Set the $returnedQuantity var
     *
     * @access public
     * @param int $returnedQuantity
     */
    public function setReturnedQuantity($returnedQuantity){
      $this->returnedQuantity = (int)$returnedQuantity;
    }
    /**
     * Set the $returnedPrice var
     *
     * @access public
     * @param float $returnedPrice
     */
    public function setReturnedPrice($returnedPrice){
      $this->returnedPrice = (float)$returnedPrice;
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductReturn
     */
    public function __construct() {
      $this->returnId           = 0;
      $this->returnReason       = "";
      $this->returnStatus       = -2;
      $this->reasonForRejection = "";
      $this->returnedQuantity   = 0;
      $this->returnedPrice      = 0.00;
    }

    /**
     * İade talebi ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertReturn(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı hatası";
      else{
        $orderNo   = (int)$this->orderNo;
        $barcode = $this->barcode;
        $userId    = (int)$this->userId;
        $returnReason = mysqli_real_escape_string($connection,$this->returnReason);
        // İade edilesi istenen ürünü, kullanıcının siparişlerinde mevcutmu kontrol edelim
        $sql = "SELECT * FROM order_headers
                INNER JOIN order_lines
                ON order_headers.order_no = order_lines.order_no
                WHERE order_headers.order_no=$orderNo
                AND order_headers.user_id=$userId
                AND order_lines.barcode='$barcode'";

        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1)return "İade talebinde bulunduğunuz ürün, siparişleriniz arasında bulunamadı!";
        else{
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $status = (int)$read["status"];
          if($status != 3) return "Talep oluşturabilmeniz için ürünün sizin tarafınıza teslim edilmesi gereklidir!";
          else{
            // Ürün önceden iade talebinde bulunulmuş mu? kontrol edelim
            $sql = "SELECT * FROM returns
                    WHERE user_id=$userId
                    AND barcode='$barcode'
                    AND order_no=$orderNo";
            $query = mysqli_query($connection,$sql);
            if($query->num_rows >= 1) return "Bu siparişteki bu ürün için zaten iade talebi oluşturulmuş!";
            else{
              $orderedQuantity = (int)$read["quantity"];
              $returnedQuantity = (int)$this->returnedQuantity;
              $returnedPrice = (float)$this->returnedPrice;
              $price = (float)$read["unit_price"] * $returnedQuantity;
              if($returnedQuantity <= 0 || $returnedQuantity > $orderedQuantity){
                return "İade etmek istediğiniz miktar, 0'dan büyük, sipariş miktarından küçük olmalıdır!";
              }
              else{
                if($price != $returnedPrice) {
                  $returnedPrice = $price;
                }
                $date = date('d.m.Y');
                $insertSql = "INSERT INTO returns
                              VALUES('',$orderNo,$userId,'$returnReason',0,'','$date','$barcode',$returnedQuantity,$returnedPrice)";
                $insertQuery = mysqli_query($connection,$insertSql);
                return ($insertQuery) ? "Success" : "İade işlemi başarısız oldu!";
              }
            }
          }
        }
      }
    }
    /**
     * İade talebini iptal eder
     *
     * @access public
     * @return string "Success" or error message
     */
    public function cancelTheReturn(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $returnId = (int)$this->returnId;
        $userId = (int)$this->userId;
        $getSql = "SELECT * FROM returns
                   WHERE user_id=$userId
                   AND return_id=$returnId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1)return "İptali istenilen iade işlemi bulunamadı!";
        else {
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $status = (int)$read["return_status"];
          if($status == -1) return "Bu Talep Zaten İptal Edilmiş Durumda.<br/>Bu iptal işlemi bizim tarafımızdan da yapılmış olabilir.";
          else if($status == 1) return "Bu Talep Onaylandığı İçin İptal Edilemez!";
          else if($status == 0){
            $reasonForRejection = mysqli_real_escape_string($connection,$this->reasonForRejection);
            if(trim($reasonForRejection) == "") return "İptal sebebi boş bırakılamaz";
            else{
              $sql = "UPDATE returns
                      SET return_status=-1,
                          reason_for_rejection='$reasonForRejection'
                      WHERE user_id=$userId
                      AND return_id=$returnId";
              $query = mysqli_query($connection,$sql);
              return ($query) ? "Success" : "İptal İşlemi Başarısız Oldu!";
            }
          }
          else return "İptal İşlemi Başarısız Oldu!";
        }
      }
    }
    /**
     * İade talebini onaylar
     *
     * @access public
     * @return boolean
     */
    public function confirmTheReturn(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $returnId = (int)$this->returnId;
        $getSql = "SELECT * FROM returns
                   WHERE return_id=$returnId";
        $getQuery=mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $status = (int)$read["return_status"];
          if($status != 0) return false;
          else{
            $sql = "UPDATE returns
                    SET return_status=1,
                        reason_for_rejection=''
                    WHERE return_id=$returnId";
            $query = mysqli_query($connection,$sql);
            return $query;
          }
        }
      }
    }
    /**
     * Kayıtlı tüm iade taleplerini döndürür
     *
     * @access public
     * @return array All returns
     */
    public function getAllReturns(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return array();
      else{
        $sql = "SELECT * FROM returns
                INNER JOIN product_variants
                ON returns.barcode=product_variants.barcode
                INNER JOIN products
                ON products.product_id=product_variants.product_id
                INNER JOIN users
                ON returns.user_id=users.user_id
                ORDER BY returns.return_id DESC";
        $query = mysqli_query($connection,$sql);
        $returns = array();
        while ($read = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
          $returns[] = $read;
        }
        return $returns;
      }
    }
    /**
     * Kullanıcıya ait kayıtlı tüm iade taleplerini döndürür
     *
     * @access public
     * @return array user's all returns
     */
    public function getReturns(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return array();
      else{
        $userId = (int)$this->userId;
        $sql = "SELECT * FROM returns
                INNER JOIN product_variants
                ON returns.barcode=product_variants.barcode
                INNER JOIN products
                ON product_variants.product_id=products.product_id
                WHERE returns.user_id=$userId
                ORDER BY returns.return_id DESC";
        $query = mysqli_query($connection,$sql);
        $returns = array();
        while ($read = mysqli_fetch_array($query,MYSQLI_ASSOC)) {
          $returns[] = $read;
        }
        return $returns;
      }
    }
    /**
     * İade talebinin detaylarını döndürür
     *
     * @access public
     * @return array return details
     */
    public function getReturnDetails(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection)return array();
      else {
        $returnId = (int)$this->returnId;
        $sql = "SELECT * FROM returns
                INNER JOIN product_variants
                ON returns.barcode = product_variants.barcode
                INNER JOIN products
                ON product_variants.product_id=products.product_id
                WHERE return_id=$returnId";
        $query = mysqli_query($connection,$sql);
        $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
        return $read;
      }
    }
    /**
     * Aylık toplam iade tutarını döndürür
     *
     * @access public
     * @return float
     */
    public function getMonthlyRefundAmount(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return -1;
      else{
        $sql = "SELECT * FROM returns
                WHERE return_status = 1";
        $query = mysqli_query($connection,$sql);
        $sum = (float)0.00;
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $returnDate = (string)$read["return_date"];
          $returnDate = explode('.',$returnDate);

          $returnMounth = (string)$returnDate[1];
          $returnYear = (string)$returnDate[2];

          $returnMounth = (string)date("m");
          $returnYear = (string)date("Y");
          if($returnYear == $nowYear && $returnMounth == $nowMounth){
            $amount = (float)$read["returned_price"];
            $sum += $amount;
          }
        }
        $sum = ($sum*118)/100;//KDV Dahil
        return $sum;
      }
    }
    /**
     * Toplam iade tutarını döndürür
     *
     * @access public
     * @return float
     */
    public function getTotalRefundAmount(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return -1;
      else{
        $sql = "SELECT * FROM returns
                WHERE return_status = 1";
        $query = mysqli_query($connection,$sql);
        $sum = (float)0.00;
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $amount = (float)$read["returned_price"];
          $sum += $amount;
        }
        $sum = ($sum * 118) / 100;
        return $sum;
      }
    }
  }

?>
