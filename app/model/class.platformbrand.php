<?php

  class PlatformBrand extends Brand{
    protected $platformBrandId;
    public function setPlatformBrandId($platformBrandId){
      $this->platformBrandId = $platformBrandId;
    }
    protected $platformBrandCode;
    public function setPlatformBrandCode($platformBrandCode){
      $this->platformBrandCode = $platformBrandCode;
    }

    public function insertPlatformBrand(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_brands
                   WHERE brand_id=$this->brandId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery);
        if(is_array($read) && count($read) > 0){
          //UPDATE
          $sql = "UPDATE platform_brands
                  SET platform_brand_code='$this->platformBrandCode'
                  WHERE brand_id=$this->brandId
                  AND platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
        else{
          //INSERT
          $sql = "INSERT INTO platform_brands
                  SET platform_brand_code='$this->platformBrandCode',
                      brand_id=$this->brandId,
                      platform_id=$this->platformId";
          return mysqli_query($connection,$sql);
        }
      }
    }
    public function deletePlatformBrand(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_brands
                WHERE platform_brand_id=$this->platformBrandId";
        return mysqli_query($connection,$sql);
      }
    }
    public function deletePlatformBrandsByBrandId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "DELETE FROM platform_brands
                WHERE brand_id=$this->brandId";
        return mysqli_query($connection,$sql);
      }
    }

    public function getPlatformBrandByPlatformIdAndBrandId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_brands
                   WHERE brand_id=$this->brandId
                   AND platform_id=$this->platformId";
        $getQuery = mysqli_query($connection,$getSql);
        $result = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        return $result;
      }
    }
    public function getPlatformBrandsByBrandId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM platform_brands
                   INNER JOIN platforms ON platform_brands.platform_id = platforms.platform_id
                   WHERE platform_brands.brand_id=$this->brandId";
        $getQuery = mysqli_query($connection,$getSql);
        $platformBrands = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $platformBrands[] = $read;
        }
        return $platformBrands;
      }
    }
  }

?>
