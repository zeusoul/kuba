<?php 
/**
 * Details sınıfı , Firma hakkında daha fazla bilginin sayfanın herhangi
 * bir yerinde çağırılarak yer almasını sağlayan sınıftır.
 * 
 * Details sınıfı,
 * Şirket hakkındaki detaylı bilgleri kaydetmeye,
 * Şirket hakkındaki detaylı bilgileri güncellemeye,
 * ve şirket hakkındaki detaylı bilgileri çekmeye yarar.
 * 
 * @package Details
 */
    class Details{
        /**
         * 
         * Detay Başlığı
         * 
         * @var string
         * @access protected
         */
        protected $detailsTitle;
        /**
         * 
         * Detay İçeriği
         * 
         * @var string
         * @access protected
         */
        protected $detailsContent;
        /**
         * 
         * Detay Başlığı
         * 
         * @var string
         * @access protected
         */
        protected $detailsTitle2;
        /**
         * 
         * Detay İçeriği
         * 
         * @var string
         * @access protected
         */
        protected $detailsContent2;
        /**
         * 
         * Detay Başlığı
         * 
         * @var string
         * @access protected
         */
        protected $detailsTitle3;
        /**
         * 
         * Detay İçeriği
         * 
         * @var string
         * @access protected
         */
        protected $detailsContent3;
        /**
         * Set the $detailsTitle var
         * 
         * @access public
         * @param string $detailsTitle
         */ 
        public function setDetailsTitle($detailsTitle){
            $this->detailsTitle = trim(strip_tags($detailsTitle));
        }
        /**
         * Set the $detailsContent var
         * 
         * @access public
         * @param string $detailsContent
         */
        public function setDetailsContent($detailsContent){
            $this->detailsContent = trim(strip_tags($detailsContent));
        }
        /**
         * Set the $detailsTitle var
         * 
         * @access public
         * @param string $detailsTitle
         */ 
        public function setDetailsTitle2($detailsTitle2){
            $this->detailsTitle2 = trim(strip_tags($detailsTitle2));
        }
        /**
         * Set the $detailsContent var
         * 
         * @access public
         * @param string $detailsContent
         */
        public function setDetailsContent2($detailsContent2){
            $this->detailsContent2 = trim(strip_tags($detailsContent2));
        }
        /**
         * Set the $detailsTitle var
         * 
         * @access public
         * @param string $detailsTitle
         */ 
        public function setDetailsTitle3($detailsTitle3){
            $this->detailsTitle3 = trim(strip_tags($detailsTitle3));
        }
        /**
         * Set the $detailsContent var
         * 
         * @access public
         * @param string $detailsContent
         */
        public function setDetailsContent3($detailsContent3){
            $this->detailsContent3 = trim(strip_tags($detailsContent3));
        }
        /**
         * Constructor, sets the inital values
         * 
         * @access public
         * @return Details
         */
        public function __construct(){
            $this->detailsTitle     ="";
            $this->detailsContent   ="";
            $this->detailsTitle2     ="";
            $this->detailsContent2   ="";
            $this->detailsTitle3     ="";
            $this->detailsContent3   ="";
        }
        /**
         * 
         * Detay Başlığını kaydeder
         * 
         * @access public
         * @return boolean
         */
        public function insertDetailsTitle(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle) == "") return false;
                else{
                $detailsTitle = mysqli_real_escape_string($connection,$this->detailsTitle);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt yok ilk kaydı yapalım.
                    $sql = "INSERT INTO details
                            VALUES('','$detailsTitle','','','','','')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var.Güncelleme Yapalım.
                    return $this->updateDetailsTitle();
                }
            }
        }
    }
        /**
         * 
         * Detay Başlığını Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsTitle(){
            $db = new Database();
            $connection = $db-> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle) == "") return false;
                else{
                    $detailsTitle = mysqli_real_escape_string($connection,$this->detailsTitle);
                    $getSql = "SELECT * FROM details";
                    $getQuery =mysqli_query($connection,$getSql);
                    if($getQuery->num_rows == 0){
                        #kayıt bulunamadı.İlk kaydı yapar.
                        return $this->insertDetailsTitle();
                    }
                    else{
                        #kayıt var.Güncelleme yapalım.
                        $sql = "UPDATE details 
                                SET details_title = '$detailsTitle'";
                                return mysqli_query($connection,$sql); 
                    }
                }
            }
        }
        /**
         * 
         * Detay İçeriğini kaydeder
         * 
         * @access public 
         * @return boolean
         */
        public function insertDetailsContent(){
            $db = new Database();
            $connection = $db -> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent) == "")return false;
                else{
                $detailsContent = mysqli_real_escape_string($connection,$this->detailsContent);
                $getSql = "SELECT * FROM details";
                $getQuery =mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt bulunamadı ilk kaydı yapalım
                    $sql = "INSERT INTO details
                            VALUES('','','$detailsContent','','','','')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var güncelleme yapalım.
                    return $this->updateDetailsContent();
                }
            }
        }
    }
        /**
         * 
         * Detay İçeriğini Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsContent(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent) == "") return false;
                else{
                $detailsContent = mysqli_real_escape_string($connection,$this->detailsContent);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery-> num_rows == 0){
                    #kayıt bulunamadı.İlk kayıdı yapalım.
                    return $this->insertDetailsContent();
                }
                else{
                    #kayıt var.Güncelleme yapalım.
                    $sql = "UPDATE details
                            SET details_content = '$detailsContent'";
                            return mysqli_query($connection,$sql);
                }
            }
        }
    }
    /*************** */
    /**
         * 
         * Detay2 Başlığını kaydeder
         * 
         * @access public
         * @return boolean
         */
        public function insertDetailsTitle2(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle2) == "") return false;
                else{
                $detailsTitle2 = mysqli_real_escape_string($connection,$this->detailsTitle2);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt yok ilk kaydı yapalım.
                    $sql = "INSERT INTO details
                            VALUES('','','','$detailsTitle2','','','')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var.Güncelleme Yapalım.
                    return $this->updateDetailsTitle2();
                }
            }
        }
    }
        /**
         * 
         * Detay Başlığını Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsTitle2(){
            $db = new Database();
            $connection = $db-> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle2) == "") return false;
                else{
                    $detailsTitle2 = mysqli_real_escape_string($connection,$this->detailsTitle2);
                    $getSql = "SELECT * FROM details";
                    $getQuery =mysqli_query($connection,$getSql);
                    if($getQuery->num_rows == 0){
                        #kayıt bulunamadı.İlk kaydı yapar.
                        return $this->insertDetailsTitle2();
                    }
                    else{
                        #kayıt var.Güncelleme yapalım.
                        $sql = "UPDATE details 
                                SET details_title2 = '$detailsTitle2'";
                                return mysqli_query($connection,$sql); 
                    }
                }
            }
        }
        /**
         * 
         * Detay İçeriğini kaydeder
         * 
         * @access public 
         * @return boolean
         */
        public function insertDetailsContent2(){
            $db = new Database();
            $connection = $db -> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent2) == "")return false;
                else{
                $detailsContent2 = mysqli_real_escape_string($connection,$this->detailsContent2);
                $getSql = "SELECT * FROM details";
                $getQuery =mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt bulunamadı ilk kaydı yapalım
                    $sql = "INSERT INTO details
                            VALUES('','','','','$detailsContent2','','')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var güncelleme yapalım.
                    return $this->updateDetailsContent2();
                }
            }
        }
    }
        /**
         * 
         * Detay İçeriğini Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsContent2(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent2) == "") return false;
                else{
                $detailsContent2 = mysqli_real_escape_string($connection,$this->detailsContent2);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery-> num_rows == 0){
                    #kayıt bulunamadı.İlk kayıdı yapalım.
                    return $this->insertDetailsContent2();
                }
                else{
                    #kayıt var.Güncelleme yapalım.
                    $sql = "UPDATE details
                            SET details_content2 = '$detailsContent2'";
                            return mysqli_query($connection,$sql);
                }
            }
        }
    }
    /**************** */
    /**
         * 
         * Detay3 Başlığını kaydeder
         * 
         * @access public
         * @return boolean
         */
        public function insertDetailsTitle3(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle3) == "") return false;
                else{
                $detailsTitle3 = mysqli_real_escape_string($connection,$this->detailsTitle3);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt yok ilk kaydı yapalım.
                    $sql = "INSERT INTO details
                            VALUES('','','','','','$detailsTitle3','')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var.Güncelleme Yapalım.
                    return $this->updateDetailsTitle3();
                }
            }
        }
    }
        /**
         * 
         * Detay3 Başlığını Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsTitle3(){
            $db = new Database();
            $connection = $db-> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsTitle3) == "") return false;
                else{
                    $detailsTitle3 = mysqli_real_escape_string($connection,$this->detailsTitle3);
                    $getSql = "SELECT * FROM details";
                    $getQuery =mysqli_query($connection,$getSql);
                    if($getQuery->num_rows == 0){
                        #kayıt bulunamadı.İlk kaydı yapar.
                        return $this->insertDetailsTitle3();
                    }
                    else{
                        #kayıt var.Güncelleme yapalım.
                        $sql = "UPDATE details 
                                SET details_title3 = '$detailsTitle3'";
                                return mysqli_query($connection,$sql); 
                    }
                }
            }
        }
        /**
         * 
         * Detay3 İçeriğini kaydeder
         * 
         * @access public 
         * @return boolean
         */
        public function insertDetailsContent3(){
            $db = new Database();
            $connection = $db -> MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent3) == "")return false;
                else{
                $detailsContent3 = mysqli_real_escape_string($connection,$this->detailsContent3);
                $getSql = "SELECT * FROM details";
                $getQuery =mysqli_query($connection,$getSql);
                if($getQuery->num_rows == 0){
                    #kayıt bulunamadı ilk kaydı yapalım
                    $sql = "INSERT INTO details
                            VALUES('','','','','','','$detailsContent3')";
                    return mysqli_query($connection,$sql);
                }
                else{
                    #kayıt var güncelleme yapalım.
                    return $this->updateDetailsContent3();
                }
            }
        }
    }
        /**
         * 
         * Detay3 İçeriğini Günceller
         * 
         * @access public
         * @return boolean
         */
        public function updateDetailsContent3(){
            $db = new Database();
            $connection = $db->MySqlConnection();
            if(!$connection) return false;
            else{
                if(trim($this->detailsContent3) == "") return false;
                else{
                $detailsContent3 = mysqli_real_escape_string($connection,$this->detailsContent3);
                $getSql = "SELECT * FROM details";
                $getQuery = mysqli_query($connection,$getSql);
                if($getQuery-> num_rows == 0){
                    #kayıt bulunamadı.İlk kayıdı yapalım.
                    return $this->insertDetailsContent3();
                }
                else{
                    #kayıt var.Güncelleme yapalım.
                    $sql = "UPDATE details
                            SET details_content3 = '$detailsContent3'";
                            return mysqli_query($connection,$sql);
                }
            }
        }
    }
        /**
     * Detayları döndürür
     *
     * @access public
     * @return array All details
     */
    public function getAllDetails(){
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
            $sql = "SELECT * FROM details";
            $query = mysqli_query($connection,$sql);
            return mysqli_fetch_array($query,MYSQLI_ASSOC);
        }
    }
    
    }
?>