<?php 
/**
 * Referans Sayısı Çok Fazla olan müşteriler için kullanılabilir.
 * Referanslar'ın alacağı kategoriler'in id ve isimleriyle
 * alakalı işlemler bu class'da gerçekleştirilir.
 * 
 */
class ReprensativeContinent{
    /**
     * Kategorinin id'si
     *
     * @var int
     * @access protected
     */
    protected $re;
    /**
     * Kategori ismi
     *
     * @var string
     * @access protected
     */
    protected $referenceCategoryName;
    /**
     * Set the $referenceCategoryId var
     *
     * @access public
     * @param int $referenceCategoryId
     */
    public function setReferenceCategoryId($referenceCategoryId){
      $this->referenceCategoryId = (int)$referenceCategoryId;
    }
    /**
     * Set the $referenceCategoryName var
     *
     * @access public
     * @param string $referenceCategoryName
     */
    public function setReferenceCategoryName($referenceCategoryName){
      $this->referenceCategoryName = trim(strip_tags($referenceCategoryName));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ReferenceCategory
     */
    public function __construct(){
      $this->referenceCategoryId   = 0;
      $this->referenceCategoryName = "";
    }
    /**
     * Referans'ın Ana Kategorisini ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertReferenceCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $referenceCategoryName = mysqli_real_escape_string($connection,$this->referenceCategoryName);
        $getSql = "SELECT * FROM reference_categories
                   WHERE name='$referenceCategoryName'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu İsimde Kategori Zaten Mevcut.";
        else {
          $insertSql = "INSERT INTO reference_categories
                        VALUES('','$referenceCategoryName')";
          $insertQuery = mysqli_query($connection,$insertSql);
          return ($insertQuery) ? "Success" : "Kategori Eklenemedi";
        }
      }
    }
    /**
     * Kayıtlı Referans'ın Ana Kategorisini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteReferenceCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $referenceCategoryId = (int)$this->referenceCategoryId;
        $getSql = "SELECT * FROM reference_categories
                   WHERE reference_category_id=$referenceCategoryId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return false;
        else{
          $deleteSql = "DELETE FROM reference_categories
                        WHERE reference_category_id=$referenceCategoryId";
          $delete = mysqli_query($connection,$deleteSql);
          if(!$delete) return false;
          else return $delete;
          }
        }
      }
    /**
     * Kayıtlı Referans'ın Ana Kategorisini günceller
     *
     * @access public
     * @return boolean
     */
    public function updateReferenceCategory(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else {
        $referenceCategoryName = mysqli_real_escape_string($connection,$this->referenceCategoryName);
        $referenceCategoryId = (int)$this->referenceCategoryId;
        if(trim($referenceCategoryName) == "" || trim($referenceCategoryName) == null) return false;
        else {
          $sql = "UPDATE reference_categories SET reference_category_name='$referenceCategoryName'
                  WHERE reference_category_id = $referenceCategoryId";
          $query = mysqli_query($connection,$sql);
          return $query;
        }
      }
    }
    /**
     * Ana Refereans Kategorisini ve o kategoriye ait referanslarını döndürür.
     *
     * @access public
     * @return array categories and sub categories
     */
    public function getReferenceCategoriesWithReferences(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM reference_categories
                ORDER BY reference_category_id DESC";
        $query = mysqli_query($connection,$sql);
        $referenceCategories = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $referenceCategories[$read["reference_category_id"]] = $read;
          $getSql = "SELECT * FROM reference WHERE reference_category_id=".$read["reference_category_id"];
          $getQuery = mysqli_query($connection,$getSql);
          $referenceCategories[$read["reference_category_id"]]["reference"] = array();
          while($referenceRead = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
            $referenceCategories[$read["reference_category_id"]]["reference"][] = $referenceRead;
          }
        }
        return $referenceCategories;
      }
    }
    /**
     * Tüm Referans Kategorilerini döndürür
     *
     * @access public
     * @return array All referencecategories
     */
    public function getAllReferenceCategories(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM reference_categories
                ORDER BY reference_category_id DESC";
        $query = mysqli_query($connection,$sql);
        $referenceCategories = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $referenceCategories[] = $read;
        }
        return $referenceCategories;
      }
    }
    /**
     * Seçilen kategorinin bilgilerini döndürür
     *
     * @access public
     * @return array Selected Reference Category
     */
    public function getSelectedReferenceCategory(){
      $db = new Database();
      $connection= $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM reference_categories
                WHERE reference_category_id = $this->referenceCategoryId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else{
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          $referenceCategory = $read;
          return $referenceCategory;
        }
      }
    }
  }
?>