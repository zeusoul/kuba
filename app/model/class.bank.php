<?php
  /**
   * Bank sınıfı, Banka ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Bank sınıfı,
   * Yeni banka bilgisi kaydetmeye,
   * kayıtlı banka bilgisini silmeye
   * ve kayıtlı banka bilgilerini döndürmeye yarar.
   *
   * Example usage:
   * if (Bank::deleteBankInformation()) {
   *   print "Bank information was deleted";
   * }
   *
   * @package Bank
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Bank{
    /**
     * Banka id'si
     *
     * @var int
     * @access protected
     */
    protected $bankId;
    /**
     * Banka adı
     *
     * @var string
     * @access protected
     */
    protected $bankName;
    /**
     * Bankanın Resmi
     *
     * @var array Resim bilgileri (Resmin boyutu gibi..)
     * @access protected
     */
    protected $bankImage;
    /**
     * Banka Hesap Adı
     *
     * @var string
     * @access protected
     */
    protected $accountName;
    /**
     * Banka şube kodu
     *
     * @var int
     * @access protected
     */
    protected $branchCode;
    /**
     * Banka Hesap No.
     *
     * @var string
     * @access protected
     */
    protected $accountNo;
    /**
     * Banka Iban No.
     *
     * @var string
     * @access protected
     */
    protected $ibanNo;
    /**
     * Set the $bankId var
     *
     * @access public
     * @param int $bankId
     */
    public function setBankId($bankId){
       $this->bankId = (int)$bankId;
    }
    /**
     * Set the $bankName var
     *
     * @access public
     * @param string $bankName
     */
    public function setBankName($bankName){
      $this->bankName = trim(strip_tags($bankName));
    }
    /**
     * Set the $bankImage var
     *
     * @access public
     * @param array $bankImage
     */
    public function setBankImage($bankImage){
      if(is_array($bankImage)){
        $this->bankImage = $bankImage;
        foreach ($bankImage as $image) {
          $str = new Str();
          if($str->IsNullOrEmptyString($image)) {
            $this->bankImage = array();
            break;
          }
        }
      }
      else {
        $this->bankImage = trim(strip_tags($bankImage));
      }
    }
    /**
     * Set the $accountName var
     *
     * @access public
     * @param string $accountName
     */
    public function setAccountName($accountName){
      $this->accountName = trim(strip_tags($accountName));
    }
    /**
     * Set the $branchCode var
     *
     * @access public
     * @param int $branchCode
     */
    public function setBranchCode($branchCode){
      $this->branchCode = (int)$branchCode;
    }
    /**
     * Set the $accountNo var
     *
     * @access public
     * @param string $accountNo
     */
    public function setAccountNo($accountNo){
      $this->accountNo = trim(strip_tags($accountNo));
    }
    /**
     * Set the $ibanNo var
     *
     * @access public
     * @param string $ibanNo
     */
    public function setIbanNo($ibanNo){
      $this->ibanNo = trim(strip_tags($ibanNo));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Bank
     */
    public function __construct() {
      $this->bankId       = 0;
      $this->bankName     = "";
      $this->bankImage    = array();
      $this->accountName  = "";
      $this->branchCode   = 0;
      $this->accountNo    = "";
      $this->ibanNo       = "";
    }

    /**
     * Yeni banka bilgisi kaydı yapar
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertBankInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $bankName = mysqli_real_escape_string($connection,$this->bankName);
        $bankImage = $this->bankImage;
        $accountName = mysqli_real_escape_string($connection,$this->accountName);
        $branchCode = (int)$this->branchCode;
        $accountNo = mysqli_real_escape_string($connection,$this->accountNo);
        $ibanNo = mysqli_real_escape_string($connection,$this->ibanNo);

        $getSql = "SELECT * FROM bank_information
                   WHERE iban_no='$ibanNo'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu Iban No Zaten Kayıtlı";
        else{
          $str = new Str();
          if($str->IsNullOrEmptyString(array($bankName,$accountName,$accountNo,$ibanNo))) {
            return "Lütfen Boş Değer Girmeyiniz";
          }
          else if($branchCode <= 0) {
            return "Lütfen Geçerli Bir Şube Kodu Giriniz";
          }
          else {
            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/bank-images";
            $imageObject->setImageName($ibanNo);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setProductImage($bankImage);

            $upload = $imageObject->uploadImage();
            if($upload != "Success") return "Banka'nın resmi şu nedenden dolayı yüklenemedi : $upload";
            else{
              $insertSql = "INSERT INTO bank_information
                            VALUES('','$bankName','$ibanNo.jpg','$accountName',
                                   '$branchCode','$accountNo','$ibanNo')";
              $insertQuery = mysqli_query($connection,$insertSql);
              return $insertQuery ? "Success" : "Banka Bilgileri Kaydedilemedi !";
            }
          }
        }
      }
    }
    /**
     * Kayıtlı banka bilgisini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteBankInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $getSql = "SELECT * FROM bank_information
                   WHERE bank_info_id=$this->bankId";
        $getQuery = mysqli_query($connection,$getSql);

        if($getQuery->num_rows != 1) return false;
        else{
          $read = mysqli_fetch_array($getQuery);
          $image = $read["bank_image"];

          $deleteSql = "DELETE FROM bank_information
                        WHERE bank_info_id=$this->bankId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          $path = PATH."/app/user-app/view/public/img/bank-images";
          $deleteImage = unlink("$path/$image");
          return ($deleteQuery && $deleteImage);
        }
      }
    }
    /**
     * Kayıtlı banka bilgisini döndürür
     *
     * @access public
     * @return array Banka bilgisi
     */
    public function getBankDetails(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $bankId = (int)$this->bankId;
        if($bankId<=0) return array();
        else{
          $sql = "SELECT * FROM bank_information
                  WHERE bank_info_id=$bankId";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows != 1) array();
          else{
            return mysqli_fetch_array($query,MYSQLI_ASSOC);
          }
        }
      }
    }
    /**
     * Kayıtlı bütün banka bilgilerini döndürür
     *
     * @access public
     * @return array Banka bilgileri
     */
    public function getBanks(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM bank_information";
        $getQuery = mysqli_query($connection,$getSql);
        $banks = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $banks[] = $read;
        }
        return $banks;
      }
    }
  }

?>
