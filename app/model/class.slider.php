<?php
  /**
   * Slider sınıfı, slider ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Slider sınıfı,
   * Slider için resim yüklemeye
   * veya var olan resmi silmeye yarar
   *
   * Example usage:
   * if (Slider::insertSlider()) {
   *   print "Slider is inserted";
   * }
   *
   * @package Slider
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Slider extends Image{
    /**
     * Sliderın idsi
     *
     * @var int
     * @access protected
     */
    protected $sliderId;
    /**
     * Sliderda yazacak yıl
     *
     * @var string
     * @access protected
     */
    protected $sliderYear;
    /**
     * Sliderın başlığı
     *
     * @var string
     * @access protected
     */
    protected $sliderTitle;
    /**
     * Sliderın açıklaması
     *
     * @var string
     * @access protected
     */
    protected $sliderDescription;
    /**
     * Slider resmi
     *
     * @var array
     * @access protected
     */
    protected $sliderImage;
    /**
     * Sliderın gideceği url
     *
     * @var string
     * @access protected
     */
    protected $sliderUrl;
    /**
     * Sliderda gösterilecek fiyat bilgisi
     *
     * @var string
     * @access protected
     */
    protected $sliderPrice;
    /**
     * Set the $sliderId var
     *
     * @access public
     * @param string $sliderId
     */
    public function setSliderId($sliderId){
      $this->sliderId = (int)$sliderId;
    }
    /**
     * Set the $sliderYear var
     *
     * @access public
     * @param string $sliderYear
     */
    public function setSliderYear($sliderYear){
      $this->sliderYear = trim(strip_tags($sliderYear));
    }
    /**
     * Set the $sliderTitle var
     *
     * @access public
     * @param string $sliderTitle
     */
    public function setSliderTitle($sliderTitle){
      $this->sliderTitle = trim(strip_tags($sliderTitle));
    }
    /**
     * Set the $sliderDescription var
     *
     * @access public
     * @param string $sliderDescription
     */
    public function setSliderDescription($sliderDescription){
      $this->sliderDescription = trim(strip_tags($sliderDescription));
    }
    /**
     * Set the $sliderImage var
     *
     * @access public
     * @param array $sliderImage
     */
    public function setSliderImage($sliderImage){
      if(is_array($sliderImage)){
        if(isset($sliderImage["name"]) && isset($sliderImage["type"]) && isset($sliderImage["tmp_name"]) && isset($sliderImage["size"])){
          $this->sliderImage = $sliderImage;
        }
        else $this->sliderImage = array();
      }
      else {
        $this->sliderImage = trim(strip_tags($sliderImage));
      }
    }
    /**
     * Set the $sliderUrl var
     *
     * @access public
     * @param string $sliderUrl
     */
    public function setSliderUrl($sliderUrl){
      $this->sliderUrl = trim(strip_tags($sliderUrl));
    }
    /**
     * Set the $sliderPrice var
     *
     * @access public
     * @param string $sliderPrice
     */
    public function setSliderPrice($sliderPrice){
      $this->sliderPrice = trim(strip_tags($sliderPrice));
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Slider
     */
    public function __construct(){
      $this->sliderYear        = "";
      $this->sliderTitle       = "";
      $this->sliderDescription = "";
      $this->sliderUrl         = "";
      $this->sliderPrice       = "";
    }
    /**
     * Sliderı veritabanına kaydeder
     *
     * @access public
     * @return string "Success" or error message
     */
     public function insertSlider(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return "Bağlantı Hatası";
       else {
         $newSliderId = 1;
         $sliderImage = $this->sliderImage;
         $sliderTitle = mysqli_real_escape_string($connection,$this->sliderTitle);
         $sliderDescription = mysqli_real_escape_string($connection,$this->sliderDescription);
         $url = mysqli_real_escape_string($connection,$this->sliderUrl);
         $sliderPrice = mysqli_real_escape_string($connection,$this->sliderPrice);

         $getSql = "SELECT * FROM sliders
                    ORDER BY slider_id DESC
                    LIMIT 1";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows == 1){
            $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
            $newSliderId = (int)$read["slider_id"];
            $newSliderId++;
          }

          $imageObject = new Image();
          $imageLocation = PATH."/app/user-app/view/public/img/slider-images";
          $imageObject->setImageName("slider-image-$newSliderId");
          $imageObject->setImageLocation($imageLocation);
          $imageObject->setProductImage($sliderImage);

          $upload = $imageObject->uploadImage();
          if($upload != "Success") return "Resim şu nedenden dolayı yüklenemedi : $upload";
          else {
            $insertSql = "INSERT INTO sliders
                          VALUES('','$this->sliderYear','$sliderTitle','$sliderDescription','slider-image-$newSliderId.jpg','$url','$sliderPrice')";
            $insertQuery = mysqli_query($connection,$insertSql);
            if($insertQuery) return "Success";
            else{
              $path = PATH."/app/user-app/view/public/img/slider-images";
              $deleteImage = unlink("$path/slider-image-$newSliderId.jpg");
              return "Slider yüklenemedi";
            }
          }
       }
     }
     /**
      * Sliderı günceller
      *
      * @access public
      * @return string "Success" or error message
      */
     public function updateSlider(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return "Bağlantı Hatası";
       else {
         $sliderId = (int)$this->sliderId;
         $sliderImage = $this->sliderImage;
         $sliderTitle = mysqli_real_escape_string($connection,$this->sliderTitle);
         $sliderDescription = mysqli_real_escape_string($connection,$this->sliderDescription);
         $url = mysqli_real_escape_string($connection,$this->sliderUrl);
         $sliderPrice = mysqli_real_escape_string($connection,$this->sliderPrice);

         $getSql = "SELECT * FROM sliders
                    WHERE slider_id=$sliderId";
         $getQuery = mysqli_query($connection,$getSql);
         $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
         $imageName = explode(".",$read["slider_image"]);
         $imageName = $imageName[0];

         $updateSql = "UPDATE sliders
                       SET slider_title='$sliderTitle',
                           slider_year='$this->sliderYear',
                           slider_description='$sliderDescription',
                           slider_url='$url',
                           slider_price='$sliderPrice'
                       WHERE slider_id=$sliderId";
         $updateQuery = mysqli_query($connection,$updateSql);
         if(!$updateQuery) return "Slider Güncellenemedi";
         else{
           if(isset($sliderImage["size"]) && (int)$sliderImage["size"] > 0){
             $imageObject = new Image();
             $imageLocation = PATH."/app/user-app/view/public/img/slider-images";
             $imageObject->setImageName("$imageName");
             $imageObject->setImageLocation($imageLocation);
             $imageObject->setProductImage($sliderImage);

             $upload = $imageObject->uploadImage();
             if($upload != "Success") return "Slider Güncellendi Fakat Resim şu nedenden dolayı yüklenemedi : $upload";
             else return "Success";
           }
           else return "Success";
         }
       }
     }
     /**
      * Sliderı siler
      *
      * @access public
      * @return boolean
      */
     public function deleteSlider(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return false;
       else {
         $sliderId = (int)$this->sliderId;
         $sliderImage = $this->sliderImage;

         $getSql = "SELECT * FROM sliders
                    WHERE slider_id=$sliderId";
         $getQuery = mysqli_query($connection,$getSql);
         $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
         $imageName = $read["slider_image"];

         $deleteSql = "DELETE FROM sliders
                       WHERE slider_id=$sliderId";
         $deleteQuery = mysqli_query($connection,$deleteSql);
         if(!$deleteQuery) return false;
         else{
           $path = PATH."/app/user-app/view/public/img/slider-images";
           $deleteImage = unlink("$path/$imageName");
           return true;
         }
       }
     }
     /**
      * Kayıtlı sliderları döndürür
      *
      * @access public
      * @return array sliders
      */
     public function getSliders(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return array();
       else {
         $getSql = "SELECT * FROM sliders";
         $getQuery = mysqli_query($connection,$getSql);
         $sliders = array();
         while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
           $sliders[] = $read;
         }
         return $sliders;
       }
     }
     /**
      * Kayıtlı sliderın bilgilerini döndürür
      *
      * @access public
      * @return array sliderDetails
      */
     public function getSliderDetails(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return array();
       else {
         $sliderId = $this->sliderId;
         $getSql = "SELECT * FROM sliders
                    WHERE slider_id=$sliderId";
         $getQuery = mysqli_query($connection,$getSql);
         $sliderDetails = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
         return $sliderDetails;
       }
     }


  }
?>
