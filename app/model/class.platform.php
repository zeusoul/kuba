<?php
  class Platform{
    protected $platformId, $platformName, $logoUrl;
    public function setPlatformId($platformId){
      $this->platformId = (int)$platformId;
    }
    public function setPlatformName($platformName){
      $this->platformName = $platformName;
    }
    public function setLogoUrl($logoUrl){
      $this->logoUrl = $logoUrl;
    }
    /**
     * Kayıtlı platformları döndürür
     *
     * @return array
     */
    public function getPlatforms(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM platforms";
        $query = mysqli_query($connection,$sql);
        $platforms = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $platforms[] = $read;
        }
        return $platforms;
      }
    }
  }

?>
