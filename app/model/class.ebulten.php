<?php
  /**
   * Ebulten sınıfı, Ebulten ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Bank sınıfı,
   * Yeni banka bilgisi kaydetmeye,
   * kayıtlı banka bilgisini silmeye
   * ve kayıtlı banka bilgilerini döndürmeye yarar.
   *
   * Example usage:
   * if (Ebulten::saveEbulten())) {
   *   print "Email was saved";
   * }
   *
   * @package Ebulten
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Ebulten{
    protected $ebultenId;
    protected $email;
    public function setEbultenId($ebultenId){
      $this->ebultenId = (int)$ebultenId;
    }
    public function setEmail($email){
      $this->email = strip_tags($email);
    }

    public function insertEbulten(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return "Bağlantı hatası";
      else{
        $email = mysqli_real_escape_string($connection,$this->email);
        $getSql = "SELECT * FROM e_bulten
                   WHERE email='$email'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0) return "Bu email adresi zaten kayıtlı";
        else{
          $sql = "INSERT INTO e_bulten
                  VALUES('','$email')";
          return mysqli_query($connection, $sql) ? "Success" : "Kayıt başarısız";
        }
      }
    }
    public function getNumRows(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return 0;
      else{
        $getSql = "SELECT * FROM e_bulten";
        $getQuery = mysqli_query($connection,$getSql);
        return $getQuery->num_rows;
      }
    }
    public function sendMessage($message = ""){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return "Bağlantı hatası";
      else{
        $getSql = "SELECT * FROM e_bulten";
        $getQuery = mysqli_query($connection,$getSql);
        $errorNumber = 0;
        $returnedValue = "Mesajınız tüm e-mail adreslerine gönderildi";
        while($read = mysqli_fetch_array($getQuery, MYSQLI_ASSOC)){
          $email = $read["email"];
          $name = "";
          $surname = "";
          $subject = "Gemici Elektronik E-Bülten";
          $content = $message;

          $mail = new Mail();
          $result = $mail->sendMail($email,$name,$surname,$subject,$content);
          if(!$result){
            $errorNumber++;
            $returnedValue = "Mesajınız $errorNumber e-mail adreslerine gönderilemedi";
          }
        }
        return $returnedValue;
      }
    }

  }
?>
