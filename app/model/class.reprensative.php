<?php
  /**
   * Product sınıfı, Ürünler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Product sınıfı,
   * Yeni ürün kaydetmeye,
   * Kayıtlı ürünü güncellemeye, silmeye
   * ve ürün bilgilerini döndürmeye yarar.
   *
   *
   * Example usage:
   * if (Product::insertPage() === "Success") {
   *   print "Product Inserted";
   * }
   *
   * @package Product
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Reprensative extends Continent{
    /**
     * Ürün id'si
     *
     * @var int
     * @access protected
     */
    protected $reprensativeId;
    /**
     * Ürünün derecelendirme puanı (1-5 arası)
     *
     * @var int
     * @access protected
     */
    protected $reprensativeRatePoint;
    /**
     * Ürün resmi
     *
     * @var array
     * @access protected
     */
    protected $reprensativeImage;
    /**
     * Ürünün başlığı
     *
     * @var string
     * @access protected
     */
    protected $title;
    /**
     * Ürünün alt başlığı
     *
     * @var string
     * @access protected
     */
    protected $subTitle;
    /**
     * Ürünün markası
     *
     * @var string
     * @access protected
     */
    protected $brandId;
    /**
     * Ürünün modeli
     *
     * @var string
     * @access protected
     */
    protected $model;
    /**
     * Ürün kodu
     *
     * @var string
     * @access protected
     */
    protected $reprensativeCode;
    /**
     * Ürünün açıklaması
     *
     * @var string
     * @access protected
     */
    protected $content;
    /**
     * Ürünün durumu (0 = Gizli, 1 = Yayında)
     *
     * @var int
     * @access protected
     */
    protected $status;

    /**
     * Set the $reprensativeId var
     *
     * @access public
     * @param int $reprensativeId
     */
    public function setreprensativeId($reprensativeId){
      $this->reprensativeId = (int)$reprensativeId;
    }
    /**
     * Set the $reprensativeRatePoint var
     *
     * @access public
     * @param int $reprensativeRatePoint
     */
    public function setReprensativeRatePoint($reprensativeRatePoint){
      $this->reprensativeRatePoint = (int)$reprensativeRatePoint;
    }
    /**
     * Set the $reprensativeImage var
     *
     * @access public
     * @param array $reprensativeImage
     */
    public function setReprensativeImage($reprensativeImage){
      if(is_array($reprensativeImage)){
        if(isset($reprensativeImage["name"]) && isset($reprensativeImage["type"]) && isset($reprensativeImage["tmp_name"]) && isset($reprensativeImage["size"])){
          if($reprensativeImage["name"] == "" || $reprensativeImage["type"] == "" || $reprensativeImage["tmp_name"] == "" ||$reprensativeImage["size"] <= 0){
            $this->reprensativeImage = array();
          }
          else $this->reprensativeImage = $reprensativeImage;
        }
        else $this->reprensativeImage = array();
      }
      else {
        $this->reprensativeImage = trim(strip_tags($reprensativeImage));
      }
    }
    /**
     * Set the $title var
     *
     * @access public
     * @param string $title
     */
    public function setTitle($title){
      $this->title = trim(strip_tags($title));
    }
    /**
     * Set the $subTitle var
     *
     * @access public
     * @param string $subTitle
     */
    public function setSubTitle($subTitle){
      $this->subTitle = trim(strip_tags($subTitle));
    }
    /**
     * Set the $brandId var
     *
     * @access public
     * @param int $brandId
     */
    public function setBrandId($brandId){
      $this->brandId = (int)$brandId;
    }
    /**
     * Set the $model var
     *
     * @access public
     * @param string $model
     */
    public function setModel($model){
      $this->model = trim(strip_tags($model));
    }
    /**
     * Set the $reprensativeCode var
     *
     * @access public
     * @param string $reprensativeCode
     */
    public function setReprensativeCode($reprensativeCode){
      $this->reprensativeCode = trim(strip_tags($reprensativeCode));
    }
    /**
     * Set the $content var
     *
     * @access public
     * @param string $content
     */
    public function setContent($content){
      $this->content = trim($content);
    }
    /**
     * Set the $status var
     *
     * @access public
     * @param int $status
     */
    public function setStatus($status){
      $this->status = trim(strip_tags($status));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return reprensative
     */
    public function __construct() {
      $this->reprensativeId        = 0;
      $this->reprensativeRatePoint = 0;
      $this->reprensativeImage     = array();
      $this->title            = "";
      $this->subTitle         = "";
      $this->brandId          = 0;
      $this->model            = "";
      $this->reprensativeCode      = "";
      $this->content          = "";
      $this->status           = -1;
    }

    /**
     * Ürün ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function insertReprensative(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else {
        $reprensativeImage = $this->reprensativeImage;
        $title = mysqli_real_escape_string($connection,$this->title);
        $subTitle = mysqli_real_escape_string($connection,$this->subTitle);
        $brandId = $this->brandId;
        $model = mysqli_real_escape_string($connection,$this->model);
        $reprensativeCode = mysqli_real_escape_string($connection,$this->reprensativeCode);
        $content = mysqli_real_escape_string($connection,$this->content);
        $status = mysqli_real_escape_string($connection,$this->status);
        $continentId = (int)$this->continentId;
        $str = new Str();
        if($str->IsNullOrEmptyString(array($title,$subTitle,$model,$reprensativeCode,$status))){
          return "Zorunlu Alanlar Boş Girilemez ( * ile gösterilen alanlar)";
        }
        else{
          $getSql = "SELECT * FROM reprensatives
                     WHERE reprensative_code='$reprensativeCode'
                     AND continent_id=$continentId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows > 0) return "Bu Ürün Zaten Mevcut. Lütfen Ürün İşlemleri Sayfasına Gidiniz.";
          else {
            $insertSql = "INSERT INTO reprensatives
                          VALUES ('','$title','$content',
                                  $brandId,'$model','',
                                  '$reprensativeCode',$continentId,$status,
                                  'not.jpg','$subTitle',
                                  0,0,0,0,0)";
            $insertQuery = mysqli_query($connection,$insertSql);
            if(!$insertQuery) return "Ürün Kaydedilmedi";
            else if(!is_array($reprensativeImage) || count($reprensativeImage) <= 0) return "Success";
            else {
              $imageObject = new Image();
              $imageLocation = PATH."/app/user-app/view/public/img/reprensative-images";
              $imageObject->setImageName($reprensativeCode);
              $imageObject->setImageLocation($imageLocation);
              $imageObject->setProductImage($reprensativeImage);

              $upload = $imageObject->uploadImage();
              if($upload != "Success") return "Ürün kaydedildi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
              else {
                $updateSql = "UPDATE reprensatives
                              SET image='$reprensativeCode.jpg'
                              WHERE reprensative_code='$reprensativeCode'
                              AND continent_id=$continentId";
                $updateQuery = mysqli_query($connection,$updateSql);
                return ($updateQuery) ? "Success" : "Ürün kaydedildi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
              }
            }
          }
        }
      }
    }
    /**
     * Kayıtlı ürünü siler
     *
     * @access public
     * @return string
     */
    public function deleteReprensative(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $reprensativeId = (int)$this->reprensativeId;
        $getSql = "SELECT * FROM reprensatives
                   WHERE reprensative_id=$reprensativeId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return "Silmek istediğiniz ürün bulunamadı";
        else{
          $read = mysqli_fetch_array($getQuery);
          $reprensativeCode = $read["reprensative_code"];
          $deleteSql = "DELETE FROM reprensatives
                        WHERE reprensative_id=$reprensativeId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          if(!$deleteQuery) return "Ürün silinemedi";
          else{
            $returnedValue = "Ürün silindi <br>";
            $infoObj = new ProductInformation();
            $infoObj->setProductId($reprensativeId);
            $deleteInfo = $infoObj->deleteProductInformations();
            if(!$deleteInfo) return $returnedValue .= "Ürünün özellikleri silinemedi! <br>";
            #Ürüne ait resimleri silelim
            $path = PATH."/app/user-app/view/public/img/reprensative-images";
            $deleteImage = unlink("$path/$reprensativeCode.jpg");
            if(!$deleteImage) $returnedValue .= "Ürünün resimleri silinemedi! <br>";
            return $returnedValue;
          }
        }
      }
    }
    /**
     * Kayıtlı ürünü günceller
     *
     * @access public
     * @return string "Success" or error message
     */
    public function updateReprensative(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı hatası";
      else{
        $reprensativeImage = $this->reprensativeImage;
        $title = mysqli_real_escape_string($connection,$this->title);
        $subTitle = mysqli_real_escape_string($connection,$this->subTitle);
        $brandId = $this->brandId;
        $model = mysqli_real_escape_string($connection,$this->model);
        $reprensativeCode = mysqli_real_escape_string($connection,$this->reprensativeCode);
        $content = mysqli_real_escape_string($connection,$this->content);
        $status = mysqli_real_escape_string($connection,$this->status);
        $continentId = (int)$this->continentId;
        $reprensativeId=(int)$this->reprensativeId;
        if($reprensativeId == 0) return "Ürün bulunamadı!";
        else{
          $updateSql = "UPDATE reprensatives
                        SET title='$title', sub_title='$subTitle',
                            brand_id=$brandId, model_name='$model',
                            reprensative_code='$reprensativeCode', content='$content',
                            release_status=$status, continent_id=$continentId
                        WHERE reprensative_id=$reprensativeId";
          $updateQuery = mysqli_query($connection,$updateSql);
          if(!$updateQuery) return "Ürün güncellenemedi!";
          else{
            $reprensativeImage = $this->reprensativeImage;
            if(!is_array($reprensativeImage) || count($reprensativeImage) <= 0) return "Success";
            else {
              $imageObject = new Image();
              $imageLocation = PATH."/app/user-app/view/public/img/reprensative-images";
              $imageObject->setImageName($reprensativeCode);
              $imageObject->setImageLocation($imageLocation);
              $imageObject->setProductImage($reprensativeImage);

              $upload = $imageObject->uploadImage();
              if($upload != "Success") return "Ürün güncellendi, fakat resim şu nedenden dolayı yüklenemedi : $upload";
              else {
                $updateSql = "UPDATE reprensative
                              SET image='$reprensativeCode.jpg'
                              WHERE reprensative_code='$reprensativeCode'
                              AND continent_id=$continentId";
                $updateQuery = mysqli_query($connection,$updateSql);
                return ($updateQuery) ? "Success" : "Ürün güncellendi, resim yüklendi fakat ürünün resim alanı güncellenemedi";
              }
            }
          }
        }
      }
    }
    /**
     * Kayıtlı tüm ürünleri döndürür
     *
     * @access public
     * @return array all reprensatives
     */
    public function getAllReprensatives($pageNumber = 1, $limit = 20){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $offset = ($pageNumber-1)*20;
        $getSql = "SELECT * FROM reprensatives
                   LIMIT $offset, $limit";
        $getQuery = mysqli_query($connection,$getSql);
        $reprensatives = array();
      /*  while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $reprensativeVariant = new ProductVariant();
          $reprensativeVariant->setReprensativeId($read["reprensative_id"]);
          $read["variants"] = $reprensativeVariant->getReprensativeVariantsByProductId();
          if(is_array($read["variants"]) && count($read["variants"]) > 0){
            $productDetails = $read;
          }
          $products["products"][] = $read;
        }*/
        $sql = "SELECT * FROM reprensatives";
        $query = mysqli_query($connection,$sql);
        $reprensativesCount = $query->num_rows;
        $reprensatives["total_reprensatives_count"] = $reprensativesCount;
        return $reprensatives;
      }
    }
    /**
     * Son eklenen ürünü döndürür
     *
     * @access public
     * @return array last reprensative
     */
    public function getLastReprensative(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $reprensatives = array();
        $getSql = "SELECT * FROM reprensatives
                   ORDER BY reprensative_id DESC
                   LIMIT 1";
        $getQuery = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
      }
    }
    /**
     * Son eklenen 4 ürünü döndürür
     *
     * @access public
     * @return array last reprensatives
     */
    public function getLastFourReprensatives(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $reprensatives = array();
        $getSql = "SELECT * FROM reprensatives
                   WHERE release_status = 1
                   ORDER BY reprensative_id DESC
                   LIMIT 8";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $this->setReprensativeId($read["reprensative_id"]);
          $reprensatives[] = $this->getReprensativeDetails();
        }
        return $reprensatives;
      }
    }
    /**
     * İstenilen kategoriye ait ürünleri döndürür
     *
     * @access public
     * @return array products
     */
    public function getReprensative(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $reprensative = array();

        $getSql = "SELECT * FROM reprensative
                   WHERE Continent_id=$this->ContinentId
                   AND release_status=1";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $productImage = new ProductImages();
          $productImage->setProductId($read["product_id"]);
          $read["product_images"] = $productImage->getProductImages();

          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $variants = $productVariant->getProductVariantsByProductId();

          $read["product_price"] = $variants[0]["discount_price"];
          $read["variants"] = $variants;
          if(is_array($read["variants"]) && count($read["variants"]) > 0){
            if(loginState()){
              $favorite = new Favorite();
              $favorite->setProductId($read["product_id"]);
              $favorite->setUserId($_SESSION[sessionPrefix()."user_id"]);
              $favoriteState = $favorite->getFavoriteState();
              $read["favorite"] = $favoriteState ? 1 : 0;
            }
            else $read["favorite"] = 0;
            $reprensative[] = $read;
          }
        }
        $sql = "SELECT * FROM continent
                WHERE parent_id=$this->continentId";
        $query = mysqli_query($connection,$sql);
        while($read = mysqli_fetch_array($query)){
          $continentId = (int)$read["continent_id"];
          $productObject = new Reprensative();
          $productObject->continentId = $continentId;
          $returnedValue = $productObject->getReprensative();
          if(is_array($returnedValue) && count($returnedValue)>0){
            foreach ($returnedValue as $key => $value) {
              $reprensative[] = $value;
            }
          }
        }
        return $reprensative;
      }
    }
    /**
     * İstenilen kategoriye ait
     * Belirtilen productId haricindeki ilk 6 ürünü döndürür
     *
     * @access public
     * @return array reprensative
     */
    public function getRelatedReprensative($productId){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $reprensative = array();
        $getSql = "SELECT * FROM reprensative
                   WHERE continent_id=$this->continentId
                   AND release_status=1
                   AND reprensative_id != $reprensativeId
                   LIMIT 6";
        $getQuery = mysqli_query($connection,$getSql);
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $productImage = new ProductImages();
          $productImage->setProductId($read["reprensative_id"]);
          $read["reprensative_images"] = $productImage->getProductImages();
        }
        $sql = "SELECT * FROM continent
                WHERE parent_id=$this->ContinentId";
        $query = mysqli_query($connection,$sql);
        while($read = mysqli_fetch_array($query)){
          $continentId = (int)$read["continent_id"];
          $productObject = new Reprensative();
          $productObject->continentId = $continentId;
          $returnedValue = $productObject->getReprensative();
          if(is_array($returnedValue) && count($returnedValue)>0){
            foreach ($returnedValue as $key => $value) {
              $reprensative[] = $value;
            }
          }
        }
        return $reprensative;
      }
    }
    /**
     * Filtreleme sonucuna göre bulunan tüm ürünleri döndürür
     *
     * @access public
     * @return array filtered products
     * @param array $get filters
     */
    public function getFilteredProducts($get = array()){
      if(!is_array($get) || count($get) < 0) return array();
      else{
        $db = new Database();
        $connection = $db->MySqlConnection();
        if(!$connection) return array();
        else{
          $i = 0;
          $max = 0;
          $min = 0;
          $rating = 0;
          $products = array();
          $getIdSql = "SELECT DISTINCT products.product_id FROM products
                       INNER JOIN product_filters
                       ON products.product_id=product_filters.product_id
                       INNER JOIN product_variants
                       ON products.product_id = product_variants.product_id
                       WHERE products.Continent_id=$this->ContinentId ";



          if(isset($get["min"]) ||isset($get["max"])){
            if(isset($get["min"])) $min = (int)$get["min"];
            if(isset($get["max"])) $max = (int)$get["max"];
            if($min > $max) {
              $temp = $max;
              $max = $min;
              $min = $temp;
            }
            if($min > 0) $getIdSql .= " AND product_variants.discount_price >= $min ";
            if($max > 0) $getIdSql .= " AND product_variants.discount_price <= $max ";
          }
          if(isset($get["rating"])){
            $rating = (float)$get["rating"];
            if($rating > 0 &&  $rating < 6) {
              $getIdSql .= " AND (Ceiling(products.rating) = $rating OR Ceiling(products.rating) = 0)";
            }
          }
          $orSql = "";
          if(isset($get["filters"]) && is_array($get["filters"]) && count($get["filters"]) > 0){
            $filters = $get["filters"];
            $orSql .= " AND ";
            $orSql .= "  ( ";
            $control = 0;
            foreach ($filters as $filter => $value) {
              $filterId = (int)$filter;
              $valueId = (int)$value;
                if($control++ > 0 ) $orSql .= " OR ";
                $orSql .= " (product_filters.filter_id=$filterId AND product_filters.filter_value_id=$valueId) ";
            }
            $orSql .= ")";
          }

          $getIdSql .= $orSql;
          $getIdQuery = mysqli_query($connection,$getIdSql);
          while ($read = mysqli_fetch_array($getIdQuery)) {
            $productId = (int)$read["product_id"];
            $getSql = "SELECT * FROM products
                       INNER JOIN product_filters
                       ON products.product_id = product_filters.product_id
                       WHERE products.product_id=$productId
                       AND release_status=1".$orSql;
            $getQuery = mysqli_query($connection,$getSql);
            if(!isset($get["filters"]) || $getQuery->num_rows == count($get["filters"])){
              $productObject = new Product();
              $productObject->setProductId($productId);
              $products[$i] = $productObject->getProductDetails();

              $exchange = new Exchange();
              $usd_selling = (float)$exchange->usd_selling;
              $euro_selling = (float)$exchange->euro_selling;
              $priceTL = (float)$products[$i]["discount_price"];

              if($products[$i]["currency"] == "EURO")$priceTL *= $euro_selling;
              else if($products[$i]["currency"] == "USD")$priceTL *= $usd_selling;
              $priceTL = (int)ceil($priceTL);
              $products[$i]["price_tl"] = $priceTL;
              ++$i;
            }
          }
          $sql = "SELECT * FROM categories
                  WHERE parent_id=$this->ContinentId ";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows > 0){
            while($subRead = mysqli_fetch_array($query)){
              $ContinentId = (int)$subRead["Continent_id"];
              $productObject = new Product();
              $productObject->setContinentId($ContinentId);
              $returnedProducts = $productObject->getFilteredProducts($get);

              foreach ($returnedProducts as $key => $returnedProduct){
                $price = (float)$returnedProduct["price"];
                $discountPrice = (float)$returnedProduct["discount_price"];
                $fark = $price-$discountPrice;
                $rate = ($price <= 0) ? 0 : (int)(($fark * 100) / $price);

                $products[$i] = $returnedProduct;
                $products[$i++]["rate"] = $rate;
              }
            }
          }
          return $products;
        }
      }
    }
    /**
     * İstenilen ürünün detaylarını döndürür
     *
     * @access public
     * @return array product details
     */
    public function getProductDetails(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql="";
        if((int)$this->productId > 0) {
          $sql = "SELECT * FROM reprensative
                  INNER JOIN brands ON products.brand_id=brands.brand_id
                  INNER JOIN categories ON products.Continent_id = categories.Continent_id
                  WHERE products.product_id=$this->productId";
        }
        else if(!is_null(trim($this->productCode)) && trim($this->productCode) != "") {
          $productCode = trim(mysqli_real_escape_string($connection,$this->productCode));
          $sql = "SELECT * FROM products
                  INNER JOIN categories ON products.Continent_id = categories.Continent_id
                  WHERE products.product_code='$productCode'";
        }
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else {
          $productImage = new ProductImages();
          $productImage->setProductId($read["product_id"]);
          $read["product_images"] = $productImage->getProductImages();
          else $read["favorite"] = 0;

          return $read;
        }
      }
    }
    /**
     * productId'ye göre ürünü döndürür
     *
     * @access public
     * @return array
     */
    public function getProductById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $sql = "SELECT * FROM products
                WHERE product_id=$this->productId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1) return array();
        else {
          $read = mysqli_fetch_array($query,MYSQLI_ASSOC);
          return $read;
        }
      }
    }
    /**
     * İstenilen ürüne ait filtrelerini ve değerlerini döndürür
     *
     * @access public
     * @return array product filters and values
     */
    public function getProductFiltersAndValues(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else {
        $productId = (int)$this->productId;
        $getProductSql = "SELECT * FROM products
        WHERE product_id=$productId";
        $getProductQuery = mysqli_query($connection,$getProductSql);
        if($getProductQuery->num_rows != 1) return array();
        else {
          $getSql = "SELECT * FROM product_filters
                     INNER JOIN filter_values
                     ON product_filters.filter_value_id=filter_values.filter_value_id
                     INNER JOIN filters
                     ON filter_values.filter_id=filters.filter_id
                     WHERE product_filters.product_id=$productId";

          $getQuery = mysqli_query($connection,$getSql);
          $productFilters = array();
          while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
            $productFilters[] = $read;
          }
          return $productFilters;
        }
      }
    }
    /**
     * En çok satan 5 ürünü döndürür
     *
     * @access public
     * @return array most viewed products
     */
    public function getTheMostSellingProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM order_lines
                INNER JOIN product_variants
                ON order_lines.barcode=product_variants.barcode
                ORDER BY product_variants.product_id ASC";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          if(count($products) == 0 || !isset($products[$read["product_id"]])){
            $products[$read["product_id"]] = array(
              "product_id" => $read["product_id"],
              "quantity"   => $read["quantity"]
            );
          }
          else{
            $products[$read["product_id"]]["quantity"] += $read["quantity"];
          }
        }
        $products = $this->record_sort($products, "quantity", true);
        $returnedProducts = array();
        foreach ($products as $key => $product) {
          $this->setProductId($product["product_id"]);
          $productDetails = $this->getProductDetails();
          $returnedProducts[] = $productDetails;
        }
        return $returnedProducts;
      }
    }

    /**
     * En çok görütülenen 5 ürünü döndürür
     *
     * @access public
     * @return array most viewed products
     */
    public function getTheMostViewedProducts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY views DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * En çok sepete eklenen 5 ürünü döndürür
     *
     * @access public
     * @return array most added products to the cart
     */
    public function getTheMostAddedProductsToTheCart(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY add_to_basket DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * En çok sepetten çıkarılan 5 ürünü döndürür
     *
     * @access public
     * @return array most items out of the cart
     */
    public function getTheMostItemsOutOfTheBasket(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT * FROM products
                WHERE release_status=1
                ORDER BY get_out_the_basket DESC
                LIMIT 5";
        $query = mysqli_query($connection,$sql);
        $products = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $productVariant = new ProductVariant();
          $productVariant->setProductId($read["product_id"]);
          $read["variants"] = $productVariant->getProductVariantsByProductId();
          $products[] = $read;
        }
        return $products;
      }
    }
    /**
     * Dizinin elemanlarını sıralar
     *
     * @access public
     * @param array $records
     * @param string $field
     * @param boolean $reverse
     */
     public function record_sort($records, $field, $reverse = false){
       $hash = array();
       $c = 'a';
       foreach($records as $record){
         $index = $record[$field];
         $index_char = '';
         if(isset($hash[$index])) $index_char = $c++;
         $hash[$index.$index_char] = $record;
       }
       ($reverse) ? krsort($hash) : ksort($hash);
       $records = array();
       foreach($hash as $record){
         $records[] = $record;
       }
       return $records;
     }
  }

?>
