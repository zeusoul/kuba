<?php
  /**
   * ProductVideos sınıfı, Ürünün videoları ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * ProductVideos sınıfı,
   * Ürüne ait videoları yükleneye ve silmeye yarar.
   *
   * Example usage:
   * if (ProductVideos::insertProductVideo()) {
   *   print "Product video inserted";
   * }
   *
   * @package ProductVideos
  */
  class ProductVideos extends Product{
    /**
     * Ürün videosunun idsi
     *
     * @var int
     * @access protected
     */
    protected $productVideoId;
    /**
     * Ürün videosunun linki
     *
     * @var string
     * @access protected
     */
    protected $productVideoUrl;
    /**
     * Set the $productVideoId var
     *
     * @access public
     * @param int $productVideoId
     */
    public function setProductVideoId($productVideoId){
      $this->productVideoId = (int)$productVideoId;
    }
    /**
     * Set the $productVideoUrl var
     *
     * @access public
     * @param string $productVideoUrl
     */
    public function setProductVideoUrl($productVideoUrl){
      $productVideoUrl = explode("?",$productVideoUrl);
      $productVideoUrl = $productVideoUrl[1];
      $productVideoUrl = explode("&",$productVideoUrl);
      $videoUrl = "https://www.youtube.com/embed/";
      foreach ($productVideoUrl as $get) {
        $get = explode("=",$get);
        if($get[0] == "v") $videoUrl .= $get[1];
        else if($get[0] == "list") $videoUrl .= "?list=".$get[1];
      }
      $this->productVideoUrl = trim(strip_tags($videoUrl));
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return ProductVideos
     */
    public function __construct() {
      $this->productVideoId  = 0;
      $this->productVideoUrl = "";
    }
    /**
     * Ürün videosunu veri tabanına kaydeder.
     *
     * @access public
     * @return boolean
     */
    public function insertProductVideo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productId = $this->productId;
        $videoUrl = $this->productVideoUrl;

        $insertSql = "INSERT INTO product_videos
                      VALUES('','$productId','$videoUrl')";
        $insertQuery = mysqli_query($connection,$insertSql);
        return $insertQuery;
      }
    }
    /**
     * Kayıtlı videoyu siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductVideo(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productVideoId = $this->productVideoId;
        $sql = "DELETE FROM product_videos
                WHERE product_video_id=$productVideoId";
        $query = mysqli_query($connection,$sql);
        return $query;
      }
    }
    /**
     * Kayıtlı videoları döndürür
     *
     * @access public
     * @return array product videos
     */
    public function getProductVideos(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $productId = $this->productId;
        $sql = "SELECT * FROM product_videos
                WHERE product_id=$productId";
        $query = mysqli_query($connection,$sql);
        if($query->num_rows <= 0) return array();
        else{
          $productVideos = array();
          while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
            $productVideos[] = $read;
          }
          return $productVideos;
        }
      }
    }
  }

?>
