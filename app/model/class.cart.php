<?php
  /**
   * Cart sınıfı, Sepet(Alışveriş sepeti) ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Cart sınıfı,
   * Sepete ürün eklemeye,
   * Sepetten ürün çıkarmaya,
   * Sepeti kaydetmeye, güncellemeye, silmeye, hesaplamaya, boşaltmaya
   * ve sepetin gerekli bilgilerini döndürmeye yarar.
   *
   * Example usage:
   * if (Cart::deleteProductInCart(1)) {
   *   print "Product has been deleted from cart";
   * }
   *
   * @package Cart
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @author Abdurrahim Bulut <abdurrahim.3078@icloud.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
   */
  class Cart extends ProductVariant{
    /**
     * Ürünün adedi
     *
     * @var int
     * @access protected
     */
    protected $quantity;
    /**
     * Set the $quantity var
     *
     * @access public
     * @param int $quantity
     */
    public function setQuantity($quantity){
      $this->quantity = (int)$quantity;
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Cart
     */
    public function __construct() {
      $this->quantity = 0;
    }
    /**
     * Sepete ürün ekler
     *
     * @access public
     * @return string "Success" or error message
     */
    public function addToCart(){
      $barcode = $this->barcode;
      $quantity = (int)$this->quantity;

      $db=new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else{
        $sql = "SELECT * FROM product_variants
                WHERE barcode='$barcode'";


        $query = mysqli_query($connection,$sql);
        if($query->num_rows != 1){
          unset($_SESSION[sessionPrefix()."cart"]["$barcode"]);
          return "Ürün bulunamadı";
        }
        else{
          $read = mysqli_fetch_array($query);
          $stock = (int)$read["stock"];
          $price = (float)$read["price"];
          $discountPrice = (float)$read["discount_price"];
          if($quantity > $stock) return "Maalesef Yeterli Stok Yok";
          else if($quantity <= 0) return "Adet 0'dan küçük olamaz";
          else if($price <= 0 || $discountPrice <= 0) return "Bu Ürün Sepete Eklenemez";
          else{
            $_SESSION[sessionPrefix()."cart"][$barcode] = array(
              "barcode"  =>  $barcode,
              "quantity"    =>  $quantity
            );
            return "Success";
          }
        }
      }
    }
    /**
     * Sepetin içinden ürün siler
     *
     * @access public
     * @return boolean
     */
    public function removeInCart(){
      $barcode = $this->barcode;
      if(isset($_SESSION[sessionPrefix()."cart"]["$barcode"])){
        unset($_SESSION[sessionPrefix()."cart"]["$barcode"]);
      }
      return (!isset($_SESSION[sessionPrefix()."cart"]["$barcode"]));
    }
    /**
     * Kullanıcının sepetini veritabanına kaydeder.
     *
     * @access public
     * @return string "Success" or error message
     * @param int $userId
     */
    public function insertCart($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else if((int)$userId <= 0) return "Kullanıcı bulunamadı";
      else {
        $userId = (int)$userId;
        $barcode = $this->barcode;
        $quantity = (int)$this->quantity;
        $date = date("d.m.Y H:i:s");

        $getSql = "SELECT * FROM product_variants
                   WHERE barcode='$barcode'";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        $stock = (int)$read["stock"];
        $price = (float)$read["price"];
        $discountPrice = (float)$read["discount_price"];
        if($getQuery->num_rows != 1){
          unset($_SESSION[sessionPrefix()."cart"]["$barcode"]);
          return "Ürün bulunamadı";
        }
        else if($quantity > $stock) return "Yeterli Stok Yok";
        else if($quantity <= 0) return "Adet 0'dan küçük olamaz";
        else if($price <= 0 || $discountPrice <= 0) return "Bu Ürün Sepete Eklenemez";
        else{
          $sql = "SELECT * FROM cart
                  WHERE barcode='$barcode'
                  AND user_id=$userId";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows <= 0){
            #kaydet
            $insertSql = "INSERT INTO cart
                          VALUES('',$userId,'$barcode',$quantity,'$date')";
            $query = mysqli_query($connection,$insertSql);
            return ($query) ? "Success" : "Ürün sepete eklenemedi";
          }
          else{
            #güncelle
            return $this->updateCart($userId);
          }
        }
      }
    }
    /**
     * Kullanıcının kayıtlı sepetini günceller
     *
     * @access public
     * @return boolean
     * @param int $userId
     */
    public function updateCart($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return "Bağlantı Hatası";
      else if((int)$userId <= 0) return "Kullanıcı Bulunamadı";
      else{
        $userId = (int)$userId;
        $barcode = $this->barcode;
        $quantity = (int)$this->quantity;
        $date = date("d.m.Y H:i:s");

        $getSql = "SELECT * FROM product_variants
                   WHERE barcode = '$barcode'";
        $getQuery = mysqli_query($connection,$getSql);
        $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
        $stock = (int)$read["stock"];
        $price = (float)$read["price"];
        $discountPrice = (float)$read["discount_price"];
        if($getQuery->num_rows != 1){
          unset($_SESSION[sessionPrefix()."cart"]["$barcode"]);
          return "Ürün bulunamadı";
        }
        else if($quantity > $stock) return "Maalesef Yeterli Stok Yok";
        else if($quantity <= 0) return "Adet 0'dan küçük olamaz";
        else if($price <= 0 || $discountPrice <= 0) return "Bu Ürün Sepete Eklenemez";
        else{
          $sql = "SELECT * FROM cart
                  WHERE user_id=$userId
                  AND barcode = '$barcode'";
          $query = mysqli_query($connection,$sql);
          if($query->num_rows != 1){
            return $this->insertCart($userId);
          }
          else{
            $updateSql = "UPDATE cart
                          SET quantity=$quantity, date_of_update='$date'
                          WHERE user_id=$userId";
            $update = mysqli_query($connection,$updateSql);
            if($update) $_SESSION[sessionPrefix()."cart"]["$barcode"]["quantity"] = $quantity;
            return ($update) ? "Success" : "Ürün güncellenemedi";
          }
        }
      }
    }
    /**
     * Kullanıcının kayıtlı sepetinden ürün siler
     *
     * @access public
     * @return boolean
     * @param int $userId
     */
    public function deleteProductInCart($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else if($userId <= 0) return false;
      else {
        $barcode = $this->barcode;
        $getSql = "SELECT * FROM cart
                   WHERE user_id=$userId
                   AND barcode='$barcode'";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows != 1) return true;
        else{
          $delete = "DELETE FROM cart
                     WHERE user_id=$userId
                     AND barcode='$barcode'";
          $query = mysqli_query($connection,$delete);
          if($query) {
            unset($_SESSION[sessionPrefix()."cart"]["$barcode"]);
          }
          return $query;
        }
      }
    }
    /**
     * Sepetteki ürünlerin tutarını hesaplar.
     *
     * @access public
     * @return array
     */
    public function calculateCart(){
      if (!isset($_SESSION[sessionPrefix()."cart"]) || !is_array($_SESSION[sessionPrefix()."cart"])) {
        return array();
      }
      $exchange = new Exchange();
    /*  $usd_buying = $exchange->usd_buying;
      $usd_selling = $exchange->usd_selling;
      $euro_buying = $exchange->euro_buying;
      $euro_selling = $exchange->euro_selling;*/
      $usd_buying   = 7.37;
      $usd_selling  = 7.37;
      $euro_buying  = 8.70;
      $euro_selling = 8.70;

      $totalPrice = 0;
      foreach ($_SESSION[sessionPrefix()."cart"] as $key => $productVariant) {
        $barcode = $productVariant["barcode"];
        $quantity = (int)$productVariant["quantity"];
        $productVariantObject = new ProductVariant();
        $productVariantObject->setBarcode($barcode);
        $productDetails = $productVariantObject->getProductVariantByBarcode();
        $discountPrice = (float)$productDetails["discount_price"];
        $currency = trim($productDetails["currency"]);
        $discountPriceTL = $discountPrice;
        if($currency == "EURO") $discountPriceTL *= $euro_selling;
        else if($currency == "USD") $discountPriceTL *= $usd_selling;
        //else if($currency != "TL") //header("location:".url());

        $totalProductPrice = $discountPrice * $quantity;
        $totalProductPriceTL = $discountPriceTL * $quantity;
        $totalPrice += $totalProductPriceTL;
      }
      $totalPrice = (float)round($totalPrice,2);
      $kdvDahil = (float)round($totalPrice + ($totalPrice*18/100),2);

      $settingsObject = new Settings();
      $settings = $settingsObject->getSettings();
      $kargoUcreti = (int)$settings["kargo_ucreti"];

      $ucretsizKargoSiniri = (int)$settings["ucretsiz_kargo_siniri"];

      if($ucretsizKargoSiniri > 0 && $kargoUcreti > 0 && $kdvDahil >= $ucretsizKargoSiniri) $kargoUcreti = 0;
      $odenecekTutar = $kdvDahil+$kargoUcreti;
      return array(
        "toplam_fiyat"    =>  $totalPrice,
        "kargo_ucreti"    =>  $kargoUcreti,
        "kdv_dahil_fiyat" =>  $kdvDahil,
        "odenecek_tutar"  =>  $odenecekTutar
      );
    }
    /**
     * Kullanıcının kayıtlı sepetini boşaltır
     *
     * @access public
     * @return boolean
     * @param int $userId
     */
    public function emptyCart($userId = 0){
      if(isset($_SESSION[sessionPrefix()."cart"])){
        unset($_SESSION[sessionPrefix()."cart"]);
      }
      if((int)$userId > 0){
        $db = new Database();
        $connection = $db->MySqlConnection();
        if($connection){
          $userId = (int)$userId;
          $deleteSql = "DELETE FROM cart
                        WHERE user_id=$userId";
          $delete = mysqli_query($connection,$deleteSql);
        }
      }
      return true;
    }
    /**
     * Sepetteki ürün sayısını döndürür
     *
     * @access public
     * @return int
     */
    public function getCountTheCart(){
      if(isset($_SESSION[sessionPrefix()."cart"]) && is_array($_SESSION[sessionPrefix()."cart"])){
        $cart = $_SESSION[sessionPrefix()."cart"];
        if(count($cart) <= 0) return 0;
        else{
          $count = 0;
          foreach ($cart as $key => $product) {
            $quantity = (int)$product["quantity"];
            $count += $quantity;
          }
          return $count;
        }
      }
      else return 0;
    }
    /**
     * Tüm kullanıcılarım sepetlerini döndürür
     *
     * @access public
     * @return array
     */
    public function getCarts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $sql = "SELECT DISTINCT user_id FROM cart
                ORDER BY date_of_update DESC";
        $query = mysqli_query($connection,$sql);
        $carts = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $userId = (int)$read["user_id"];
          $userCart = $this->getUserCart($userId);
          $carts[] = $userCart;
        }
        return $carts;
      }
    }
    /**
     * Kullanıcının sepetini döndürür
     *
     * @access public
     * @return array
     * @param int $userId
     */
    public function getUserCart($userId = 0){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else if(is_null($userId) || (int)$userId <= 0) return array();
      else{
        $exchange = new Exchange();
        $usd_buying   = $exchange->usd_buying;
        $usd_selling  = $exchange->usd_selling;
        $euro_buying  = $exchange->euro_buying;
        $euro_selling = $exchange->euro_selling;

        $totalPrice = 0.00;
        $cartCount = 0;

        $userId = (int)$userId;
        $getSql = "SELECT * FROM cart
                   WHERE user_id=$userId";
        $getQuery = mysqli_query($connection,$getSql);
        $cart = array();
        $member = new Member();
        $member->setUserId($userId);
        $cart = $member->getUserInformation();
        $cart["total_price"] = 0;
        $cart["cart_count"] = 0;

        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $barcode = $read["barcode"];
          if(!isset($cart["date_of_update"])){
            $nowYear   = (int)date("Y");
            $nowMonth  = (int)date("m");
            $nowDay    = (int)date("d");
            $nowHour   = (int)date("H");
            $nowMinute = (int)date("i");
            $nowSecond = (int)date("s");

            $dateAndTime = (string)date($read["date_of_update"]);
            $dateAndTime = explode(' ',$dateAndTime);

            $date = $dateAndTime[0];
            $time = $dateAndTime[1];

            $date = explode('.',$date);
            $time = explode(':',$time);

            $year   = (int)$date[2];
            $month  = (int)$date[1];
            $day    = (int)$date[0];
            $hour   = (int)$time[0];
            $minute = (int)$time[1];
            $second = (int)$time[2];

            $yearDifference   = ($nowYear - $year) * 12 * 30 * 24 * 60 * 60;
            $monthDifference  = ($nowMonth - $month) * 30 * 24 * 60 * 60;
            $dayDifference    = ($nowDay - $day) * 24 * 60 * 60;
            $hourDifference   = ($nowHour - $hour) * 60 * 60;
            $minuteDifference = ($nowMinute - $minute) * 60;
            $secondDifference = ($nowSecond - $second);

            $difference = $yearDifference + $monthDifference + $dayDifference + $hourDifference + $minuteDifference + $secondDifference;

            $cart["date_of_update"] = "$difference Saniye önce";
            if($difference > 59){
              $difference /= 60;
              $difference = (int)$difference;
              $cart["date_of_update"] = "$difference Dakika önce";
              if($difference > 59){
                $difference /= 60;
                $difference = (int)$difference;
                $cart["date_of_update"] = "$difference Saat önce";
                if($difference > 23){
                  $difference /= 24;
                  $difference = (int)$difference;
                  $cart["date_of_update"] = "$difference Gün önce";
                  if($difference > 29){
                    $difference /= 30;
                    $difference = (int)$difference;
                    $cart["date_of_update"] = "$difference Ay önce";
                    if($difference > 11){
                      $difference /= 12;
                      $difference = (int)$difference;
                      $cart["date_of_update"] = "$difference Yıl önce";
                    }
                  }
                }
              }
            }
          }
          $cart["cart"]["$barcode"] = $read;

          $productVariantObj = new ProductVariant();
          $productVariantObj->setBarcode($barcode);
          $productVariant = $productVariantObj->getProductVariantByBarcode();
          $cart["cart"]["$barcode"]["product_details"] = $productVariant;

          $discountPrice = (float)$productVariant["discount_price"];

          $currencyType = $productVariant["currency"];

          $discountPriceTL = $discountPrice;
          if($productVariant["currency"] == "EURO") $discountPriceTL *= $euro_selling;
          else if($productVariant["currency"] == "USD") $discountPriceTL *= $usd_selling;
          else if($productVariant["currency"] != "TL") {}//return array();

          $quantity = (float)$read["quantity"];
          $cartCount+=$quantity;

          $totalProductPrice = $discountPrice * $quantity;
          $totalProductPriceTL = $discountPriceTL * $quantity;
          $totalPrice += $totalProductPriceTL;

          if(isset($_SESSION[sessionPrefix()."login"]) && $_SESSION[sessionPrefix()."login"] == "true"){
            if((int)$_SESSION[sessionPrefix()."user_id"] == $userId)
            $_SESSION[sessionPrefix()."cart"]["$barcode"] = array(
              "barcode"  =>  $read["barcode"],
              "quantity"    =>  $read["quantity"]
            );
          }

        }
        $cart["total_price"] = round($totalPrice,2);
        $cart["cart_count"] = $cartCount;
        $cart["test"] = "test";
        return $cart;
      }
    }
    /**
     * Tüm kayıtlı sepetlerin toplam sepet tutarını döndürür
     *
     * @access public
     * @return float
     */
    public function getItemPriceInAllCarts(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return -1;
      else{
        $carts = $this->getCarts();
      //  return $carts;
        $totalPrice = (float)0.00;
        foreach ($carts as $key => $cart) {
          $price = (float)$cart["total_price"];
          $totalPrice += $price;
        }
        return round($totalPrice,2);
      }
    }
  }
?>
