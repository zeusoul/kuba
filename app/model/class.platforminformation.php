<?php

  class PlatformInformation extends Platform{
    protected $platformInformationId, $marketName, $key1, $key2, $key3, $key4, $key5;
    public function setPlatformInformationId($platformInformationId){
      $this->platformInformationId = (int)$platformInformationId;
    }
    public function setMarketName($marketName){
      $this->marketName = $marketName;
    }
    public function setKey1($key1){
      $this->key1 = $key1;
    }
    public function setKey2($key2){
      $this->key2 = $key2;
    }
    public function setKey3($key3){
      $this->key3 = $key3;
    }
    public function setKey4($key4){
      $this->key4 = $key4;
    }
    public function setKey5($key5){
      $this->key5 = $key5;
    }
    /**
     * Girilen platform bilgilerini kaydeder
     *
     * @return boolean
     */
    public function insertPlatformInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else if(trim($this->marketName) == "" || (int)$this->platformId <= 0) return false;
      else{
        $sql = "INSERT INTO platform_api_informations
                SET platform_id=$this->platformId,
                    market_name='$this->marketName',
                    key1='$this->key1',
                    key2='$this->key2',
                    key3='$this->key3',
                    key4='$this->key4',
                    key5='$this->key5'";
        return $query = mysqli_query($connection, $sql);
      }
    }
    /**
     * Girilen platform bilgilerini ID'ye göre günceller
     *
     * @return boolean
     */
    public function updatePlatformInformation(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $sql = "UPDATE platform_api_informations
                SET key1='$this->key1',
                    key2='$this->key2',
                    key3='$this->key3',
                    key4='$this->key4',
                    key5='$this->key5'";
        return $query = mysqli_query($connection, $sql);
      }
    }
    /**
     * Kayıtlı platform bilgilerini döndürür
     *
     * @return array
     */
    public function getPlatformInformations(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM platform_api_informations
                   INNER JOIN platforms ON platform_api_informations.platform_id=platforms.platform_id";
        $query = mysqli_query($connection,$getSql);
        $platforms = array();
        while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
          $platforms[] = $read;
        }
        return $platforms;
      }
    }
    /**
     * Kayıtlı platform bilgisini ID'ye göre döndürür
     *
     * @return array
     */
    public function getPlatformInformationById(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM platform_api_informations
                   INNER JOIN platforms ON platform_api_informations.platform_id=platforms.platform_id
                   WHERE platform_api_informations.id=$this->platformInformationId";
        $query = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
    /**
     * Kayıtlı platform bilgisini Platform ID'ye göre döndürür
     *
     * @return array
     */
    public function getPlatformInformationByPlatformId(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return array();
      else{
        $getSql = "SELECT * FROM platform_api_informations
                   WHERE platform_id=$this->platformId";
        $query = mysqli_query($connection,$getSql);
        return mysqli_fetch_array($query,MYSQLI_ASSOC);
      }
    }
  }



?>
