<?php
  /**
   * ProductImages sınıfı, Ürün resimleri ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * ProductImages sınıfı,
   * Ürün resmi yükleneye ve silmeye yarar.
   *
   * Example usage:
   * if (ProductImages::insertProductImage()) {
   *   print "Product image inserted";
   * }
   *
   * @package ProductImages
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class ProductImages extends ProductVariant{
    /**
     * Ürün resminin idsi
     *
     * @var int
     * @access protected
     */
    protected $productImageId;
    /**
     * Ürün resmi
     *
     * @var array
     * @access protected
     */
    protected $productImage;
    /**
     * Set the $productImageId var
     *
     * @access public
     * @param int $productImageId
     */
    public function setProductImageId($productImageId){
      $this->productImageId = (int)$productImageId;
    }

    /**
     * Set the $productImage var
     *
     * @access public
     * @param array $productImage
     */
    public function setProductImage($productImage){
      if(is_array($productImage)){
        if(isset($productImage["name"]) && isset($productImage["type"]) && isset($productImage["tmp_name"]) && isset($productImage["size"])){
          $this->productImage = $productImage;
        }
        else $this->productImage = array();
      }
      else {
        $this->productImage = trim(strip_tags($productImage));
      }
    }

    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Product
     */
    public function __construct() {
      $this->productImageId   = 0;
      $this->productImage     = array();
    }

    /**
     * Ürün resmi ekler
     *
     * @access public
     * @return boolean
     */
    public function insertProductImage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productImage = $this->productImage;
        $productId = (int)$this->productId;
        if(!isset($productImage["size"]) || (int)$productImage["size"] == 0) return false;
        else{
          $getSql = "SELECT * FROM products
                     WHERE product_id=$productId";
          $getQuery = mysqli_query($connection,$getSql);
          if($getQuery->num_rows != 1) return false;
          else {
            $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
            $productCode = $read["product_code"];

            $newId = 1;
            $getSql = "SELECT * FROM product_images
                       ORDER BY product_image_id DESC
                       LIMIT 1";
            $getQuery = mysqli_query($connection,$getSql);
            if($getQuery->num_rows == 1){
              $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
              $newId = (int)$read["product_image_id"];
              $newId++;
            }
            $imageObject = new Image();
            $imageLocation = PATH."/app/user-app/view/public/img/product-images";
            $imageObject->setImageName($productCode."-".$newId);
            $imageObject->setImageLocation($imageLocation);
            $imageObject->setImage($productImage);

            $upload = $imageObject->uploadImage();
            if($upload != "Success") false;
            else{
              $insertSql = "INSERT INTO product_images
                            VALUES('','$productId','$productCode-$newId.jpg')";
              $insertQuery = mysqli_query($connection,$insertSql);
              if($insertQuery) return true;
              else{
                unlink("$imageLocation/$productCode-$newId.jpg");
                return false;
              }
            }
          }
        }
      }
    }
    /**
     * Ürün resmini siler
     *
     * @access public
     * @return boolean
     */
    public function deleteProductImage(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if(!$connection) return false;
      else{
        $productImageId = (int)$this->productImageId;
        $getSql = "SELECT * FROM product_images
                   WHERE product_image_id=$productImageId";
        $getQuery = mysqli_query($connection,$getSql);

        if($getQuery->num_rows != 1) return false;
        else{
          $read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC);
          $productImage = $read["product_image"];
          $deleteSql = "DELETE FROM product_images
                        WHERE product_image_id=$productImageId";
          $deleteQuery = mysqli_query($connection,$deleteSql);
          if(!$deleteQuery) return false;
          else{
            $imageLocation = PATH."/app/user-app/view/public/img/product-images";
            unlink("$imageLocation/".$productImage);
            return true;
          }
        }
      }
    }
    /**
     * Ürüne ait resmleri döndürür
     *
     * @access public
     * @return array productImages
     */
     public function getProductImages(){
       $db = new Database();
       $connection = $db->MySqlConnection();
       if(!$connection) return array();
       else{
         $productId = (int)$this->productId;
         $sql = "SELECT * FROM product_images
                 WHERE product_id=$productId";
         $query = mysqli_query($connection,$sql);
         $productImages = array();
         while($read = mysqli_fetch_array($query,MYSQLI_ASSOC)){
           $productImages[] = $read;
         }
         return $productImages;
       }
     }

  }


?>
