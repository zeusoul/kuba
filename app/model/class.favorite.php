<?php
  /**
   * Favorite sınıfı, Favoriye eklenen ürünler ile ilgili işlemleri gerçekleştirmeye yarayan sınıftır.
   *
   * Favorite sınıfı,
   * Ürünü favoriye eklemeye ve ürünü favoriden cıkarmaya yarar.
   *
   * Example usage:
   * $favorite = new Favorite();
   * if($favorite->insertFavorite()){
   *    echo "favorite inserted !";
   * }
   *
   * @package Favorite
   * @author Murat Can Koçak  <7m.kocak@gmail.com>
   * @version $Revision: 1.0 $
   * @access public
   * @see
  */
  class Favorite extends Product{
    /**
     * Favori ID'si
     *
     * @var int
     * @access public
     */
    public $favoriteId;
    /**
     * Kullanıcı ID'si
     *
     * @var int
     * @access public
     */
    public $userId;

    /**
     * Set the $favoriteId var
     *
     * @access public
     * @param int $favoriteId
     */
    public function setFavoriteId($favoriteId){
      $this->favoriteId = (int)$favoriteId;
    }
    /**
     * Set the $userId var
     *
     * @access public
     * @param int $userId
     */
    public function setUserId($userId){
      $this->userId = (int)$userId;
    }
    /**
     * Constructor, sets the initial values
     *
     * @access public
     * @return Filter
     */
    public function __construct(){
      $this->favoriteId = 0;
      $this->userId = 0;
    }
    /**
     * Yeni favori kaydı yapar
     *
     * @access public
     * @return string "failed" or success message
     */
    public function insertFavorite(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return "failed";
      else{
        $getSql = "SELECT * FROM favorites
                   WHERE user_id=$this->userId
                   AND product_id=$this->productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows > 0){
          return $this->deleteFavorite();
        }
        else {
          $sql="INSERT INTO favorites
                VALUES ('',$this->userId, $this->productId)";
          $query = mysqli_query($connection,$sql);
          return $query ? "Ürün favorilerinize eklendi" : "failed";
        }
      }
    }
    /**
     * Favori kaydını siler
     *
     * @access public
     * @return string "failed" or success message
     */
    public function deleteFavorite(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return "failed";
      else{
        $getSql = "SELECT * FROM favorites
                   WHERE user_id=$this->userId
                   AND product_id=$this->productId";
        $getQuery = mysqli_query($connection,$getSql);
        if($getQuery->num_rows == 0){
          return $this->insertFavorite();
        }
        else {
          $sql="DELETE FROM favorites
                WHERE user_id=$this->userId
                AND product_id=$this->productId";
          $query = mysqli_query($connection,$sql);
          return $query ? "Ürün favorilerinizden çıkarıldı" : "failed";
        }
      }
    }
     /**
      * Kullanıcının Favorilerini döndürür
      *
      * @access public
      * @return array
      */
    public function getFavorites(){
      $db = new Database();
      $connection = $db->MySqlConnection();
      if (!$connection) return array();
      else{
        $getSql = "SELECT * FROM favorites
                   WHERE user_id=$this->userId";
        $getQuery = mysqli_query($connection,$getSql);

        $favorites = array();
        while($read = mysqli_fetch_array($getQuery,MYSQLI_ASSOC)){
          $this->setProductId($read["product_id"]);
          $productDetails = $this->getProductDetails();
          $read["product_details"] = $productDetails;
          $favorites[] = $read;
        }
        return $favorites;
      }
    }
    /**
     * 1 ürünün Belirtilen kullanıcının
     * favorileri arasında olup olmadığı durumunu döndürür
     *
     * @access public
     * @return bool
     */
   public function getFavoriteState(){
     $db = new Database();
     $connection = $db->MySqlConnection();
     if (!$connection) return false;
     else{
       $getSql = "SELECT * FROM favorites
                  WHERE user_id=$this->userId
                  AND product_id=$this->productId";
       $getQuery = mysqli_query($connection,$getSql);

       return ($getQuery->num_rows == 0) ? false : true;
     }
   }
  }

?>
