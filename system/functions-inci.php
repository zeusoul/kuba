<?php
  /**
   * Sitenin URL(link) bilgisini tutar
   *
   * @var string
   */
  define("URL", $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].str_ireplace("/index.php","",$_SERVER["SCRIPT_NAME"]));
  /**
   * Sitenin PATH(Dosya yolu) bilgisini tutar
   *
   * @var string
   */
  define("PATH", str_ireplace("/index.php","",$_SERVER["SCRIPT_FILENAME"]));
  /**
   * $_SESSION'ların (Oturumların) başına gelecek olan ön eki döndürür.
   *
   * @access public
   * @return string
   */
   function sessionPrefix(){
     $session_prefix = "cenqo_";
     return $session_prefix;
   }

  /**
   * Model klasöründeki classları(sınıfları) dahil eder.
   *
   * @access public
   */
  function loadClasses(){

    require_once("./app/model/class.string.php");
    require_once("./app/model/class.database.php");
    require_once("./app/model/class.person.php");
    require_once("./app/model/class.admin.php");
    require_once("./app/model/class.category.php");
    require_once("./app/model/class.product.php");
    require_once("./app/model/class.productInformation.php");
    require_once("./app/model/class.image.php");
    require_once("./app/model/class.filter.php");
    require_once("./app/model/class.filtervalues.php");
    require_once("./app/model/class.pages.php");
    require_once("./app/model/class.productimages.php");
    require_once("./app/model/class.productvideos.php");
    require_once("./app/model/class.settings.php");
    require_once("./app/model/class.details.php");
    require_once("./app/model/class.mainImage.php");
    require_once("./app/model/class.message.php");
    require_once("./app/model/class.company.php");
    require_once("./app/model/class.slider.php");
    require_once("./app/model/class.news.php");
    require_once("./app/model/class.references.php");
    require_once("./app/model/class.mail.php");
    require_once("./app/model/class.platform.php");
    require_once("./app/model/class.platforminformation.php");
    require_once("./app/model/class.user.php");
    require_once("./app/model/class.member.php");
    require_once("./app/model/class.platformcategory.php");
    require_once("./app/model/class.platformproductattribute.php");
    require_once("./app/model/class.brand.php");
    require_once("./app/model/class.platformbrand.php");
    require_once("./app/model/class.productvariant.php");
    require_once("./app/model/class.variant.php");
    require_once("./app/model/class.platformvariant.php");
    require_once("./app/model/class.variantvalue.php");
    require_once("./app/model/class.platformvariantvalue.php");
    require_once("./app/model/class.cart.php");
    require_once("./app/model/class.phpmailer.php");
    require_once("./app/model/class.productcomments.php");
    require_once("./app/model/class.address.php");
    require_once("./app/model/class.bank.php");
    require_once("./app/model/class.order.php");
    require_once("./app/model/class.productreturn.php");
    require_once("./app/model/class.earning.php");
    require_once("./app/model/class.exchange.php");
  }
  /**
   * Model klasöründeki classları(sınıfları) dahil eder.
   *
   * @access public
   * @return string
   * @param string $pages = Sayfalar
   * @param string $url = Sayfanın linki
   */
  function map($pages, $url){
    $pages = explode(',',$pages);
    $url = explode(',',$url);
    $map = "<i class='fa fa-home'></i><span>";
    foreach ($pages as $i => $page) {
      $map .= "
        <a href='".url($url[$i])."'>
          $page
        </a>
      ";
      if($i < count($pages)-1) $map .= " <i class='fa fa-angle-right'></i> ";
    }
    $map .= "</span>";
    return $map;
  }
  /**
   * User-app için siteye dahil edilmesi istenen
   * view klasörü içindeki dosyayı dahil eder.
   *
   * @access public
   * @param string $page = Sayfa
   */
  function view($page = ""){
    include("app/user-app/controller/controller.php");
    include("app/user-app/controller/".$page."_controller.php");
    include( staticUrl("header.php"));
    if($page != "" && $page != "index") include(staticUrl("page_info.php"));
    include("app/user-app/view/".$page.".php");
    include( staticUrl("footer.php"));
      }
  /**
   * Admin-app için siteye dahil edilmesi istenen
   * view klasörü içindeki dosyayı dahil eder.
   *
   * @access public
   * @param string $page = Sayfa
   */
  function adminView($page = ""){
    $pageMessage = "";
    if($page != "giris" && !adminLoginState()){
      header("location:".url("yonetim/giris"));
      exit;
    }
    else {
      include("app/admin-app/controller/".$page."_controller.php");
      include("app/admin-app/controller/controller.php");
      if($page != "giris") include( staticAdminUrl("header.php"));
      include("app/admin-app/view/".$page.".php");
      if($page != "giris") include( staticAdminUrl("footer.php"));
    }
  }
  /**
   * User-app klasörünün linkini döndürür.
   * Parametre ile sayfa gönderildiyse, o sayfanın
   * bulunduğu linki döndürür
   *
   * @access public
   * @return string
   * @param string $page = sayfa
   */
  function url($page = ""){
    return URL."/".$page;
  } 
    function basicUrl($page = ""){
      $url = URL;
      //https://localhost/renkgrup-mvc/ru
      $urlArr = explode('/',$url);
  
      $basicUrl = "";
      for($i=0; $i<count($urlArr)-1; $i++){
        $basicUrl .= $urlArr[$i]."/";
      }
      return $basicUrl.$page;
  }
  /**
   * Admin-app klasörünün linkini döndürür.
   * Parametre ile sayfa gönderildiyse, o sayfanın
   * bulunduğu linki döndürür
   *
   * @access public
   * @return string
   * @param string $page = sayfa
   */
  function adminUrl($page=""){
    return URL."/yonetim/".$page;
  }
  /**
   * User-app klasörü altındaki Public klasörünün linkini döndürür.
   *
   * @access public
   * @return string
   * @param string $file = dosya
   */
  function publicUrl($file = ""){
    $url = URL."/app/user-app/view/public/$file";
    return $url;
  }
  /**
   * Admin-app klasörü altındaki Public klasörünün linkini döndürür.
   *
   * @access public
   * @return string
   * @param string $file = dosya
   */
  function publicAdminUrl($file = ""){
    $url = URL."/app/admin-app/view/public/$file";
    return $url;
  }
  /**
   * User-app klasörü altındaki,
   * Public klasörü altındaki static klasörünün linkini döndürür.
   *
   * @access public
   * @return string
   * @param string $file = dosya
   */
  function staticUrl($file = ""){
    $url = "./app/user-app/view/public/static/$file";
    return $url;
  }
  /**
   * Admin-app klasörü altındaki,
   * Public klasörü altındaki static klasörünün linkini döndürür.
   *
   * @access public
   * @return string
   * @param string $file = dosya
   */
  function staticAdminUrl($value=""){
    $url = "./app/admin-app/view/public/static/$value";
    return $url;
  }
  /**
   * Parametre ile gönderilen değeri SEO için
   * hazır hale getirir ve döndürür.
   *
   * @access public
   * @return string
   * @param string $s = kelime, cümle
   */
  function seoUrl($s) {
    $tr = array('ş','Ş','ı','I','İ','ğ','Ğ','ü','Ü','ö','Ö','Ç','ç','(',')','/',':',',');
    $eng = array('s','s','i','i','i','g','g','u','u','o','o','c','c','','','-','-','');
    $s = str_replace($tr,$eng,$s);
    $s = strtolower($s);
    $s = preg_replace('/&amp;amp;amp;amp;amp;amp;amp;amp;amp;.+?;/', '', $s);
    $s = preg_replace('/\s+/', '-', $s);
    $s = preg_replace('|-+|', '-', $s);
    $s = preg_replace('/#/', '', $s);
    $s = str_replace('.', '', $s);
    $s = trim($s, '-');
    return $s;
  }
  /**
   * User-app klasörü altındaki,
   * Public klasörünün dosya yolunu döndürür.
   *
   * @access public
   * @return string
   * @param string $file = dosya
   */
  function publicPath($value=""){
    $path = "./app/user-app/view/public/$value";
    return $path;
  }
  /**
   * Kullanıcının giriş durumunu döndürür
   * (Giriş yapmışsa = true, Yapmamışsa = false)
   *
   * @access public
   * @return boolean
   */
  function loginState(){
    return (isset($_SESSION[sessionPrefix()."login"]) && $_SESSION[sessionPrefix()."login"] == "true");
  }
  /**
   * Yöneticinin giriş durumunu döndürür
   * (Giriş yapmışsa = true, Yapmamışsa = false)
   *
   * @access public
   * @return boolean
   */
  function adminLoginState(){
    return (isset($_SESSION[sessionPrefix()."admin_login"]) && $_SESSION[sessionPrefix()."admin_login"] == "true");
  }
  /**
   * Genel ayarlardan, sitenin yayın durumunu döndürür
   * (Site; yayındaysa = true, Yapım aşamasındaysa = false)
   *
   * @access public
   * @return boolean
   */
  function sitePublicationStatus(){
    #Genel ayarları çek.
    $settingsObject = new Settings();
    $settings = $settingsObject->getSettings();
    return (adminLoginState() || (isset($settings["site_yayin_durumu"]) && (int)$settings["site_yayin_durumu"] == 1));
  }

  /**
   * Parametre ile gönderilen kelimeyi, yine parametre ile
   * gönderilen sayı değeri kadar yazar ve eğer yazı devam ediyorsa
   * sonuna "..." koyar ve değeri döndürür.
   *
   * @access public
   * @return string
   * @param string $kelime
   * @param int $str
   */
  function kisalt($kelime, $str = 10){
    if (strlen($kelime) > $str){
      if (function_exists("mb_substr")) $kelime = mb_substr($kelime, 0, $str, "UTF-8").'...';
      else $kelime = substr($kelime, 0, $str).'...';
    }
    return $kelime;
  }


?>
