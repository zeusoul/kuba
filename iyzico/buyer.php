<?php

  require_once('config.php');


  $orderObject = new Order();
  $orderNo = $orderObject->getNewOrderNo();

  $userId = $_SESSION[sessionprefix()."user_id"];
  $member = new Member();
  $member->setUserId($userId);
  $memberInfo = $member->getUserInformation();

  $nameSurname = $memberInfo["name"]." ".$memberInfo["surname"];
  $nameSurname_p = $memberInfo["name"];

  $name = $memberInfo["name"];
  $surname = $memberInfo["surname"];
  $tel = $memberInfo["tel"];
  $email = $memberInfo["email"];
  $date = date("Y-m-d H:i:s");
  $address = $faturaAddress["address"];
  $city = $faturaAddress["city"];
  $odenecekTutar = $cartInfo["odenecek_tutar"];

  # create request class
  $request = new \Iyzipay\Request\CreateCheckoutFormInitializeRequest();
  $request->setLocale(\Iyzipay\Model\Locale::TR);
  $request->setConversationId($orderNo);
  $request->setPrice($odenecekTutar);
  $request->setPaidPrice($odenecekTutar);
  $request->setCurrency(\Iyzipay\Model\Currency::TL);
  $request->setBasketId($orderNo);
  $request->setPaymentGroup(\Iyzipay\Model\PaymentGroup::PRODUCT);
  $_URL = $_SERVER["REQUEST_SCHEME"]."://".$_SERVER["SERVER_NAME"].str_ireplace("/index.php","",$_SERVER["SCRIPT_NAME"]);
  $teslimatId = $teslimatAddress["address_book_id"];
  $faturaId = $faturaAddress["address_book_id"];

  $request->setCallbackUrl("$_URL/payment-result?user_id=$userId&teslimat_id=$teslimatId&fatura_id=$faturaId&cart_info_index=$cartInfoIndex&user_cart_index=$userCartIndex&siparis_no=$orderNo");
  $request->setEnabledInstallments(array(2, 3, 6, 9));

  $buyer = new \Iyzipay\Model\Buyer();
  $buyer->setId($orderNo);
  $buyer->setName($name);
  $buyer->setSurname($surname);
  $buyer->setGsmNumber($tel);
  $buyer->setEmail($email);
  $indenityNumber = ((int)$faturaAddress["type"] == 1) ? $faturaAddress["tc_no"] : $faturaAddress["tax_number"];
  $buyer->setIdentityNumber($indenityNumber);
  $buyer->setLastLoginDate($date);

  $register_date = $memberInfo["register_date"];
  $register_date = explode(".",$register_date);
  $register_day = $register_date[0];
  $register_mounth = $register_date[1];
  $register_year = $register_date[2];

  $register_date = date($register_year."-".$register_mounth."-".$register_day." H:i:s");

  $buyer->setRegistrationDate($register_date);
  $buyer->setRegistrationAddress($address);
  $ip = $_SERVER["REMOTE_ADDR"];
  $buyer->setIp($ip);
  $buyer->setCity($city);
  $buyer->setCountry("Turkey");
  $buyer->setZipCode($faturaAddress["post_code"]);
  $request->setBuyer($buyer);

  $shippingAddress = new \Iyzipay\Model\Address();
  $shippingAddress->setContactName($teslimatAddress["name_surname"]);
  $shippingAddress->setCity($teslimatAddress["city"]);
  $shippingAddress->setCountry("Turkey");
  $shippingAddress->setAddress($teslimatAddress["address"]);
  $shippingAddress->setZipCode($teslimatAddress["post_code"]);
  $request->setShippingAddress($shippingAddress);

  $billingAddress = new \Iyzipay\Model\Address();
  $billingAddress->setContactName($faturaAddress["name_surname"]);
  $billingAddress->setCity($faturaAddress["city"]);
  $billingAddress->setCountry("Turkey");
  $billingAddress->setAddress($faturaAddress["address"]);
  $billingAddress->setZipCode($faturaAddress["post_code"]);
  $request->setBillingAddress($billingAddress);

  $basketItems = array();
  $firstBasketItem = new \Iyzipay\Model\BasketItem();
  $firstBasketItem->setId($orderNo);
  $firstBasketItem->setName("Binocular");
  $firstBasketItem->setCategory1("Collectibles");
  $firstBasketItem->setItemType(\Iyzipay\Model\BasketItemType::PHYSICAL);
  $firstBasketItem->setPrice($odenecekTutar);
  $basketItems[0] = $firstBasketItem;
  $request->setBasketItems($basketItems);


  # make request
  $checkoutFormInitialize = \Iyzipay\Model\CheckoutFormInitialize::create($request, Config::options());

  # print result
  //print_r($checkoutFormInitialize);
  //print_r($checkoutFormInitialize->getstatus());
  print_r($checkoutFormInitialize->getErrorMessage());
  print_r($checkoutFormInitialize->getCheckoutFormContent());

?>
