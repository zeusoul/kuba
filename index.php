<?php
  //
  // index.php
  // MVC Sisteminin ilk çalışacak olan dosyasıdır.
  // Bu PHP dosyası sitede ilk çalışacak olan kodları içermektedir.
  // Siteye dahil etmek istediğimiz Controler, Router, Classlar gibi dosyaları burada dahil ederiz.
  //
  // @author  Murat Koçak       <7m.kocak@gmail.com>
  // @author  Abdurrahim Bulut  <abdurrahim.3078@icloud.com>
  //
  // Sitede hata göstermeyi kapatır.
  //error_reporting(0);
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
  ini_set('display_startup_errors', 1);
  error_reporting(E_ALL);
  // Oturumu başlatır.
  session_start();
  // Bu işlev çıktı tamponlamasını ektin kılar.
  ob_start();
  // Sitenin yazıldığı işaretleme dili html ve Türkçe karakter setini destekler
  header('Content-type: text/html; charset=utf-8');
  // Tarih ve zaman bilgilerini Avrupa/İstanbul olarak ayarlar.
  date_default_timezone_set('Europe/Istanbul');
  // System klasörü içerisinde "functions.php" dosyasını dahil eder.
  require_once("system/functions.php");
  // "loadClasses" methodunu çalıştırır.
  // Bu method siteye classları(sınıfları) dahil eder.
  loadClasses();
  // "router.php" dosyasını dahil eder.
  require_once("router.php");
  // Çıktı tamponunu boşaltır (gönderir) ve tamponu kapatır
  ob_end_flush();
?>
